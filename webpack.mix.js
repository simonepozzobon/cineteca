const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/assets/js/admin/order-products', 'public/js')
    .js('resources/assets/js/admin/galleries', 'public/js')
    .js('resources/assets/js/admin/galleries-index.js', 'public/js')
    .js('resources/assets/js/admin/media-library.js', 'public/js')
    .js('resources/assets/js/admin/funders/admin-funders.js', 'public/js')
    .js('resources/assets/js/admin/shipping/admin-shipping.js', 'public/js')
    .js('resources/assets/js/cinestore', 'public/js')
    .js('resources/assets/js/calendar/calendar', 'public/js')
    .js('resources/assets/js/filler/content-filler', 'public/js')
    .js('resources/assets/js/newsletter/newsletter', 'public/js')
    .js('resources/assets/js/news.js', 'public/js')
    .js('resources/assets/js/main/funders/funders', 'public/js')
    .js('resources/assets/js/cart', 'public/js')
    .js('resources/assets/js/app.js','public/js')
    .extract(['jquery', 'tether', 'bootstrap'])
    .autoload({
        jquery: ['$', 'jQuery', 'jquery'],
        tether: ['Tether'],
    })
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/admin.scss', 'public/css')
    .browserSync({
        proxy: 'http://cineteca.test',
        browser: 'google chrome',
        port: 3003
    })
    .webpackConfig({
        resolve:{
            alias: {
                'styles': path.resolve(__dirname, 'resources/assets/sass'),
                'app': path.resolve(__dirname, 'resources/assets/js/app'),
                '_js': path.resolve(__dirname, 'resources/assets/js')
            }
        }
    });
