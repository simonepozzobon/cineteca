<?php

use Illuminate\Database\Seeder;

class EventTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('event_types')->truncate();
      DB::table('event_types')->insert([
        [
          'name' => 'Presentazione Libro'
        ],
        [
          'name' => 'Laboratorio'
        ],
        [
          'name' => 'Cocktail'
        ],
        [
          'name' => 'Evento Speciale'
        ],
        [
          'name' => 'Conferenza'
        ],
      ]);
    }
}
