<?php

use Illuminate\Database\Seeder;

class FilmCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('film_categories')->truncate();
      DB::table('film_categories')->insert([
        [
          'name' => 'Musica Dal Vivo'
        ],
        [
          'name' => 'Muto'
        ],
        [
          'name' => 'Colori'
        ],
        [
          'name' => 'B/N'
        ],
        [
          'name' => 'V.O.'
        ],
        [
          'name' => 'Sott. Italiano'
        ],
        [
          'name' => 'Gratuito'
        ],
      ]);
    }
}
