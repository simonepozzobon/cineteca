<?php

use Illuminate\Database\Seeder;

class FilmLocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('film_locations')->truncate();
      DB::table('film_locations')->insert([
          [
            'name' => 'Area Metropolis 2.0',
            'address' => 'via Oslavia 8 – 20037',
            'city' => 'Paderno Dugnano',
          ],
          [
            'name' => 'Spazio Oberdan',
            'address' => 'Viale Vittorio Veneto 2, angolo piazza Oberdan',
            'city' => 'Milano',
          ]
      ]);
    }
}
