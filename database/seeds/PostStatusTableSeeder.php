<?php

use Illuminate\Database\Seeder;

class PostStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_statuses')->truncate();
        DB::table('post_statuses')->insert([
            ['status' => 'Draft'],
            ['status' => 'Published']
        ]);
    }
}
