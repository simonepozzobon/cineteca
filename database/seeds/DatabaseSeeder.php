<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(AdminsTableSeeder::class);
        // $this->call(PostStatusTableSeeder::class);
        // $this->call(CategoriesTableSeeder::class);
        // $this->call(PostTypesTableSeeder::class);
        // $this->call(FilmLocationTableSeeder::class);
        // $this->call(LocationTableSeeder::class);
        // $this->call(LocationTypeTableSeeder::class);
        // $this->call(FilmCategoriesTableSeeder::class);
        // $this->call(EventTypeTableSeeder::class);
        // $this->call(ProductCategoryTableSeeder::class);
        $this->call(OrderStatusesTableSeeder::class);
    }
}
