<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->truncate();
      DB::table('categories')->insert([
          [
            "name"  => "Festival",
            "color" => "#F1B340",
            "img"   => "test",
            "link"  => "#",
          ],
          [
            "name"  => "Archivio",
            "color" => "#E48548",
            "img" => "test",
            "link"  => "#",
          ],
          [
            "name"  => "Museo",
            "color" => "#9C5A40",
            "img" => "test",
            "link"  => "#",
          ],
          [
            "name"  => "Film",
            "color" => "#CA4A4A",
            "img" => "test",
            "link"  => "#",
          ],
          [
            "name"  => "News",
            "color" => "#C84F8A",
            "img" => "test",
            "link"  => "#",
          ],
          [
            "name"  => "Kids",
            "color" => "#755482",
            "img" => "test",
            "link"  => "#",
          ],
          [
            "name"  => "Educational",
            "color" => "#29638E",
            "img" => "test",
            "link"  => "#",
          ],
          [
            "name"  => "Cinestore",
            "color" => "#32B8C9",
            "img" => "test",
            "link"  => "#",
          ],
          [
            "name"  => "Exhibition",
            "color" => "#48DEE4",
            "img" => "test",
            "link"  => "#",
          ],
          [
            "name"  => "Biblioteca",
            "color" => "#2AD68C",
            "img" => "test",
            "link"  => "#",
          ]
      ]);
    }
}
