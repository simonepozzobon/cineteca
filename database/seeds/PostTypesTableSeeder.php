<?php

use Illuminate\Database\Seeder;

class PostTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('post_types')->truncate();
      DB::table('post_types')->insert([
          ['name' => 'film'],
          ['name' => 'exhibition'],
          ['name' => 'exhibit'],
          ['name' => 'festival'],
          ['name' => 'cinestore'],
          ['name' => 'news'],
          ['name' => 'event'],
          // ['name' => 'museo'],
          // ['name' => 'biblioteca'],
          // ['name' => 'archivio'],
          // ['name' => 'kids'],
          // ['name' => 'educational'],

      ]);
    }
}
