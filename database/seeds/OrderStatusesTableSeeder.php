<?php

use Illuminate\Database\Seeder;

class OrderStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_statuses')->truncate();
        DB::table('order_statuses')->insert([
          ['name' => 'In Attesa'],
          ['name' => 'Pagamento Effettuato'],
          ['name' => 'N.C.'],
          ['name' => 'N.C.'],
          ['name' => 'N.C.'],
          ['name' => 'Problema Pagamento'],
        ]);
    }
}
