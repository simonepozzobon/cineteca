<?php

use Illuminate\Database\Seeder;

class ProductCategoryTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('product_categories')->truncate();
    DB::table('product_categories')->insert([
      [
        'name' => 'CD e DVD'
      ],
      [
        'name' => 'Libri'
      ],
      [
        'name' => 'Video'
      ],
    ]);
  }
}
