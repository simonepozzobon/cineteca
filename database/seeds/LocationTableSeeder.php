<?php

use Illuminate\Database\Seeder;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('locations')->truncate();
      DB::table('locations')->insert([
          [
            'name' => 'Area Metropolis 2.0',
            'slug' => 'area-metropolis',
            'type_id' => '1',
            'address' => 'via Oslavia 8 – 20037',
            'city' => 'Paderno Dugnano',
          ],
          [
            'name' => 'Spazio Oberdan',
            'slug' => 'spazio-oberdan',
            'type_id' => '1',
            'address' => 'Viale Vittorio Veneto 2, angolo piazza Oberdan',
            'city' => 'Milano',
          ],
          [
            'name' => 'Sala Cinema MIC',
            'slug' => 'cinema-mic',
            'type_id' => '1',
            'address' => 'Viale Fulvio Testi, 121 - 20162',
            'city' => 'Milano',
          ],
          [
            'name' => 'Area Metropolis 2.0',
            'slug' => 'area-metropolis-exhibitions',
            'type_id' => '2',
            'address' => 'via Oslavia 8 – 20037',
            'city' => 'Paderno Dugnano',
          ],
          [
            'name' => 'Spazio Oberdan',
            'slug' => 'spazio-oberdan-exhibitions',
            'type_id' => '2',
            'address' => 'Viale Vittorio Veneto 2, angolo piazza Oberdan',
            'city' => 'Milano',
          ],
          [
            'name' => 'Sala Cinema MIC',
            'slug' => 'cinema-mic-exhibitions',
            'type_id' => '2',
            'address' => 'Viale Fulvio Testi, 121 - 20162',
            'city' => 'Milano',
          ],
          [
            'name' => 'Area Metropolis 2.0',
            'slug' => 'area-metropolis-exhibits',
            'type_id' => '3',
            'address' => 'via Oslavia 8 – 20037',
            'city' => 'Paderno Dugnano',
          ],
          [
            'name' => 'Spazio Oberdan',
            'slug' => 'spazio-oberdan-exhibits',
            'type_id' => '3',
            'address' => 'Viale Vittorio Veneto 2, angolo piazza Oberdan',
            'city' => 'Milano',
          ],
          [
            'name' => 'Sala Cinema MIC',
            'slug' => 'cinema-mic-exhibits',
            'type_id' => '3',
            'address' => 'Viale Fulvio Testi, 121 - 20162',
            'city' => 'Milano',
          ]
      ]);
    }
}
