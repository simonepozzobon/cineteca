<?php

use Illuminate\Database\Seeder;

class LocationTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('location_types')->truncate();
      DB::table('location_types')->insert([
          [
            'name' => 'film',
          ],
          [
            'name' => 'exhibition',
          ],
          [
            'name' => 'exhibit',
          ],
          [
            'name' => 'event',
          ]
      ]);
    }
}
