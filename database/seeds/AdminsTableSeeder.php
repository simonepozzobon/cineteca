<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('admins')->truncate();
      DB::table('admins')->insert([
          [
            'name'      =>  'Simone',
            'email'     =>  'info@simonepozzobon.com',
            'role'      =>  'superadmin',
            'password'  =>  bcrypt('password'),
          ],
          [
            'name'      =>  'Licia',
            'email'     =>  'mic@cinetecamilano.it',
            'role'      =>  'admin',
            'password'  =>  bcrypt('password'),
          ],
          [
            'name'      => 'Cristiana',
            'email'     => 'cristiana.ferrari@cinetecamilano.it',
            'role'      => 'admin',
            'password'  => bcrypt('password'),
          ],
      ]);
    }
}
