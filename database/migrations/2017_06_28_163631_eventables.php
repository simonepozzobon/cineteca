<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Eventables extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::create('eventables', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('event_id')->unsigned();
          $table->integer('eventable_id')->unsigned();
          $table->string('eventable_type');
          $table->timestamps();
          $table->index('event_id');
          $table->index('eventable_id');
          $table->index('eventable_type');
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('eventables');
  }
}
