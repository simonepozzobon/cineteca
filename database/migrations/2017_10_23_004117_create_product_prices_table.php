<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->decimal('price', 8, 2);
            $table->timestamps();
        });

        Schema::create('product_product_price', function (Blueprint $table) {
            $table->integer('product_id');
            $table->integer('product_price_id');
            $table->primary(['product_id', 'product_price_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prices');
        Schema::dropIfExists('product_product_price');
    }
}
