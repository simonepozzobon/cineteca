<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFestivalToContents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function(Blueprint $table) {
          $table->boolean('festival')->default(0)->after('event_type_id');
          $table->integer('festival_id')->nullable()->after('festival');
          $table->integer('fest_cat_id')->nullable()->after('festival_id');
        });

        Schema::table('films', function(Blueprint $table) {
          $table->boolean('festival')->default(0)->after('kids');
          $table->integer('festival_id')->nullable()->after('festival');
          $table->integer('fest_cat_id')->nullable()->after('festival_id');
        });

        Schema::table('exhibitions', function(Blueprint $table) {
          $table->boolean('festival')->default(0)->after('kids');
          $table->integer('festival_id')->nullable()->after('festival');
          $table->integer('fest_cat_id')->nullable()->after('festival_id');
        });

        Schema::table('exhibits', function(Blueprint $table) {
          $table->boolean('festival')->default(0)->after('kids');
          $table->integer('festival_id')->nullable()->after('festival');
          $table->integer('fest_cat_id')->nullable()->after('festival_id');
        });

        Schema::table('pages', function(Blueprint $table) {
          $table->boolean('festival')->default(0)->after('kids');
          $table->integer('festival_id')->nullable()->after('festival');
          $table->integer('fest_cat_id')->nullable()->after('festival_id');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function(Blueprint $table) {
          $table->dropColumn('festival');
          $table->dropColumn('festival_id');
          $table->dropColumn('fest_cat_id');
        });

        Schema::table('films', function(Blueprint $table) {
          $table->dropColumn('festival');
          $table->dropColumn('festival_id');
          $table->dropColumn('fest_cat_id');
        });

        Schema::table('exhibitions', function(Blueprint $table) {
          $table->dropColumn('festival');
          $table->dropColumn('festival_id');
          $table->dropColumn('fest_cat_id');
        });

        Schema::table('exhibits', function(Blueprint $table) {
          $table->dropColumn('festival');
          $table->dropColumn('festival_id');
          $table->dropColumn('fest_cat_id');
        });

        Schema::table('pages', function(Blueprint $table) {
          $table->dropColumn('festival');
          $table->dropColumn('festival_id');
          $table->dropColumn('fest_cat_id');
        });
    }
}
