<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFormsToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('events', function(Blueprint $table) {
        $table->boolean('limit_guest')->default(0)->after('kids');
        $table->integer('guest_list')->default(0)->after('limit_guest');
        $table->integer('wait_list')->default(0)->after('guest_list');
        $table->boolean('kids_list')->default(0)->after('wait_list');
        $table->integer('guest_left')->nullable()->after('kids_list');
        $table->integer('wait_left')->nullable()->after('guest_left');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('events', function(Blueprint $table) {
        $table->dropColumn('limit_guest');
        $table->dropColumn('guest_list');
        $table->dropColumn('wait_list');
        $table->dropColumn('kids_list');
        $table->dropColumn('guest_left');
        $table->dropColumn('wait_left');
      });
    }
}
