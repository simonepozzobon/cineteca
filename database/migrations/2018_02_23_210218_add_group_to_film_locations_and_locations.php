<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGroupToFilmLocationsAndLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->integer('group')->nullable()->after('id');
        });

        Schema::table('film_locations', function (Blueprint $table) {
            $table->integer('group')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function (Blueprint $table) {
            $table->dropColumn('group');
        });

        Schema::table('film_locations', function (Blueprint $table) {
            $table->dropColumn('group');
        });
    }
}
