<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTagsMorph extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('tags', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->timestamps();
      });

      Schema::create('tagables', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('tag_id')->unsigned();
          $table->integer('tagable_id')->unsigned();
          $table->string('tagable_type');
          $table->timestamps();
          $table->index('tag_id');
          $table->index('tagable_id');
          $table->index('tagable_type');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
        Schema::dropIfExists('tagables');
    }
}
