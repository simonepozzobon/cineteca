<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlideImgToFilmsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('films', function(Blueprint $table) {
      $table->string('slide_img')->nullable()->after('featured_img');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('films', function(Blueprint $table) {
      $table->dropColumn('slide_img');
    });
  }
}
