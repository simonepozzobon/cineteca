<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('presses', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name')->unique();
          $table->timestamps();
      });

      Schema::create('press_release', function (Blueprint $table) {
          $table->integer('press_id');
          $table->integer('release_id');
          $table->primary(['press_id', 'release_id']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presses');
        Schema::dropIfExists('press_release');
    }
}
