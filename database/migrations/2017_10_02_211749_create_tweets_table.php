<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('tweets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_id');
            $table->dateTime('date_time');
            $table->text('text');
            $table->string('link')->nullable();
            $table->integer('tweetable_id')->unsigned();
            $table->string('tweetable_type');
            $table->timestamps();
            $table->index('tweetable_id');
            $table->index('tweetable_type');
        });

        Schema::create('tweet_statuses', function (Blueprint $table) {
          $table->increments('id');
          $table->string('status');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tweets');
        Schema::dropIfExists('tweet_statuses');
    }
}
