<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTeacherToNewsletters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('newsletters', function(Blueprint $table) {
        $table->boolean('teacher')->default(0)->after('oberdan');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('newsletters', function(Blueprint $table) {
        $table->dropColumn('teacher');
      });
    }
}
