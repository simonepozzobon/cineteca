<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsRedirectToPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::table('pages', function(Blueprint $table) {
         $table->boolean('redirect')->default(0)->after('slug');
         $table->string('redirect_url')->nullable()->after('redirect');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::table('newsletters', function(Blueprint $table) {
         $table->dropColumn('redirect');
         $table->dropColumn('redirect_url');
       });
     }
}
