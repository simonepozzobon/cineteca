<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditOrdersTablePayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function(Blueprint $table) {
          $table->dropColumn('order_product_id');
          $table->dropColumn('total');
          $table->decimal('amount', 8, 2)->nullable()->after('order_status_id');
          $table->decimal('paid', 8, 2)->nullable()->after('amount');
          $table->string('transaction_id')->nullable()->after('paid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('orders', function(Blueprint $table) {
        $table->dropColumn('paid');
        $table->dropColumn('amount');
        $table->dropColumn('transaction_id');
        $table->decimal('total')->nullable()->after('order_status_id');
        $table->integer('order_product_id')->nullable();
      });
    }
}
