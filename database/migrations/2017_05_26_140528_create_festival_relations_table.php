<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFestivalRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('festival_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('festival_id');
            $table->integer('exhibit_id')->nullable();
            $table->integer('exhibition_id')->nullable();
            $table->integer('film_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('festival_relations');
    }
}
