<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExhibitExhibitionPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('exhibit_exhibition', function (Blueprint $table) {
          $table->integer('exhibit_id');
          $table->integer('exhibition_id');
          $table->primary(['exhibit_id', 'exhibition_id']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exhibit_exhibition');
    }
}
