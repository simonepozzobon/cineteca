<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExhibitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exhibitions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_id')->default(0);
            $table->string('title');
            $table->string('img');
            $table->string('slug')->unique();
            $table->boolean('kids')->default(false);
            $table->date('start_date');
            $table->date('end_date');
            $table->text('description');
            $table->timestamps();
        });

        Schema::create('exhibition_film', function (Blueprint $table) {
            $table->integer('film_id');
            $table->integer('exhibition_id');
            $table->primary(['film_id', 'exhibition_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exhibitions');
        Schema::dropIfExists('exhibition_film');
    }
}
