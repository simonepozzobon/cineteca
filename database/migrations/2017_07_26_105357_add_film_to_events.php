<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFilmToEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::table('events', function(Blueprint $table) {
         $table->boolean('film_id')->nullable()->after('event_type_id');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::table('events', function(Blueprint $table) {
         $table->dropColumn('film_id');
       });
     }
}
