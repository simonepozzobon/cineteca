<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlideImgToEveryPostTypeTables extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('exhibits', function(Blueprint $table) {
      $table->string('slide_img')->nullable()->after('img');
    });
    Schema::table('exhibitions', function(Blueprint $table) {
      $table->string('slide_img')->nullable()->after('img');
    });
    Schema::table('festivals', function(Blueprint $table) {
      $table->string('slide_img')->nullable()->after('img');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('exhibits', function(Blueprint $table) {
      $table->dropColumn('slide_img');
    });
    Schema::table('exhibitions', function(Blueprint $table) {
      $table->dropColumn('slide_img');
    });
    Schema::table('festivals', function(Blueprint $table) {
      $table->dropColumn('slide_img');
    });
  }
}
