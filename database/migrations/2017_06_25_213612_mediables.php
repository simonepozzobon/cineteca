<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Mediables extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::create('mediables', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('media_id')->unsigned();
          $table->integer('mediable_id')->unsigned();
          $table->string('mediable_type');
          $table->timestamps();
          $table->index('media_id');
          $table->index('mediable_id');
          $table->index('mediable_type');
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('mediables');
  }
}
