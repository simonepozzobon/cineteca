<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Galleryables extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::create('galleryables', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('gallery_id')->unsigned();
          $table->integer('galleryable_id')->unsigned();
          $table->string('galleryable_type');
          $table->timestamps();
          $table->index('gallery_id');
          $table->index('galleryable_id');
          $table->index('galleryable_type');
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('galleryables');
  }
}
