<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_id')->nullable()->default(0);
            $table->string('title');
            $table->string('featured_img');
            $table->integer('location_id')->nullable();
            $table->string('slug')->unique();
            $table->date('start_date');
            $table->date('end_date');
            $table->string('region');
            $table->integer('production_date');
            $table->string('duration');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
