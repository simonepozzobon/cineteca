<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('orders', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('order_status_id')->default(1);
          $table->decimal('total')->nullable();
          $table->decimal('shipping_cost')->nullable();
          $table->integer('order_product_id')->nullable();
          $table->string('name');
          $table->string('surname');
          $table->string('phone');
          $table->string('email');
          $table->string('address');
          $table->integer('CAP');
          $table->string('city');
          $table->string('state');
          $table->boolean('same_as_shipping');
          $table->string('b_name');
          $table->string('b_surname');
          $table->string('company_name')->nullable();
          $table->string('vat_num')->nullable();
          $table->string('b_phone');
          $table->string('b_email');
          $table->string('b_address');
          $table->integer('b_CAP');
          $table->string('b_city');
          $table->string('b_state');
          $table->timestamps();
      });

      Schema::create('order_products', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('order_id');
          $table->integer('product_id');
          $table->integer('quantity');
          $table->decimal('shipping')->nullable();
          $table->timestamps();
      });

      Schema::create('order_statuses', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('name');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('orders');
      Schema::dropIfExists('order_products');
      Schema::dropIfExists('order_statuses');
    }
}
