<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvailableHideAndGuestCountToEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::table('events', function(Blueprint $table) {
         $table->boolean('available_hidden')->default(0)->after('wait_left');
         $table->boolean('single_guest')->default(0)->after('available_hidden');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::table('events', function(Blueprint $table) {
         $table->dropColumn('available_hidden');
         $table->dropColumn('single_guest');
       });
     }
}
