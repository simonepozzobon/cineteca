<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocationIdToExhibitionsExhibitsEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('exhibitions', function(Blueprint $table) {
        $table->integer('location_id')->nullable()->after('slug');
      });
      Schema::table('exhibits', function(Blueprint $table) {
        $table->integer('location_id')->nullable()->after('slug');
      });
      Schema::table('events', function(Blueprint $table) {
        $table->integer('location_id')->nullable()->after('slug');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('exhibitions', function(Blueprint $table) {
        $table->dropColumn('location_id');
      });
      Schema::table('exhibits', function(Blueprint $table) {
        $table->dropColumn('location_id');
      });
      Schema::table('events', function(Blueprint $table) {
        $table->dropColumn('location_id');
      });
    }
}
