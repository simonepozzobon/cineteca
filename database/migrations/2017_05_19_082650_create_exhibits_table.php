<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExhibitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exhibits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status_id')->default(0);
            $table->string('title');
            $table->string('img');
            $table->string('slug')->unique();
            $table->boolean('kids')->default(false);
            $table->date('start_date');
            $table->date('end_date');
            $table->text('description');
            $table->timestamps();
        });

        Schema::create('exhibit_film', function (Blueprint $table) {
            $table->integer('film_id');
            $table->integer('exhibit_id');
            $table->primary(['film_id', 'exhibit_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exhibits');
        Schema::dropIfExists('exhibit_film');
    }
}
