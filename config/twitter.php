<?php

return [
  'oauth_access_token' => env('TW_OAUTH_ACCESS_TOKEN'),
  'oauth_access_token_secret' => env('TW_OAUTH_ACCESS_TOKEN_SECRET'),
  'consumer_key' => env('TW_CONSUMER_KEY'),
  'consumer_secret' => env('TW_CONSUMER_SECRET'),
];
