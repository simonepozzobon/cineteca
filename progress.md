
- Inserire descrizione in inglese
- Sistemare layout mail (Cri)
- Cambiare immagini archivio
- Consultazione DVD, immagine sgranata
- Sistemare menu mobile
- Aggiungere tasto cerca in admin
- Sistema twitter update con tweet un giorno prima della proiezione
- Aggiungere possibilità di resettare il numero di partecipanti rimasti in un evento
- pagamento come opzione
- Aggiungere voce admin per non pubblicarlo nel sito
- Creare pagina archivio news
- Possibilità di cambiare titolo link riquadri in fondo alle pagine
- Risolvere errore quando si assegnano due gallerie alla stessa pagina (se le cancello elimina tutti i collegamenti)
- Risolvere errore quando si carica la libreria (carica solo le prime 30 immagini)
- Aggiungere Amministrazione Trasparente Seo
- sistemare admin exhibit edit

**Da Fare oggi**


## PROGRESS
- Terminare View Video Cinestore
- Ingrandire dimensione Freccette slider


## COMPLETATO

22 Settembre

- Sistemata pagina Piccolo Grande Cinema con tutti i vecchi cataloghi
- Aggiunta possibilità di collegare rassgne alle esposizioni (front-end e back-end)
- Sistemata pagina famiglie rimuovendo i contenuti vecchi (back-end)
- Terminato invio documenti paypal
- corretto link sl titolo rassegne nelle esposizioni


21 Settembre
___

- Aperti account braintree e paypal


14 Settembre
___

- Creata nuova funzione per visualizzare la categoria prodotti su cinestore
- Category view sul cinestore


13 Settembre
___

- Risolto problema con i film (rimosso da searchable attori e registi)


12 Settembre
___

- risolto problema con le news
- Modificato link ritorno su destroy post


11 Settembre
___

- Aggiunti eventi nelle proiezioni, anche con loading ajax
- Aggiunto filtro per la location nel caricamento dinamico
- Sistemati eventi nella main delle proiezioni
- Aggiunto riquadro featured nella sidebar su main delle proiezioni
- Sistemato posti limitati sugli eventi
- Risolto problema news
- Aggiunto obbligo login per il pannello di amministrazione (__contrsuct__)
- Sistemato sitema immagini sulla ricerca


9 Settembre
___

- Aggiornati i prezzi per la sala oberdan
- Aggiunti eventi nelle view delle sale
- Da terminare eventi nella view delle sale con loading ajax


5 Settembre
___

- Sistemata tabella prodotti
- Aggiunto campo mobile per i prodotti nel pannello admin
- Sistemata immagine principale view singola prodotto


4 Settembre
___

- Cambio orari Mic nelle pagine singole
- Aggiunta mappa alle pagine del museo
- Risolto problema video embed su http://www.cinetecamilano.it/esposizione/laguerranegl-occhilaguerranelcuoremostra
- Risolto problema doppioni gallerie
- Aggiunto mappa e orari alla pagina della biblioteca di Morando


14 Agosto
___

- Acquista biglietti con icona
- Creata pagina per le gallerie da mettere nel footer
- Modificato terrazza in terrazza MIC


10 Agosto
___

- nuovo tipo di status per i post (non in lista)
- Nuova differenziazione sugli eventi



8 Agosto
___

- Rimosso calendario dalle rassegne
- Ordine della programmazione nei festival per data d'inizio
- Galleria nelle esposizione nella sezione centrale sotto la scheda
- Sistema delle date anche nelle sale con calendari separati
- Gallerie come nella biblioteca anche nelle mostre


7 Agosto
___

- Sistemati pulsanti sulla sinistra anche nella view index ajax dei film con il calendario
- Sistemati pulsanti sulla sinistra anche nella view location ajax dei film con il calendario
- Aggiunto nome sala nella view interna dei film
- Sistemato titoli dei film nelle view settimanale e mensile e con location
- Aggiunto plugin gallery
- Aggiunto plugin gallery alla pagina esposizioni


6 Agosto 2017
___

- Ordine eventi nelle rassegne Corretto
- Aggiunto nuovo calendario nelle rassegne
- Sistemato immagine marnie
- Configurazione samba
- Pulsanti dei biglietti sulla sinistra
- inserire possibilità di vedere programmazione nei prossimi 7 gg (solo dentro le sale)
- inserire possibilità di vedere programmazione mensile (solo dentro le sale) (no metropolis, solo settimanale)


4 Agosto 2017
___

- Conclusa configurazione e migrazione su nuovo server
- Ordine film in rassegne ascendente



3 Agosto 2017
___

- Concludere rassegna stampa
- Caricata rassegna stampa mandata da Margherita
- Sistemato Admin exhibit edit (manca risolvere le relazioni con i film)
- Commentato il sistema di cookies per far funzionare il calendario
- Inizio configurazione nuovo server
- Inserire Documenti rassegna stampa


2 Agosto 2017
___

- Datepicker non permette di selezionare date passate
- Nella view singola dei film non si vedono le date passate
- Nelle rassegne tutti i contenuti della programmazione sono ordinati in modo decrescente per data
- Aggiunto gallerie a rassegne
- Aggiungere gallerie alle esposizioni
- Cambiato video festival piccolo grande cinema
- Aggiunto V.O. Inglese e V.O. Francese a Film categories
- Mettere Gallery nel footer
- Aggiunto gallery biblioteca
- Sistemata schermata di edit per la galleria
- Possibilità di upload sulla libreria media
- Possibilità di upload sulle gallerie nella view create
- Possibilità di upload sulle gallerie nella view edit
- Aggiustato scss per avere i modal più bassi del menu
- Aggiungere Mappa
- Sistemato link Mappa


1 Agosto 2017
___

- Predisporre gallerie lato sx nella view singola (esposizioni, eventi)
- creare front-end sale nei film
- Cambiato immagine piccolo grande cinema
- Cambiare Immagine piccolo grande cinema home
- Ordine decrescente per data homeslider
- Film in ordine cronologico decrescente nella view singola rassegna
- Diversificato view dei prodotti per tipo. Modificando la struttura dell'url
- Creato una nuova view per i video da aggiungere nel cinestore


31 Luglio 2017
___

- Aggiungere mail per ospiti in lista d'attesa


27 Luglio 2017
___

- sistema messaggi di risposta custom (back-end)
- sistema messaggi di risposta custom (front-end)



26 Luglio 2017
___

- Sistemato problema campo immagine quando si salva una pagina
- Rimosso vecchio sistema riquadri dall'editor delle pagine
- Risolto problema modifica conenuto immagine ($item->media()->detach()) prima di modificarla con una nuova
- Aggiunto nuovo sistema immagini anche alle pagine
- Creato nuovo sistema per le sale, applicato ai film per ora
- Aggiunta selezione automatica sala, quando carica il film in modifica
- Sistemare lista -> numero lista d'attesa
- possibilità di collegare film agli eventi
- inserire link per esteso sotto slug da copiare velocemente
- pulsante per copiare il link direttamente negli appunti
- inibire pulsante dopo il primo click sugli eventi front-end
- se ci sono due posti liberi ed uno ne prenota tre, da errore e chiede se vuoi aggiungere un posto in più
- Sistema front-end per i bambini


25 Luglio 2017
___

- Sistema di modifica per le immagini.
- Caricato pdf per Schermi di Classe con la lista dei film
- Cambiato permessi file index per la ricerca (errore quando si modificava un contenuto)
- Infinite scroll alla pagina dei media
- Concludere gallery admin
- upload pdf


24 Luglio 2017
___

- Sistemato Evento cars 3 (_c'erano font diversi_)
- Sistemato slug che impediva la visualizzazione del film http://www.cineteca.tk/film/michelangeloamoreemorte
- Aggiungere biglietti su rassegne
- Creato account test di twitter (mail: simone@webmas.it password:la mia solita)
- Aggiunto pdf lettera della regione alle scuole
- Rimosso il vecchio sistema di immagini dal database test


20 luglio 2017
___

- News sui festival
- Sezione Amministrazione Trasparente
- Caricamento pdf mancanti
- Sistema Link in fondo alla pagina
- Rimosso vecchio sistema per i riquadri
- Aggiunto nuovo sistema per i riquadri
- Sistemare quadratini nelle pagine


19 Luglio 2017
___

- Sistemato problema immagini news.
- **Creare sistema di rsvp per gli eventi**
- pannello di controllo per gli invitati



18 Luglio 2017
___

- Modificata immagine pagina statica scuole
- Aggiungere pdf consultazione DVD
- Affrontare struttura e collegamenti dei festival
- Definizione Festival


17 Luglio 2017
___

- Caricato pdf nella news
- Aggiunto Ingresso Libero nella view esposizione singola
- Aggiunto possibilità di caricare immagini all'interno dei post
- Nascondere interpreti nella view film singolo se non presenti (se è uno solo, toglie virgola)
- Togliere virgola da regia in view singola (se il regista è uno)
- Possibilità di agigungere video nei post
- Aggiunta la descrizione SEO per Google in tutte le pagine
- Foto cineteca 70, tagliare in alto
- Aggiunto e modificato bando concorso "tempo limite"


13 Luglio 2017
___

- Sistemato locale (date in italiano)
- Aggiunto amministrazione trasparente nel footer
- Aggiunti pdf a schermi di classe
- Aggiunto incontro con il regista nella tipologia di eventi
- Rimosso indirizzo booking nel footer
- Aggiunto orari e prezzi in tutte le pagine del museo
- Aggiunta condizione per rimuovere news dalla home se non sono in evidenza.
- Aggiunta possibilità di ordinare i box nelle pagine statiche (aggiunta colonna order a pages)
- Ordinate i box nella pagina Scuole
- Settings analytics
- Bozza interfaccia per analytics
- Aggiunti eventi alle Rassegne
- Agiunti eventi nella sezione film
- Sistemata immagine scuole
- Rimosse sezioni stampa (come indicato da cristiana)
- Aggiunto testo dedicato all'artista nella sezione Biblioteca
- Rimosso testo e aggiunte icone facebook e twitter nella newsletter
-

5 Luglio 2017
___

- Rimosso il pulsante acquista biglietti sulla scheda singola dei film
- Modificati prezzi dei biglietti
- Risolto problema di encoding utf-8 con i film del 6 e 9 Luglio
- Aggiunta la possibilità di acquistare i biglietti anche all'interno della scheda singola del film
- Aggiornato il componente per le informazioni generali (anche per le rassegne, caso particolare per estate tabacchi)


4 Luglio 2017
___

- Mail Creaweb per problema
- Risolto Problema Creaweb
- Creato algoritmo per prevenire errori nella ricerca dei biglietti con il sistema di Creaweb
- Spostato l'algoritmo in php
- Aggiunta la possibilità di acquistare i biglietti appena sdi carica la pagina.



3 Luglio 2017
___

- Configurato nuovo server per i file tmp (permessi e directory)
- Risolto problema doppioni nei film e film con location.
- Risolto problema dei biglietti nella sala Oberdan, non funzionava quando non erano presenti.
- Corretto testo archivio
- Sistemare foto Organizza evento (footer)
- Cambiare foto selezione Sala Oberdan


2 Luglio 2017
___

- Aggiunto pulsante per tornare indietro su tutte le pagine


30 Giugno 2017
___

- Sistemate immagini singole
- Ritagliata immagine del museo
- Ritagliata immagine della biblioteca
- Creato ritaglio dell'immagine del museo per lo slider in home
- Film location, data carica 1 gg dopo
- Verificare immagini film con location
- Rimuovere overlay su mobile
- Se non si può acquistare, togliere il pulsante
- Sui quadrati far venire fuori la faccia di morando e anche nella pagina interna
- Film view generale, togliere spazio sopra slider
- Rimosso overlay dalle news
- Togliere costo biglietti su esposizioni
- Togliere calendario esposizioni
- Verificare link slider esposizioni
- Sistemato link slider component, modificato su tutte le parti
- Cambiata l'intestazione delle pagine come su tutti gli altri articoli
- Formato immagini landscape più piccolo. Così è troppo lento
- Scuole cambiare intestazione come le altre
- Cambiare foto esposizioni in home (_ripristinata quella precendente perché non andava bene_)
- **Collegare newsletter con mail chimp**
- Archivio, invertire i blocchi subito sotto il testo prima dei servizi
- Cambiare orari con estivi (_mic_)
- Cambiare prezzi biglietti
- Pulsante torna indietro
- Cambiare immagine scuole (attimo fuggente)
- Rimuovere link non attivi nel footer
- **Fatto lo switch del vecchio sito**


29 Giugno 2017
___

- Sistemato dimensioni immagini, rigenerato la tabella e i formati
- Sistemato problema con la richiesta Ajax per i biglietti, usando JSONP
- Sistemato componente slider
- aggiunto overlay allo slider per migliorarne la leggibilità
- Biglietti funzionanti per oberdan con webtic
- Aggiunto sistema biglietti anche sulla view della singola location (_solo oberdan_)
- Sistemate dimensioni slider home ('più grande')
- parallax sulle immagini di testata per le pagine e i contenuti singoli




28 Giugno 2017
___

- sistemato layout della pagina statica del museo
- sistemato layout pagina singola in generale con la nuova testata
- Cambiate le immagini di archivio
- Immagine in home
- Nelle rassegne ora i film sono ordinati per data del primo show
- Sistemato il divisore nei film (era tutto spostato a sinistra)
- Sistemato Pannello admin, problema con eventi nei festival (_creata route, controller e modificato ajax_)
- Aggiunto link in home a cineteca70
- Prima il programma completo degli show nei festival (se non ci sono show)
- Sistemato problema con il formato delle immagini (era sbagliato nel controller)
- Sistemato problema con l'utility mediaMove, non cancellava la relazione precedente se era già presente


27 Giugno 2017
____

- Sistemato layout pagina museo e componenti (header-img, film-header, square-boxes)
- Sistemato layout pagina archivio (dimensione colonna centrale)
- Spostati servizi in alto nella pagina biblioteca
- Spostate attività in alto nella pagina museo
- Cambiata il formato delle date nei film (index, index with location)




26 Giugno 2017
____

- Impostato la relazione polimorfica per lo slider della home
- Sistemato il problema in HomeSliderController (_se il file esisteva già generava errore_)
- Ripristinato gli indici di Searchable
- Corretti bug nella pagina museo
- Corretto bug immagini
- Rigenerate le immagini e le tabelle media



25 Giugno 2017
___

- Sistemata formattazione home page, corretti gli sfondi (_si intravedeva lo slider scorrendo nella sezione News_)
- Installato Pace.js (per usarlo su una pagina passare la variabile _loading yes_ al layout)
- Creato componente blade per slider principale da inserire nelle pagine
- Creato componente blade per menu box principale da inserire nelle pagine
- Cambiato riquadri location nella view generale dei Film
- Ottimizzato queries DB nella pagina film generale
- Creato Tool per tagliare le immagini e adattarle al formato dello slider (film)
- Sostituito slider nella view Film generale con quello nuovo
- Aggiunto fast link al sito pubblico nel pannello amministrazione
- Aggiornato slider anche sulla view delle sale
- Aggiornato il tool per le immagini panoramiche dello slider a Esposizioni, Rassegne, Festival
- Convertite tutte le immagini per il nuovo slider
- Aggiunti i nuovi slide ad Esposizioni, Rassegne, Festival
- Aggiunta la colonna slide_img anche agli eventi
- Aggiornato il tool per le immagini panoramiche con gli eventi
- Aggiunto nuovo slider ad Eventi
- Aggiunti i nuovi riquadri anche alla pagina famiglie
- Aggiornata pagina museo
- Messa scritta bianca sulla home nello slider
- **Creato sistema per le immagini e modificato in tutti i template**



24 Giugno 2017
___

- Modificato main slider in home
- Terminato slider News in fondo alla pagina
- Sistemata compatibilità mobile dello slider News
- Sistemato i divisori anche su mobile (home)
- Sistemato posizionamento verticale del menu mobile (bootstrap)
- Cambiato quadrato in home Festival, punta solo a Piccolo Grande Cinema
- Aggiunta una nuova colonna per ordinare i riquadri in home
- Aggiunta nuova colonna per le immagini dello slider in home
- Sistemato ordine riquadri in home come da indicazioni di Matteo e Licia
- Cambiato il riquadro mostre in esposizioni
- Sistemato problema ID nelle categorie dell'home slider (admin)
- Creato Modello Utility per le operazioni comuni o per i tools
- Creato Tool per generare le immagini per lo slider in home (può essere usato anche per gli altri slider)
- Adattamento Home slider su mobile
- Creata la cartella components con i componenti ricorrenti di Blade
- Creato il componente Blade per le informazioni generali
- Creato il componente Blade per le mappe
- Ottimizzato queries DB nella pagina film singolo
- Sostituiti al codice i nuovi componenti blade creati per Mappe e Informazioni Generali nella view film singolo
- Attivato il menu principale (ora diventa nero quando si è all'interno di quella sezione)
- Parallax sullo slider in home


23 Giugno 2017
___

- Sistemato problema con gli eventi nei festival
- Cambiato le immagini in home di Biblioteca di Morando e Famiglie
- Aggiunti loghi partner
- Creata view per evento singolo
- Ingrandite voci del menu
- Inserito Logo della cineteca
- Aggiunto spazio tra i riquadri in home
- Posizionato scritte sotto
- Cambiato colore scritte quadrati in nero
- Cambiato slider con quello di bootstrap, tolte le descrizioni (_solo titolo e pulsante_)
- Cambiato dicitura prossime proiezioni in "film in proiezione"


20 Giugno 2017
___

- Completata la selezione delle proiezioni per giorno nella lista generale dei film.
- Aggiunto limite di 3 giorni alle programmazioni anche all'interno delle singole location
- Aggiunto link alla location per ogni film nella lista generale
- Completata la selezione delle proiezioni per data nella lista delle singole location
- sistemato problema in PostController $festival->location_id per la creazione di un nuovo festival (_non sono ancora attive le location per i festival quindi dava errore_)



19 Giugno 2017
___

- Aggiunto sistema upload pdf per le pagine
- Sistemato pannello Admin per le pagina con ordinamento decrescente per ID (ultime pagine in alto)
- Sistemato ordine decrescente nel Pannello Admin Posts
- Aggiunti link Footer 5x1000 e Organizza un evento
- Aggiunta Location per i Film (_Terrazza Mic_)
- Cambiata immagine Riquadro Scuole (_con immagine Captain Fantastic_)
- Cambiata immagine Archivio e MIC Lab con quella passata da Licia
- Corretti tutti gli slug dei film
- Modificato "_Sala di proiezione_" con "_location_" in Esposizioni e Rassegne (Admin)
- Aggiunte le nuove location:
	- _Terrazza MIC (questo nei film ed eventi)_
  - _FOYER CINEMA SPAZIO OBERDAN (esposizioni, eventi)_
  - _FOYER AREA METROPOLIS 2.0 (esposizioni, eventi)_
- Location funzionanti per Esposizioni, Rassegne ed Eventi.
- Aggiunto tipo di evento conferenza
- Rimosso dallo slider nella pagina esposizioni (http://www.cineteca.tk/esposizioni) tutte le esposizioni passate
- Aggiunte Location nelle view lista di rassegne ed esposizioni
- Limitata la lista dei film ai tre giorni successivi ad oggi
- Iniziato a sviluppare la lista dei film in base al giorno selezionato (AJAX, senza caricamento di pagina)


16 Giugno 2017
___

- Aggiunta pagina statica museo con contenuti dinamici.
- Corretti i link relativi al museo (quadrati in home e menu principale), ora puntano a questa nuova pagina.
- Aggiunta pagina statica Biblioteca e sistemata la formattazione
- Corretti i link relativi alla biblioteca (quadrati in home e menu principale), ora puntano a questa nuova pagina.
- Aggiunta pagina statica Archivio e sistemata la formattazione
- Corretti i link relativi alla archivio (quadrati in home e menu principale), ora puntano a questa nuova pagina.
- Sistemato layout pagine singole
- Create le tre pagine relative all'archivio: "_prestito copie e consultazione_", "_restauro e lavori in corso_", "_i tesori del MIC dal 2012 ad oggi_"
