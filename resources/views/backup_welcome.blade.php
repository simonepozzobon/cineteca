@extends('layouts.public')
@section('title', 'Home Page')
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/slider.css') }}">
  <style media="screen">

  .cd-slider .content {
    padding: 5rem;
    color: #ffffff;
    background-color: #767070;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  @foreach ($slides as $key => $slide)
    .cd-slider li:nth-of-type({{ $key+1 }}) .content {
      background-color: {{ $slide['color'] }};
    }
  @endforeach

  @foreach ($slides as $key => $slide)

    @if ($key === 0)
      .cd-slider li:first-of-type .image {
        background-image: url({{ Storage::disk('local')->url($slide['img']) }});
      }
    @else
      .cd-slider li:nth-of-type({{ $key+1 }}) .image {
        background-image: url({{ Storage::disk('local')->url($slide['img']) }});
      }
    @endif

  @endforeach
  </style>
@endsection
@section('content')
  <section id="main-slider">
    <div class="row no-gutters">

      {{-- Slider --}}
      <div class="cd-slider-wrapper">
    		<ul class="cd-slider">
          @foreach ($slides as $key => $slide)
            <li class="{{ $key === 0 ? 'is-visible' : '' }}">
      				<div class="cd-half-block image"></div>
              <div class="cd-half-block content">
      					<div>
      						<h2>{{ $slide['title'] }}</h2>
                  @if ( isset($slide['type']) )
                    <h5><span class="badge badge-default">{{ $slide['type'] }}</span></h5>
                  @endif
      						<p>
      							{{ substr(strip_tags($slide['content']), 0, 500) }}{{ strlen(strip_tags($slide['content'])) > 500 ? '...' : ""  }}
      						</p>
      						<a href="{{ url($slide['url']) }}" class="btn btn-primary">Leggi di più</a>
      					</div>
      				</div>
      			</li>
          @endforeach
    		</ul> <!-- .cd-slider -->
    	</div>

    </div>
  </section>
  <section id="second-menu">
    <div class="row no-gutters">
      <div class="col">
        <div class="row pt-5 pb-5">
          <div class="col pt-5 pl-5 pr-5 pb-3">
            <h3 align="center">Visita Fondazione Cineteca Italiana</h3>
          </div>
        </div>
        <div class="row">
          <div class="col pb-5">
            @foreach ($categories as $key => $category)
              @if ($key % 3 == 0)
                <div class="row no-gutters">
              @endif
              <div class="col-md-4 menu-box" style="background: url('{{ Storage::disk('local')->url($category->square_big) }}'); background-size: cover; background-position: center center;">
                <h2 class="text-white p-5">
                  <a href="{{ $category->link }}" class="text-white">{{ $category->name }}</a>
                </h2>
              </div>
              @if ($key % 3 == 2)
                </div>
              @endif
            @endforeach

          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="news">
    <div class="row no-gutters pb-5">
      <div class="col">
        <div class="row pt-5 pb-5">
          <div class="col pl-5 pr-5 pb-3">
            <h3 align="center">News</h3>
          </div>
        </div>
        <div class="row">
          <div class="col">
            @if (!$news->isEmpty())
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                  @foreach ($news as $key => $newsItem)
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $key }}" class="{{ $key==0 ? 'active' : '' }}"></li>
                  @endforeach
              </ol>
              <div class="carousel-inner" role="listbox">
                @foreach ($news as $key => $newsItem)
                  <div class="carousel-item {{ $key==0 ? 'active' : '' }}">
                    <div class="carousel-overlay"></div>
                    <img class="d-block img-fluid w-100" src="{{ Storage::disk('local')->url($newsItem->img) }}" alt="{{ $newsItem->title }}">
                    <div class="carousel-caption d-none d-md-block home-carousel">
                      <h3>{{ $newsItem->title }}</h3>
                      <p>{{ substr(strip_tags($newsItem->content), 0, 100) }}{{ strlen(strip_tags($newsItem->content)) > 100 ? '...' : "" }}</p>
                      <a href="{{ route('single.news', $newsItem->slug) }}" class="btn btn-primary">Vai alla News</a>
                    </div>
                  </div>
                @endforeach
              </div>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="collection">
    <div class="row no-gutters pb-5">
      <div class="col-md-7">
        <h3 class="pt-5 pl-5 pr-5 pb-3">Dalla Collezione</h3>
        <p class="pl-5 pr-5">
          Un documento unico, inedito ed estremamente sentimentale: “Il bacio di Luca Comerio” (1914) che ci mostra, per la prima volta al cinema, il primo reporter cinematografico della storia, Luca Comerio, filmato dalla sua stessa macchina da presa in un gesto intimo e appassionato. Comerio, infatti, si riprende in una sorta di Home Movies ante litteram girato in 35mm in un momento privato e idilliaco: una passeggiata romantica con la moglie Ines Negri che si conclude con un bacio ardente…un documento solo per veri innamorati!
          <br>
          <a href="#" class="btn btn-primary mt-4 disabled">See the collection</a>
        </p>
      </div>
      <div class="col-md-5">
        <div class="embed-responsive embed-responsive-1by1">
          <iframe class="embed-responsive-item" src="//www.youtube.com/embed/L1HDXVBn9Wg" frameborder="0" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </section>
  <section id="iniziative">
    <div class="row no-gutters">
      <div class="col-md-5">
        <img class="img-fluid w-100" src="img/MIC.jpg" alt="">
      </div>
      <div class="col-md-7">
        <div class="row">
          <div class="col">
            <h3 class="pt-5 pl-5 pr-5 pb-3">Iniziative</h3>
            <p class="pl-5 pr-5">
              Il Museo Interattivo del Cinema (MIC), è un progetto di Fondazione Cineteca Italiana e Regione Lombardia.
              Visitare il MIC significa essere protagonisti di un’esperienza culturale unica! Il MIC infatti mette al centro della propria offerta il visitatore sollecitandolo a utilizzare le macchine interattive disseminate lungo il percorso per costruire un personale itinerario di visita, seguendo le proprie passioni e i propri interessi. Il MIC è un museo da sperimentare, tornandoci più volte per ripetere una visita che non sarà mai la stessa. I contenuti delle postazioni interattive sono infatti in costante trasformazione e implementazione e permettono al visitatore di approfondire diversi argomenti con materiali storici provenienti dai nostri archivi e, nel caso delle scolaresche, con proiezioni o laboratori che danno modo di avvicinarsi ai mestieri del cinema. Il MIC è un museo fuori dagli schemi, dove anche gli oggetti e i cimeli esposti trovano una corrispondenza nei contenuti interattivi delle macchine che ne approfondiscono la storia e l’utilizzo.
              <br>
              <a href="{{ url('pagina/museo') }}" class="btn btn-primary mt-4">Visita il MIC</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('scripts')
  <script src="{{ asset('js/helpers/jquery.ba-cond.min.js') }}"></script>
  <script src="{{ asset('js/slider.js') }}"></script>
  <script type="text/javascript">
    var boxWidth = $('.menu-box').width();
    $('.menu-box').height(boxWidth);
    var mainNav = $('.main-nav').height();
    var windowHeight = $(window).height();
    $('.cd-slider-wrapper').height(windowHeight - mainNav);
  </script>
  <script type="text/javascript">
  $('#carouselExampleIndicators').on('slid.bs.carousel', function () {
      $(".carousel-item.active:nth-child(" + ($(".carousel-inner .carousel-item").length -1) + ") + .carousel-item").insertBefore($(".carousel-item:first-child"));
      $(".carousel-item.active:last-child").insertBefore($(".carousel-item:first-child"));
  });
  </script>
@endsection
