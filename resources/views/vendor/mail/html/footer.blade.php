<tr>
    <td>
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0">
            <tr>
                <td class="content-cell" align="center">
                    {{ Illuminate\Mail\Markdown::parse($slot) }}
                </td>
            </tr>
            <tr>
              <td class="content-cell" align="center">
                <p><em>Fondazione Cineteca Italiana<br>
                Manifattura Tabacchi<br>
                Viale Fulvio Testi 121<br>
                Milano<br></em></p>
              </td>
            </tr>
        </table>
    </td>
</tr>
