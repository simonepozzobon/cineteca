<div class="row no-gutters">
  <div class="col">
    <div class="row pt-md-5">
      <div class="col px-5">
        <h4 align="center">{{ $title }}</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-6 offset-3">
        <hr>
      </div>
    </div>
  </div>
</div>
