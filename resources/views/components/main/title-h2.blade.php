<div class="row no-gutters">
  <div class="col">
    <div class="row pt-md-5">
      <div class="col px-5">
        <h2 align="center">{{ $title }}</h2>
      </div>
    </div>
  </div>
</div>
