
<div class="row no-gutters">
  <div class="col pt-5">
    <div class="row pt-5">
      <div class="col-md-10 offset-md-1" style="height: 33vh">
        <div class="container-fluid h-100">
          <div class="header-img w-100 h-100" style="background: url('{{ $image }}') center center; background-size: cover;"></div>
        </div>
      </div>
    </div>
    <div class="row pt-md-5">
      <div class="col pt-5 px-5">
        <h1 align="center">{{ $title }}</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-6 offset-3">
        <hr>
      </div>
    </div>
  </div>
</div>
