<div class="row">
  <div class="col-md-4 offset-md-4 pb-5 text-center">
    <a href="{{ url()->previous() }}" class="btn btn-info"><i class="fa fa-hand-o-left" aria-hidden="true"></i> Torna indietro</a>
  </div>
</div>
