<div class="row no-gutters">
  <div class="col pt-5 pb-5">
    <div class="row pt-5">
      <div class="col pt-5 pl-5 pr-5">
        <h1 align="center">{{ $type }} In Evidenza</h1>
      </div>
    </div>
    <div class="row pb-5">
      <div class="col-md-6 offset-md-3">
        <hr>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div id="carouselFilm" class="carousel-custom carousel slide" data-ride="carousel">
          <div class="carousel-overlay"></div>
          <div class="carousel-inner" role="listbox">
            @if (isset($items))
              @foreach ($items as $key => $item)
                <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                  <img class="d-block" style="height: 100%; width: 100%;" src="{{ Storage::disk('local')->url($item['slide_img']) }}" alt="First slide">
                  <div class="carousel-caption">
                    <h1 class="pt-3">{{ $item['title'] }}</h1>
                    <p class="pt-4"><a class="btn btn-primary" href="{{ url($root) }}/{{ $item['slug'] }}" role="button"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Scheda {{ $button }}</a></p>
                  </div>
                </div>
              @endforeach
            @endif
          </div>
          <a class="carousel-control-prev text-default" href="#carouselFilm" role="button" data-slide="prev">
            <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next text-default" href="#carouselFilm" role="button" data-slide="next">
            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
