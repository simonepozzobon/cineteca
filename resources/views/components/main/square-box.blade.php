<div class="row no-gutters">
  <div class="col">
    <div class="row pt-5">
      <div class="col pt-5 px-5">
        <h1 align="center">{{ $title }}</h1>
      </div>
    </div>
    <div class="row pb-5">
      <div class="col-6 offset-3">
        <hr>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div class="row no-gutters">
          @foreach ($items as $key => $item)
            <div class="col-md-4 pb-5">
              <div class="container box-container">
                <img class="img-fluid pb-3 w-100" src="{{ Storage::disk('local')->url($item->media->first()->thumb) }}" alt="{{ $item['title'] }}">
                <h2 class="text-center">
                  <a href="{{ url($type) }}/{{ $item['slug'] }}" class="text-default">{{ $item['title'] }}</a>
                </h2>
              </div>
            </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
