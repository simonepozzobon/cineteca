
<div class="row no-gutters">
  <div class="col pt-5">
    <div class="row pt-md-5">
      <div class="col pt-5 px-5">
        <h1 align="center">{{ $title }}</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-6 offset-3">
        <hr>
      </div>
    </div>
    <div class="row pt-5">
      <div class="col-md-10 offset-md-1">
        <div class="embed-responsive embed-responsive-21by9">
          <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/{{ $videoSrc }}" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
