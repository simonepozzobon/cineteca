<div class="row">
  <div class="col-md-10 offset-md-1">
    @include('components.main.title', ['title' => 'Prodotti Collegati'])
    <div class="row">
      @foreach ($products as $key => $product)
          <div class="col-md-4">
            <img src="{{ Storage::disk('local')->url($product->media()->first()->landscape) }}" alt="{{ $product->name }}" class="img-fluid w-100">
            <a href="{{ route('cinestore.single', [$product->category, $product->slug]) }}">
              <h2 class="text-default text-center p-4">{{ $product->name }}</h2>
            </a>
          </div>
      @endforeach
    </div>
  </div>
</div>
