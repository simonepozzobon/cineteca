@if (isset($products))
  {{-- @if ($products->count() > 0) --}}
    <div class="row">
      <div class="col pt-5 px-5">
        <h1 align="center">{{ $title }}</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-6 offset-3">
        <hr>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div class="container-fluid pb-5 pr-3 pl-3">
          <div class="row no-gutters">
            @foreach ($products as $key => $feat)
                  <div class="col-md-4 p-3">
                    <img src="{{ Storage::disk('local')->url($feat->featurable()->first()->media()->first()->thumb) }}" alt="{{ $feat->featurable()->first()->name }}" class="img-fluid w-100">
                    <div class="container-fluid p-5 bg-dark-gray">
                      <a href="{{ route('cinestore.single', ['percorsi', $feat->featurable()->first()->slug]) }}">
                        <h3>{{ $feat->featurable()->first()->name }}</h3>
                      </a>
                      <p><span class="badge badge-default">{{ $feat->featurable()->first()->category_name }}</span></p>
                      <p>{{ substr(strip_tags($feat->featurable()->first()->description), 0, 100) }}{{ strlen(strip_tags($feat->featurable()->first()->description)) > 100 ? '...' : "" }}</p>
                      <a href="{{ route('cinestore.single', ['percorsi', $feat->featurable()->first()->slug]) }}" class="btn btn-info mb-3"><i class="fa fa-info" aria-hidden="true"></i> Dettagli</a>
                    </div>
                  </div>
            @endforeach
        </div>
        </div>
      </div>
    </div>
  {{-- @endif --}}
@endif
