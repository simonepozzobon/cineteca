@if (isset($products))
  @if ($products->count() > 0)
    <div class="row">
      <div class="col pt-5 px-5">
        <h1 align="center">{{ $title }}</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-6 offset-3">
        <hr>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div class="container-fluid pb-5 pr-3 pl-3">
          <div class="row no-gutters">
            @foreach ($products as $key => $product)
                  <div class="col-md-4 p-3">
                     <img src="{{ Storage::disk('local')->url($product->media->first()->thumb) }}" alt="{{ $product->name }}" class="img-fluid w-100">
                    <div class="container-fluid p-5 bg-dark-gray">
                      <a href="{{ route('cinestore.single', ['percorsi', $product->slug]) }}">
                        <h3>{{ $product->name }}</h3>
                      </a>
                      <p><span class="badge badge-default">{{ $product->category->name }}</span></p>
                      <p>{{ substr(strip_tags($product->description), 0, 100) }}{{ strlen(strip_tags($product->description)) > 100 ? '...' : "" }}</p>
                      <a href="{{ route('cinestore.single', ['percorsi', $product->slug]) }}" class="btn btn-info mb-3"><i class="fa fa-info" aria-hidden="true"></i> Dettagli</a>
                    </div>
                  </div>
            @endforeach
        </div>
        </div>
      </div>
    </div>
  @endif
@endif
