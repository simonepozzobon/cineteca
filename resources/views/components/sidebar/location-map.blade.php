<div class="row pb-5">
  <div class="col">
    <h5>Come Raggiungerci</h5>
    <hr>
    <p>
      <strong>{{ $name }}</strong><br>
      {{ $address }}<br>
      {{ $city }}<br>
      @if ($slug === 'spazio-oberdan' || $slug === 'spazio-oberdan-exhibitions' || $slug === 'spazio-oberdan-exhibits' || $slug === 'foyer-spazio-oberdan' || $slug === 'foyer-spazio-oberdan-events' || $slug === 'spazio-oberdan-events')
        Tel: 0283982421
      @elseif ($slug === 'area-metropolis' || $slug === 'area-metropolis-exhibitions' || $slug === 'area-metropolis-exhibits' || $slug === 'foyer-area-metropolis' || $slug === 'foyer-area-metropolis-events' || $slug === 'area-metropolis-events' )
        Tel: 029189181
      @elseif ($slug === 'cinema-mic' || $slug === 'terrazza-mic' || $slug === 'cinema-mic-exhibitions' || $slug === 'cinema-mic-exhibits' || $slug === 'cinema-mic-events' || $slug === 'terrazza-mic-events')
        Tel: 0287242114
      @elseif ($slug === 'biblioteca-morando' || $slug === 'biblioteca')
        Tel: 0266986901
      @endif
    </p>
    @if ($slug === 'spazio-oberdan' || $slug === 'spazio-oberdan-exhibitions' || $slug === 'spazio-oberdan-exhibits' || $slug === 'foyer-spazio-oberdan' || $slug === 'foyer-spazio-oberdan-events' || $slug === 'spazio-oberdan-events')
      <div id="map" style="height: 200px;"></div>
      <script>
        var map;
        function initMap() {
          var myLatLng = {lat: 45.475395, lng: 9.204428};
          map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 16
          });

          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: '{{ $name }}'
          });
        }
      </script>
    @elseif ($slug === 'area-metropolis' || $slug === 'area-metropolis-exhibitions' || $slug === 'area-metropolis-exhibits' || $slug === 'foyer-area-metropolis' || $slug === 'foyer-area-metropolis-events' || $slug === 'area-metropolis-events' )
      <div id="map" style="height: 200px;"></div>
      <script>
        var map;
        function initMap() {
          var myLatLng = {lat: 45.568631, lng: 9.160292};
          map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 16
          });

          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: '{{ $name }}'
          });
        }
      </script>
    @elseif ($slug === 'cinema-mic' || $slug === 'terrazza-mic' || $slug === 'cinema-mic-exhibitions' || $slug === 'cinema-mic-exhibits' || $slug === 'cinema-mic-events' || $slug === 'terrazza-mic-events')
      <div id="map" style="height: 200px;"></div>
      <script>
        var map;
        function initMap() {
          var myLatLng = {lat: 45.513398, lng: 9.204513};
          map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 16
          });

          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: '{{ $name }}'
          });
        }
      </script>
    @elseif ($slug === 'biblioteca-morando' || $slug === 'biblioteca' || $slug === 'biblioteca-di-morando' || $slug === 'biblioteca-di-morando-exhibitions' || $slug === 'biblioteca-di-morando-events' || $slug === 'biblioteca-di-morando-exhibitions' || $slug === 'biblioteca-di-morando-exhibits')
      <div id="map" style="height: 200px;"></div>
      <script>
        var map;
        function initMap() {
          var myLatLng = {lat: 45.498711, lng: 9.214352};
          map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 16
          });

          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: '{{ $name }}'
          });
        }
      </script>
    @endif
  </div>
</div>
