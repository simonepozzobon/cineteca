<div class="row pb-5">
  <div class="col">
    <h5>Info Generali</h5>
    <hr>
    <p>
      <a class="" data-toggle="collapse" href="#orari" aria-expanded="false" aria-controls="biglietti">
        Orari apertura
      </a>
      <div class="collapse" id="orari">
        @if ($location === 'spazio-oberdan' || $location === 'spazio-oberdan-exhibitions' || $location === 'estate' || $location === 'spazio-oberdan-events' || $location === 'foyer-spazio-oberdan-events' || $location === 'foyer-spazio-oberdan')
          <p>
            <strong>le sale aprono mezz'ora prima dell'inizio della proiezione</strong>
            <br>
            <ul>
              <li>
                dal lunedì al venerdì<br>
                dalle 16:30 alle 21:00
              </li>
              <li>
                sabato e domenica<br>
                dalle 14:30 alle 21:00
              </li>
            </ul>
            <strong>Se non ci sono proiezioni in programma le sale rimangono chiuse</strong>
          </p>
        @elseif ($location === 'area-metropolis' || $location === 'area-metropolis-exhibitions' || $location === 'area-metropolis-events' || $location === 'foyer-area-metropolis-events' || $location === 'foyer-area-metropolis')
          <p>
            <strong>le sale aprono mezz'ora prima dell'inizio della proiezione</strong>
            <br>
            <ul>
              <li>
                dal lunedì al giovedi<br>
                dalle 20:30
              </li>
              <li>
                venerdì, sabato e domenica<br>
                dalle 14:30
              </li>
            </ul>
            <strong>Se non ci sono proiezioni in programma le sale rimangono chiuse</strong>
          </p>
        @elseif ($location === 'cinema-mic' || $location === 'cinema-mic-exhibitions' || $location === 'cinema-mic-events' || $location === 'terrazza-mic-events')
          <p>
            <ul>
              <li>
                lunedì chiuso<br>
              </li>
              <li>
                dal martedi al venerdi<br>
                dalle 17:00 alle 21:00<br>
              </li>
              <li>
                sabato e domenica<br>
                dalle 15:00 alle 19:00<br>
              </li>
            </ul>
          </p>
        @elseif ($location === 'biblioteca-di-morando' || $location === 'biblioteca-di-morando-exhibitions' || $location === 'biblioteca-di-morando-events' || $location === 'biblioteca-di-morando-exhibitions' || $location === 'biblioteca-di-morando-exhibits')
          <p>
            <b>Orario estivo consultazione biblioteca</b>
            Da Lunedì a Venerdì<br>
            dalle 14.30 alle 18.00<br><br>
            <b>Orario eventi biblioteca</b>
            Martedì, Mercoledì, Giovedì<br>
            dalle 18:30 alle 20:30<br><br>
          </p>
        @endif
      </div>
    </p>
    <p>
      @if (isset($free) && $free == true)
        <a class="" data-toggle="collapse" href="#biglietti" aria-expanded="false" aria-controls="biglietti">
          Ingresso Gratuito
        </a>
      @elseif ($location === 'festival-pgc')
        <a class="" data-toggle="collapse" href="#biglietti" aria-expanded="false" aria-controls="biglietti">
          Costo Biglietti
        </a>
        <div class="collapse" id="biglietti">
          <strong>Modalità di ingresso</strong><br>
          Tutti i biglietti delle proiezioni sono disponibili in prevendita alle casse delle sale di riferimento.<br>
          È possibile acquistare anche online i biglietti di ingresso alle proiezioni di Spazio Oberdan sulla piattaforma Webtic o direttamente sul sito della Cineteca<br>
          <br>
          <b>Ingresso unico: € 5,00</b><br><br>
          <u>Salvo diversa indicazione segnalata nel singolo evento</u>:<br>
          <br>
          <u>Programma family</u><br>
          Adulti: ingresso gratuito con *Cinetessera 2018 / ingresso intero € 5<br>
          Bambini: ingresso unico € 5<br><br>
          Ove è indicato è richiesta la prenotazione obbligatoria.<br>
          <br>
          <u>Concorso Rivelazioni</u><br>
          Ingresso gratuito con Cinetessera 2018 / ingresso intero € 5<br>
          <br>
          <u>Speciale Una notte al museo</u><br>
          Max 20 partecipanti. Costo euro 35,00.<br>
          Per prenotazioni e info: 0287242114<br>
          <br>
          <u>Open day scuole, corsi e professioni del cinema</u><br>
          MIC – Museo Interattivo del Cinema<br>
          Ingresso libero fino ad esaurimento posti. Prenotazione obbligatoria.<br>
          <br>
          <u>Seminario - Il mondo del lavoro nei film di Ken Loach</u><br>
          Seminario IL MONDO DEL LAVORO NEI FILM DI KEN LOACH<br>
          Ingresso libero, prenotazione obbligatoria.<br>
          <br>
          <u>Laboratorio - La materia dei sogni</u><br>
          Biglietto unico euro 5,00<br>
          <br>
          <u>Proiezioni Tempo Limite</u><br>
          Ingresso libero.<br>
          <br>
          *La Cinetessera 2018, la tessera annuale delle sale della Cineteca, ha un costo di € 10.
        </div>
      @elseif ($location === 'biblioteca-di-morando' || $location === 'biblioteca-di-morando-exhibitions' || $location === 'biblioteca-di-morando-events' || $location === 'biblioteca-di-morando-exhibitions' || $location === 'biblioteca-di-morando-exhibits')
        <a class="" data-toggle="collapse" href="#biglietti" aria-expanded="false" aria-controls="biglietti">
          Costo Biglietti
        </a>
        <div class="collapse" id="biglietti">
          <p>
            Ingresso libero con *Cinetessera 2018 fino a esaurimento posti
            disponibili (max 30).<br><br>
            *Cinetessera annuale: €5.<br>
            Dà diritto all’ingresso libero alle
            attività della Biblioteca di Morando e al biglietto ridotto fino al
            31 dicembre 2018 presso le sale di Fondazione Cineteca Italiana:
            MIC - Museo Interattivo del Cinema, Cinema Spazio Oberdan e
            Area Metropolis 2.0.<br>
            La Cinetessera è acquistabile presso la Biblioteca di Morando<br>
          </p>
        </div>
      @else
        <a class="" data-toggle="collapse" href="#biglietti" aria-expanded="false" aria-controls="biglietti">
          Costo Biglietti
        </a>
        <div class="collapse" id="biglietti">
          @if ($location  === 'cinema-mic' || $location === 'cinema-mic-exhibitions')
              <strong>Proiezioni</strong>
              <ul>
                <li>Intero € 6,50</li>
                <li>Ridotto* € 5,00</li>
                <li>1 adulto + 1 bambino € 7,00</li>
              </ul>
              <strong>Rassegna Estate Tabacchi</strong>
              <ul>
                <li>Intero € 6,50</li>
                <li>Ridotto* € 5,00</li>
              </ul>
              <em>*Under 14, studenti universitari, cinetessera, tessera FAI, Touring Junior, Soci Touring Club Adulti, Milan ID Card</em><br>
              <br>
              Inoltre venerdi, sabato e domenica alle ore 15.30 e 17.30, alle medesime condizioni economiche, è possibile visitare anche il Nuovo Archivio Storico dei Film, previa prenotazione obbligatoria al numero 0287242114.
          @elseif ($location === 'spazio-oberdan' || $location === 'spazio-oberdan-exhibitions')
            <ul>
              <li>Ingresso intero: € 7,50</li>
              <li>Ingresso ridotto*: € 6,00</li>
              <li>Ingresso Abbonamento Musei Lombardia e studenti universitari: € 6,50</li>
              <li>Ingresso giorni feriali spettacolo ore 17: € 7,50 intero o € 5,00 con cinetessera</li>
              <li>Cinetessera annuale valida da gennaio a dicembre: € 10,00</li>
              <li>Abilitato per APP 18  e CARTA DOCENTI</li>
            </ul>
            <p>
              *Riduzioni: Cinetessera, studenti universitari muniti di tessera universitaria.<br><br>
              I biglietti possono essere acquistati in prevendita alla cassa di Spazio Oberdan da una settimana prima dell'evento, nei giorni e negli orari di apertura della biglietteria.
            </p>
          @elseif ($location === 'area-metropolis' || $location === 'area-metropolis-exhibitions')
            <ul>
              <li>Ingresso intero: € 8,50</li>
              <li>Ingresso ridotto (con Cinetessera): € 6,50</li>
              <li>Ingresso under 14: € 5,50</li>
              <li>Ingresso studenti universitari muniti di tesserino o carta IO STUDIO: € 7,50</li>
              <li>Ingresso venerdì feriali spettacolo ore 15: € 4,50</li>
              <li>
                Convenzioni: € 7,00 / € 7,50
                <br>
                <small>(carta oro lun-dom, cral lun-ven.)</small>
              </li>
              <li>Ingresso proiezioni scolastiche: € 3,50</li>
              <li>Cinetessera annuale valida da gennaio a dicembre: € 10,00</li>
              <li>Abilitato per APP 18  e CARTA DOCENTI</li>
            </ul>
          @elseif ($location === 'estate')
            <ul>
              <li>Ingresso intero: € 6,50</li>
              <li>Ingresso ridotto (con Cinetessera): € 5,00</li>
            </ul>
          @endif
        </div>
      @endif
    </p>
  </div>
</div>
