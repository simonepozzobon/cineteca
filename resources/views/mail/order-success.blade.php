@component('mail::message')
Gentile {{ ucfirst($data['surname']) }} {{ ucfirst($data['name']) }},
<br>
<br>

Il pagamento del suo ordine è **andato a buon fine**, di seguito troverà il riepilogo.

<br>
#Riepilogo dell'ordine
-----
<br>

@php
  $subtotal = $data['order']->amount - $data['order']->shipping_cost;
@endphp

<table style="width: 100%;">
  <thead>
    <th style="text-align: left;">Prodotto</th>
    <th style="text-align: left;">Dettaglio</th>
    <th style="text-align: left;">Quantità</th>
    <th style="text-align: left;">Prezzo</th>
  </thead>
  @foreach ($data['products'] as $product)
    <tbody>
      <tr>
        <td>{{ $product->product->name }}</td>
        <td>{{ $product->price->description }}</td>
        <td>{{ $product->quantity }}</td>
        <td>€ {{ $product->price->price }}</td>
      </tr>
    </tbody>
  @endforeach
  <tbody>
    <tr>
      <td colspan="4"><br></td>
    </tr>
  </tbody>
  <tbody>
    <tr valign="middle">
      <td colspan="3"><h3>Subtotale</h3></td>
      <td><h3>€ {{ number_format((float)$subtotal, 2, '.', '') }}</h3></td>
    </tr>
  </tbody>
  <tbody>
    <tr valign="middle">
      <td colspan="3"><h3>Spedizione</h3></td>
      <td><h3>€ {{ $data['order']->shipping_cost }}</h3></td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td colspan="4"><br></td>
    </tr>
  </tbody>
  <tbody>
    <tr valign="middle">
      <td colspan="3"><h1>Totale</h1></td>
      <td><h1>€ {{ $data['order']->amount }}</h1></td>
    </tr>
  </tbody>
</table>
<br>
#Supporto
-----
<br>
Qualora dovesse riscontrare dei problemi, può rispondere a questa mail
o scrivere all'indirizzo cinestore@cinetecamilano.it
indicando come oggetto il codice identificativo dell'ordine **{{ $data['transaction_id'] }}**.
<br>
<br>


@component('mail::subcopy')
  <small>Cinestore è il sito di vendita dei prodotti realizzati da Fondazione Cineteca Italiana, Milano.</small>

  <small>NOTE LEGALI</small>

  <small>Copyright</small>

  <small>La presente pagina web e le immagini, i testi, la grafica e gli altri contenuti sono tutelati dalle norme legali sul diritto di autore. Titolare dei diritti di autore è la Fondazione Cineteca Italiana (Milano). Tutti i diritti sono riservati. L'utilizzo della pagina web e dei suoi contenuti è consentito soltanto per la consultazione e lo shopping online.</small>

  <small>Immagini, video</small>

  <small>La pagina web contiene numerosi marchi, personaggi, immagini, video e prodotti tutelati dalle norme sul copyright e di proprietà degli aventi diritto.</small>

  <small>Esclusione della responsabilità</small>

  <small>La Fondazione Cineteca Italiana (Milano) non garantiscono in nessun modo che le funzioni della presente pagina web siano esenti da errori e che la presente pagina web o il rispettivo server siano privi di virus o componenti dannosi. È esclusa qualsiasi responsabilità della Fondazione Cineteca Italiana (Milano) per i danni derivanti dall'accesso a o dall'utilizzo di questa pagina web.</small>

  <small>Links da e verso altre pagine web</small>

  <small>Possono esserci links verso la presente pagina web. La Fondazione Cineteca Italiana (Milano) non ha controllato tali links né le pagine web corrispondenti e non risponde del loro contenuto.</small>
@endcomponent

@endcomponent
