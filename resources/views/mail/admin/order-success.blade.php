@component('mail::message')

È stato effettuato un ordine sul cinestore

<br>
#Riepilogo dell'ordine
-----
<br>

@php
  $subtotal = $data['order']->amount - $data['order']->shipping_cost;
@endphp

<table style="width: 100%;">
  <thead>
    <th style="text-align: left;">Prodotto</th>
    <th style="text-align: left;">Dettaglio</th>
    <th style="text-align: left;">Quantità</th>
    <th style="text-align: left;">Prezzo</th>
  </thead>
  @foreach ($data['products'] as $product)
    <tbody>
      <tr>
        <td>{{ $product->product->name }}</td>
        <td>{{ $product->price->description }}</td>
        <td>{{ $product->quantity }}</td>
        <td>€ {{ $product->price->price }}</td>
      </tr>
    </tbody>
  @endforeach
  <tbody>
    <tr>
      <td colspan="4"><br></td>
    </tr>
  </tbody>
  <tbody>
    <tr valign="middle">
      <td colspan="3"><h3>Subtotale</h3></td>
      <td><h3>€ {{ number_format((float)$subtotal, 2, '.', '') }}</h3></td>
    </tr>
  </tbody>
  <tbody>
    <tr valign="middle">
      <td colspan="3"><h3>Spedizione</h3></td>
      <td><h3>€ {{ $data['order']->shipping_cost }}</h3></td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td colspan="4"><br></td>
    </tr>
  </tbody>
  <tbody>
    <tr valign="middle">
      <td colspan="3"><h1>Totale</h1></td>
      <td><h1>€ {{ $data['order']->amount }}</h1></td>
    </tr>
  </tbody>
</table>
<br>
#Dettagli
-----
<br>
Codice della transazione **{{ $data['transaction_id'] }}**.

Per verificare tutti i dettagli dell'ordine, accedi al <a href="http://www.cinetecamilano.it/admin/cinestore/{{ $data['order']->id }}">pannello di controllo</a>
<br>
<br>

@endcomponent
