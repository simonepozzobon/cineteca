@extends('layouts.admin')
@section('title')
  Welcome
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          @if (isset($user_sessions))
            <div class="row px-5">
              <div class="col-md-4">
                <div class="container-fluid bg-faded p-4">
                    <h3>Sessioni Visitatori</h3>
                    <hr>
                    <h1>{{ $user_sessions }}</h1>
                </div>
              </div>
              <div class="col-md-4">
                <div class="container-fluid bg-faded p-4">
                    <h3>Ip del Server</h3>
                    <hr>
                    <h1>{{ $_SERVER['SERVER_ADDR'] }}</h1>
                </div>
              </div>
            </div>
          @endif
        </div>
    </div>
</div>
@endsection
