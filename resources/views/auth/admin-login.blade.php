@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-4 offset-md-4 bg-faded">
        <h2 class="pt-4">Login</h2>
        <hr>
        <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.login.submit') }}">
            {{ csrf_field() }}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <h6>Email</h6>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <h6>Password</h6>
                <input id="password" type="password" class="form-control" name="password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
              <div class="checkbox">
                  <label>
                      <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> <h6 class="d-inline">Ricordami</h6>
                  </label>
              </div>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block">
                  Login
              </button>
            </div>

            <div class="form-group">
              <a class="" href="{{ route('password.request') }}">
                  Forgot Your Password?
              </a>
            </div>
        </form>
      </div>
    </div>
</div>
@endsection
