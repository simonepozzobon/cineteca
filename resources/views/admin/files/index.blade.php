@extends('layouts.admin')
@section('title', 'Carica file')
@section('content')
<div class="row">
  <div class="col-md-8">
    <div class="container-fluid bg-faded p-4">
      <h3>Lista Pdf</h3>
      <hr>
      <table class="table table-hover">
        <thead>
          <th>Titolo</th>
          <th>Link</th>
          <th></th>
        </thead>
        <tbody>
          @foreach ($files as $key => $file)
            <tr>
              <td class="align-middle">{{ $file->title }}</td>
              <td class="align-middle">
                <input id="full-slug-{{ $file->id }}" type="text" value="{{ url('/') }}{{ Storage::disk('local')->url($file->url) }}" class="form-control">
              </td>
              <td class="align-middle">
                <div class="btn-group">
                  <button href="{{ Storage::disk('local')->url($file->url) }}" class="btn btn-warning">Vedi</button>
                  <button id="clip-copy-{{ $file->id }}" href="#" class="copy-link btn btn-info" data-id="{{ $file->id }}">Copia Link</button>
                  <button type="button" name="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal-{{ $file->id }}">Elimina</button>
                </div>
              </td>
              <div class="modal fade" id="deleteModal-{{ $file->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModal-{{ $file->id }}Label" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="deleteModal{{ $file->id }}Label">Elimina File</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body p-5">
                      <h4 class="text-center">Sei sicuro di voler eliminare il file?</h4>
                      <p class="text-center">
                        Questa operazione non potrà essere annullata!
                      </p>
                    </div>
                    <div class="modal-footer">
                      <form class="ml-3" action="{{ route('files.destroy', $file->id) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <div class="btn-group">
                          <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Annulla</button>
                          <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Elimina</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-md-4">
    <div class="container-fluid bg-faded p-4">
      <h3>Carica Nuovo</h3>
      <hr>
      <form action="{{ route('files.upload') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <div class="form-group">
          <h6>Seleziona File</h6>
          <input type="file" name="media" class="form-control">
        </div>
        <div class="form-group">
          <h6>Titolo</h6>
          <input type="text" name="title" class="form-control">
        </div>
        <button type="submit" name="button" class="btn btn-primary btn-block"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salva</button>
      </form>
    </div>
  </div>
</div>
@endsection
@section('scripts')
  <script>
  $('.copy-link').on('click', function(e){
    e.preventDefault();
    var id = $(this).data('id');
    var elem = document.getElementById('full-slug-'+id);
    copyToClipboard(elem, id);
  });

  function copyToClipboard(elem, id) {
      // create hidden text element, if it doesn't already exist
      var targetId = '_hiddenCopyText_';
      var isInput = elem.tagName === 'INPUT' || elem.tagName === 'TEXTAREA';
      var origSelectionStart, origSelectionEnd;
      var button = document.getElementById('clip-copy-'+id);
      if (isInput) {
          // can just use the original source element for the selection and copy
          target = elem;
          origSelectionStart = elem.selectionStart;
          origSelectionEnd = elem.selectionEnd;
      } else {
          // must use a temporary form element for the selection and copy
          target = document.getElementById(targetId);
          if (!target) {
              var target = document.createElement('textarea');
              target.style.position = 'absolute';
              target.style.left = '-9999px';
              target.style.top = '0';
              target.id = targetId;
              document.body.appendChild(target);
          }
          target.textContent = elem.textContent;
      }
      // select the content
      var currentFocus = document.activeElement;
      target.focus();
      target.setSelectionRange(0, target.value.length);

      // copy the selection
      var succeed;
      try {
          succeed = document.execCommand('copy');
          button.classList.remove('btn-info');
          button.classList.add('btn-success');
          button.innerHTML = '<i class="fa fa-check" aria-hidden="true"></i> Copiato';
      } catch(e) {
          succeed = false;
      }
      // restore original focus
      if (currentFocus && typeof currentFocus.focus === 'function') {
          currentFocus.focus();
      }

      if (isInput) {
          // restore prior selection
          elem.setSelectionRange(origSelectionStart, origSelectionEnd);
      } else {
          // clear temporary content
          target.textContent = '';
      }
      return succeed;
  }
  </script>
@endsection
