@extends('layouts.admin')
@section('title', 'Crea una slide')
@section('content')
<div class="row">
  <div class="col">
    <div class="row">
      @foreach ($items as $key => $item)
        <div class="col-md-3 d-inline-block p-2">
          <div class="container bg-faded p-4">
            <h6>{{ $item['title'] }}</h6>
            <h6><span class="badge badge-info">{{ $item['type'] }}</span></h6>
            <img src="{{ Storage::disk('local')->url($item['img']) }}" alt="{{ $item['title'] }}" class="img-fluid">
            <p class="pt-2">{{ substr(strip_tags($item['content']), 0, 100) }}{{ strlen(strip_tags($item['content'])) > 100 ? '...' : ""  }}</p>
            <form class="" action="{{ route('homeslider.store') }}" method="POST">
              {{ csrf_field() }}
              {{ method_field('POST') }}
              <input type="hidden" name="post_id" value="{{ $item['id'] }}">
              <input type="hidden" name="post_type_id" value="{{ $item['type_id'] }}">
              <input type="hidden" name="img_path" value="{{ $item['img'] }}">
              <button type="submit" class="btn btn-primary btn-block mt-2"><i class="fa fa-check" aria-hidden="true"></i> Seleziona</button>
            </form>
          </div>
        </div>
      @endforeach
    </div>
  </div>
</div>
@endsection
