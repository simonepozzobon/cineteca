@extends('layouts.admin')
@section('title', 'Home Slider')
@section('content')
<div class="row">
  <div class="col pb-4">
    <a href="{{ route('homeslider.create') }}" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add New Slide</a>
  </div>
</div>
<div class="row">
  <div class="col">
    <table class="table table-hover">
      <thead>
        <th>Id</th>
        <th>Titolo</th>
        <th>Img</th>
        <th>Tools</th>
      </thead>
      <tbody>
        @foreach ($slides as $key => $slide)
          <tr>
            <td class="align-middle">{{ $slide['id'] }}</td>
            <td class="align-middle">{{ $slide['title'] }}</td>
            <td class="align-middle"><img src="{{ Storage::disk('local')->url($slide['img'] )}}" width="57"></td>
            <td class="align-middle">
              <form class="" action="{{ route('homeslider.destroy', $slide['id']) }}" method="POST">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button type="submit" name="button" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
