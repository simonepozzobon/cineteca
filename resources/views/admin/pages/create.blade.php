@extends('layouts.admin')
@section('title', 'Nuova Pagina')
@section('content')
  <form class="" action="{{ route('pages.store') }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('POST') }}
    <div class="row">
      <div class="col-md-8">
        <div class="container-fluid bg-faded p-4">
          <h3>Nuova Pagina</h3>
          <hr>
          <div class="form-group">
            <h6>Title</h6>
            <input id="title" type="text" name="title" class="form-control" placeholder="Titolo della pagina">
          </div>
          <div class="form-group">
            <h6>Slug</h6>
            <input id="slug" type="text" name="slug" class="form-control" placeholder="Url della pagina">
          </div>
          <div class="form-group">
            <h6>Contenuto</h6>
            <textarea name="content" id="editor" rows="8" class="form-control"></textarea>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="container-fluid bg-faded p-4">
          <h3>Dettagli</h3>
          <hr>
          <div class="form-group">
            <h6>Redirect</h6>
            <label class="custom-control custom-checkbox">
              <input id="redirect" name="redirect" type="checkbox" class="custom-control-input">
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">È un link?</span>
            </label>
          </div>
          <div id="redirect-url" class="form-group d-none">
            <h6>Link</h6>
            <input type="text" name="redirect_url" value="{{ old('redirect_url') }}" class="form-control">
          </div>
          <div class="form-group">
            <h6>Stato</h6>
            <select class="form-control" name="status_id">
              @if (isset($statuses))
                @foreach ($statuses as $key => $status)
                  <option value="{{ $status->id }}">{{ $status->status }}</option>
                @endforeach
              @endif
            </select>
          </div>
          <div class="form-group">
            <h6>Festival</h6>
            <label class="custom-control custom-checkbox">
              <input id="festival" name="festival" type="checkbox" class="custom-control-input">
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">Appartiene ad un festival?</span>
            </label>
          </div>
          <div id="festival-id" class="form-group d-none">
            <h6>Festival</h6>
            <select class="form-control" name="festival_id">
              <option value="0"></option>
              @isset($festivals)
                @foreach ($festivals as $key => $festival)
                  <option value="{{ $festival->id }}">{{ $festival->title }}</option>
                @endforeach
              @endisset
            </select>
          </div>
          <div id="festival-cat" class="form-group d-none">
            <h6>Categoria</h6>
            <select class="form-control" name="festival_cat_id">
            </select>
          </div>
          <div class="form-group">
            <h6>Bambini</h6>
            <select class="form-control" name="kids">
              <option value="0" selected>Generico</option>
              <option value="1">Per Bambini</option>
            </select>
          </div>
          <div class="form-group">
            <h6>Immagine Principale</h6>
            <div class="row">
              <div class="col-md-6">
                <input type="file" name="media" value="{{ old('media') }}" class="form-control-file form-control" aria-describedby="fileHelp">
                <input type="hidden" name="media-library" value="">
              </div>
              <div class="col-md-6">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#mediaLibrary">
                  <i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria
                </button>

                <!-- Modal -->
                <div class="modal fade" id="mediaLibrary" tabindex="-1" role="dialog" aria-labelledby="mediaLibraryLabel" aria-hidden="true">
                  <div id="media-wrapper" class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="mediaLibraryLabel"><i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria Immagini</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div id="MediaLibrary">
                          <media-library />
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <h6>Categoria</h6>
            <select class="form-control" name="category_id">
              @if (isset($categories))
                @foreach ($categories as $key => $category)
                  <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
              @endif
            </select>
          </div>

          <div class="form-group">
            <h6>Informazioni Generali</h6>
            <label class="custom-control custom-checkbox">
              <input id="hide_info" name="hide_info" type="checkbox" class="custom-control-input">
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">Nascondi le informazioni nella barra laterale?</span>
            </label>
          </div>

          <div class="pt-2">
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salva</button>
          </div>
        </div>
      </div>
    </div>
  </form>
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote.css') }}">
  <script src="{{ asset('plugins/summernote/summernote.js') }}"></script>
  <script src="/js/media-library.js"></script>
  <script src="{{ asset('plugins/summernote/plugin/boxes/summernote-ext-boxes.js') }}"></script>
  <script>
    $('#editor').summernote({
        height: 300,
        focus: true,
        lang: 'it-IT',
        toolbar: [
          ['font', ['style']],
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['tbl', ['table', 'hr']],
          ['media', ['link', 'picture', 'video']],
          ['action', ['undo', 'redo']],
          ['code', ['codeview']],
          ['pro', ['help']],
        ],
        callbacks: {
          onImageUpload: function(files) {
              for (var i = 0; i < files.length; i++) {
                  var file = files[i];
                  console.log(file);
                  data = new FormData();
                  data.append('media', file);
                  $.ajax({
                    type: 'post',
                    url:  '{{ route('media.api.upload') }}',
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: data,
                    success: function(response) {
                      $('#editor').summernote("insertImage", response.path);
                    }
                  });
              }
          }
        }
    });

    $("#title").keyup(function(event) {
      var stt = $(this).val().toLowerCase().replace(/\s/g,'-');
      $("#slug").val(stt);
    });

    $('#redirect').on('change', function(e) {
      if ($(this).prop('checked') === true) {
        $('#redirect-url').removeClass('d-none');
      } else {
        $('#redirect-url').addClass('d-none');
      }
    });

    $('#festival').on('change', function(e) {
      if ($(this).prop('checked') === true) {
        $('#festival-id').removeClass('d-none');
      } else {
        $('#festival-id').addClass('d-none');
      }
    });

    $('#festival-id select').on('change', function(e) {
      var festival_id = $(this).val();
      if (festival_id > 0) {
        $.ajax({
          type: 'get',
          url:  '/admin/festival-cat/'+festival_id,
          headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
          success: function(data) {
            console.log(data);
            $('#festival-cat select option').each(function(){
              $(this).remove();
            });
            $.each(data, function(){
              var option = '';
              option += '<option value="'+this.id+'">'+this.name+'</option>'
              $('#festival-cat select').append(option);
            });
            $('#festival-cat').removeClass('d-none');
          }
        });
      } else {
        $('#festival-cat').addClass('d-none');
        var empty = '<option value="0"></option>';
        $('#festival-cat select option').each(function(){
          $(this).remove();
        });
        $('#festival-cat select').append(empty);
      }
    });
  </script>
@endsection
