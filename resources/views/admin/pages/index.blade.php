@extends('layouts.admin')
@section('title', 'Pages')
@section('content')
  <div class="row">
    <div class="col pb-4">
      <a href="{{ route('pages.create') }}" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Add New Page</a>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <table class="table table-hover">
        <thead>
          <th>Id</th>
          <th>Titolo</th>
          <th>Status</th>
          <th>Contenuto</th>
          <th></th>
        </thead>
        <tbody>
          @foreach ($pages as $key => $page)
            <tr>
              <td>{{ $page->id }}</td>
              <td>{{ $page->title }}</td>
              <td>
                @if ($page->status_id == 1)
                  <i class="fa fa-check-circle-o text-warning" aria-hidden="true"></i>
                @else
                  <i class="fa fa-check-circle text-success" aria-hidden="true"></i>
                @endif
              </td>
              <td>{{ substr(strip_tags($page->content), 0, 50) }}{{ strlen(strip_tags($page->content)) > 50 ? '...' : "" }}</td>
              <td>
                <div class="btn-group">
                  <a href="{{ route('pages.edit', $page->id) }}" class="btn btn-info">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                  </a>
                  <a href="{{ route('single.page', $page->slug) }}" target="_blank" class="btn btn-warning">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </a>
                  <a class="btn btn-danger text-white" data-toggle="modal" data-target="#deleteModal-{{ $page->id }}">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                  </a>
                </div>
                <div class="modal fade" id="deleteModal-{{ $page->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModal{{ $page->id }}Label" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="deleteModal{{ $page->id }}Label">Elimina Pagina</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body p-5">
                        <h4 class="text-center">Sei sicuro di voler eliminare la pagina?</h4>
                        <p class="text-center">
                          Questa operazione non potrà essere annullata!
                        </p>
                      </div>
                      <div class="modal-footer">
                        <form class="" action="{{ route('pages.destroy', $page->id) }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}
                          <div class="btn-group">
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Annulla</button>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Elimina</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection
