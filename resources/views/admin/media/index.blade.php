@extends('layouts.admin')
@section('title', 'Libreria Media')
@section('content')
<div class="row pb-4">
  <div class="col-md-6">
    <button href="" class="btn btn-primary" data-toggle="modal" data-target=".uploadForm"><i class="fa fa-upload" aria-hidden="true"> Immagine</i></button>
    <div class="modal fade uploadForm" tabindex="-1" role="dialog" aria-labelledby="uploadFormModal" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <form id="upload-form" method="post" action="{{ route('media.upload') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="modal-header">
              <h5 class="modal-title" id="uploadFormModalLabel">Carica Una Nuova Immagine</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h6 class="text-center">Selezione l'immagine da caricare</h6>
                <div class="form-group">
                  <input id="media" type="file" class="form-control" name="media">
                  <input type="hidden" name="path" value="">
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"> Annulla</i></button>
              <button id="uploadForm" type="submit" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"> Salva</i></button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="d-flex justify-content-end">
      <a href="{{ route('gallery.create') }}" class="btn btn-primary mr-2"><i class="fa fa-plus" aria-hidden="true"> Galleria</i></a>
      <a href="{{ route('gallery.index') }}" class="btn btn-info"><i class="fa fa-picture-o" aria-hidden="true"> Vedi Gallerie</i></a>
    </div>
  </div>
</div>
<div class="row">
  <div class="col">
    <div id="loader" class="loader-container d-none">
      <div class="loader"></div>
    </div>
    {{-- <div id="media-grid" class="row">
      @foreach ($medias as $key => $media)
        <div class="col-md-2 pb-4">
          <div id="{{ $media->id }}" class="container p-3 bg-faded">
            <img src="{{ Storage::disk('local')->url($media->thumb) }}" class="img-fluid w-100">
            <a href="{{ route('media.edit', $media->id) }}" class="btn btn-primary btn-block mt-3"><i class="fa fa-wrench" aria-hidden="true"></i> Modifica</a>
          </div>
        </div>
      @endforeach
    </div> --}}
    <div id="MediaLibrary">
      <media-library
        :editable="true"
      />
    </div>

  </div>
</div>
@endsection
@section('scripts')
  <script src="/js/media-library.js"></script>
  <script type="text/javascript">
    // Init counter
    var objs = 30;
    var $loader = $('#loader');

    // Detect scroll position
    // $(window).scroll(function() {
    //   if(($(window).scrollTop() + $(window).height()) >= $('#media-grid').height()) {
    //     console.log(objs);
    //     var data = {
    //       '_token' : $('meta[name="csrf-token"]').attr('content'),
    //       'objs' : objs,
    //     }
    //     var currentRequest = null;
    //
    //     currentRequest = $.ajax({
    //       type: 'post',
    //       url:  '{{ route('media.loadmore') }}',
    //       headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //       },
    //       data: data,
    //       beforeSend: function() {
    //         if(currentRequest != null) {
    //             currentRequest.abort();
    //         }
    //         $loader.removeClass('d-none');
    //       },
    //       success: function (response) {
    //         console.log(response);
    //         objs = objs + response.newObjs;
    //         for (var i = 0; i < response.newObjs; i++) {
    //           var data = '';
    //           data += '<div class="col-md-2 pb-4">';
    //           data +=   '<div class="container p-3 bg-faded">';
    //           data +=     '<img src="'+response.imgs[i].url+'" class="img-fluid w-100">';
    //           data +=     '<a href="/admin/media/edit/'+response.imgs[i].id+'" class="btn btn-primary btn-block mt-3"><i class="fa fa-wrench" aria-hidden="true"></i> Modifica</a>';
    //           data +=   '</div>';
    //           data += '</div>';
    //           $('#media-grid').append(data);
    //         }
    //         $loader.addClass('d-none');
    //       },
    //       error: function (errors) {
    //         console.log(errors);
    //       }
    //     });
    //   }
    // });
  </script>
@endsection
