@extends('layouts.admin')
@section('title', 'Modifica Media')
@section('meta')
  <meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
@endsection
@section('stylesheets')
<style media="screen">
  img {
    max-width: 100% !important;
  }
  .cropper {
    max-width: 100% !important;
  }
</style>
@endsection
@section('content')
  <div class="row">
    <div class="col-md-8">
      <div class="container-fluid bg-faded p-5">
        <h3>Modifica Media</h3>
        <hr>
        <input id="original-img" type="hidden" name="original" value="{{ Storage::disk('local')->url($media->img) }}">
        <h4 class="pb-3">Formato slider</h4>
        <div class="cropper">
          <img id="slider" src="{{ Storage::disk('local')->url($media->slide_img) }}" alt="Immagine {{ $media->id }} Slider" class="img-fluid w-100 pb-5">
        </div>
        <div id="slider-ctrl" class="text-center pt-3">
          <a id="crop" href="#" class="btn btn-primary crop"><i class="fa fa-crop" aria-hidden="true"></i> Modifica Ritaglio</a>
          <a id="save" href="#" class="btn btn-success save d-none"><i class="fa fa-crop" aria-hidden="true"></i> Taglia</a>
          <a id="undo" href="#" class="btn btn-danger undo d-none"><i class="fa fa-undo" aria-hidden="true"></i> Annulla</a>
        </div>
        <h4 class="text-center pt-5 pb-3">Formato Landscape</h4>
        <div class="text-center pb-5 cropper">
          <img id="landscape" src="{{ Storage::disk('local')->url($media->landscape) }}" alt="Immagine {{ $media->id }} Landscape" class="img-fluid">
        </div>
        <div id="landscape-ctrl" class="text-center pt-3">
          <a id="crop" href="#" class="btn btn-primary crop"><i class="fa fa-crop" aria-hidden="true"></i> Modifica Ritaglio</a>
          <a id="save" href="#" class="btn btn-success save d-none"><i class="fa fa-crop" aria-hidden="true"></i> Taglia</a>
          <a id="undo" href="#" class="btn btn-danger undo d-none"><i class="fa fa-undo" aria-hidden="true"></i> Annulla</a>
        </div>
        <div class="row pt-5">
          <div class="col-md-6">
            <h4 class="text-center pb-3">Formato Ritratto</h4>
            <div class="text-center pb-5 cropper">
              <img id="portrait" src="{{ Storage::disk('local')->url($media->portrait) }}" alt="Immagine {{ $media->id }} Ritratto" class="img-fluid">
            </div>
            <div id="portrait-ctrl" class="text-center pt-3">
              <a id="crop" href="#" class="btn btn-primary crop"><i class="fa fa-crop" aria-hidden="true"></i> Modifica Ritaglio</a>
              <a id="save" href="#" class="btn btn-success save d-none"><i class="fa fa-crop" aria-hidden="true"></i> Taglia</a>
              <a id="undo" href="#" class="btn btn-danger undo d-none"><i class="fa fa-undo" aria-hidden="true"></i> Annulla</a>
            </div>
          </div>
          <div class="col-md-6">
            <h4 class="text-center pb-3">Formato Quadrato</h4>
            <div class="text-center pb-5 cropper">
              <img id="thumb" src="{{ Storage::disk('local')->url($media->thumb) }}" alt="Immagine {{ $media->id }} Quadrato" class="img-fluid">
            </div>
            <div id="thumb-ctrl" class="text-center pt-3">
              <a id="crop" href="#" class="btn btn-primary crop"><i class="fa fa-crop" aria-hidden="true"></i> Modifica Ritaglio</a>
              <a id="save" href="#" class="btn btn-success save d-none"><i class="fa fa-crop" aria-hidden="true"></i> Taglia</a>
              <a id="undo" href="#" class="btn btn-danger undo d-none"><i class="fa fa-undo" aria-hidden="true"></i> Annulla</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-5">
        <h3>Dettagli</h3>
        <hr>
        <h6>Creato il</h6>
        <p>{{ $media->created_at }}</p>
        <h6>Modificato il</h6>
        <p>{{ $media->updated_at }}</p>
        <div class="row">
          <div class="col-md-6">
            <a id="auto-cut" href="#" class="btn btn-info btn-block disabled"><i class="fa fa-picture-o" aria-hidden="true"></i> Taglia Auto</a>
          </div>
          <div class="col-md-6">
            <a href="" class="btn btn-danger btn-block" data-toggle="modal" data-target=".replace"><i class="fa fa-upload" aria-hidden="true"></i> Sostituisci</a>
            <div class="modal fade replace" tabindex="-1" role="dialog" aria-labelledby="replaceModal" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <form id="replace-form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="modal-header">
                      <h5 class="modal-title" id="replaceModalLabel">Sostituisci Immagine</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <h4 class="text-center">Attenzione!</h4>
                      <h6 class="text-center">Se sostituisci l'immagine verrà modificata ovunque.</h6>
                        <div class="form-group">
                          <input id="media" type="file" class="form-control" name="media">
                          <input type="hidden" name="path" value="{{ $media->img }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-warning" data-dismiss="modal">Annulla</button>
                      <button id="replace" type="submit" class="btn btn-danger">Sostituisci</button>

                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <a href="#" class="btn btn-primary btn-block mt-3"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salva e Chiudi</a>
      </div>
    </div>
  </div>
@endsection
@section('scripts')
  <link  href="{{ asset('plugins/cropper/cropper.css') }}" rel="stylesheet">
  <script src="{{ asset('plugins/cropper/cropper.js') }}"></script>
  <script type="text/javascript">

  $('#slider-ctrl .crop').on('click', function(e) {
    e.preventDefault();
    var ctrl = $('#slider-ctrl');
    var el = $('#slider');
    var img = '{{ Storage::disk('local')->url($media->slide_img) }}';
    cropIt(ctrl, el, img, 1920, 720);
  });

  $('#landscape-ctrl .crop').on('click', function(e) {
    e.preventDefault();
    var ctrl = $('#landscape-ctrl');
    var el = $('#landscape');
    var img = '{{ Storage::disk('local')->url($media->landscape) }}';
    cropIt(ctrl, el, img, 960, 540);
  });

  $('#portrait-ctrl .crop').on('click', function(e) {
    e.preventDefault();
    var ctrl = $('#portrait-ctrl');
    var el = $('#portrait');
    var img = '{{ Storage::disk('local')->url($media->portrait) }}';
    cropIt(ctrl, el, img, 720, 960);
  });

  $('#thumb-ctrl .crop').on('click', function(e) {
    e.preventDefault();
    var ctrl = $('#thumb-ctrl');
    var el = $('#thumb');
    var img = '{{ Storage::disk('local')->url($media->thumb) }}';
    cropIt(ctrl, el, img, 1, 1);
  });


  function cropIt(ctrl, el, img, width, height)
  {
    var crop = ctrl.find('.crop');
    var save = ctrl.find('.save');
    var undo = ctrl.find('.undo');

    crop.addClass('d-none');
    save.removeClass('d-none');
    undo.removeClass('d-none');

    // Cambio l'immagine
    el.attr('src', '{{ Storage::disk('local')->url($media->img) }}');

    var id = el.attr('id');


    // Attivo il cropper
    el.cropper({
      aspectRatio: width / height,
      // crop: function(e) {
      // }
    });

    save.on('click', function(e) {
      e.preventDefault();
      // Salvo l'immagine in un oggetto
      var image = el.cropper('getCroppedCanvas').toDataURL();
      console.log(img);
      var data = {
        '_token' : $('meta[name="csrf-token"]').attr('content'),
        'id' : '{{ $media->id }}',
        'path' : img,
        'base64' : image
      }

      $.ajax({
        type: 'post',
        url:  '{{ route('media.crop') }}',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: data,
        success: function (response) {
          console.log(response);
          // Distrugge il cropper
          el.cropper("destroy");

          // ripristina i controlli allo stato iniziale
          save.addClass('d-none');
          undo.addClass('d-none');
          crop.removeClass('d-none');

          // Ripristina l'immagine originale
          el.attr('src', response.path);
        },
        error: function (errors) {
          console.log(errors);
        }
      });
    });

    // Annulla
    undo.on('click', function(e) {
      e.preventDefault();
      // Distrugge il cropper
      el.cropper("destroy");

      // ripristina i controlli allo stato iniziale
      save.addClass('d-none');
      undo.addClass('d-none');
      crop.removeClass('d-none');

      // Ripristina l'immagine originale
      el.attr('src', img);
    });

  }

  $('#auto-cut').on('click', function(e) {
    e.preventDefault();
  });

  $('form#replace-form').submit(function(event) {
    event.preventDefault();
    $('.replace').modal('hide');

    var formData = new FormData();
    formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
    formData.append('media', $('#media')[0].files[0]);
    formData.append('path', $('input[name^="path"]').val());

    console.log(formData);
    $.ajax({
      type: 'post',
      url:  '{{ route('media.replace') }}',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: formData,
      processData: false,
      contentType: false,
      success: function (response) {
        console.log(response);
      },
      error: function (errors) {
        console.log(errors);
      }
    });
  });

  </script>
@endsection
