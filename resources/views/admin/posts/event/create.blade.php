@extends('layouts.admin')
@section('title', 'Nuovo Evento')
@section('content')
<form class="" action="{{ route('posts.store', 'event') }}" enctype="multipart/form-data" method="post">
  {{ csrf_field() }}
  {{ method_field('POST') }}
  <div class="row">
    <div class="col-md-8">
      <div class="row pb-5">
        <div class="col">
          <div class="container-fluid bg-faded p-4">
            <h3>Nuovo Evento</h3>
            <hr>
            <div class="form-group">
              <h6>Titolo</h6>
              <input id="title" type="text" name="title" class="form-control" vlaue="{{ old('title') }}" placeholder="Titolo dell'evento">
            </div>
            <div class="form-group">
              <h6>Slug</h6>
              <input id="slug" type="text" name="slug" class="form-control" value="{{ old('slug') }}" placeholder="Link dell'evento">
            </div>
            <div class="form-group">
              <h6>Link</h6>
              <div class="row">
                <div class="col-md-9">
                  <input id="full-slug" type="text" name="" value="old('slug')" class="form-control">
                </div>
                <div class="col-md-3">
                  <a id="clip-copy" href="" class="btn btn-block btn-info"><i class="fa fa-clipboard" aria-hidden="true"></i> Copia link</a>
                </div>
              </div>
            </div>
            <div class="form-group">
              <h6>Descrizione</h6>
              <textarea id="editor" name="description" rows="8" class="form-control">{{ old('description') }}</textarea>
            </div>
          </div>
        </div>
      </div>
      <div id="guestForm" class="row d-none">
        <div class="col">
          <div class="container-fluid bg-faded p-4">
            <h3>Partecipanti</h3>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <h6>Numero Massimo di partecipanti</h6>
                  <input type="text" name="guest" value="{{ old('guest') }}" class="form-control">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <h6>Dimensione Lista d'attesa</h6>
                  <input type="text" name="waiting" value="{{ old('waiting') }}" class="form-control">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <h6>Bambini</h6>
                  <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="kidsList">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Campo per i bambini</span>
                  </label>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <h6>Altri ospiti</h6>
                  <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="single_guest">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Prenotazione per singola persona</span>
                  </label>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <h6>Pagamento</h6>
                  <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="payment">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Pagamento</span>
                  </label>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <h6>Nascondi Disponibilità</h6>
                  <label class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="available_hidden">
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Nascondi posti disponibili</span>
                  </label>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h3>Dettagli</h3>
        <hr>
        <div class="form-group">
          <h6>Stato</h6>
          <select class="form-control" name="status_id">
            @if (isset($statuses))
              @foreach ($statuses as $key => $status)
                <option value="{{ $status->id }}">{{ $status->status }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="form-group">
          <h6>Festival</h6>
          <label class="custom-control custom-checkbox">
            <input id="festival" name="festival" type="checkbox" class="custom-control-input">
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">Appartiene ad un festival?</span>
          </label>
        </div>
        <div id="festival-id" class="form-group d-none">
          <h6>Festival</h6>
          <select class="form-control" name="festival_id">
            <option value="0"></option>
            @isset($festivals)
              @foreach ($festivals as $key => $festival)
                <option value="{{ $festival->id }}">{{ $festival->title }}</option>
              @endforeach
            @endisset
          </select>
        </div>
        <div id="festival-cat" class="form-group d-none">
          <h6>Categoria</h6>
          <select class="form-control" name="festival_cat_id">
            <option value="0"></option>
          </select>
        </div>
        <div class="form-group">
          <h6>Tipo di Evento</h6>
          <select class="form-control" name="event_type">
            @if (isset($event_types))
              @foreach ($event_types as $key => $event_type)
                <option value="{{ $event_type->id }}">{{ $event_type->name }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="form-group">
          <h6>Bambini</h6>
          <select class="form-control" name="kids">
            <option value="0" selected>Generico</option>
            <option value="1">Per Bambini</option>
          </select>
        </div>
        <div class="form-group">
          <h6>Immagine Principale</h6>
          <div class="row">
            <div class="col-md-6">
              <input type="file" name="media" value="{{ old('media') }}" class="form-control-file" aria-describedby="fileHelp">
              <input type="hidden" name="media-library" value="">
            </div>
            <div class="col-md-6">
              <!-- Button trigger modal -->
              <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#mediaLibrary">
                <i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria
              </button>

              <!-- Modal -->
              <div class="modal fade" id="mediaLibrary" tabindex="-1" role="dialog" aria-labelledby="mediaLibraryLabel" aria-hidden="true">
                <div id="media-wrapper" class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="mediaLibraryLabel"><i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria Immagini</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div id="MediaLibrary">
                        <media-library />
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <h6>Location</h6>
          <select class="form-control" name="location_id">
          @if (isset($locations))
            @foreach ($locations as $key => $location)
              <option value="{{ $location->id }}">{{ $location->name }}</option>
            @endforeach
          @endif
          </select>
        </div>
        <div class="form-group">
          <h6>Data Evento</h6>
          <input type="text" name="date" id="date" value="{{ old('date') }}" class="form-control date-picker">
        </div>
        <div class="form-group">
          <h6>Ora Inizio Evento</h6>
          <input type="text" name="time" id="time" Value="{{ old('time') }}" class="form-control">
        </div>
        <div class="form-group">
          <h6>Film Collegato</h6>
          <select id="films" class="form-control" name="film">
            <option value="" selected></option>
            @if (isset($films))
              @foreach ($films as $key => $film)
                <option value="{{ $film->id }}">{{ $film->title }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="form-group">
          <h6>Partecipanti limitati</h6>
          <label class="custom-control custom-checkbox">
            <input id="limitGuest" name="limitGuest" type="checkbox" class="custom-control-input">
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">Abilita partecipazioni limitate</span>
          </label>
        </div>
        <div class="pt-2">
          <button type="submit" class="btn btn-primary btn-block">
            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salva
          </button>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote.css') }}">
  <script src="{{ asset('plugins/summernote/summernote.js') }}"></script>
  <script src="/js/media-library.js"></script>
  <script type="text/javascript">
    $('#editor').summernote({
        height: 300,
        focus: true,
        lang: 'it-IT',
        toolbar: [
          ['font', ['style']],
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['tbl', ['table', 'hr']],
          ['media', ['link', 'picture', 'video']],
          ['action', ['undo', 'redo']],
          ['code', ['codeview']],
          ['pro', ['help']],
        ],
        callbacks: {
          onImageUpload: function(files) {
              for (var i = 0; i < files.length; i++) {
                  var file = files[i];
                  console.log(file);
                  data = new FormData();
                  data.append('media', file);
                  $.ajax({
                    type: 'post',
                    url:  '{{ route('media.api.upload') }}',
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: data,
                    success: function(response) {
                      $('#editor').summernote("insertImage", response.path);
                    }
                  });
              }
          }
        }
    });

    $('#films').select2();

    $('.date-picker').datepicker({
      autoclose: true
    });
    $('#film-titles').select2({});

    $('#title').keyup(function(event) {
      var stt = $(this).val().toLowerCase().replace(/\s/g,'-');
      $('#slug').val(stt);
      var root = '{{ url('/') }}/evento/';
      $('#full-slug').val(root+stt);
    });

    $('#slug').keyup(function(e) {
      var root = '{{ url('/') }}/evento/';
      var stt = $(this).val().toLowerCase().replace(/\s/g,'-');
      $('#full-slug').val(root+stt);
    });

    $('#festival').on('change', function(e) {
      if ($(this).prop('checked') === true) {
        $('#festival-id').removeClass('d-none');
        var festival_id = $('#festival-id select').val();
        if (festival_id > 0) {
          $('#festival-cat').removeClass('d-none');
        }
      } else {
        $('#festival-id').addClass('d-none');
        $('#festival-cat').addClass('d-none');
      }
    });

    $('#festival-id select').on('change', function(e) {
      var festival_id = $(this).val();
      if (festival_id > 0) {
        $.ajax({
          type: 'get',
          url:  '/admin/festival-cat/'+festival_id,
          headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
          success: function(data) {
            console.log(data);
            $('#festival-cat select option').each(function(){
              $(this).remove();
            });
            $.each(data, function(){
              var option = '';
              option += '<option value="'+this.id+'">'+this.name+'</option>'
              $('#festival-cat select').append(option);
            });
            $('#festival-cat select').prepend('<option value="0"></option>');
            $('#festival-cat').removeClass('d-none');
          }
        });
      } else {
        $('#festival-cat').addClass('d-none');
        var empty = '<option value="0"></option>';
        $('#festival-cat select option').each(function(){
          $(this).remove();
        });
        $('#festival-cat select').append(empty);
      }
    });

    $('#limitGuest').on('change', function(e) {
      if ($(this).prop('checked') === true) {
        $('#guestForm').removeClass('d-none');
      } else {
        $('#guestForm').addClass('d-none');
      }
    });

    document.getElementById('clip-copy').addEventListener("click", function(e) {
        e.preventDefault();
        copyToClipboard(document.getElementById('full-slug'));
    });

    function copyToClipboard(elem) {
    	  // create hidden text element, if it doesn't already exist
        var targetId = '_hiddenCopyText_';
        var isInput = elem.tagName === 'INPUT' || elem.tagName === 'TEXTAREA';
        var origSelectionStart, origSelectionEnd;
        var button = document.getElementById('clip-copy');
        if (isInput) {
            // can just use the original source element for the selection and copy
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            // must use a temporary form element for the selection and copy
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement('textarea');
                target.style.position = 'absolute';
                target.style.left = '-9999px';
                target.style.top = '0';
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        // select the content
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);

        // copy the selection
        var succeed;
        try {
        	  succeed = document.execCommand('copy');
            button.classList.remove('btn-info');
            button.classList.add('btn-success');
            button.innerHTML = '<i class="fa fa-check" aria-hidden="true"></i> Copiato';
        } catch(e) {
            succeed = false;
        }
        // restore original focus
        if (currentFocus && typeof currentFocus.focus === 'function') {
            currentFocus.focus();
        }

        if (isInput) {
            // restore prior selection
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            // clear temporary content
            target.textContent = '';
        }
        return succeed;
    }
  </script>
@endsection
