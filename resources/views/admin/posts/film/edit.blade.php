@extends('layouts.admin')
@section('title', 'Nuovo Film')
@section('page-title', 'Nuovo Film')
@section('content')
<form class="" action="{{ url('/admin/posts') }}/film/{{ $film->id }}" enctype="multipart/form-data" method="post">
  {{ csrf_field() }}
  {{ method_field('PATCH') }}
  <div class="row">
    <div class="col-md-8">
      <div class="container-fluid bg-faded p-4">
        <div class="form-group">
          <h6>Titolo</h6>
          <input id="title" type="text" name="title" class="form-control" value="{{ $film->title }}">
        </div>
        <div class="form-group">
          <h6>Slug</h6>
          <input id="slug" type="text" name="slug" class="form-control" value="{{ $film->slug }}">
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <h6>Regista</h6>
              <select id="directors" class="form-control" name="directors[]" multiple="multiple">
                @if (isset($directors))
                  @foreach ($directors as $key => $director)
                    <option value="{{ $director->name }}" {{ $film->hasDirector($director->name) == 1 ? 'selected' : '' }}>{{ $director->name }}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <h6>Attori</h6>
              <select id="actors" class="form-control" name="actors[]" multiple="multiple">
                @if (isset($actors))
                  @foreach ($actors as $key => $actor)
                    <option value="{{ $actor->name }}" {{ $film->hasActor($actor->name) == 1 ? 'selected' : '' }}>{{ $actor->name }}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <h6>Sceneggiatori</h6>
              <select id="screenwriters" class="form-control" name="screenwriters[]" multiple="multiple">
                @if (isset($screenwriters))
                  @foreach ($screenwriters as $key => $screenwriter)
                    <option value="{{ $screenwriter->name }}" {{ $film->hasScreenwriter($screenwriter->name) == 1 ? 'selected' : '' }}>{{ $screenwriter->name }}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <h6>Fotografia</h6>
              <select id="photographers" class="form-control" name="photographers[]" multiple="multiple">
                @if (isset($photographers))
                  @foreach ($photographers as $key => $photographer)
                    <option value="{{ $photographer->name }}" {{ $film->hasPhotographer($photographer->name) == 1 ? 'selected' : '' }}>{{ $photographer->name }}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <h6>Montaggio</h6>
              <select id="editors" class="form-control" name="editors[]" multiple="multiple">
                @if (isset($editors))
                  @foreach ($editors as $key => $editor)
                    <option value="{{ $editor->name }}" {{ $film->hasEditor($editor->name) == 1 ? 'selected' : '' }}>{{ $editor->name }}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <h6>Produttori</h6>
              <select id="producers" class="form-control" name="producers[]" multiple="multiple">
                @if (isset($producers))
                  @foreach ($producers as $key => $producer)
                    <option value="{{ $producer->name }}" {{ $film->hasProducer($producer->name) == 1 ? 'selected' : '' }}>{{ $producer->name }}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <h6>Distributori</h6>
              <select id="distributors" class="form-control" name="distributors[]" multiple="multiple">
                @if (isset($distributors))
                  @foreach ($distributors as $key => $distributor)
                    {{-- <option value="">{{ $distributor->name }}</option> --}}
                    <option value="{{ $distributor->name }}" {{ $film->hasDistributor($distributor->name) == 1 ? 'selected' : '' }}>{{ $distributor->name }}</option>
                  @endforeach
                @endif
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <h6>Anno di produzione</h6>
              <input type="text" name="production_date" class="form-control" value="{{ $film->production_date }}">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <h6>Durata</h6>
              <input type="text" name="duration" class="form-control" value="{{ $film->duration }}">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <h6>Paese</h6>
              <input type="text" name="region" class="form-control" value="{{ $film->region }}">
            </div>
          </div>
        </div>
        <div class="form-group">
          <h6>Descrizione</h6>
          <textarea name="description" id="editor" rows="8" class="form-control">{{ $film->description }}</textarea>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h3>Dettagli</h3>
        <hr>
        <div class="form-group">
          <h6>Stato</h6>
          <select class="form-control" name="status_id">
            @if (isset($statuses))
              @foreach ($statuses as $key => $status)
                <option value="{{ $status->id }}" {{ $status->id == $film->status_id ? 'selected' : '' }}>{{ $status->status }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="form-group">
          <h6>Festival</h6>
          <label class="custom-control custom-checkbox">
            <input id="festival" name="festival" type="checkbox" class="custom-control-input" {{ $film->festival == 1 ? 'checked' : '' }}>
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">Appartiene ad un festival?</span>
          </label>
        </div>
        <div id="festival-id" class="form-group {{ $film->festival == 1 ? '' : 'd-none' }}">
          <h6>Festival</h6>
          <select class="form-control" name="festival_id">
            <option value="0"></option>
            @isset($festivals)
              @foreach ($festivals as $key => $festival)
                <option value="{{ $festival->id }}" {{ $festival->id == $film->festival_id ? 'selected' : '' }}>{{ $festival->title }}</option>
              @endforeach
            @endisset
          </select>
        </div>
        <div id="festival-cat" class="form-group {{ $film->festival == 1 ? '' : 'd-none' }}">
          <h6>Categoria</h6>
          <select class="form-control" name="festival_cat_id">
            <option value="0"></option>
            @isset($fest_cats)
              @foreach ($fest_cats as $key => $cat)
                <option value="{{ $cat->id }}" {{ $film->fest_cat_id == $cat->id ? 'selected' : '' }}>{{ $cat->name }}</option>
              @endforeach
            @endisset
          </select>
        </div>
        <div class="form-group">
          <h6>Bambini</h6>
          <select class="form-control" name="kids">
            <option value="0" {{ $film->kids == 0 ? 'selected' : '' }}>Generico</option>
            <option value="1" {{ $film->kids == 1 ? 'selected' : '' }}>Per Bambini</option>
          </select>
        </div>
        <div class="form-group">
          <h6>Immagine Principale</h6>
          <div class="row">
            <div class="col-md-6">
              <input type="file" name="media" value="{{ old('media') }}" class="form-control-file" aria-describedby="fileHelp">
              <input type="hidden" name="media-library" value="">
            </div>
            <div class="col-md-6">
              <!-- Button trigger modal -->
              <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#mediaLibrary">
                <i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria
              </button>

              <!-- Modal -->
              <div class="modal fade" id="mediaLibrary" tabindex="-1" role="dialog" aria-labelledby="mediaLibraryLabel" aria-hidden="true">
                <div id="media-wrapper" class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="mediaLibraryLabel"><i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria Immagini</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div id="MediaLibrary">
                        <media-library />
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <h6>Location</h6>
          <select id="locations" class="form-control" name="location_id">
            @if (isset($locations))
              @foreach ($locations as $key => $location)
                <option value="{{ $location->id }}" {{ $location->id == $film->location_id ? 'selected' : '' }}>{{ $location->name }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="form-group">
          <h6>Sala</h6>
          <select id="places" class="form-control" name="place_id">
          </select>
        </div>
        <div class="form-group">
          <h6>Caratteristiche</h6>
          <select id="film_categories" class="form-control" name="film_categories[]" multiple="multiple">
            @if (isset($film_categories))
              @foreach ($film_categories as $key => $film_category)
                <option value="{{ $film_category->name }}" {{ $film->hasCategory($film_category->name) == 1 ? 'selected' : '' }}>{{ $film_category->name }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="form-group">
          <h6>Data d'inizio programmazione</h6>
          <input type="text" name="start_date" class="form-control date-picker" value="{{ $film->start_date }}">
        </div>
        <div class="form-group">
          <h6>Ultimo giorno di programmazione</h6>
          <input type="text" name="end_date" class="form-control date-picker" value="{{ $film->end_date }}">
        </div>
        <div class="pt-2">
          <button type="submit" class="btn btn-primary btn-block">
            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salva
          </button>
        </div>
      </div>
    </div>
  </div>
</form>

  {{-- PROGRAMMAZIONE --}}
  <div class="row pt-5">
    <div class="col pb-3">
      <h2>Programmazione</h2>
    </div>
  </div>
  <div class="row add">
    <div class="col-md-8">
      <div class="container-fluid bg-faded p-4">
        <table class="table table-hover" id="shows">
          <thead>
            <th>Giorno</th>
            <th>Ora</th>
            <th>Tools</th>
          </thead>
          <tbody>
            @if ($shows->count() > 0)
              @foreach ($shows as $key => $show)
                <tr id="show-{{ $show->id }}" class="show">
                  <td>{{ $show->day }}</td>
                  <td>{{ $show->time }}</td>
                  <td>
                    <a onclick="destroy({{ $show->id }})" class="btn btn-danger text-white">
                      <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </a>
                  </td>
                </tr>
              @endforeach
            @else
              <tr id="no-shows">
                <td colspan="3">
                  <span class="alert alert-danger d-block">
                    <b>Attenzione: </b>
                    Ancora nessuna programmazione per questo film!
                  </span>
                </td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h3>Aggiungi Spettacolo</h3>
        <hr>
        <div class="form-group">
          {{ csrf_field() }}
          <input type="text" name="day" id="day" class="form-control" placeholder="Giorno" required>
          <p class="error text-center alert alert-danger invisible"></p>
          <input type="text" name="time" id="time" class="form-control" placeholder="Ora" required>
          <p class="error text-center alert alert-danger invisible"></p>
          <button class="btn btn-primary btn-block" id="add">
            <i class="fa fa-plus" aria-hidden="true"></i> Aggiungi
          </button>
        </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote.css') }}">
  <script src="{{ asset('plugins/summernote/summernote.js') }}"></script>
  <script src="/js/media-library.js"></script>
  <script type="text/javascript">
    $('#editor').summernote({
        height: 300,
        focus: true,
        lang: 'it-IT',
        toolbar: [
          ['font', ['style']],
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['tbl', ['table', 'hr']],
          ['media', ['link', 'picture', 'video']],
          ['action', ['undo', 'redo']],
          ['code', ['codeview']],
          ['pro', ['help']],
        ],
        callbacks: {
          onImageUpload: function(files) {
              for (var i = 0; i < files.length; i++) {
                  var file = files[i];
                  console.log(file);
                  data = new FormData();
                  data.append('media', file);
                  $.ajax({
                    type: 'post',
                    url:  '{{ route('media.api.upload') }}',
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: data,
                    success: function(response) {
                      $('#editor').summernote("insertImage", response.path);
                    }
                  });
              }
          }
        }
    });
    $('.date-picker').datepicker();
    $('#day').datepicker();

    var select_settings = {
      tags: true,
      tokenSeparators: [',']
    };

    $('#film_categories').select2();
    $('#actors').select2(select_settings);
    $('#directors').select2(select_settings);
    $('#screenwriters').select2(select_settings);
    $('#photographers').select2(select_settings);
    $('#editors').select2(select_settings);
    $('#producers').select2(select_settings);
    $('#distributors').select2(select_settings);

    $("#title").keyup(function(event) {
      var stt = $(this).val().toLowerCase().replace(/\s/g,'-');
      $("#slug").val(stt);
    });

    $('#festival').on('change', function(e) {
      if ($(this).prop('checked') === true) {
        $('#festival-id').removeClass('d-none');
        var festival_id = $('#festival-id select').val();
        if (festival_id > 0) {
          $('#festival-cat').removeClass('d-none');
        }
      } else {
        $('#festival-id').addClass('d-none');
        $('#festival-cat').addClass('d-none');
      }
    });

    $('#festival-id select').on('change', function(e) {
      var festival_id = $(this).val();
      if (festival_id > 0) {
        $.ajax({
          type: 'get',
          url:  '/admin/festival-cat/'+festival_id,
          headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
          success: function(data) {
            console.log(data);
            $('#festival-cat select option').each(function(){
              $(this).remove();
            });
            $.each(data, function(){
              var option = '';
              option += '<option value="'+this.id+'">'+this.name+'</option>'
              $('#festival-cat select').append(option);
            });
            $('#festival-cat select').prepend('<option value="0"></option>');
            $('#festival-cat').removeClass('d-none');
          }
        });
      } else {
        $('#festival-cat').addClass('d-none');
        var empty = '<option value="0"></option>';
        $('#festival-cat select option').each(function(){
          $(this).remove();
        });
        $('#festival-cat select').append(empty);
      }
    });

    $('#add').on('click', function(event) {
      event.preventDefault();
      $.ajax({
        type: 'post',
        url:  '{{ route('store.film.show') }}',
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
        data: {
          '_token':       $('input[name=_token]').val(),
          'film_id':      '{{ $film->id }}',
          'day':          $('input[name=day]').val(),
          'time':         $('input[name=time]').val(),
        },
        success: function(data) {
          console.log(data);
          $('#no-shows').remove();
          $('#shows').append('<tr id="show-'+data.id+'" class="show"><td>'+data.day+'</td><td>'+data.time+'</td><td><a onclick="destroy('+data.id+')" class="btn btn-danger text-white"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td></tr>');
        }
      });
    });

    function destroy(id) {
      $.ajax({
        type: 'delete',
        url:  '/admin/scheduling/'+id,
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
        data: {
          '_token':  $('input[name=_token]').val(),
          'id':      id,
        },
        success: function(data) {
          console.log(data);
          $('#show-'+data.id).remove();
          var count = countShow();
          if (count == 0) {
            $('#shows').append('<tr id="no-shows"><td colspan="3"><span class="alert alert-danger d-block"><b>Attenzione: </b>Ancora nessuna programmazione per questo film!</span></td></tr>');
          }
        }
      });
    }

    function countShow() {
      var count = 0;
      $('.show').each(function() {
        count = count + 1;
      });
      return count;
    }

    $(document).ready(function(){
      var location = $("option:selected", '#locations').val();
      placesLoad(location, 1);
    });

    $('#locations').on('change', function() {
      var location = $("option:selected",this).val();
      placesLoad(location, 0);
    });

    function placesLoad(location, init)
    {
      $.ajax({
        type: 'post',
        url:  '{{ route('places.load') }}',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          'location_id' : location
        },
        success: function (response) {
          console.log(response);
          var l = Object.keys(response).length;
          var data = '';
          for (var i = 0; i < l; i++) {
            if (init == 1 && (response[i].id == {{ $film->place_id == null ? 0 : $film->place_id }})) {
              data += '<option value="'+response[i].id+'" selected>'+response[i].name+'</option>';
            } else {
              data += '<option value="'+response[i].id+'">'+response[i].name+'</option>';
            }
          }
          $('#places').html(data);
        }
      });
    }
  </script>
@endsection
