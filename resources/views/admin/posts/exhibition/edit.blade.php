@extends('layouts.admin')
@section('title', 'Modifica Rassegna')
@section('page-title', 'Modifica Rassegna')
@section('content')
<form class="" action="{{ url('/admin/posts') }}/exhibition/{{ $exhibition->id }}" enctype="multipart/form-data" method="post">
  {{ csrf_field() }}
  {{ method_field('PUT') }}
  <div class="row">
    <div class="col-md-8">
      <div class="container-fluid bg-faded p-4">
        <div class="form-group">
          <h6>Titolo</h6>
          <input id="title" type="text" name="title" class="form-control" value="{{ $exhibition->title }}">
        </div>
        <div class="form-group">
          <h6>Slug</h6>
          <input id="slug" type="text" name="slug" class="form-control" value="{{ $exhibition->slug }}">
        </div>
        <div class="form-group">
          <h6>Descrizione</h6>
          <textarea name="description" id="editor" rows="8" class="form-control">{{ $exhibition->description }}</textarea>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h3>Dettagli</h3>
        <hr>
        <div class="form-group">
          <h6>Stato</h6>
          <select class="form-control" name="status_id">
            @if (isset($statuses))
              @foreach ($statuses as $key => $status)
                <option value="{{ $status->id }}" {{ $exhibition->status_id == $status->id ? 'selected' : '' }}>{{ $status->status }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="form-group">
          <h6>Festival</h6>
          <label class="custom-control custom-checkbox">
            <input id="festival" name="festival" type="checkbox" class="custom-control-input" {{ $exhibition->festival == 1 ? 'checked' : '' }}>
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">Appartiene ad un festival?</span>
          </label>
        </div>
        <div id="festival-id" class="form-group {{ $exhibition->festival == 1 ? '' : 'd-none' }}">
          <h6>Festival</h6>
          <select class="form-control" name="festival_id">
            <option value="0"></option>
            @isset($festivals)
              @foreach ($festivals as $key => $festival)
                <option value="{{ $festival->id }}" {{ $festival->id == $exhibition->festival_id ? 'selected' : '' }}>{{ $festival->title }}</option>
              @endforeach
            @endisset
          </select>
        </div>
        <div id="festival-cat" class="form-group {{ $exhibition->festival == 1 ? '' : 'd-none' }}">
          <h6>Categoria</h6>
          <select class="form-control" name="festival_cat_id">
            <option value="0"></option>
            @isset($fest_cats)
              @foreach ($fest_cats as $key => $cat)
                <option value="{{ $cat->id }}" {{ $exhibition->fest_cat_id == $cat->id ? 'selected' : '' }}>{{ $cat->name }}</option>
              @endforeach
            @endisset
          </select>
        </div>
        <div class="form-group">
          <h6>Bambini</h6>
          <select class="form-control" name="kids">
            <option value="0" {{ $exhibition->kids == 0 ? 'selected' : '' }}>Generico</option>
            <option value="1" {{ $exhibition->kids == 1 ? 'selected' : '' }}>Per Bambini</option>
          </select>
        </div>
        <div class="form-group">
          <h6>Immagine Principale</h6>
          <div class="row">
            <div class="col-md-6">
              <input type="file" name="media" value="{{ old('media') }}" class="form-control-file" aria-describedby="fileHelp">
              <input type="hidden" name="media-library" value="">
            </div>
            <div class="col-md-6">
              <!-- Button trigger modal -->
              <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#mediaLibrary">
                <i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria
              </button>

              <!-- Modal -->
              <div class="modal fade" id="mediaLibrary" tabindex="-1" role="dialog" aria-labelledby="mediaLibraryLabel" aria-hidden="true">
                <div id="media-wrapper" class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="mediaLibraryLabel"><i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria Immagini</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div id="MediaLibrary">
                        <media-library />
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <h6>Location</h6>
          <select class="form-control" name="location_id">
          @if (isset($locations))
            @foreach ($locations as $key => $location)
              <option value="{{ $location->id }}" {{ $exhibition->location_id == $location->id ? 'selected' : '' }}>{{ $location->name }}</option>
            @endforeach
          @endif
          </select>
        </div>
        <div class="form-group">
          <h6>Data d'inizio programmazione</h6>
          <input type="text" name="start_date" class="form-control date-picker" value="{{ $exhibition->start_date }}">
        </div>
        <div class="form-group">
          <h6>Ultimo giorno di programmazione</h6>
          <input type="text" name="end_date" class="form-control date-picker" value="{{ $exhibition->end_date }}">
        </div>
        <div class="pt-2">
          <button type="submit" class="btn btn-primary btn-block">
            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salva
          </button>
        </div>
      </div>
    </div>
  </div>
</form>

  {{-- FILM IN RASSEGNA --}}
    <div class="row pt-5">
      <div class="col pb-3">
        <h2>Film</h2>
      </div>
    </div>
    <div class="row add">
      <div class="col-md-8">
        <div class="container bg-faded p-4">
          <table class="table table-hover" id="films">
            <thead>
              <th>Tipo</th>
              <th>Titolo</th>
              <th>Tools</th>
            </thead>
            <tbody>
              @if (count($contents) != 0)
                @foreach ($contents as $key => $content)
                  <tr id="film-{{ $content['id'] }}">
                    <td class="align-middle">{{ $content['type'] }}</td>
                    <td class="align-middle">{{ $content['title'] }}</td>
                    <td class="align-middle">
                      <button onclick="destroyFilm({{ $content['id'] }}, '{{ $content['type'] }}')" name="button" class="btn btn-danger">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                      </button>
                    </td>
                  </tr>
                @endforeach
              @else
                <tr id="no-film">
                  <td colspan="2">
                    <span class="alert alert-danger d-block">
                      <b>Attenzione: </b>
                      Ancora nessun contenuto assegnato a questa rassegna!
                    </span>
                  </td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
      <div class="col-md-4">
        <div class="container-fluid bg-faded p-4">
          <h3>Aggiungi Film</h3>
          <hr>
          <div class="form-group">
            {{ csrf_field() }}
            <h6>Film</h6>
            <select id="film-titles" class="form-control" name="film-title[]" multiple="multiple">
              @if (isset($films))
                @foreach ($films as $key => $film)
                  <option value="{{ $film->id }}">{{ $film->title }}</option>
                @endforeach
              @endif
            </select>
          </div>
          <div class="form-group">
            <h6>Eventi</h6>
            <select id="events" class="form-control" name="events[]" multiple="multiple">
              @if (isset($events))
                @foreach ($events as $key => $event)
                  <option value="{{ $event->id }}">{{ $event->title }}</option>
                @endforeach
              @endif
            </select>
          </div>
          <div class="form-group">
            <p class="error text-center alert alert-danger invisible"></p>
            <button class="btn btn-primary btn-block" id="add">
              <i class="fa fa-plus" aria-hidden="true"></i> Aggiungi
            </button>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote.css') }}">
  <script src="{{ asset('plugins/summernote/summernote.js') }}"></script>
  <script src="/js/media-library.js"></script>
  <script type="text/javascript">
    $('#editor').summernote({
        height: 300,
        focus: true,
        lang: 'it-IT',
        toolbar: [
          ['font', ['style']],
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['tbl', ['table', 'hr']],
          ['media', ['link', 'picture', 'video']],
          ['action', ['undo', 'redo']],
          ['code', ['codeview']],
          ['pro', ['help']],
        ],
        callbacks: {
          onImageUpload: function(files) {
              for (var i = 0; i < files.length; i++) {
                  var file = files[i];
                  console.log(file);
                  data = new FormData();
                  data.append('media', file);
                  $.ajax({
                    type: 'post',
                    url:  '{{ route('media.api.upload') }}',
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: data,
                    success: function(response) {
                      $('#editor').summernote("insertImage", response.path);
                    }
                  });
              }
          }
        }
    });
    $('.date-picker').datepicker();
    $('#day').datepicker();
    $('#film-titles').select2();
    $('#events').select2();

    $("#title").keyup(function(event) {
      var stt = $(this).val().toLowerCase().replace(/\s/g,'-');
      $("#slug").val(stt);
    });

    $('#festival').on('change', function(e) {
      if ($(this).prop('checked') === true) {
        $('#festival-id').removeClass('d-none');
        var festival_id = $('#festival-id select').val();
        if (festival_id > 0) {
          $('#festival-cat').removeClass('d-none');
        }
      } else {
        $('#festival-id').addClass('d-none');
        $('#festival-cat').addClass('d-none');
      }
    });

    $('#festival-id select').on('change', function(e) {
      var festival_id = $(this).val();
      if (festival_id > 0) {
        $.ajax({
          type: 'get',
          url:  '/admin/festival-cat/'+festival_id,
          headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
          success: function(data) {
            console.log(data);
            $('#festival-cat select option').each(function(){
              $(this).remove();
            });
            $.each(data, function(){
              var option = '';
              option += '<option value="'+this.id+'">'+this.name+'</option>'
              $('#festival-cat select').append(option);
            });
            $('#festival-cat select').prepend('<option value="0"></option>');
            $('#festival-cat').removeClass('d-none');
          }
        });
      } else {
        $('#festival-cat').addClass('d-none');
        var empty = '<option value="0"></option>';
        $('#festival-cat select option').each(function(){
          $(this).remove();
        });
        $('#festival-cat select').append(empty);
      }
    });

    {{-- STORE FUNCTION --}}
    $('#add').on('click', function(e) {
      e.preventDefault();

      var filmTitles = [];
      var eventTitles = [];

      $('#film-titles :selected').each(function(i, selected) {
        filmTitles[i] = $(selected).val();
      });

      $('#events :selected').each(function(i, selected) {
        eventTitles[i] = $(selected).val();
      });

      var data = {
        '_token':           $('input[name=_token]').val(),
        'exhibition_id':    '{{ $exhibition->id }}',
        'film_ids':         filmTitles,
        'event_ids':        eventTitles,
      };

      console.log('-------');
      console.log(data);
      console.log('-------');

      $.ajax({
        type: 'post',
        url:  '{{ route('store.exhibition.film') }}',
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
        data: data,
        success: function(data) {
          console.log('-------');
          console.log('Response');
          console.log(data);
          console.log('-------');
          $('#film-titles').val(null).trigger("change");
          $('#events').val(null).trigger("change");
          var dataLenght = Object.keys(data.objects).length;
          if (dataLenght > 0) {
            $('#no-film').remove();
          }
          for (var k = 0; k < dataLenght; k++) {
            var obj = '';
            obj += '<tr id="film-'+data.objects[k].id+'">';
            obj += '<td class="align-middle">'+data.objects[k].type+'</td>';
            obj += '<td class="align-middle">'+data.objects[k].title+'</td>';
            obj += '<td class="align-middle"><button onclick="destroyFilm('+data.objects[k].id+', \''+data.objects[k].type+'\')" type="button" name="button" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>';
            obj += '</tr>';
            if (data.objects[k].type == 'Film') {
              $('#films').append(obj);
            } else if (data.objects[k].type == 'Evento') {
              $('#films').prepend(obj);
            }
          }
        }
      });

    });

    {{-- DELETE FUNCTION --}}
    function destroyFilm (id, type) {
      var data = {
        '_token':         $('input[name=_token]').val(),
        'exhibition_id':  '{{ $exhibition->id }}',
        'id':             id,
        'type':           type
      };
      $.ajax({
        type: 'delete',
        url:  '{{ route('destroy.exhibition.film') }}',
        headers:  { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
        data: data,
        success: function(data) {
          console.log(data);
          if (data.id) {
            $('#film-'+data.id).remove();
            var table = 0;
            $('#films > tbody > tr').each(function(i) {
              table = i+1;
            });
            if (table === 0) {
              $('#films').append('<tr id="no-film"><td colspan="2"><span class="alert alert-danger d-block"><b>Attenzione: </b>Ancora nessun contenuto assegnato a questa rassegna!</span></td></tr>');
            }
          }
        }
      });
    }

  </script>
@endsection
