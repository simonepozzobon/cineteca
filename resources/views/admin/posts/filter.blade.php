@extends('layouts.admin')
@section('title', 'Posts')
@section('content')
  <div class="row">
    <div class="col pb-4">
      <div class="row">
        <div class="col-md-6">
          <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#post-type"><i class="fa fa-plus" aria-hidden="true"></i> Nuovo Contenuto</a>
        </div>
        <div class="col-md-6">
          <div class="d-flex justify-content-end">
              <div class="form-group d-inline-block m-2">
                <input id="search-query" type="text" name="search_query" value="" class="form-control" placeholder="Cerca...">
              </div>
              <div class="form-group d-inline-block m-2">
                <select id="filter-type" class="form-control" name="post_type">
                  <option value="">Selezione il modello</option>
                  <option value="all" data-post-type="all">Tutti</option>
                  @foreach ($post_types as $key => $post_type)
                    <option value="{{ $post_type->id }}" data-post-type="{{ $post_type->name }}" {{ $post_type->name == $filter ? 'selected' : '' }}>
                      @if ($post_type->name == 'film')
                        Film
                      @elseif ($post_type->name == 'exhibition')
                        Rassegna
                      @elseif ($post_type->name == 'exhibit')
                        Esposizione
                      @elseif ($post_type->name == 'festival')
                        Festival
                      @elseif ($post_type->name == 'cinestore')
                        Cinestore
                      @elseif ($post_type->name == 'news')
                        News
                      @elseif ($post_type->name == 'event')
                        Evento
                      @elseif ($post_type->name == 'release')
                        Rassegna Stampa
                      @endif
                    </option>
                  @endforeach
                </select>
              </div>
              <a class="btn btn-info d-inline-block m-2 text-white" id="post-filter"><i class="fa fa-filter" aria-hidden="true"></i> Filtra</a>
          </div>
        </div>
      </div>
      <div class="modal fade m-5" id="post-type" tabindex="-1" role="dialog" aria-labelledby="post-typeLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="post-typeLabel">Tipo di Contenuto</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body p-5">
              <div class="form-control border-0">
                <select id="post-types" class="form-control" name="post_type">
                  <option value="">Selezione il modello</option>
                  @foreach ($post_types as $key => $post_type)
                    <option value="{{ $post_type->id }}" data-post-type="{{ $post_type->name }}">
                      @if ($post_type->name == 'film')
                        Film
                      @elseif ($post_type->name == 'exhibition')
                        Rassegna
                      @elseif ($post_type->name == 'exhibit')
                        Esposizione
                      @elseif ($post_type->name == 'festival')
                        Festival
                      @elseif ($post_type->name == 'cinestore')
                        Cinestore
                      @elseif ($post_type->name == 'news')
                        News
                      @elseif ($post_type->name == 'event')
                        Evento
                      @endif
                    </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="modal-footer">
              <div class="btn-group">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Annulla</button>
                <a class="btn btn-small btn-primary text-white" id="post-create" ><i class="fa fa-plus" aria-hidden="true"></i> Crea</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <div id="loader" class="loader-container d-none">
        <div class="loader"></div>
      </div>
      <table id="posts-list" class="table table-hover w-100">
        <thead>
          <th>Titolo</th>
          <th>Immagine</th>
          <th>Tipo di Post</th>
          <th>Post Status</th>
          <th>Tools</th>
        </thead>
        <tbody>
          @foreach ($items as $key => $item)
            <tr class="post">
              <td class="align-middle">{{ $item->title }}</td>
              <td class="align-middle">
                <img src="{{ Storage::disk('local')->url($item->media->first()->thumb) }}" width="57">
              </td>
              <td class="align-middle">{{ $item['type'] }}</td>
              <td class="align-middle">
                @if ($item['status_id'] == 1)
                  <i class="fa fa-check-circle-o text-warning" aria-hidden="true"></i>
                @elseif ($item['status_id'] == 2)
                  <i class="fa fa-check-circle text-success" aria-hidden="true"></i>
                @elseif ($item['status_id'] == 3)
                  <i class="fa fa-check-circle text-primary" aria-hidden="true"></i>
                @endif
              </td>
              <td class="align-middle">
                <div class="btn-group">
                  <a class="btn btn-info" href="{{ url($item->url) }}/{{ $item->id }}/edit">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                  </a>
                  <a href="{{ url($item['public_url']) }}" target="_blank" class="btn btn-warning">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </a>
                  <a class="btn btn-danger text-white" data-toggle="modal" data-target="#deleteModal-{{ $item['type'] }}-{{ $item['id'] }}">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                  </a>
                </div>
                @isset($item['limit_guest'])
                  @if ($item['limit_guest'] == 1)
                    <a href="{{ route('rsvp.index', $item['id']) }}" class="btn btn-primary">
                      <i class="fa fa-users" aria-hidden="true"></i>
                    </a>
                  @endif
                @endisset
                <div class="modal fade" id="deleteModal-{{ $item['type'] }}-{{ $item['id'] }}" tabindex="-1" role="dialog" aria-labelledby="deleteModal{{ $item['type'] }}-{{ $item['id'] }}Label" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="deleteModal{{ $item['type'] }}-{{ $item['id'] }}Label">Elimina Post</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body p-5">
                        <h4 class="text-center">Sei sicuro di voler eliminare il post?</h4>
                        <p class="text-center">
                          Questa operazione non potrà essere annullata!
                        </p>
                      </div>
                      <div class="modal-footer">
                        <form class="" action="{{ url($item->url) }}/{{ $item->id }}" method="post">
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}
                          <div class="btn-group">
                            <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Annulla</button>
                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Elimina</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div id="pagination" class="row">
    <div class="col d-flex justify-content-around">
        {{ $items->links('vendor.pagination.bootstrap-4') }}
    </div>
  </div>
@endsection
@section('scripts')
  <script type="text/javascript">
    var $post_list = $('#posts-list');
    var $pagination = $('#pagination');
    var $loader = $('#loader');

    $('select#post-types').on('change', function(e) {
        var post_type = $("option:selected",this).data("post-type");
        var href = $('#post-create').attr('href', '{{ route('posts.index') }}/'+post_type+'/create');
    });
    $('select#filter-type').on('change', function(e) {
        var post_type = $("option:selected",this).data("post-type");
        var href = $('#post-filter').attr('href', '{{ route('posts.index') }}/filter/'+post_type);
    });

    $('#search-query').keyup(function(e) {
      var query = $(this).val();
      if (query) {
        $post_list.find('.post').addClass('d-none').addClass('not-visible');
        $pagination.addClass('d-none').addClass('not-visible');
        var currentRequest = null;
        currentRequest = $.ajax({
          type: 'get',
          url: '/admin/search/{{ $filter }}/'+query,
          headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
          beforeSend: function() {
            if(currentRequest != null) {
                currentRequest.abort();
            }
            $loader.removeClass('d-none');
          },
          success: function(response) {
            console.log(response);

            // Rimuovo i vecchi risultati
            $('.result').remove();

            // Aggiungo i nuovi
            $.each(response, function(){
              var data = '';
              data += '<tr class="result">';
              data +=   '<td class="align-middle">'+this.title+'</td>';
              data +=   '<td class="align-middle"><img src="'+this.img+'" class="img-fluid" width="57"></td>';
              data +=   '<td class="align-middle">'+this.type+'</td>';

              switch (this.status_id) {
                case 1:
                  data += '<td class="align-middle"><i class="fa fa-check-circle-o text-warning" aria-hidden="true"></i></td>'
                  break;
                case 2:
                  data += '<td class="align-middle"><i class="fa fa-check-circle text-success" aria-hidden="true"></i></td>'
                  break;
                case 3:
                  data += '<td class="align-middle"><i class="fa fa-check-circle text-primary" aria-hidden="true"></i></td>'
                  break;
              }

              data +=   '<td class="align-middle">';
              data +=     '<div class="btn-group">';
              data +=       '<a class="btn btn-info" href="'+this.url+'/'+this.id+'/edit">';
              data +=         '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
              data +=       '</a>';
              data +=       '<a href="'+this.public_url+'" target="_blank" class="btn btn-warning">';
              data +=         '<i class="fa fa-eye" aria-hidden="true"></i>';
              data +=       '</a>';
              data +=       '<a class="btn btn-danger text-white" data-toggle="modal" data-target="#deleteModal-'+this.type+'-'+this.id+'">';
              data +=         '<i class="fa fa-trash-o" aria-hidden="true"></i>';
              data +=       '</a>';
              data +=     '</div>';

              if (this.limit_guest && this.limit_guest == 1) {
                data +=       '<a href="'+this.rsvp_url+'" class="btn btn-primary">';
                data +=         '<i class="fa fa-users" aria-hidden="true"></i>';
                data +=       '</a>';
              }

              data +=     '<div class="modal fade" id="deleteModal-'+this.type+'-'+this.id+'" tabindex="-1" role="dialog" aria-labelledby="deleteModal'+this.type+'-'+this.id+'Label" aria-hidden="true">';
              data +=       '<div class="modal-dialog" role="document">';
              data +=         '<div class="modal-content">';
              data +=           '<div class="modal-header">';
              data +=             '<h5 class="modal-title" id="deleteModal'+this.type+'-'+this.id+'Label">Elimina Post</h5>';
              data +=             '<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
              data +=               '<span aria-hidden="true">&times;</span>';
              data +=             '</button>';
              data +=           '</div>';
              data +=           '<div class="modal-body p-5">';
              data +=             '<h4 class="text-center">Sei sicuro di voler eliminare il post?</h4>';
              data +=             '<p class="text-center">';
              data +=               'Questa operazione non potrà essere annullata!';
              data +=             '</p>';
              data +=           '</div>';
              data +=           '<div class="modal-footer">';
              data +=             '<form class="" action="'+this.url+'/'+this.id+'" method="post">';
              data +=               '{{ csrf_field() }}';
              data +=               '{{ method_field('DELETE') }}';
              data +=               '<div class="btn-group">';
              data +=                 '<button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Annulla</button>';
              data +=                 '<button type="submit" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Elimina</button>';
              data +=               '</div>';
              data +=             '</form>';
              data +=           '</div>';
              data +=         '</div>';
              data +=      ' </div>';
              data +=     '</div>';
              data +=   '</td>';

              data += '</tr>';
              $post_list.append(data);
            });

          },
          error: function(errors){
            console.log(errors);
          },
          complete: function() {
            $loader.addClass('d-none');
          }
        });
      } else {
        $('.result').remove();
        $('.not-visible').removeClass('d-none').removeClass('not-visible');
      }

    });
  </script>
@endsection
