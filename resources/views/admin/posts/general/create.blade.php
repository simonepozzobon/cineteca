@extends('layouts.admin')
@section('title', 'Nuovo Post')
@section('page-title', '')
@section('content')
<form class="" action="{{ route('posts.store') }}" enctype="multipart/form-data" method="post">
  {{ csrf_field() }}
  {{ method_field('POST') }}
  <div class="row">
    <div class="col-md-8">
      <div class="container-fluid bg-faded p-4">
        <div class="form-group">
          <h6>Titolo</h6>
          <input id="title" type="text" name="title" class="form-control" placeholder="Titolo del Film">
        </div>
        <div class="form-group">
          <h6>Slug</h6>
          <input id="slug" type="text" name="slug" class="form-control" placeholder="url pubblico di accesso alla pagina">
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <h6>Data d'inizio programmazione</h6>
              <input type="text" name="start_date" class="form-control">
            </div>
          </div>
          <div class="col-md-6">
            <h6>Data di fine programmazione</h6>
            <input type="text" name="end_date" class="form-control">
          </div>
        </div>

        <div class="form-group">
          <h6>Contenuto</h6>
          <textarea name="content" rows="8" class="form-control"></textarea>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h3>Dettagli</h3>
        <hr>
        <div class="form-group">
          <h6>Stato</h6>
          <select class="form-control" name="status_id">
            @if (isset($statuses))
              @foreach ($statuses as $key => $status)
                <option value="{{ $status->id }}">{{ $status->status }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="form-group">
          <h6>Immagine Principale</h6>
          <input type="file" name="media" class="form-control-file" aria-describedby="fileHelp">
        </div>
        <div class="form-group">
          <h6>Autore</h6>
          <select class="form-control" name="author_id">
            @foreach ($admins as $key => $admin)
              <option value="{{ $admin->id }}">{{ $admin->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <h6>Categoria</h6>
          <select class="form-control" name="category_id">
          @if (isset($categories))
            @foreach ($categories as $key => $category)
              <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
          @endif
          </select>
        </div>
        <div class="pt-2">
          <button type="submit" class="btn btn-primary btn-block">Salva</button>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection
