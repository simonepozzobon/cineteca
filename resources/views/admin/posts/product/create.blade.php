@extends('layouts.admin')
@section('title', 'Nuovo Prodotto')
@section('page-title', '')
@section('content')
<form class="" action="{{ route('posts.store', 'cinestore') }}" enctype="multipart/form-data" method="post">
  {{ csrf_field() }}
  {{ method_field('POST') }}
  <div class="row">
    <div class="col-md-8">
      <div class="container-fluid bg-faded p-4">
        <h3>Nuovo Prodotto</h3>
        <hr>
        <div class="form-group">
          <h6>Nome</h6>
          <input id="title" type="text" name="product_name" class="form-control" placeholder="Nome del prodotto" value="{{ old('product_name') }}">
        </div>
        <div class="form-group">
          <h6>Slug</h6>
          <input id="slug" type="text" name="slug" class="form-control" placeholder="Url pubblico del singolo prodotto" value="{{ old('slug') }}">
        </div>
        <div class="form-group">
          <h6>Descrizione</h6>
          <textarea id="editor" name="description" rows="8" class="form-control">{{ old('description') }}</textarea>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h3>Dettagli</h3>
        <hr>
        <div class="form-group">
          <h6>Stato</h6>
          <select class="form-control" name="status_id">
            @if (isset($statuses))
              @foreach ($statuses as $key => $status)
                <option value="{{ $status->id }}">{{ $status->status }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="form-group">
          <h6>Immagine Principale</h6>
          <input type="file" name="media" class="form-control-file" aria-describedby="fileHelp">
        </div>
        <div class="form-group">
          <h6>Categoria Prodotto</h6>
          <select id="category" class="form-control" name="category_id">
          @if (isset($product_categories))
            @foreach ($product_categories as $key => $category)
              <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
          @endif
          </select>
        </div>
        <div id="video" class="form-group d-none variable">
          <h6>Video Principale</h6>
          <input type="text" name="video" class="form-control">
        </div>
        <div id="available" class="form-group">
          <h6>Disponibilità Pezzi</h6>
          <input type="text" name="available" value="{{ old('available') }}" class="form-control">
        </div>
        <div class="form-group">
          <h6>Acquistabile</h6>
          <label class="custom-control custom-checkbox">
            <input id="purchasable" name="purchasable" type="checkbox" class="custom-control-input">
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">Il prodotto è acquistabile?</span>
          </label>
        </div>
        <div class="form-group">
          <h6>Prezzi</h6>
          <select id="product_prices" class="form-control" name="product_prices[]"  multiple="multiple" style="width: 100%">
            @foreach ($product_prices as $key => $product_price)
              <option value="{{ $product_price->id }}">{{ $product_price->description }} - €{{ $product_price->price }}</option>
            @endforeach
          </select>
          <div class="pt-3">
            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddPrice"><i class="fa fa-plus"></i> Aggiungi prezzo</a>
            <div class="modal fade" id="modalAddPrice">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      <span class="sr-only">Chiudi</span>
                    </button>
                    <h4 class="modal-title">Aggiungi Prezzo</h4>
                  </div>
                  <div class="modal-body p-5">
                    <div class="form-group">
                      <h6>Descrizione</h6>
                      <input type="text" name="price_description" class="form-control">
                    </div>
                    <div class="form-group">
                      <h6>Prezzo</h6>
                      <input type="text" name="price_val" class="form-control">
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-undo"></i> Annulla</button>
                    <button id="addPrice" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Aggiungi Prezzo</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {{-- <div id="price" class="form-group">
          <h6>Prezzo</h6>
          <input type="text" name="price" class="form-control" value="{{ old('price') }}">
        </div> --}}
        <div class="form-group">
          <h6>Tags</h6>
          <select id="tags" class="form-control" name="tags[]" multiple="multiple" style="width: 100%;">
            @if (isset($tags))
              @foreach ($tags as $key => $tag)
                <option value="{{ $tag->name }}">{{ $tag->name }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div id="pics-collection" class="form-group d-none">
          <h6>Collezione foto</h6>
          <select id="collection-select" class="form-control" name="pics_collection[]" multiple="multiple">
            @if (isset($pic_collects))
              @foreach ($pic_collects as $key => $pic_collect)
                <option value="{{ $pic_collect->name }}">{{ $pic_collect->name }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="pt-2">
          <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-floppy-o"></i> Salva</button>
        </div>
      </div>
    </div>
  </div>
  <div id="products-image" class="row pt-5">
    <div class="col-md-8">
      <div class="container-fluid bg-faded p-4">
        <h3>Immagini Prodotto</h3>
        <hr>
        <div id="media-container" class="row">
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h3>Aggiungi Immagine Prodotto</h3>
        <hr>
        <div class="row">
          <div class="col-md-6 pb-3">
            <input id="file" type="file" name="media_file" value="{{ old('media_file') }}" class="form-control-file" aria-describedby="fileHelp">
          </div>
          <div class="col-md-6 pb-3">
            <button id="upload" onclick="uploadFile()" type="button" name="upload" class="btn btn-primary btn-block"><i class="fa fa-upload" aria-hidden="true"></i> Upload</button>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <input id="media-list-arr" type="hidden" name="media_list" value="">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#mediaLibrary">
              <i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria
            </button>

            <!-- Modal -->
            <div class="modal fade" id="mediaLibrary" tabindex="-1" role="dialog" aria-labelledby="mediaLibraryLabel" aria-hidden="true">
              <div id="media-wrapper" class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="mediaLibraryLabel"><i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria Immagini</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div id="media-grid" class="row">
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="products-group" class="row pt-5 d-none">
    <div class="col-md-8">
      <div class="container-fluid bg-faded p-4">
        <h3>Prodotti Collegati</h3>
        <hr>
        <input id="product-list-arr" type="hidden" name="product_list" value="">
        <table id="product-list" class="table table-hover">
          <thead>
            <th>ID</th>
            <th>Nome</th>
            <th>Tipo</th>
            <th></th>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h3>Aggiungi Prodotto</h3>
        <hr>
        <div class="form-group">
          <select id="product-sel" class="form-control w-100">
            @foreach ($products as $key => $product)
              <option value="{{ $product->id }}" data-product-name="{{ $product->name }}" data-type="{{ $product->category->name }}">{{ $product->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="container-fluid">
          <a id="add-product" href="#" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Aggiungi Prodotto</a>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote.css') }}">
  <script src="{{ asset('plugins/summernote/summernote.js') }}"></script>
  <script>
    var galleryIds = [];
    // prendo i dati nell'input con l'array e li separo in una array javascript
    var media_list = getMediaList();

    var str = $('#product-list-arr').val();
    var temp = str.split(',');
    for (a in temp ) {
        temp[a] = parseInt(temp[a], 10);
    }
    var product_list = str.split(',');

    $('#tags').select2({
      tags: true,
      tokenSeparators: [',']
    });

    // $('#collection-select').select2({
    //   tags: true,
    //   tokenSeparators: [',']
    // });


    /*
     *
     * Add Price
     *
     */

    $('#product_prices').select2();

    $('#addPrice').on('click', function(e) {
        e.preventDefault();

        $.ajax({
          url: '{{ route('admin.add.price') }}',
          type: 'POST',
          headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
          data: {
            'description': $('input[name="price_description"]').val(),
            'price': $('input[name="price_val"]').val()
          },
          success: function(response)
          {
            console.log(response);
            var data = '';
            data += '<option value="'+response.id+'" selected>';
            data += response.description+' - €'+response.price;
            data += '</option>';
            $('#product_prices').append(data);
            $('input[name="price_description"]').val('');
            $('input[name="price_val"]').val('');
            $('#modalAddPrice').modal('hide');
          },
          error: function(error)
          {
            console.log(error);
          }
        });



    });


    /*
     *
     * Category select
     *
    */

    $('#category').change(function(e) {
      var val = $(this).val();
      $('#available').removeClass('d-none');
      if (val == 1) {
          // Cd e DVD
          $('#video').removeClass('d-none');
          $('#pics-collection').addClass('d-none');
      }
      else if (val == 4)
      {
          // Foto
          $('#video').addClass('d-none');
          $('#pics-collection').removeClass('d-none');
          $('#collection-select').select2({
            tags: true,
            tokenSeparators: [',']
          });
      }
      else if (val == 5)
      {
          // Video
          $('#video').removeClass('d-none');
          $('#pics-collection').addClass('d-none');
      }
      else if (val == 6)
      {
          // Percorsi
          $('#product-sel').select2();
          $('#price').addClass('d-none');
          $('#pics-collection').addClass('d-none');
          $('#products-group').removeClass('d-none');
      }
      else
      {
          $('.variable').addClass('d-none');
          $('#pics-collection').addClass('d-none');
      }

    });


    $('#editor').summernote({
        height: 300,
        focus: true,
        lang: 'it-IT',
        toolbar: [
          ['font', ['style']],
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['tbl', ['table', 'hr']],
          ['media', ['link', 'picture', 'video']],
          ['action', ['undo', 'redo']],
          ['code', ['codeview']],
          ['pro', ['help']],
        ],
        callbacks: {
          onImageUpload: function(files) {
              for (var i = 0; i < files.length; i++) {
                  var file = files[i];
                  console.log(file);
                  data = new FormData();
                  data.append('media', file);
                  $.ajax({
                    type: 'post',
                    url:  '{{ route('media.api.upload') }}',
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: data,
                    success: function(response) {
                      $('#editor').summernote("insertImage", response.path);
                    }
                  });
              }
          }
        }
    });

    $("#title").keyup(function(event) {
      var stt = $(this).val().toLowerCase().replace(/\s/g,'-');
      $("#slug").val(stt);
    });

    var product_list = [];

    $('#add-product').on('click', function(e) {
      e.preventDefault();
      var $selector = $('#product-sel');
      var id = $('#product-sel').val();
      var name = $('#product-sel option:selected').data('product-name');
      var type = $('#product-sel option:selected').data('type');

      var data = '';
      data += '<tr id="product-id-'+id+'">';
      data +=   '<td>'+id+'</td>';
      data +=   '<td>'+name+'</td>';
      data +=   '<td>'+type+'</td>';
      data +=   '<td>';
      data +=     '<a href="#" onclick="return deleteProduct('+id+')" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
      data +=   '</td>';
      data += '</tr>';

      // se non è presente lo aggiunge altrimenti no
      if ($.inArray(id, product_list) == -1) {
        product_list.push(id);
        $('#product-list-arr').val(product_list);
        $('#product-list').append(data);
      }
    });

    function deleteProduct(id) {
      $('#product-id-'+id).remove();
      var index = product_list.indexOf(id);
      if (index > -1) {
        product_list.splice(index, 1);
      }
      $('#product-list-arr').val(product_list);
      return false;
    }

    $('#mediaLibrary').on('show.bs.modal', function (e) {
      var count = loadMore(0);
      $(this).scroll(function() {
         if($(this).scrollTop() > $('#media-grid').height() - 600) {
          count = loadMore(count + 30);
         }
      });
    });

    function loadMore(objs)
    {
      var data = {
        '_token' : $('meta[name="csrf-token"]').attr('content'),
        'objs' : objs,
      };

      var count = 0;

      $.ajax({
        type: 'post',
        url:  '{{ route('media.loadmore') }}',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: data,
        success: function (response) {
          console.log(response);
          objs = objs + response.newObjs;
          count = response.newObjs;
          for (var i = 0; i < response.newObjs; i++) {
            var data = '';
            data += '<div class="col-md-4 pb-4">';
            data +=   '<div class="container p-3 bg-faded">';
            data +=     '<img src="'+response.imgs[i].url+'" class="img-fluid w-100">';
            data +=     '<button type="button" onclick="selectMedia('+response.imgs[i].id+', \''+response.imgs[i].url+'\', true)" class="btn btn-info btn-block mt-3"><i class="fa fa-check" aria-hidden="true"></i> Seleziona</a>';
            data +=   '</div>';
            data += '</div>';
            $('#media-grid').append(data);
          }
        }
      });

      return objs;
    }

    function selectMedia(id, url, pop)
    {
      // prendo i dati nell'input con l'array e li separo in una array javascript
      media_list = getMediaList();

      // Creo l'oggetto da aggiungere in HTML
      var data = '';
      data += '<div id="media-'+id+'" class="col-md-3">';
      data +=   '<img src="'+url+'" class="img-fluid w-100">';
      data +=   '<button type="button" onclick="removeMedia(\''+id+'\')" class="btn btn-danger mt-3"><i class="fa fa-trash-o" aria-hidden="true"></i> Rimuovi</a></button>';
      data += '</div>';

      // Se è un pop-up lo nascondo
      if (pop == true) {
        $('#mediaLibrary').modal('hide');
      }

      // Verifico che l'immagine non sia già presente nell'array
      if ($.inArray(''+id, media_list) == -1) {
        // Se non è presente allora aggiungo l'oggetto all'HTML e il suo id all'array nel campo di testo
        media_list.push(id);
        $('input[name="media_list"]').val(media_list);
        $('#media-container').append(data);
      } else {
        // Altrimenti genero un avvertimento
        alert('immagine già inserita');
      }
    }

    function uploadFile()
    {
      var formData = new FormData();
      formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
      formData.append('media', $('#file')[0].files[0]);

      console.log(formData);
      $.ajax({
        type: 'post',
        url:  '{{ route('gallery.upload') }}',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
          console.log(response);
          selectMedia(response.id, response.url, false);
        },
        error: function (errors) {
          console.log(errors);
        }
      });
    }

    function removeMedia(id)
    {
      media_list = getMediaList();
      $('#media-'+id).remove();
      var index = media_list.indexOf(id);
      if (index > -1) {
        media_list.splice(index, 1);
      }

      $('input[name="media_list"]').val(media_list);
      return false;
    }

    function getMediaList()
    {
      var m_str = $('input[name="media_list"]').val();
      var temp = m_str.split(',');
      for (a in temp ) {
          temp[a] = parseInt(temp[a], 10);
      }

      media_list = m_str.split(',');

      return media_list;
    }
  </script>
@endsection
