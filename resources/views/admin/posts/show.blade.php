@extends('layouts.admin')
@section('title', 'Create a new post')
@section('page-title', '')
@section('content')
  <div class="row">
    <div class="col-md-8">
      <div class="container-fluid bg-faded p-4">
        <div class="row">
          <div class="col">
            <div class="form-group">
              <h3>{{ $post->title }}</h3>
            </div>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <h6>Url</h6>
              <p><a href="{{ url('/post').'/'.$post->slug }}">{{ url('/post').'/'.$post->slug }}</a></p>
            </div>
            <div class="form-group">
              <h6>Category</h6>
              <p>Post</p>
            </div>
            <div class="form-group">
              <h6>Tags</h6>
              <p>tag, altro tag.</p>
            </div>
          </div>
          <div class="col-md-6">
            @if ($post->media_url)
              <img class="w-100" src="{{ $post->media_url }}" alt="{{ $post->title }}">
            @endif
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <p>{!! $post->content !!}</p>
            </div>
          </div>
        </div>

      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h3>Post Details</h3>
        <hr>
        <div class="form-group">
          <h6>Status</h6>
          <p>{{ $post->status->status }}</p>
        </div>
        <div class="form-group">
          <h6>Author</h6>
          <p>{{ $post->author->name }}</p>
        </div>
        <div class="form-group">
          <h6>Post Created</h6>
          <p>{{ $post->created_at }}</p>
        </div>
        <div class="form-group">
          <h6>Post Updated</h6>
          <p>{{ $post->updated_at }}</p>
        </div>
        <div class="pt-2">
          <div class="row">
            <div class="col">
              <a href="#" class="btn btn-info btn-block">Edit</a>
            </div>
            <div class="col">
              <button type="button" class="btn btn-danger btn-block">Delete</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
