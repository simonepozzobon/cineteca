@extends('layouts.admin')
@section('title', 'Modifica Festival')
@section('content')
<form class="" action="{{ url('/admin/posts') }}/festival/{{ $festival->id }}" enctype="multipart/form-data" method="post">
  {{ csrf_field() }}
  {{ method_field('PATCH') }}
  <div class="row">
    <div class="col-md-8">
      <div class="container-fluid bg-faded p-4">
        <h3>Modifica Festival</h3>
        <hr>
        <div class="form-group">
          <h6>Titolo</h6>
          <input id="title" type="text" name="title" class="form-control" value="{{ $festival->title }}">
        </div>
        <div class="form-group">
          <h6>Slug</h6>
          <input id="slug" type="text" name="slug" class="form-control" value="{{ $festival->slug }}">
        </div>
        <div class="form-group">
          <h6>Descrizione</h6>
          <textarea name="description" id="editor" rows="8" class="form-control">{{ $festival->description }}</textarea>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h3>Dettagli</h3>
        <hr>
        <div class="form-group">
          <h6>Stato</h6>
          <select class="form-control" name="status_id">
            @if (isset($statuses))
              @foreach ($statuses as $key => $status)
                <option value="{{ $status->id }}" {{ $status->id == $festival->status_id ? 'selected' : '' }}>{{ $status->status }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="form-group">
          <h6>Bambini</h6>
          <select class="form-control" name="kids">
            <option value="0" {{ $festival->kids == 0 ? 'selected' : '' }}>Generico</option>
            <option value="1" {{ $festival->kids == 1 ? 'selected' : '' }}>Per Bambini</option>
          </select>
        </div>
        <div class="form-group">
          <h6>Immagine Principale</h6>
          <div class="row">
            <div class="col-md-6">
              <input type="file" name="media" value="{{ old('media') }}" class="form-control-file" aria-describedby="fileHelp">
              <input type="hidden" name="media-library" value="">
            </div>
            <div class="col-md-6">
              <!-- Button trigger modal -->
              <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#mediaLibrary">
                <i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria
              </button>

              <!-- Modal -->
              <div class="modal fade" id="mediaLibrary" tabindex="-1" role="dialog" aria-labelledby="mediaLibraryLabel" aria-hidden="true">
                <div id="media-wrapper" class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="mediaLibraryLabel"><i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria Immagini</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div id="MediaLibrary">
                        <media-library />
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <h6>Data d'inizio programmazione</h6>
          <input type="text" name="start_date" class="form-control date-picker" value="{{ $festival->start_date }}">
        </div>
        <div class="form-group">
          <h6>Ultimo giorno di programmazione</h6>
          <input type="text" name="end_date" class="form-control date-picker" value="{{ $festival->end_date }}">
        </div>
        <div class="pt-2">
          <button type="submit" class="btn btn-primary btn-block">
            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salva
          </button>
        </div>
      </div>
    </div>
  </div>
  <div class="row mt-4">
    <div class="col-md-8">
      <div class="continer-fluid bg-faded p-4">
        <h3>Eventi</h3>
        <hr>
        <table id="relations" class="table table-hover">
          <thead>
            <th>Id</th>
            <th>Titolo</th>
            <th>Tipologia</th>
            <th></th>
          </thead>
          <tbody>
            @if ($relations->count() > 0)
              @foreach ($relations as $key => $relation)
                <tr id="relation-{{ $relation['id'] }}">
                  <td>{{ $relation['id'] }}</td>
                  <td>{{ $relation['title'] }}</td>
                  <td>{{ $relation['type'] }}</td>
                  <td>
                    <button onclick="destroyRelation({{ $relation['id'] }})" type="button" name="button" class="btn btn-danger">
                      <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr>
              @endforeach
            @else
              <tr id="no-festival">
                <td colspan="3">
                  <span class="alert alert-danger d-block">
                    <b>Attenzione: </b>
                    Ancora nessun evento assegnato a questo festival!
                  </span>
                </td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h3>Aggiungi Evento</h3>
        <hr>
          <div class="form-group">
            <h6>Film</h6>
            <select id="film" class="form-control" name="film">
                <option value="" selected></option>
              @foreach ($films as $key => $film)
                <option value="{{ $film->id }}">{{ $film->title }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <h6>Rassegna</h6>
            <select id="exhibition" class="form-control" name="exhibition">
              <option value="" selected></option>
              @foreach ($exhibitions as $key => $exhibition)
                <option value="{{ $exhibition->id }}">{{ $exhibition->title }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <h6>Esposizione</h6>
            <select id="exhibit" class="form-control" name="exhibit">
              <option value="" selected></option>
              @foreach ($exhibits as $key => $exhibit)
                <option value="{{ $exhibit->id }}">{{ $exhibit->title }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <h6>Evento</h6>
            <select id="event" class="form-control" name="event">
              <option value="" selected></option>
              @foreach ($events as $key => $event)
                <option value="{{ $event->id }}">{{ $event->title }}</option>
              @endforeach
            </select>
          </div>
          <div class="pt-2">
            <button id="add" class="btn btn-primary btn-block">
              <i class="fa fa-plus" aria-hidden="true"></i> Aggiungi
            </button>
          </div>
      </div>
    </div>
  </div>
</form>
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote.css') }}">
  <script src="{{ asset('plugins/summernote/summernote.js') }}"></script>
  <script src="/js/media-library.js"></script>
  <script type="text/javascript">
    $('#editor').summernote({
        height: 300,
        focus: true,
        lang: 'it-IT',
        toolbar: [
          ['font', ['style']],
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['tbl', ['table', 'hr']],
          ['media', ['link', 'picture', 'video']],
          ['action', ['undo', 'redo']],
          ['code', ['codeview']],
          ['pro', ['help']],
        ],
        callbacks: {
          onImageUpload: function(files) {
              for (var i = 0; i < files.length; i++) {
                  var file = files[i];
                  console.log(file);
                  data = new FormData();
                  data.append('media', file);
                  $.ajax({
                    type: 'post',
                    url:  '{{ route('media.api.upload') }}',
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: data,
                    success: function(response) {
                      $('#editor').summernote("insertImage", response.path);
                    }
                  });
              }
          }
        }
    });
    $('.date-picker').datepicker();
    $('#day').datepicker();
    $('#film').select2();
    $('#exhibition').select2();
    $('#exhibit').select2();
    $('#event').select2();

    $("#title").keyup(function(event) {
      var stt = $(this).val().toLowerCase().replace(/\s/g,'-');
      $("#slug").val(stt);
    });

    {{-- STORE FUNCTION --}}
    $('#add').on('click', function(event) {
      event.preventDefault();
      var film = $('#film').val();
      var exhibit = $('#exhibit').val();
      var exhibition = $('#exhibition').val();
      var evento = $('#event').val();
      var data = {};

      if (film) {
        data = {
          '_token': $('input[name=_token]').val(),
          'festival_id': '{{ $festival->id }}',
          'type' : 'film',
          'id' :  film,
        };
      } else if (exhibit) {
        data = {
          '_token': $('input[name=_token]').val(),
          'festival_id': '{{ $festival->id }}',
          'type' : 'exhibit',
          'id' :  exhibit,
        };
      } else if (exhibition) {
        data = {
          '_token': $('input[name=_token]').val(),
          'festival_id': '{{ $festival->id }}',
          'type' : 'exhibition',
          'id' :  exhibition,
        };
      } else if (evento) {
        data = {
          '_token': $('input[name=_token]').val(),
          'festival_id': '{{ $festival->id }}',
          'type' : 'event',
          'id' :  evento,
        };
      }

      $.ajax({
        type: 'post',
        url:  '{{ route('store.festivalrelation') }}',
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
        data: data,
        success: function(data) {
          console.log(data);

          // Verifico se ci sono relazioni presenti e
          var table = 0;
          $('#relations > tbody > tr').each(function(i) {
            table = i+1;
          });
          if (table > 0) {
            $('#no-festival').remove();
          }

          $('#film').val(null).trigger("change");
          $('#exhibit').val(null).trigger("change");
          $('#exhibition').val(null).trigger("change");
          $('#event').val(null).trigger("change");
          $('#relations').append('<tr id="relation-'+data.id+'"><td class="align-middle">'+data.id+'</td><td class="align-middle">'+data.title+'</td><td class="align-middle">'+data.type+'</td><td class="align-middle"><button onclick="destroyRelation('+data.id+')" type="button" name="button" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td></tr>')
        }
      });

    });

    {{-- DELETE FUNCTION --}}
    function destroyRelation (id) {
      $.ajax({
        type: 'post',
        url:  '{{ route('destroy.festivalrelation') }}',
        headers:  { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
        data: {
          '_token':         $('input[name=_token]').val(),
          'relation_id':    id,
        },
        success: function(data) {
          console.log(data);
          if (data.relation_id) {
            $('#relation-'+data.relation_id).remove();
            var table = 0;
            $('#relations > tbody > tr').each(function(i) {
              table = i+1;
            });
            if (table === 0) {
              $('#relations').append('<tr id="no-festival"><td colspan="3"><span class="alert alert-danger d-block"><b>Attenzione: </b>Ancora nessun evento assegnato a questo festival!</span></td></tr>');
            }
          }
        }
      });
    }

  </script>
@endsection
