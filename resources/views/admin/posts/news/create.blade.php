@extends('layouts.admin')
@section('title', 'Create a new post')
@section('page-title', '')
@section('content')
<form class="" action="{{ route('posts.store', 'news') }}" enctype="multipart/form-data" method="post">
  {{ csrf_field() }}
  {{ method_field('POST') }}
  <div class="row">
    <div class="col-md-8">
      <div class="container-fluid bg-faded p-4">
        <div class="form-group">
          <h6>Titolo</h6>
          <input id="title" type="text" name="title" class="form-control" value="{{ old('title') }}" placeholder="titolo della news">
        </div>
        <div class="form-group">
          <h6>Slug</h6>
          <input id="slug" type="text" name="slug" class="form-control" value="{{ old('slug') }}" placeholder="url della news">
        </div>
        <div class="form-group">
          <h6>Contenuto</h6>
          <textarea id="editor" name="content" rows="8" class="form-control">{{ old('content') }}</textarea>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h3>Dettagli</h3>
        <hr>
        <div class="form-group">
          <h6>Stato</h6>
          <select class="form-control" name="status_id">
            @if (isset($statuses))
              @foreach ($statuses as $key => $status)
                <option value="{{ $status->id }}">{{ $status->status }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="form-group">
          <h6>In Evidenza</h6>
          <select class="form-control" name="in_home">
            <option value="0">Non In Evidenza</option>
            <option value="1">Metti In Evidenza</option>
          </select>
        </div>
        <div class="form-group">
          <h6>Immagine Principale</h6>
          <div class="row">
            <div class="col-md-6">
              <input type="file" name="media" value="{{ old('media') }}" class="form-control-file" aria-describedby="fileHelp">
              <input type="hidden" name="media-library" value="">
            </div>
            <div class="col-md-6">
              <!-- Button trigger modal -->
              <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#mediaLibrary">
                <i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria
              </button>

              <!-- Modal -->
              <div class="modal fade" id="mediaLibrary" tabindex="-1" role="dialog" aria-labelledby="mediaLibraryLabel" aria-hidden="true">
                <div id="media-wrapper" class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="mediaLibraryLabel"><i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria Immagini</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div id="MediaLibrary">
                        <media-library />
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <h6>Categoria</h6>
          <select id="category" class="form-control" name="category_id">
          @if (isset($categories))
            @foreach ($categories as $key => $category)
              <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
          @endif
          </select>
        </div>
        <div id="location-sel" class="form-group d-none">
          <h6>Location</h6>
          <select name="location_id" id="location" class="form-control">
            @if (isset($locations))
              <option value=""></option>
              @foreach ($locations as $location)
                <option value="{{ $location->id }}">{{ $location->name }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="pt-2">
          <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salva</button>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote.css') }}">
  <script src="{{ asset('plugins/summernote/summernote.js') }}"></script>
  <script src="/js/media-library.js"></script>
  <script type="text/javascript">
    $('#editor').summernote({
        height: 300,
        focus: true,
        lang: 'it-IT',
        toolbar: [
          ['font', ['style']],
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['tbl', ['table', 'hr']],
          ['media', ['link', 'picture', 'video']],
          ['action', ['undo', 'redo']],
          ['code', ['codeview']],
          ['pro', ['help']],
        ],
        callbacks: {
          onImageUpload: function(files) {
              for (var i = 0; i < files.length; i++) {
                  var file = files[i]
                  console.log(file)
                  data = new FormData()
                  data.append('media', file)
                  $.ajax({
                    type: 'post',
                    url:  '{{ route('media.api.upload') }}',
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: data,
                    success: function(response) {
                      $('#editor').summernote("insertImage", response.path)
                    }
                  })
              }
          }
        }
    })

    $("#title").keyup(function(event) {
      var stt = $(this).val().toLowerCase().replace(/\s/g,'-')
      $("#slug").val(stt)
    })

    $('#category').change(function() {
      if (parseInt($(this).val()) == 4) {
        $('#location-sel').removeClass('d-none')
      } else {
        $('#location-sel').addClass('d-none')
        $('#location').val(null)
      }
    })
  </script>
@endsection
