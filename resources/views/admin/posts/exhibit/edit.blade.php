@extends('layouts.admin')
@section('title', 'Modifica Esposizione')
@section('page-title', 'Modifica Esposizione')
@section('content')
<form class="" action="{{ url('/admin/posts') }}/exhibit/{{ $exhibit->id }}" enctype="multipart/form-data" method="post">
  {{ csrf_field() }}
  {{ method_field('PATCH') }}
  <div class="row">
    <div class="col-md-8">
      <div class="container-fluid bg-faded p-4">
        <div class="form-group">
          <h6>Titolo</h6>
          <input id="title" type="text" name="title" class="form-control" value="{{ $exhibit->title }}">
        </div>
        <div class="form-group">
          <h6>Slug</h6>
          <input id="slug" type="text" name="slug" class="form-control" value="{{ $exhibit->slug }}">
        </div>
        <div class="form-group">
          <h6>Descrizione</h6>
          <textarea name="description" id="editor" rows="8" class="form-control">{{ $exhibit->description }}</textarea>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h3>Dettagli</h3>
        <hr>
        <div class="form-group">
          <h6>Stato</h6>
          <select class="form-control" name="status_id">
            @if (isset($statuses))
              @foreach ($statuses as $key => $status)
                <option value="{{ $status->id }}" {{ $status->id == $exhibit->status_id ? 'selected' : '' }}>{{ $status->status }}</option>
              @endforeach
            @endif
          </select>
        </div>
        <div class="form-group">
          <h6>Festival</h6>
          <label class="custom-control custom-checkbox">
            <input id="festival" name="festival" type="checkbox" class="custom-control-input" {{ $exhibit->festival == 1 ? 'checked' : '' }}>
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">Appartiene ad un festival?</span>
          </label>
        </div>
        <div id="festival-id" class="form-group {{ $exhibit->festival == 1 ? '' : 'd-none' }}">
          <h6>Festival</h6>
          <select class="form-control" name="festival_id">
            <option value="0"></option>
            @isset($festivals)
              @foreach ($festivals as $key => $festival)
                <option value="{{ $festival->id }}" {{ $festival->id == $exhibit->festival_id ? 'selected' : '' }}>{{ $festival->title }}</option>
              @endforeach
            @endisset
          </select>
        </div>
        <div id="festival-cat" class="form-group {{ $exhibit->festival == 1 ? '' : 'd-none' }}">
          <h6>Categoria</h6>
          <select class="form-control" name="festival_cat_id">
            <option value="0"></option>
            @isset($fest_cats)
              @foreach ($fest_cats as $key => $cat)
                <option value="{{ $cat->id }}" {{ $exhibit->fest_cat_id == $cat->id ? 'selected' : '' }}>{{ $cat->name }}</option>
              @endforeach
            @endisset
          </select>
        </div>
        <div class="form-group">
          <h6>Bambini</h6>
          <select class="form-control" name="kids">
            <option value="0" {{ $exhibit->kids == 0 ? 'selected' : '' }}>Generico</option>
            <option value="1" {{ $exhibit->kids == 1 ? 'selected' : '' }}>Per Bambini</option>
          </select>
        </div>
        <div class="form-group">
          <h6>Immagine Principale</h6>
          <div class="row">
            <div class="col-md-6">
              <input type="file" name="media" value="{{ old('media') }}" class="form-control-file" aria-describedby="fileHelp">
              <input type="hidden" name="media-library" value="">
            </div>
            <div class="col-md-6">
              <!-- Button trigger modal -->
              <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#mediaLibrary">
                <i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria
              </button>

              <!-- Modal -->
              <div class="modal fade" id="mediaLibrary" tabindex="-1" role="dialog" aria-labelledby="mediaLibraryLabel" aria-hidden="true">
                <div id="media-wrapper" class="modal-dialog modal-lg" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="mediaLibraryLabel"><i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria Immagini</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div id="MediaLibrary">
                        <media-library />
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <h6>Location</h6>
          <select class="form-control" name="location_id">
          @if (isset($locations))
            @foreach ($locations as $key => $location)
              <option value="{{ $location->id }}" {{ $location->id == $exhibit->location_id ? 'selected' : '' }}>{{ $location->name }}</option>
            @endforeach
          @endif
          </select>
        </div>
        <div class="form-group">
          <h6>Data d'inizio programmazione</h6>
          <input type="text" name="start_date" class="form-control date-picker" value="{{ $exhibit->start_date }}">
        </div>
        <div class="form-group">
          <h6>Ultimo giorno di programmazione</h6>
          <input type="text" name="end_date" class="form-control date-picker" value="{{ $exhibit->end_date }}">
        </div>
        <div class="pt-2">
          <button type="submit" class="btn btn-primary btn-block">
            <i class="fa fa-floppy-o" aria-hidden="true"></i> Salva
          </button>
        </div>
      </div>
    </div>
  </div>
</form>

{{-- Aggiungi Evento --}}
<div class="row mt-4">
  <div class="col-md-8">
    <div class="continer-fluid bg-faded p-4">
      <h3>Eventi</h3>
      <hr>
      <table id="relations" class="table table-hover">
        <thead>
          <th>Id</th>
          <th>Titolo</th>
          <th>Tipologia</th>
          <th></th>
        </thead>
        <tbody>
          @isset($items)
            @if ($items->count() > 0)
              @foreach ($items as $key => $item)
                <tr id="#relation-{{ $item->type }}-{{ $item->id }}">
                  <td>{{ $item->id }}</td>
                  <td>{{ $item->title }}</td>
                  <td>{{ $item->type }}</td>
                  <td>
                    <button onclick="destroyRelation({{ $item->id }}, '{{ $item->type }}')" type="button" name="button" class="btn btn-danger">
                      <i class="fa fa-trash-o" aria-hidden="true"></i>
                    </button>
                  </td>
                </tr>
              @endforeach
            @else
              <tr id="no-relations">
                <td colspan="3">
                  <span class="alert alert-danger d-block">
                    <b>Attenzione: </b>
                    Ancora nessun elemento assegnato a questa esposizione!
                  </span>
                </td>
              </tr>
            @endif
          @endisset
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-md-4">
    <div class="container-fluid bg-faded p-4">
      <h3>Aggiungi Evento</h3>
      <hr>
        <div class="form-group">
          <h6>Film</h6>
          <select id="film" class="form-control" name="film">
              <option value="" selected></option>
            @foreach ($films as $key => $film)
              <option value="{{ $film->id }}">{{ $film->title }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <h6>Rassegna</h6>
          <select id="exhibition" class="form-control" name="exhibition">
            <option value="" selected></option>
            @foreach ($exhibitions as $key => $exhibition)
              <option value="{{ $exhibition->id }}">{{ $exhibition->title }}</option>
            @endforeach
          </select>
        </div>
        <div class="pt-2">
          <button id="add" class="btn btn-primary btn-block">
            <i class="fa fa-plus" aria-hidden="true"></i> Aggiungi
          </button>
        </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote.css') }}">
  <script src="{{ asset('plugins/summernote/summernote.js') }}"></script>
  <script src="/js/media-library.js"></script>
  <script type="text/javascript">
    $('#editor').summernote({
        height: 300,
        focus: true,
        lang: 'it-IT',
        toolbar: [
          ['font', ['style']],
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['tbl', ['table', 'hr']],
          ['media', ['link', 'picture', 'video']],
          ['action', ['undo', 'redo']],
          ['code', ['codeview']],
          ['pro', ['help']],
        ],
        callbacks: {
          onImageUpload: function(files) {
              for (var i = 0; i < files.length; i++) {
                  var file = files[i];
                  console.log(file);
                  data = new FormData();
                  data.append('media', file);
                  $.ajax({
                    type: 'post',
                    url:  '{{ route('media.api.upload') }}',
                    headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
                    processData: false,
                    contentType: false,
                    cache: false,
                    data: data,
                    success: function(response) {
                      $('#editor').summernote("insertImage", response.path);
                    }
                  });
              }
          }
        }
    });
    $('.date-picker').datepicker();
    $('#day').datepicker();

    $("#title").keyup(function(event) {
      var stt = $(this).val().toLowerCase().replace(/\s/g,'-');
      $("#slug").val(stt);
    });

    $('#festival').on('change', function(e) {
      if ($(this).prop('checked') === true) {
        $('#festival-id').removeClass('d-none');
        var festival_id = $('#festival-id select').val();
        if (festival_id > 0) {
          $('#festival-cat').removeClass('d-none');
        }
      } else {
        $('#festival-id').addClass('d-none');
        $('#festival-cat').addClass('d-none');
      }
    });

    $('#festival-id select').on('change', function(e) {
      var festival_id = $(this).val();
      if (festival_id > 0) {
        $.ajax({
          type: 'get',
          url:  '/admin/festival-cat/'+festival_id,
          headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' },
          success: function(data) {
            console.log(data);
            $('#festival-cat select option').each(function(){
              $(this).remove();
            });
            $.each(data, function(){
              var option = '';
              option += '<option value="'+this.id+'">'+this.name+'</option>'
              $('#festival-cat select').append(option);
            });
            $('#festival-cat select').prepend('<option value="0"></option>');
            $('#festival-cat').removeClass('d-none');
          }
        });
      } else {
        $('#festival-cat').addClass('d-none');
        var empty = '<option value="0"></option>';
        $('#festival-cat select option').each(function(){
          $(this).remove();
        });
        $('#festival-cat select').append(empty);
      }
    });

    $('#film').select2();
    $('#exhibition').select2();
    $('#exhibit').select2();
    $('#event').select2();

    {{-- STORE FUNCTION --}}
    $('#add').on('click', function(event) {
      event.preventDefault();
      var _film = $('#film :selected').val();
      var _exhibition = $('#exhibition :selected').val();

      var types = [];
      var item_ids = [];

      if (_film != null && _film > 0) {
        types.push('film');
        item_ids.push(_film);
      }

      if (_exhibition != null && _exhibition > 0) {
        types.push('exhibition');
        item_ids.push(_exhibition);
      }

      $.ajax({
        type: 'post',
        url:  '{{ route('store.exhibit.relation') }}',
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
        data: {
          '_token':         $('input[name=_token]').val(),
          'exhibit_id':     '{{ $exhibit->id }}',
          'item_ids':       item_ids,
          'types':          types
        },
        success: function(response) {
          console.log(response);
          $('#film').val(null).trigger("change");
          $('#exhibition').val(null).trigger("change");
          var dataLenght = Object.keys(response.objects).length;
          if (dataLenght > 0) {
            $('#no-film').remove();
          }
          for (var i = 0; i < dataLenght; i++) {
            var data = '';
            data += '<tr id="relation-'+response.objects[i].type+'-'+response.objects[i].id+'">';
            data += '<td class="align-middle">'+response.objects[i].id+'</td>';
            data += '<td class="align-middle">'+response.objects[i].title+'</td>';
            data += '<td class="align-middle">'+response.objects[i].type+'</td>';
            data += '<td class="align-middle"><button onclick="destroyRelation('+response.objects[i].id+', \''+response.objects[i].type+'\')" type="button" name="button" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>';
            data += '</tr>';

            var count = countObjs();
            if (count > 0) {
              $('#no-relations').remove();
            }

            $('#relations').append(data);

          }
        }
      });

    });

    {{-- DELETE FUNCTION --}}
    function destroyRelation (id, type) {
      $.ajax({
        type: 'delete',
        url:  '{{ route('destroy.exhibit.relation') }}',
        headers:  { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
        data: {
          '_token':      $('input[name=_token]').val(),
          'exhibit_id':  '{{ $exhibit->id }}',
          'type':        type,
          'id':          id
        },
        success: function(response) {
          console.log(response);
          if (response.id) {
            $('#relation-'+response.type+'-'+response.id).remove();
            var count = countObjs();
            if (count == 0) {
              $('#relations').append('<tr id="no-relations"><td colspan="2"><span class="alert alert-danger d-block"><b>Attenzione: </b>Ancora nessun film assegnato a questa rassegna!</span></td></tr>');
            }
          }
        }
      });
    }

    function countObjs()
    {
      var count = 0;
      $('#relations > tbody > tr').each(function(i) {
        count = i+1;
      });
      return count;
    }
  </script>
@endsection
