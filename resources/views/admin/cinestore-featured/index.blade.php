@extends('layouts.admin')
@section('title', 'Cinestore Featured')
@section('content')
{{ csrf_field() }}
<div class="row pb-5">
  <div class="col bg-faded p-4">
    <h3>Prodotti in evidenza</h3>
    <hr>
    <div class="row">
      <div class="col-md-10">
        <div class="form-group">
          <h6>Prodotti</h6>
          <select id="feat_products" class="form-control" name="">
            <option value=""></option>
            @foreach ($products as $key => $product)
              <option value="{{ $product->id }}">{{ $product->name }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="col-md-2">
          <div class="d-flex justify-content-around align-middle">
            <a id="feat_products_add" href="#" class="btn btn-primary"><i class="fa fa-plus"></i> Aggiungi</a>
          </div>
      </div>
    </div>
    <table id="feat_products_grid" class="table table-hover">
      <thead>
        <th>Id</th>
        <th>Titolo</th>
        <th>Img</th>
        <th>Tools</th>
      </thead>
      <tbody>
        @foreach ($feat_prods as $key => $feat)
          <tr>
            <td class="align-middle">{{ $feat->featurable()->first()->id }}</td>
            <td class="align-middle">{{ $feat->featurable()->first()->name }}</td>
            <td class="align-middle"><img src="{{ Storage::disk('local')->url($feat->featurable()->first()->media()->first()->thumb) }}" width="57"></td>
            <td class="align-middle">
              <form class="" action="{{ route('cinestore.featured.destroy', $feat->id) }}" method="POST">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button type="submit" name="button" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
<div class="row pb-5">
  <div class="col bg-faded p-4">
    <h3>Video in evidenza</h3>
    <hr>
    <div class="row">
      <div class="col-md-10">
        <div class="form-group">
          <h6>Prodotti</h6>
          <select id="feat_videos" class="form-control" name="">
            <option value=""></option>
            @foreach ($videos as $key => $product)
              <option value="{{ $product->id }}">{{ $product->name }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="col-md-2">
          <div class="d-flex justify-content-around align-middle">
            <a id="feat_videos_add" href="#" class="btn btn-primary"><i class="fa fa-plus"></i> Aggiungi</a>
          </div>
      </div>
    </div>
    <table id="feat_videos_grid" class="table table-hover">
      <thead>
        <th>Id</th>
        <th>Titolo</th>
        <th>Img</th>
        <th>Tools</th>
      </thead>
      <tbody>
        @foreach ($feat_videos as $key => $feat)
          <tr>
            <td class="align-middle">{{ $feat->featurable()->first()->id }}</td>
            <td class="align-middle">{{ $feat->featurable()->first()->name }}</td>
            <td class="align-middle"><img src="{{ Storage::disk('local')->url($feat->featurable()->first()->media()->first()->thumb) }}" width="57"></td>
            <td class="align-middle">
              <form class="" action="{{ route('cinestore.featured.destroy', $feat->id) }}" method="POST">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button type="submit" name="button" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
<div class="row pb-5">
  <div class="col bg-faded p-4">
    <h3>Foto in evidenza</h3>
    <hr>
    <div class="row">
      <div class="col-md-10">
        <div class="form-group">
          <h6>Prodotti</h6>
          <select id="feat_pics" class="form-control" name="">
            <option value=""></option>
            @foreach ($pics as $key => $product)
              <option value="{{ $product->id }}">{{ $product->name }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="col-md-2">
          <div class="d-flex justify-content-around align-middle">
            <a id="feat_pics_add" href="#" class="btn btn-primary"><i class="fa fa-plus"></i> Aggiungi</a>
          </div>
      </div>
    </div>
    <table id="feat_pics_grid" class="table table-hover">
      <thead>
        <th>Id</th>
        <th>Titolo</th>
        <th>Img</th>
        <th>Tools</th>
      </thead>
      <tbody>
        @foreach ($feat_pics as $key => $feat)
          <tr>
            <td class="align-middle">{{ $feat->featurable()->first()->id }}</td>
            <td class="align-middle">{{ $feat->featurable()->first()->name }}</td>
            <td class="align-middle"><img src="{{ Storage::disk('local')->url($feat->featurable()->first()->media()->first()->thumb) }}" width="57"></td>
            <td class="align-middle">
              <form class="" action="{{ route('cinestore.featured.destroy', $feat->id) }}" method="POST">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button type="submit" name="button" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
<div class="row pb-5">
  <div class="col bg-faded p-4">
    <h3>Percorsi in evidenza</h3>
    <hr>
    <div class="row">
      <div class="col-md-10">
        <div class="form-group">
          <h6>Prodotti</h6>
          <select id="feat_groups" class="form-control" name="">
            <option value=""></option>
            @foreach ($prod_groups as $key => $product)
              <option value="{{ $product->id }}">{{ $product->name }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="col-md-2">
          <div class="d-flex justify-content-around align-middle">
            <a id="feat_groups_add" href="#" class="btn btn-primary"><i class="fa fa-plus"></i> Aggiungi</a>
          </div>
      </div>
    </div>
    <table id="feat_groups_grid" class="table table-hover">
      <thead>
        <th>Id</th>
        <th>Titolo</th>
        <th>Img</th>
        <th>Tools</th>
      </thead>
      <tbody>
        @foreach ($feat_groups as $key => $feat)
          <tr>
            <td class="align-middle">{{ $feat->featurable()->first()->id }}</td>
            <td class="align-middle">{{ $feat->featurable()->first()->name }}</td>
            <td class="align-middle"><img src="{{ Storage::disk('local')->url($feat->featurable()->first()->media()->first()->thumb) }}" width="57"></td>
            <td class="align-middle">
              <form class="" action="{{ route('cinestore.featured.destroy', $feat->id) }}" method="POST">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button type="submit" name="button" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
  <script type="text/javascript">
    var select2_opts = {
      // tags: true,
      // tokenSeparators: [',']
    }

    $('#feat_products').select2(select2_opts);
    $('#feat_pics').select2(select2_opts);
    $('#feat_videos').select2(select2_opts);
    $('#feat_groups').select2(select2_opts);

    $('#feat_products_add').on('click', function(e) {
        e.preventDefault();
        var id = $('#feat_products').val();
        sendRequest(id, 0, '#feat_products_grid');
    });

    $('#feat_videos_add').on('click', function(e) {
        e.preventDefault();
        var id = $('#feat_videos').val();
        sendRequest(id, 5, '#feat_videos_grid');
    });

    $('#feat_pics_add').on('click', function(e) {
        e.preventDefault();
        var id = $('#feat_pics').val();
        sendRequest(id, 4, '#feat_pics_grid');
    });

    $('#feat_groups_add').on('click', function(e) {
        e.preventDefault();
        var id = $('#feat_groups').val();
        sendRequest(id, 6, '#feat_groups_grid');
        console.log(id);
    });

    function sendRequest(id, category, target = null)
    {
        $.ajax({
            url: '/admin/cinestore-evidenza/store',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'id' : id,
                'category' : category,
                '_token' : $('input[name="_token"]').val()
            },
            success: function (response) {
                console.log(response);
                var data = '';
                data += '<tr id="" class="align-middle">';
                data += '<td class="align-middle">'+response.product.id+'</td>';
                data += '<td class="align-middle">'+response.product.name+'</td>';
                data += '<td class="align-middle"><img src="'+response.product.img+'" width="57" class="img-fluid"></td>';
                data += '<td class="align-middle">';
                data += '<form class="" action="/admin/cinestore-evidenza/'+response.feat.id+'" method="POST">';
                data += '{{ csrf_field() }}';
                data += '{{ method_field('DELETE') }}';
                data += '<button type="submit" name="button" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>';
                data += '</form>';
                data += '</td>';
                data += '</tr>';
                $(target).append(data);
            }
        });
    }

    function gridAppend(obj, target)
    {
      console.log(obj);

    }

  </script>
@endsection
