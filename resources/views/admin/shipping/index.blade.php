@extends('layouts.admin')
@section('title', 'Spedizioni')
@section('content')
  <div class="container-fluid bg-faded p-4">
    <h3>Spedizioni</h3>
    <hr>
    <div id="shipping">
      <shipping />
    </div>
  </div>
@endsection
@section('scripts')
  <script src="/js/admin-shipping.js"></script>
@endsection
