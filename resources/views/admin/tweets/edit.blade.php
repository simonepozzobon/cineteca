@extends('layouts.admin')
@section('title', 'Twitter')
@section('content')
<form class="" action="{{ route('admin.twitter.update', $tweet->id) }}" method="post">
  {{ csrf_field() }}
  {{ method_field('PATCH') }}
  <div class="row">
    <div class="col-md-8">
      <div class="container-fluid p-4 bg-faded mb-5">
        <h3>Contenuto</h3>
        <hr>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <h6>Data</h6>
              <input type="text" name="date" value="{{ $tweet->date }}" class="form-control date-picker">
            </div>
          </div>
          <div class="col-md-6">
            <h6>Ora</h6>
            <input type="text" name="time" value="{{ $tweet->time }}" class="form-control">
          </div>
        </div>
        <div class="form-group">
          <textarea name="text" rows="8" cols="80" class="form-control">{!! $tweet->text !!}</textarea>
        </div>
      </div>
      <div class="container-fluid p-4 bg-faded">
        <h3>Anteprima del testo</h3>
        <hr>
        <p>{{ substr(strip_tags($tweet->text), 0, 110) }}{{ strlen(strip_tags($tweet->text)) > 110 ? '...' : "" }} {{ $tweet->link }}</p>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid p-4 bg-faded">
        <h3>Dettagli</h3>
        <hr>
        <div class="row">
          <div class="col">
            <h6>Stato</h6>
            <div class="form-group">
              <select class="form-control" name="status_id">
                @foreach ($statuses as $key => $status)
                  <option value="{{ $status->id }}" {{ $tweet->status_id == $status->id ? 'selected' : '' }}>{{ $status->status }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="row pb-3">
          <div class="col">
            <h6>Contenuto Originale</h6>
            <a href="{{ $tweet->obj_link }}" target="_blank">Vedi Contenuto</a>
          </div>
        </div>
        <div class="row pb-3">
          <div class="col">
            <h6>Immagine</h6>
            <img src="{{ Storage::disk('local')->url($tweet->media()->first()->landscape) }}" alt="" class="img-fluid">
          </div>
        </div>
        <div class="row pb-3">
          <div class="col">
            <button type="submit" name="button" class="btn btn-primary btn-block">Salva</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}">
  <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script type="text/javascript">
    $('.date-picker').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy',
      startDate: new Date(),
      orientation: 'bottom',
      todayHighlight: true
    });
  </script>
@endsection
