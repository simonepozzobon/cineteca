@extends('layouts.admin')
@section('title', 'Home Slider')
@section('content')
  <div class="row pb-5">
    <div class="col bg-faded p-4">
      <h3>Aggiungi Slide</h3>
      <hr>
      <form class="" action="{{ route('cinestore.slider.store') }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <div class="form-group">
          <h6>Prodotti</h6>
          <select id="products" class="form-control" name="product" style="width: 100%">
            <option value=""></option>
            @foreach ($products as $key => $product)
              <option value="{{ $product->id }}">{{ $product->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="form-group">
          <h6>Percorsi</h6>
          <select id="product_groups" class="form-control" name="product_groups" style="width: 100%">
            <option value=""></option>
            @foreach ($product_groups as $key => $product_group)
              <option value="{{ $product_group->id }}">{{ $product_group->name }}</option>
            @endforeach
          </select>
        </div>
        <div class="d-flex d-justify-content-around">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Aggiungi</button>
        </div>
      </form>
    </div>
  </div>
  <div class="row">
    <div class="col bg-faded p-4">
      <h3>Articoli in evidenza</h3>
      <hr>
      <table class="table table-hover">
        <thead>
          <th>Id</th>
          <th>Titolo</th>
          <th>Img</th>
          <th>Tools</th>
        </thead>
        <tbody>
          @foreach ($slides as $key => $slide)
            <tr>
              <td class="align-middle">{{ $slide['id'] }}</td>
              <td class="align-middle">{{ $slide['name'] }}</td>
              <td class="align-middle"><img src="{{ $slide['img'] }}" width="57"></td>
              <td class="align-middle">
                <form class="" action="{{ route('cinestore.slider.destroy', $slide['slider_id']) }}" method="POST">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
                  <button type="submit" name="button" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                </form>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
  <script type="text/javascript">
    $('#products').select2({
      tags: true,
      tokenSeparators: [',']
    });
    $('#product_groups').select2();
  </script>
@endsection
