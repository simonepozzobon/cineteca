@extends('layouts.admin')
@section('title', 'Categories')
@section('content')
  <div class="row">
    <div class="col-md-8">
      <div class="container-fluid bg-faded p-4">
        <table class="table table-hover">
          <thead>
            <th>id</th>
            <th>Name</th>
            <th>In Home</th>
            <th>Image</th>
            <th>Color</th>
            <th>Tools</th>
          </thead>
          <tbody>
            @foreach ($categories as $key => $category)
              <tr>
                <td class="align-middle">{{ $category->id }}</td>
                <td class="align-middle">{{ $category->name }}</td>
                <td class="align-middle">
                  @if ($category->in_home == 0)
                    <i class="fa fa-circle-thin text-warning" aria-hidden="true"></i>
                  @else
                    <i class="fa fa-check-circle text-success" aria-hidden="true"></i>
                  @endif
                </td>
                <td class="align-middle">
                  @if ($category->thumb)
                    <img src="{{ Storage::disk('local')->url($category->thumb) }}" width="57" alt="" class="rounded-circle center-block">
                  @else
                    no img
                  @endif
                </td>
                <td class="align-middle text-center">
                  <div style="width:32px; height:32px; background-color:{{ $category->color }};">
                  </div>
                </td>
                <td class="align-middle">
                  <div class="btn-group">
                    <a href="#" class="btn btn-info" data-toggle="modal" data-target="#edit{{ $category->id }}">
                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                    </a>
                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#delete{{ $category->id }}">
                      <i class="fa fa-trash" aria-hidden="true"></i> Delete
                    </a>
                  </div>
                  <form class="" action="{{ route('categories.update', $category->id) }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    {{-- Modal --}}
                    <div class="modal fade m-5" id="edit{{ $category->id }}" tabindex="-1" role="dialog" aria-labelledby="edit{{ $category->id }}Label" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="edit{{ $category->id }}Label">Edit Category</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="form-group border-0">
                              <img src="{{ Storage::disk('local')->url($category->square_medium) }}" alt="" class="rounded img-fluid mx-auto d-block">
                            </div>
                            <div class="form-group border-0">
                              <h6>Name</h6>
                              <input type="text" name="name" class="form-control" value="{{ $category->name }}">
                            </div>
                            <div class="form-group border-0">
                              <h6>Color</h6>
                              <input type="text" name="color" class="form-control" value="{{ $category->color }}">
                            </div>
                            <div class="form-group border-0">
                              <h6>Change Image</h6>
                              <input type="file" name="media" class="form-control">
                            </div>
                            <div class="form-group border-0">
                              <h6>Link</h6>
                              <input type="text" name="link" class="form-control" value="{{ $category->link }}">
                            </div>
                            <div class="form-group border-0">
                              <h6>In Home</h6>
                              <input type="checkbox" name="in_home"><small> Checked means yes</small>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-small btn-primary" value="Submit">Save</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                  <form class="" action="{{ route('categories.destroy', $category->id) }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}{{-- Modal Trigger --}}

                    {{-- Modal --}}
                    <div class="modal fade m-5" id="delete{{ $category->id }}" tabindex="-1" role="dialog" aria-labelledby="delete{{ $category->id }}Label" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="delete{{ $category->id }}Label">Delete Category</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            Are you sure?
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-small btn-danger" value="Submit">Delete</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>

      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h3>Add New Category</h3>
        <form class="" action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
            <h6>Name</h6>
            <input type="text" name="name" class="form-control" placeholder="Name of the category">
          </div>
          <div class="form-group">
            <h6>Color</h6>
            <input type="color" name="color" class="form-control" placeholder="#FFFFFF with '#'">
          </div>
          <div class="form-group border-0">
            <h6>Change Image</h6>
            <input type="file" name="media" class="form-control">
          </div>
          <div class="form-group border-0">
            <h6>Link</h6>
            <input type="text" name="link" class="form-control" value="">
          </div>
          <div class="form-group border-0">
            <h6>In Home</h6>
            <input type="checkbox" name="in_home"><small> Checked means yes</small>
          </div>
          <div class="pt-2">
            <button type="submit" class="btn btn-primary btn-block">
              <i class="fa fa-save" aria-hidden="true"></i> Save
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
