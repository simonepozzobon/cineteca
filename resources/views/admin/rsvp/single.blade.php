@extends('layouts.admin')
@section('title', "Ospiti - $event->title")
@section('content')
  <div class="row pb-5">
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-5">
        <h3>Posti Disponibili</h3>
        <hr>
        <h4>{{ $event->guest_left }} <small>di</small> {{ $event->guest_list }}</h4>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-5">
        <h3>In Lista D'attesa</h3>
        <hr>
        <h4>{{ $waiting }} <small>di</small> {{ $event->wait_list }}</h4>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-5">
        <h3>File Excel</h3>
        <hr>
        <a href="{{ route('rsvp.download', $event->id) }}" class="btn btn-primary btn-block">
          <i class="fa fa-download"></i> Scarica file excel
        </a>
      </div>
    </div>
  </div>
  <div class="row pb-5">
    <div class="col">
      <div class="container-fluid bg-faded p-5">
        <h3>Prenotazioni</h3>
        <hr>
        <table class="table table-hover">
          <thead>
            <th>Id.</th>
            <th>Nome</th>
            <th>Cognome</th>
            <th>Adulti</th>
            <th>Bambini</th>
            <th>Email</th>
            <th>Status</th>
          </thead>
          <tbody>
            @foreach ($guests as $key => $guest)
              <tr>
                <td class="align-middle">{{ $key + 1 }}</td>
                <td class="align-middle">{{ $guest->name }}</td>
                <td class="align-middle">{{ $guest->surname }}</td>
                <td class="align-middle">{{ $guest->guests }}</td>
                <td class="align-middle">{{ $guest->kids }}</td>
                <td class="align-middle"><a href="mailto:{{ $guest->email }}">{{ $guest->email }}</a></td>
                <td class="align-middle">
                  @if ($guest->status == 0)
                    <span class="text-success">Registrato</span>
                  @else
                    <span class="text-warning">In Attesa</span>
                  @endif
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @if ($pendings->count() > 0)
    <div class="row pb-5">
      <div class="col">
        <div class="container-fluid bg-faded p-5">
          <h3>Lista d'attesa</h3>
          <hr>
          <table class="table table-hover">
            <thead>
              <th>Id.</th>
              <th>Nome</th>
              <th>Cognome</th>
              <th>Adulti</th>
              <th>Bambini</th>
              <th>Email</th>
              <th>Status</th>
            </thead>
            <tbody>
              @foreach ($pendings as $key => $guest)
                <tr>
                  <td class="align-middle">{{ $key + 1 }}</td>
                  <td class="align-middle">{{ $guest->name }}</td>
                  <td class="align-middle">{{ $guest->surname }}</td>
                  <td class="align-middle">{{ $guest->guests }}</td>
                  <td class="align-middle">{{ $guest->kids }}</td>
                  <td class="align-middle"><a href="mailto:{{ $guest->email }}">{{ $guest->email }}</a></td>
                  <td class="align-middle">
                    @if ($guest->status == 0)
                      <span class="text-success">Registrato</span>
                    @else
                      <span class="text-warning">In Attesa</span>
                    @endif
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  @endif
@endsection
@section('scripts')
<script type="text/javascript">
  $('.payment').on('change', function(e) {
    var guest_id = '#'+$(this).attr('id')+'-status';
    var btn_id = '#'+$(this).attr('id')+'-btn';
    var btn = $(btn_id);
    var obj = $(guest_id);
    if (this.value == 0) {
      obj.removeClass('fa-check-circle').removeClass('text-success');
      obj.removeClass('fa-minus-circle').removeClass('text-danger');
      obj.addClass('fa-question-circle-o').addClass('text-warning');
      btn.removeClass('invisible');
    }
    if (this.value == 1) {
      obj.removeClass('fa-question-circle-o').removeClass('text-warning');
      obj.removeClass('fa-minus-circle').removeClass('text-danger');
      obj.addClass('fa-check-circle').addClass('text-success');
      btn.removeClass('invisible');
    }
    if (this.value == 2) {
      obj.removeClass('fa-check-circle').removeClass('text-success');
      obj.removeClass('fa-question-cinrcle-o').removeClass('text-warning');
      obj.addClass('fa-minus-circle').addClass('text-danger');
      btn.removeClass('invisible');
    }
  });
</script>
@endsection
