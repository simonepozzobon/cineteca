@extends('layouts.admin')
@section('title', 'Cinestore')
@section('content')
  <div class="container-fluid">
      <div class="row">
          <div class="col-md-8">
            <div class="container-fluid bg-faded p-4">
              <h3>Ordini</h3>
              <hr>
              <table class="table table-hover">
                <thead>
                  <th>Id</th>
                  <th>Transazione</th>
                  <th>Stato</th>
                  <th>Data</th>
                  <th>Prezzo</th>
                  <th></th>
                </thead>
                <tbody>
                  @foreach ($orders as $key => $order)
                    <tr>
                      <td>{{ $order->id }}</td>
                      <td>{{ $order->transaction_id }}</td>
                      <td>
                        @if ($order->order_status_id == 1)
                          <i class="fa fa-question-circle-o text-warning" aria-hidden="true"></i> {{ $order->orderStatus->name }}
                        @elseif ($order->order_status_id == 2)
                          <i class="fa fa-money text-success" aria-hidden="true"></i> {{ $order->orderStatus->name }}
                        @elseif ($order->order_status_id == 6)
                          <i class="fa fa-times-circle text-danger" aria-hidden="true"></i> {{ $order->orderStatus->name }}
                        @else
                          {{ $order->order_status_id }}
                        @endif
                      </td>
                      <td>{{ $order->created_at }}</td>
                      <td>{{ $order->amount }} €</td>
                      <td>
                        <a href="{{ route('admin.cinestore.single', $order->id) }}" class="btn btn-warning"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="col-md-4">
            <div class="container-fluid bg-faded p-4">
              <h3>Ordini Da Controllare</h3>
              <hr>
              <p>{{ $checkOrders }}</p>
            </div>
          </div>
      </div>
      <div class="row">
        <div class="col-md-4 offset-md-4 py-5 text-center">
          <div class="container-fluid d-flex justify-content-around">
            {{ $orders->links('vendor.pagination.bootstrap-4') }}
          </div>
        </div>
      </div>
  </div>
@endsection
