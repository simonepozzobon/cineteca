@extends('layouts.admin')
@section('title', 'Ordina Prodotti Cinestore')
@section('content')
<div id="app" class="container-fluid">
  <order-products categories="{{ $product_categories }}"></order-products>
</div>
@endsection
@section('scripts')
  <script src="/js/order-products.js"></script>
@endsection
