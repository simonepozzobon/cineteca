@extends('layouts.admin')
@section('title', 'Cinestore')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
          <div class="container-fluid bg-faded p-4">
            <h3>Ordine</h3>
            <hr>
            <table class="table table-hover pb-5">
              <thead>
                <th>Id</th>
                <th>Immagine</th>
                <th>Nome Prodotto</th>
                <th>Quantità</th>
                <th>Prezzo</th>
              </thead>
              <tbody>
                @foreach ($order->orderProducts()->get() as $key => $ord_prod)
                  <tr>
                    <td>{{ $ord_prod->product->id }}</td>
                    <td><img src="{{ Storage::disk('local')->url($ord_prod->product->media->first()->thumb) }}" alt="" class="img-fluid" width="57"></td>
                    <td><a href="{{ route('cinestore.single', [$ord_prod->product->category->slug, $ord_prod->product->slug]) }}" target="_blank">{{ $ord_prod->product->name }}</a></td>
                    <td>{{ $ord_prod->quantity }}</td>
                    <td>{{ $ord_prod->product->price }}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            <hr>
            <div class="container-fluid d-flex justify-content-between">
              <h3>Totale</h3>
              <h3>{{ $tot }} €</h3>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="container-fluid bg-faded p-4">
            <form class="" action="{{ route('admin.cinestore.order.edit', $order->id) }}" method="post">
              {{ csrf_field() }}
              {{ method_field('POST') }}
              <h3>Dettagli</h3>
              <hr>
              <h5>Data di creazione</h5>
              <p>{{ $order->created_at }}</p>
              <div class="form-group pb-5">
                <h5>Stato dell'ordine</h5>
                <select class="form-control" name="status">
                  @foreach ($orderStatuses as $key => $orderStatus)
                    <option value="{{ $orderStatus->id }}" {{ $orderStatus->id == $order->order_status_id ? 'selected' : '' }}>{{ $orderStatus->name }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group pb-5">
                <h3>Dati Cliente</h3>
                <hr>
                <p><b>Nome e Cognome:</b> {{ $order->name }}, {{ $order->surname }}</p>
                <p><b>Numero di Telefono:</b> {{ $order->phone }}</p>
                <p><b>Email:</b> <a href="mailto:{{ $order->email }}">{{ $order->email }}</a></p>
                <p><b>Indirizzo:</b> {{ $order->address }} - {{ $order->CAP }}, {{ $order->city }}, {{ $order->state }}</p>
              </div>

              @if ($order->same_as_shipping != 1)
                <h3>Dati Fatturazione</h3>
                <hr>
                <p><b>Nome e Cognome:</b> {{ $order->b_name }}, {{ $order->b_surname }}</p>
                <p><b>Azienda:</b> {{ $order->company_name }}</p>
                <p><b>P.IVA:</b> {{ $order->vat_number }}</p>
                <p><b>Numero di Telefono:</b> {{ $order->b_phone }}</p>
                <p><b>Email:</b> <a href="mailto:{{ $order->b_email }}">{{ $order->b_email }}</a></p>
                <p><b>Indirizzo:</b> {{ $order->b_address }} - {{ $order->b_CAP }}, {{ $order->b_city }}, {{ $order->b_state }}</p>
              @endif

              <div class="btn-group">
                <button type="submit" name="button" class="btn btn-primary w-50"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salva</button>
                <a href="{{ route('admin.cinestore.index') }}" class="btn btn-danger w-50"><i class="fa fa-times"></i> Chiudi</a>
              </div>
            </form>
          </div>
        </div>
    </div>
</div>
@endsection
