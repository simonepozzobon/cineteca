@extends('layouts.admin')
@section('title', 'Finanziatori')
@section('content')
<div class="row">
  <div class="col">
    <div id="app" class="container-fluid bg-faded p-4">
      <funders />
    </div>
  </div>
</div>
@endsection
@section('scripts')
  <script src="/js/admin-funders.js"></script>
@endsection
