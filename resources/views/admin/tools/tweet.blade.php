@extends('layouts.admin')
@section('title', 'Twitter')
@section('content')
<div class="row">
  <div class="col-md-8">
    <div class="container-fluid p-4 bg-faded">
      <h3>Contenuto</h3>
      <hr>
      <table class="table table-hover">
        <thead>
          <th>Stato</th>
          <th>Data</th>
          <th>Img</th>
          <th>Testo</th>
          <th></th>
        </thead>
        <tbody>
          @foreach ($tweets as $key => $tweet)
            <tr class="align-middle">
              <td class="align-middle">
                <h4>
                  @if ($tweet->status_id == 1)
                    <i class="fa fa-clock-o text-info" aria-hidden="true"></i>
                  @elseif ($tweet->status_id == 2)
                    <i class="fa fa-twitter text-success" aria-hidden="true"></i>
                  @elseif ($tweet->status_id == 3)
                    <i class="fa fa-minus-circle text-danger" aria-hidden="true"></i>
                  @endif
                </h4>
              </td>
              <td class="align-middle">{{ $tweet->diffForHumans }}</td>
              <td class="align-middle">
                <img src="{{ Storage::disk('local')->url($tweet->media()->first()->thumb) }}" alt="{{ $tweet->id }}" class="img-fluid" width="200">
              </td>
              <td class="align-middle">{{ $tweet->text }}</td>
              <td class="align-middle">
                <div class="btn-group">
                  <a class="btn btn-info" href="#">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                  </a>
                  <a href="#" target="_blank" class="btn btn-warning">
                    <i class="fa fa-eye" aria-hidden="true"></i>
                  </a>
                  <a class="btn btn-danger text-white">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                  </a>
                </div>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-md-4">
    <div class="container-fluid p-4 bg-faded">
      <h3>Azioni</h3>
      <hr>
      <a href="{{ route('admin.twitter.tweet') }}" class="btn btn-block btn-primary"><i class="fa fa-search" aria-hidden="true"></i> Cerca Nuovi Tweet</a>
    </div>
  </div>
</div>
@endsection
