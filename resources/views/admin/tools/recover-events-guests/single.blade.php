@extends('layouts.admin')
@section('title', 'Recupera Lista Ospiti')
@section('content')
  <div class="row">
    <div class="col-md-8">
      <div class="container-fluid bg-faded p-4">
        <h3>Lista Ospiti all'evento {{ $id }}</h3>
        <hr>
        <table class="table table-hover">
          <thead>
            <th>id</th>
            <th>Nome</th>
            <th>Cognome</th>
            <th>Telefono</th>
            <th>Email</th>
            <th>Bambini</th>
            <th>Ospiti</th>
          </thead>
          <tbody>
            @foreach ($guests as $key => $guest)
              <tr>
                <td>{{ $guest->id }}</td>
                <td>{{ $guest->name }}</td>
                <td>{{ $guest->surname }}</td>
                <td>{{ $guest->phone }}</td>
                <td>{{ $guest->email }}</td>
                <td>{{ $guest->kids }}</td>
                <td>{{ $guest->guests }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
        <div class="d-flex justify-content-center">
          <a href="{{ route('admin.recover.guests_events') }}" class="btn btn-primary">Torna Indietro</a>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="container-fluid bg-faded p-4">
        <h4>Recupero</h4>
        <hr>
        <p class="pt-4 text-muted">
          <i>
            <i class="fa fa-exclamation-triangle"></i>
            Purtroppo l'evento non è recuperabile in alcun modo.
            Per ripristinare la situazione iniziale bisogna individuare l'id risalendo dalla data dell'evento in base alle poche informazioni disponibili:
            <ul class="text-muted">
              <li><strong>Data della prima registrazione di un ospite</strong>, che molto probabilmente sarà vicina alla data di pubblicazione dell'evento</li>
              <li><strong>Numero di ospiti</strong></li>
              <li><strong>Nome degli ospiti</strong></li>
              <li><strong>Immagine</strong> (se disponibile)</li>
            </ul>
          </i>
        </p>
        <p>
          <i>
            Passaggi da seguire per il ripristino
            <ol>
              <li>Individuare l'id dell'evento come descritto sopra</li>
              <li>Cliccare sul pulsante qui sotto e ricreare l'evento</li>
              <li>Creato l'evento sarà possibile rivedere gli ospiti come per tutti gli altri eventi</li>
            </ol>
          </i>
        </p>
        <div class="d-flex justify-content-center">
          {{-- <a href="{{ route('admin.restore_events.single', $id) }}" class="btn btn-primary">Ripristina Evento</a> --}}
          <a href="#" class="btn btn-primary disabled">Ripristina Evento (non disponibile)</a>
        </div>
      </div>
    </div>
  </div>
@endsection
