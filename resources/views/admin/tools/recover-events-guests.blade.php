@extends('layouts.admin')
@section('title', 'Recupera Ospiti Eventi')
@section('content')
  <div class="row">
    <div class="col">
      <div class="container-fluid bg-faded p-4">
        <h3>Eventi Recuperati (eliminati) - <small>in totale {{ $results['not_matched']->count() }}</small></h3>
        <hr>
        <table class="table table-hover">
          <thead>
            <th>Id</th>
            <th>immagine</th>
            <th>Data di creazione (stimata)</th>
            <th>Numero di ospiti registrati</th>
          </thead>
          <tbody>
            @foreach ($results['not_matched'] as $key => $restored)
              <tr>
                <td>{{ $restored->id }}</td>
                <td>
                  @if (isset($restored->medias->first()->thumb))
                    <img src="{{ Storage::disk('local')->url($restored->medias->first()->thumb) }}" alt="" class="img-rounded center-block">
                  @else
                    No image
                  @endif
                </td>
                <td>{{ $restored->created_at }}</td>
                <td>{{ $restored->guests_count }}</td>
                <td>
                  <a href="{{ route('admin.recover.guests_events.single', $restored->id) }}" class="btn btn-warning">Vedi Ospiti Registrati</a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="row pt-4">
    <div class="col">
      <div class="container-fluid bg-faded p-4">
        <h3>Eventi con un riscontro</h3>
        <hr>
        <table class="table table-hover">
          <thead>
            <th>id</th>
            <th>title</th>
          </thead>
          <tbody>
            @foreach ($results['matching'] as $key => $match)
              <tr>
                <td>{{ $match->id }}</td>
                <td>{{ $match->title }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection
