@extends('layouts.admin')
@section('title', 'Gallerie')
@section('content')
<div id="app">
  <div class="row pb-4">
    <div class="col">
      <a href="{{ route('gallery.create') }}" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Crea Galleria</a>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <div class="container-fluid bg-faded p-4">
        <h3>Gallerie Immagini</h3>
        <hr>
        <galleries galleries="{{ $galleries }}"></galleries>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
  <script src="/js/galleries-index.js"></script>
@endsection
