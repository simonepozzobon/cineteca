@extends('layouts.admin')
@section('title', 'Galleria')
@section('content')

    <div class="row">
      <div class="col-md-8">
        <div class="container-fluid bg-faded p-4">
          <h3>Crea Galleria</h3>
          <hr>
          <!-- Button trigger modal -->
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadMedia">
            <i class="fa fa-upload" aria-hidden="true"></i> Immagine
          </button>

          <!-- Modal -->
          <div class="modal fade align-middle" id="uploadMedia" tabindex="-1" role="dialog" aria-labelledby="uploadMediaLabel" aria-hidden="true">
            <div id="media-wrapper" class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <form id="upload-form" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  {{ method_field('POST') }}
                  <div class="modal-header">
                    <h5 class="modal-title" id="uploadFormModalLabel"><i class="fa fa-upload" aria-hidden="true"></i> Carica Una Nuova Immagine</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <h6 class="text-center">Selezione l'immagine da caricare</h6>
                      <div class="form-group">
                        <input id="media" type="file" class="form-control" name="media">
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"> Annulla</i></button>
                    <button id="uploadForm" type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"> Salva</i></button>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <!-- Button trigger modal -->
          <button type="button" class="btn btn-info" data-toggle="modal" data-target="#mediaLibrary">
            <i class="fa fa-plus" aria-hidden="true"></i> Libreria Immagini
          </button>

          <!-- Modal -->
          <div class="modal fade" id="mediaLibrary" tabindex="-1" role="dialog" aria-labelledby="mediaLibraryLabel" aria-hidden="true">
            <div id="media-wrapper" class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="mediaLibraryLabel"><i class="fa fa-hdd-o" aria-hidden="true"></i> Libreria Immagini</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div id="media-grid" class="row">
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                  <button type="button" class="btn btn-primary">Salva</button>
                </div>
              </div>
            </div>
          </div>
          <form class="" action="{{ route('gallery.store') }}" method="post">
            {{ csrf_field() }}
            {{ method_field('POST') }}
          <div id="gallery-imgs" class="row">

          </div>
          <select id="imgs" class="d-none" name="imgs[]" multiple>
            @foreach ($medias as $key => $media)
              <option id="slct-img-{{ $media->id }}" value="{{ $media->id }}"></option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <div class="container-fluid bg-faded p-4">
          <h3>Dettagli</h3>
          <hr>
          <div class="form-group">
            <h6>Titolo</h6>
            <input type="text" name="title" class="form-control">
          </div>
          <div class="form-group">
            <h6>Descrizione</h6>
            <textarea name="description" cols="30" rows="10" class="form-control"></textarea>
          </div>
          <div class="form-group">
            <h6>Seleziona Pagina</h6>
            <select id="page" class="form-control" name="page">
              <option value=""></option>
              @foreach ($pages as $key => $page)
                <option value="{{ $page->id }}">{{ $page->title }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <h6>Seleziona Evento</h6>
            <select id="event" class="form-control" name="event">
              <option value=""></option>
              @foreach ($events as $key => $event)
                <option value="{{ $event->id }}">{{ $event->title }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <h6>Seleziona Pagina Statica</h6>
            <select id="static_page" class="form-control" name="static_page">
              <option value=""></option>
              @foreach ($static_pages as $key => $static_page)
                <option value="{{ $static_page->id }}">{{ $static_page->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <h6>Seleziona Rassegna</h6>
            <select id="exhibition" class="form-control" name="exhibition">
              <option value=""></option>
              @foreach ($exhibitions as $key => $exhibition)
                <option value="{{ $exhibition->id }}">{{ $exhibition->title }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <h6>Seleziona Esposizione</h6>
            <select id="exhibit" class="form-control" name="exhibit">
              <option value=""></option>
              @foreach ($exhibits as $key => $exhibit)
                <option value="{{ $exhibit->id }}">{{ $exhibit->title }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <h6>Seleziona Festival</h6>
            <select id="festival" class="form-control" name="festival">
              <option value=""></option>
              @foreach ($festivals as $key => $festival)
                <option value="{{ $festival->id }}">{{ $festival->title }}</option>
              @endforeach
            </select>
          </div>
          <button type="submit" name="button" class="btn btn-primary btn-block"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salva</button>
        </div>
      </div>
    </div>
  </form>
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
  <script type="text/javascript">
    $('#page').select2();
    $('#event').select2();
    $('#static_page').select2();
    $('#exhibition').select2();
    $('#exhibit').select2();
    $('#festival').select2();

    $('#mediaLibrary').on('show.bs.modal', function (e) {
        var count = loadMore(0);
        $(this).scroll(function() {
           if($(this).scrollTop() > $('#media-grid').height() - 600) {
            count = loadMore(count + 30);
           }
        });
    });


    function loadMore(objs)
    {
      var data = {
        '_token' : $('meta[name="csrf-token"]').attr('content'),
        'objs' : objs,
      };

      var count = 0;

      $.ajax({
        type: 'post',
        url:  '{{ route('media.loadmore') }}',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: data,
        success: function (response) {
          console.log(response);
          objs = objs + response.newObjs;
          count = response.newObjs;
          for (var i = 0; i < response.newObjs; i++) {
            var data = '';
            data += '<div class="col-md-4 pb-4">';
            data +=   '<div class="container p-3 bg-faded">';
            data +=     '<img src="'+response.imgs[i].url+'" class="img-fluid w-100">';
            data +=     '<button type="button" onclick="selectMedia('+response.imgs[i].id+', \''+response.imgs[i].url+'\')" class="btn btn-info btn-block mt-3"><i class="fa fa-check" aria-hidden="true"></i> Seleziona</a>';
            data +=   '</div>';
            data += '</div>';
            $('#media-grid').append(data);
          }
        }
      });

      return objs;
    }

    function selectMedia(id, url)
    {
      $('#mediaLibrary').modal('hide');

      var data = '';
      data += '<div id="img-'+id+'" class="col-md-3 pb-4">';
      data +=   '<div class="container p-3 bg-faded">';
      data +=     '<img src="'+url+'" class="img-fluid w-100">';
      data +=     '<button type="button" onclick="removeMedia(\'#img-'+id+'\')" class="btn btn-danger btn-block mt-3"><i class="fa fa-trash" aria-hidden="true"></i> Rimuovi</a>';
      data +=   '</div>';
      data += '</div>';

      $('#gallery-imgs').append(data);
      $('#slct-img-'+id).attr('selected', 'selected');
    }

    function removeMedia(id)
    {
      $(id).remove();
    }

    $('form#upload-form').submit(function(event) {
      event.preventDefault();
      $('#uploadMedia').modal('hide');

      var formData = new FormData();
      formData.append('_token', $('meta[name="csrf-token"]').attr('content'));
      formData.append('media', $('#media')[0].files[0]);

      console.log(formData);
      $.ajax({
        type: 'post',
        url:  '{{ route('gallery.upload') }}',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
          console.log(response);
          var data = '';
          data += '<div id="img-'+response.id+'" class="col-md-3 pb-4">';
          data +=   '<div class="container p-3 bg-faded">';
          data +=     '<img src="'+response.url+'" class="img-fluid w-100">';
          data +=     '<button type="button" onclick="removeMedia(\'#img-'+response.id+'\')" class="btn btn-danger btn-block mt-3"><i class="fa fa-trash" aria-hidden="true"></i> Rimuovi</a>';
          data +=   '</div>';
          data += '</div>';

          $('#gallery-imgs').append(data);
          $('#imgs').append('<option id="slct-img-'+response.id+'" value="'+response.id+'" selected="selected"></option>')
        },
        error: function (errors) {
          console.log(errors);
        }
      });
    });
  </script>
@endsection
