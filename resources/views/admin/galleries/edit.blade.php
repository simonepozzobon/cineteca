@extends('layouts.admin')
@section('title', 'Galleria')
@section('content')
  <div id="app">
    <div class="row">
      <div class="col-md-8">
        <div class="container-fluid bg-faded p-4">
          <h3>Crea Galleria</h3>
          <hr>
          <upload-image images="{{ $medias }}" gallery="{{ $gallery }}"></upload-image>
          <hr>
          <list images="{{ $gallery->medias }}" gallery="{{ $gallery }}"></list>
        </div>
      </div>
      <div class="col-md-4">
        <div class="container-fluid bg-faded p-4">
          <h3>Dettagli</h3>
          <hr>
          {{-- <gallery-details gallery="{{ $gallery }}"></gallery-details> --}}
          <form class="" action="{{ route('gallery.update', $gallery->id) }}" method="post">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <input type="text" id="gallery-ids">
            <div class="form-group">
              <h6>Titolo</h6>
              <input type="text" name="title" class="form-control" value="{{ $gallery->title }}">
            </div>
            <div class="form-group">
              <h6>Descrizione</h6>
              <textarea name="description" cols="30" rows="10" class="form-control">{{ $gallery->description }}</textarea>
            </div>
            <div class="form-group">
              <h6>Seleziona Pagina</h6>
              <select id="page" class="form-control" name="page">
                <option value=""></option>
                @foreach ($pages as $key => $page)
                  <option value="{{ $page->id }}" {{ $link->type === 'page' && $link->id === $page->id ? 'selected' : '' }}>{{ $page->title }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <h6>Seleziona Evento</h6>
              <select id="event" class="form-control" name="event">
                <option value=""></option>
                @foreach ($events as $key => $event)
                  <option value="{{ $event->id }}" {{ $link->type === 'event' && $link->id === $event->id ? 'selected' : '' }}>{{ $event->title }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <h6>Seleziona Pagina Statica</h6>
              <select id="static_page" class="form-control" name="static_page">
                <option value=""></option>
                @foreach ($static_pages as $key => $static_page)
                  <option value="{{ $static_page->id }}" {{ $link->type === 'static' && $link->id === $static_page->id ? 'selected' : '' }}>{{ $static_page->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <h6>Seleziona Rassegna</h6>
              <select id="exhibition" class="form-control" name="exhibition">
                <option value=""></option>
                @foreach ($exhibitions as $key => $exhibition)
                  <option value="{{ $exhibition->id }}" {{ $link->type === 'exhibition' && $link->id === $exhibition->id ? 'selected' : '' }}>{{ $exhibition->title }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <h6>Seleziona Esposizione</h6>
              <select id="exhibit" class="form-control" name="exhibit">
                <option value=""></option>
                @foreach ($exhibits as $key => $exhibit)
                  <option value="{{ $exhibit->id }}" {{ $link->type === 'exhibit' && $link->id === $exhibit->id ? 'selected' : '' }}>{{ $exhibit->title }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <h6>Seleziona Festival</h6>
              <select id="festival" class="form-control" name="festival">
                <option value=""></option>
                @foreach ($festivals as $key => $festival)
                  <option value="{{ $festival->id }}">{{ $festival->title }}</option>
                @endforeach
              </select>
            </div>
            <button type="submit" name="button" class="btn btn-primary btn-block"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salva</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('scripts')
  <script src="/js/galleries.js"></script>
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <script src="{{ asset('plugins/select2/js/select2.min.js') }}"></script>
  <script type="text/javascript">
    $('#page').select2();
    $('#event').select2();
    $('#static_page').select2();
    $('#exhibition').select2();
    $('#exhibit').select2();
    $('#festival').select2();
  </script>
@endsection
