<nav class="main-nav navbar navbar-light bg-faded navbar-toggleable-md fixed-top">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#containerNavbar" aria-controls="containerNavbar" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#">Admin Panel</a>
  @if (!isset($page_active))
    {{ $page_active = '' }}
  @endif

  <div class="collapse navbar-collapse justify-content-md-start" id="containerNavbar">
    <ul class="navbar-nav">
      <li class="nav-item {{ $page_active === "posts" ? "active" : "" }}">
        <a class="nav-link" href="{{ url('/') }}">Vai al sito</a>
      </li>
    </ul>
  </div>

  <div class="collapse navbar-collapse justify-content-md-end" id="containerNavbar">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="contentDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Contenuti
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="contentDropdown">
          <a class="dropdown-item" href="{{ route('posts.index') }}">Contenuti</a>
          <a class="dropdown-item" href="{{ route('pages.index') }}">Pagine</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route('homeslider.index') }}">Home Slider</a>
          <a class="dropdown-item" href="{{ route('admin.funders.index') }}">Cinetecari</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="mediaDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Media
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="mediaDropdown">
          <a class="dropdown-item" href="{{ route('media.index') }}">Immagini</a>
          <a class="dropdown-item" href="{{ route('gallery.index') }}">Gallerie</a>
          <a class="dropdown-item" href="{{ route('files.index') }}">File</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="cinestoreDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cinestore
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="cinestoreDropdown">
          <a class="dropdown-item" href="{{ route('admin.cinestore.index') }}">Ordini</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{ route('admin.cinestore.order_products') }}">Ordina prodotti</a>
          <a class="dropdown-item" href="{{ route('cinestore.slider.index') }}">Cinestore Slider</a>
          <a class="dropdown-item" href="{{ route('cinestore.featured.index') }}">Cinestore Evidenza</a>
          <a class="dropdown-item" href="{{ route('admin.cinestore.shipping') }}">Spedizioni</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="socialDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Social
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="socialDropdown">
          <a class="dropdown-item" href="{{ route('admin.twitter.index') }}">Twitter</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="ToolsDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Strumenti
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="ToolsDropdown">
          <a class="dropdown-item" href="{{ route('admin.recover.guests_events') }}">Recupera Ospiti</a>
        </div>
      </li>
      {{-- <li class="nav-item">
        <a class="nav-link" href="{{ route('homeslider.index') }}">Home Slider</a>
      </li> --}}
    </ul>
  </div>
</nav>
