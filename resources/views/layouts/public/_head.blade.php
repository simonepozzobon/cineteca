<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="BH050ogg4vGRm3YqxKWv3OWKlBdqDGO-S5iLgIJAbdI" />
    @if ($active === 'home')
      <meta name="description" content="Dal 1947 Fondazione Cineteca Italiana svolge un’ininterrotta attività di diffusione della cultura cinematografica in Italia e all’estero. Vieni da noi a scoprire la magia del cinema." />
    @else
      @yield('seo')
    @endif
    {{-- csrf token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="57x57" href="/img/logo/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/logo/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/logo/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/logo/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/logo/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/logo/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/logo/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/logo/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/logo/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/logo/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/logo/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/logo/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/logo/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <title>Cineteca Milano - @yield('title')</title>

    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    {{-- csrfToken for Json --}}
    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};
    </script>
    <script src="/js/manifest.js"></script>
    <script src="/js/vendor.js"></script>
    <script src="/js/app.js"></script>
</head>
