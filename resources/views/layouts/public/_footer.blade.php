<footer class="footer">
  <div class="row no-gutters">
    <div class="col">
      {{-- Footer --}}
      <div class="bg-inverse text-white">
        <div class="row p-5">
          <div class="col-md-3">
            <div class="row mb-2">
              <div class="col">
                <h5>Dove</h5>
                <p class="text-muted">
                  Fondazione Cineteca Italiana<br>
                  Manifattura Tabacchi<br>
                  viale Fulvio Testi 121<br>
                  Milano
                  <br><br>
                  MM Bicocca
                </p>
                <ul class="list-unstyled">
                  <li><a class="text-muted" href="https://goo.gl/maps/Ei9uT2sdkXB2" target="_blank">Vedi Mappa</a></li>

                </ul>
              </div>
            </div>
            <div class="row mb-2">
              <div class="col">
                <h5>About</h5>
                <ul class="list-unstyled">
                  <li><a class="text-muted" href="{{ url('/pagina/chi-siamo') }}">Chi siamo</a></li>
                  <li><a class="text-muted" href="{{ route('amministrazione') }}">Amministrazione Trasparente</a></li>
                </ul>
              </div>
            </div>
            <div class="row mb-2">
              <div class="col">
                <h5>Organizza Un Evento</h5>
                <ul class="list-unstyled">
                  <li><a class="text-muted" href="{{ url('/pagina/organizza-un-evento') }}">Scopri come organizzare un evento</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="row mb-2">
              <div class="col">
                <h5>Supportaci</h5>
                <ul class="list-unstyled">
                  <li><a class="text-muted" href="{{ url('/pagina/5x1000') }}">Dona il 5x1000</a></li>
                </ul>
              </div>
            </div>
            <div class="row mb-2">
              <div class="col">
                <h5>Servizio Civile</h5>
                <ul class="list-unstyled">
                  <li><a class="text-muted" href="{{ url('/pagina/servizio-civile-nazionale') }}">Diventa volontario</a></li>
                </ul>
              </div>
            </div>
            <div class="row mb-2">
              <div class="col">
                <h5>Stampa</h5>
                <ul class="list-unstyled">
                  <li><a class="text-muted" href="mailto:ufficiostampa@cinetecamilano.it?subject=Inserimento in mailing list press">Ricevi Comunicati Stampa</a></li>
                  <li><a class="text-muted" href="{{ route('press.index') }}">Rassegna Stampa</a></li>
                </ul>
              </div>
            </div>
            <div class="row mb-2">
              <div class="col">
                <h5>Sale Cinematografiche</h5>
                <ul class="list-unstyled">
                  <li><a class="text-muted" href="{{ route('single.page', 'ingresso-nelle-sale-cinematografiche-di-cineteca') }}">Modalità di ingresso</a></li>
                </ul>
              </div>
            </div>
            @if(App\StaticPage::footerGallery()->count() > 0)
              <div class="row mb-2">
                <div class="col">
                  <h5>Galleria</h5>
                  <ul class="list-unstyled">
                    <li><a class="text-muted" href="{{ route('public.gallery.index') }}">Galleria</a></li>
                  </ul>
                </div>
              </div>
            @endif
          </div>
          <div class="col-md-3">
            <div class="row">
              <div class="col">
                <h5>Contatti Veloci</h5>
                <p class="text-muted">
                  Office Cineteca<br>
                  02 8724 2114<br>
                  <br>
                  Press Office<br>
                  02 8724 2114<br>
                  <a href="mailto:ufficiostampa@cinetecamilano.it" class="text-muted">ufficiostampa@cinetecamilano.it</a><br>
                  <br>
                  Info<br>
                  <a href="mailto:info@cinetecamilano.it" class="text-muted">info@cinetecamilano.it</a>
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="row mb-2">
              <div class="col">
                <h5>Partners</h5>
              </div>
            </div>
            <div class="row pb-3">
              <div class="col-6">
                <img src="{{ asset('img/partner/logo-regione-lombardia.png') }}" alt="Regione Lombardia" class="img-fluid w-50">
              </div>
              <div class="col-6">
                <img src="{{ asset('img/partner/logo-citta-metropolitana-di-milano.png') }}" alt="Città Metropolitana di Milano" class="img-fluid w-50">
              </div>
            </div>

            <div class="row pb-3">
              <div class="col-6">
                <img src="{{ asset('img/partner/logo-comune-di-milano.png') }}" alt="Comune di Milano" class="img-fluid w-50">
              </div>
              <div class="col-6">
                <img src="{{ asset('img/partner/logo-citta-di-paderno-dugnano.png') }}" alt="Città di Paderno Dugnano" class="img-fluid w-50">
              </div>
            </div>

            <div class="row pb-3">
              <div class="col-6">
                <img src="{{ asset('img/partner/logo-MiBACT.png') }}" alt="Ministero dei Beni e delle Attività Culturali" class="img-fluid w-50">
              </div>
              <div class="col-6">
                <img src="{{ asset('img/partner/logo-siae.png') }}" alt="Siae" class="img-fluid w-50">
              </div>
            </div>

            <div class="row pb-3">
              <div class="col-6">
                <img src="{{ asset('img/partner/logo-epson.png') }}" alt="Epson" class="img-fluid w-50">
              </div>
              <div class="col-6">
                <img src="{{ asset('img/partner/logo-ace.png') }}" alt="Association des Cinematheques Europeennes" class="img-fluid w-50">
              </div>
            </div>

            <div class="row pb-3">
              <div class="col-6">
                <img src="{{ asset('img/partner/logo-fiaf.png') }}" alt="Fiaf" class="img-fluid w-50">
              </div>
              <div class="col-6">
                <img src="{{ asset('img/partner/logo-europacinemas.png') }}" alt="Europa Cinemas" class="img-fluid w-50">
              </div>
            </div>

            <div class="row pb-3">
              <div class="col-6">
                <img src="{{ asset('img/partner/logo-ecfa.png') }}" alt="European Children's Film Association" class="img-fluid w-50">
              </div>
              <div class="col-6">
                <img src="{{ asset('img/partner/logo-novotel.png') }}" alt="Novotel" class="img-fluid w-50">
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
