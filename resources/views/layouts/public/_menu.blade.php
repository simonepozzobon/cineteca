<nav id="main-nav" class="main-nav navbar sticky-top navbar-light bg-faded navbar-toggleable-md">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#containerNavbar" aria-controls="containerNavbar" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="{{ url('/') }}">@include('components.logo')</a>

  <div class="collapse navbar-collapse justify-content-md-end" id="containerNavbar">
    <ul class="navbar-nav">
      @if (Auth::guard('admin')->check())
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/admin') }}">Admin Panel</a>
        </li>
      @endif
      <li class="nav-item {{ $active == 'home' ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">{{ $active == 'home' ? '(current)' : '' }}</span></a>
      </li>
      <li class="nav-item {{ $active == 'proiezioni' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('film.index') }}">Proiezioni <span class="sr-only">{{ $active == 'proiezioni' ? '(current)' : '' }}</span></a>
      </li>
      <li class="nav-item {{ $active == 'rassegne' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('exhibitions.index') }}">Rassegne  <span class="sr-only">{{ $active == 'rassegne' ? '(current)' : '' }}</span></a>
      </li>
      <li class="nav-item {{ $active == 'museo' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('museum.index') }}">Museo <span class="sr-only">{{ $active == 'museo' ? '(current)' : '' }}</span></a>
      </li>
      <li class="nav-item {{ $active == 'scuole' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('schools.index') }}">Scuole  <span class="sr-only">{{ $active == 'biblioteca' ? '(current)' : '' }}</span></a>
      </li>
      <li class="nav-item {{ $active == 'biblioteca' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('biblio.index') }}">Biblioteca  <span class="sr-only">{{ $active == 'biblioteca' ? '(current)' : '' }}</span></a>
      </li>
      <li class="nav-item {{ $active == 'archivio' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('archive.index') }}">Archivio <span class="sr-only">{{ $active == 'archivio' ? '(current)' : '' }}</span></a>
      </li>
      <li class="nav-item {{ $active == 'cinestore' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('cinestore.index') }}">Cinestore <span class="sr-only">{{ $active == 'cinestore' ? '(current)' : '' }}</span></a>
      </li>
      <li class="nav-item {{ $active == 'news' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('news.index') }}">News <span class="sr-only">{{ $active == 'news' ? '(current)' : '' }}</span></a>
      </li>
      <li>
        <a id="search-button-listener" class="nav-link search-trigger fullscreen-trigger" href="#"><i id="search-button" class="fa fa-search" aria-hidden="true"></i></a>
      </li>
    </ul>
  </div>
</nav>
@if ($active == 'cinestore' && isset($categories))
  <nav id="cinestore-nav" class="cinestore-nav navbar navbar-inverse bg-dark-gray navbar-toggleable-md">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#cinestoreNavBar" aria-controls="cinestoreNavBar" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-md-end" id="cinestoreNavBar">
      <ul class="navbar-nav">
          @foreach ($categories as $key => $category)
            <li class="nav-item">
              <a class="nav-link text-muted" href="{{ route('cinestore.category', $category->slug) }}">{{ $category->name }}</a>
            </li>
          @endforeach
      </ul>
    </div>
  </nav>
@endif

{{-- <nav id="cinestore-menu" class="navbar-light bg-inverse mr-4 invisible">
  <div class="container-navbar">
    <ul>
      <li class="">
        <a class="nav-link text-white" href="#"><h5><i class="fa fa-shopping-cart" aria-hidden="true"></i> Carrello</h5></a>
      </li>
    </ul>
  </div>
</nav> --}}
<div class="fullscreen-search-overlay" id="search-overlay">
  <a href="#" class="fullscreen-close" id="fullscreen-close-button"><i class="fa fa-times"></i></a>
  <div id="fullscreen-search-wrapper">
    <form method="post" id="fullscreen-searchform" action="{{ route('search.post') }}">
      {{ csrf_field() }}
      {{ method_field('POST') }}
      <input type="text" name="query" placeholder="Cerca..." id="fullscreen-search-input">
      <i class="fa fa-search fullscreen-search-icon"><input type="submit"></i>
    </form>
  </div>
</div>

<div class="fullscreen-cart-overlay" id="cart-overlay">
  <a href="#" class="fullscreen-close" id="fullscreen-close-button"><i class="fa fa-times"></i></a>
  <div id="fullscreen-cart-wrapper" class="text-white">
    <div class="row no-gutters p-5">
      <div class="col-md-8 offset-md-2">
        <h1 class="text-center text-white">Carrello</h1>
        <hr>
        <table id="checkout" class="table table-hover text-white text-align-left">
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
