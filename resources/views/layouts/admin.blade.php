<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
  {{-- Head Section --}}
  @include('layouts.admin._head')

  {{-- Custom styles section --}}
  @yield('stylesheets')

  <body>
    <div class="container-fluid">
        {{-- Menu Section --}}
        @include('layouts.admin._menu')

        <main class="mt-5 p-4">

          {{-- Page Title --}}
          <section id="second-menu">
            <div class="row no-gutters">
              <div class="col pb-3">
                <h2>@yield('page-title')</h2>
              </div>
            </div>
          </section>

          <section>
            @if (session()->has('success'))
              <div class="alert alert-success" role="alert">
                <strong>Success:</strong> {{ session()->get('success') }}
              </div>
            @endif

            @if (count($errors) > 0)
              <div class="alert alert-danger" role="alert">
                  <strong>Errors:</strong>
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
              </div>
            @endif
          </section>

          {{-- Main Section --}}
          <section id="main-content">
            <div class="row no-gutters">
              <div class="col">
                @yield('content')
              </div>
            </div>
          </section>
        </main>

        {{-- Default scripts and footer sections --}}
        @include('layouts.admin._footer')
        @include('layouts.admin._scripts')
    </div>
    {{-- Custom scripts section --}}
    @yield('scripts')
  </body>
</html>
