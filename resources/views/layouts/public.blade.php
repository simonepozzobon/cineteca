@if (!isset($active))
  {{ $active = '' }}
@endif

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

  {{-- Head Section --}}
  @include('layouts.public._head', ['active' => $active])

  {{-- Custom styles section --}}
  @yield('stylesheets')

  {{-- Home Page Main Slider Styles --}}
  @if ($active === 'home' || $active === 'cinestore-home')
    <style media="screen">
      #main-slider {
        position: fixed;
      }

      main {
        z-index: 2;
      }
    </style>
  @endif

  {{-- Pace.js --}}
  @if (isset($loading))
    @if ($loading === 'yes')
      <link rel="stylesheet" href="{{ asset('plugins/pace/theme.css') }}">
    @endif
  @endif
  <body>
    {{-- Pace.js --}}
    @if (isset($loading))
      @if ($loading === 'yes')
        <div class="loading-logo">@include('components.logo')</div>
      @endif
    @endif

    {{-- Menu Section --}}
    @if (! isset($categories))
      @include('layouts.public._menu', ['active' => $active])
    @else
      @include('layouts.public._menu', ['active' => $active, 'categories' => $categories])
    @endif


    @if ($active === 'home')
      <div id="main-slider" class="w-100">
        <div class="row px-2 px-md-5 bg-faded mx-4">
          <div class="col">
            <div class="container-fluid">
              <div id="myCarousel" class="carousel-custom carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="1" class="active"></li>
                  @foreach ($slides as $key => $slide)
                    <li data-target="#myCarousel" data-slide-to="{{ $key + 1 }}"></li>
                  @endforeach
                  <li data-target="#myCarousel" data-slide-to="3" class=""></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                  <div class="carousel-item active">
                    <img class="img-fluid h-100 w-100" src="/img/mic-chiuso.jpg" alt="First slide">
                    <div class="carousel-caption">
                      <h1 class="pt-3">IL MIC VA IN VACANZA...MA LE INIZIATIVE IN CINETECA NON SI FERMANO MAI!</h1>
                      <p class="pt-4"><a class="btn btn-primary" href="{{ url('/news/chiusuramic') }}" role="button"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Scopri di più</a></p>
                    </div>
                  </div>
                  @foreach ($slides as $key => $slide)
                    <div class="carousel-item">
                      <img class="img-fluid h-100 w-100" src="{{ Storage::disk('local')->url($slide['img']) }}" alt="First slide">
                      <div class="carousel-caption">
                        <h1 class="pt-3">{{ $slide['title'] }}</h1>
                        <p class="pt-4"><a class="btn btn-primary" href="{{ url($slide['url']) }}" role="button"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Scopri di più</a></p>
                      </div>
                    </div>
                  @endforeach
                  <div class="carousel-item">
                    <img class="img-fluid h-100 w-100" src="{{ asset('img/museo_slider.jpg') }}" alt="First slide">
                    <div class="carousel-caption">
                      <h1 class="pt-3">MIC - Museo Interattivo del Cinema</h1>
                      <p class="pt-4"><a class="btn btn-primary" href="{{ url('/museo') }}" role="button"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Scopri di più</a></p>
                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev text-default" href="#myCarousel" role="button" data-slide="prev">
                  <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next text-default" href="#myCarousel" role="button" data-slide="next">
                  <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    @endif
    <div id="main-wrapper" class="mx-4 container-fluid">
        {{-- Main Section --}}
        <main class="bg-faded {{ $active === 'home' ? 'pt-md-4' : '' }} {{ $active === 'cinestore' ? 'mt-5 pt-5' : '' }}">
          @yield('content')

          @if (isset($teacher))
            @include('layouts.public._newsletter', ['teacher' => $teacher])
          @else
            @include('layouts.public._newsletter')
          @endif
        </main>

        {{-- Default scripts and footer sections --}}
        @include('layouts.public._footer')
        @include('layouts.public._scripts')
    </div>
    @yield('scripts')

    {{-- Custom scripts section --}}

    {{-- Main slider Script  --}}
    @if ($active === 'home')
      <script type="text/javascript">
        $(document).ready(function() {
          getSize();

          $(window).resize(function() {
            getSize();
          });

          function getSize() {
            var mainNav = $('.main-nav').outerHeight();
            var mainSlider = $('#main-slider').outerHeight();
            $('#main-slider').css('top', mainNav);
            $('#main-wrapper').css('margin-top', mainSlider+mainNav);
          }
        });
      </script>
    @endif

    {{-- Pace.js --}}
    @if (isset($loading))
      @if ($loading === 'yes')
        <script src="{{ asset('plugins/pace/pace.min.js') }}"></script>
        <script src="{{ asset('plugins/pace/load.js') }}"></script>
      @endif
    @endif

    {{-- Analytics --}}
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-102499592-1', 'auto');
      ga('send', 'pageview');

    </script>
  </body>
</html>
