@extends('layouts.public')
@section('title', "Galleria")
@section('seo')
  <meta name="description" content="Galleria immagini della Fondazione Cineteca Italiana" />
@endsection
@section('content')
  <section>
    <div class="row no-gutters">
      <div class="col pt-5">
        <div class="row pt-md-5">
          <div class="col pt-5 px-5">
            <h1 align="center">Galleria Immagini</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-6 offset-3">
            <hr>
          </div>
        </div>
      </div>
    </div>
    @foreach ($galleries as $key => $gallery)
      <div class="row">
        <div class="col-md-10 offset-md-1">
          @include('components.main.title', ['title' => $gallery->title])
          @if ($gallery->description && strlen($gallery->description) > 1)
            <div class="row">
                <div class="col-md-8 offset-md-2 px-md-5 px-4">
                  <p>{{ $gallery->description }}</p>
                </div>
            </div>
          @endif
          <div class="row gallery">
            @foreach ($gallery->medias()->get() as $key => $media)
              <div class="col-md-4 pb-4">
                  <a href="{{ Storage::disk('local')->url($media->img) }}">
                    <img class="img-fluid w-100" src="{{ Storage::disk('local')->url($media->thumb) }}" alt="">
                  </a>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    @endforeach
  </section>
  @include('components.main.back')
@endsection
