@extends('layouts.public')
@section('title', 'Famiglie')
@section('seo')
  <meta name="description" content="Tutte le attibità per le famiglie in programmazione alla Fondazione Cineteca Italiana di Milano" />
@endsection
@section('content')
<section id="slider">
  @include('components.main.slider_collection', ['type' => '', 'button' => '', 'items' => $items, 'root' => ''])
</section>
<section id="pages">
  @include('components.main.square-box', ['title' => 'Attività per le Famiglie', 'items' => $pages, 'type' => 'pagina'])
</section>
<section id="events">
  <div class="row pt-5">
    <div class="col-md-10 offset-md-1">
      @if ($items->count() > 0)
        <h5>Attività</h5>
        <hr>
        @foreach ($items as $key => $item)
          <div class="row">
            <div class="col-md-2">
              <h5><span class="badge badge-default" style="background-color:{{ $item['color'] }}">{{ $item['type'] }}</span></h5>
            </div>
            <div class="col-md-4">
              <img src="{{ Storage::disk('local')->url($item['img']) }}" class="img-fluid">
            </div>
            <div class="col-md-6">
              <h3>{{ $item['title'] }}</h3>
              <p>{{ substr(strip_tags($item['description']), 0, 200) }}{{ strlen(strip_tags($item['description'])) > 200 ? '...' : "" }}</p>
              <a href="{{ url($item['slug']) }}" class="btn btn-primary">Scopri di più</a>
            </div>
          </div>
          <div class="row pb-5">
            <div class="col-md-6 offset-md-3">
              <hr>
            </div>
          </div>
        @endforeach
      @else
        <h1 class="text-center p-5"><i class="fa fa-meh-o" aria-hidden="true"></i></h1>
        <h3 class="text-center pb-5">Ooops, nessuna attività trovata al momento</h3>
        <div class="row p-5">
          <div class="col-md-4 offset-md-4">
            <a class="btn btn-primary btn-block" href="{{ url()->previous() }}">
              <i class="fa fa-arrow-left" aria-hidden="true"></i> Indietro
            </a>
          </div>
        </div>
      @endif
    </div>
  </div>
  @include('components.main.back')
</section>
@endsection
