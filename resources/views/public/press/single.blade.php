@extends('layouts.public')
@section('title', "Rassegna Stampa - $release->title")
@section('seo')
  <meta name="description" content="Rassegna Stampa della Fondazione Cineteca Italiana - {{ $release->description }}" />
@endsection
@section('content')
  <section>
    <div class="row no-gutters">
      <div class="col pt-5">
        <div class="row pt-md-5">
          <div class="col pt-5 px-5">
            <h1 align="center">{{ $release->title }}</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-6 offset-3">
            <hr>
          </div>
        </div>
      </div>
    </div>
    <div class="row no-gutters pb-5">
      <div class="col-md-10 offset-md-1">
        @if ($release->media->first()->img != null)
          <img src="{{ Storage::disk('local')->url($release->media->first()->img) }}" class="img-fluid w-100" alt="{{ $release->title }}">
        @endif
      </div>
    </div>
  </section>
@endsection
