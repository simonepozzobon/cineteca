@extends('layouts.public')
@section('title', 'Rassegna Stampa')
@section('seo')
  <meta name="description" content="Rassegna Stampa della Fondazione Cineteca Italiana" />
@endsection
@section('content')
  <section>
    <div class="row no-gutters">
      <div class="col pt-5">
        <div class="row pt-md-5">
          <div class="col pt-5 px-5">
            <h1 align="center">Rassegna Stampa</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-6 offset-3">
            <hr>
          </div>
        </div>
      </div>
    </div>
    <div class="row no-gutters pb-5">
      <div class="col-md-10 offset-md-1">
        <div class="row">
          <div class="col-md-8 offset-md-2">
            @foreach ($releases as $key => $release)
              <div class="row pt-5">
                <div class="col-md-2">
                  <h3>{{ $release->year }}</h3>
                  <p>
                    {{ $release->day_string }}<br>
                    {{ $release->day }} {{ $release->month }}
                  </p>
                </div>
                <div class="col-md-4">
                  @if ($release->media->first()->landscape != null)
                    <img src="{{ Storage::disk('local')->url($release->media->first()->landscape) }}" class="img-fluid w-100" alt="{{ $release->title }}">
                  @endif
                </div>
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-md-8">
                      <h3>
                        <a href="{{ route('press.single', $release->slug) }}">
                          {{ $release->title }}
                        </a>
                      </h3>
                    </div>
                    <div class="col-md-4">
                      <a href="{{ route('press.single', $release->slug) }}" class="btn btn-primary">Leggi L'articolo</a>
                    </div>
                  </div>
                  <p>
                    @foreach ($release->presses()->get() as $key => $press)
                      <span class="badge badge-info">
                        {{ $press->name }}
                      </span>
                    @endforeach
                  </p>
                  <p>{{ substr(strip_tags($release->description), 0, 150) }}{{ strlen(strip_tags($release->description)) > 150 ? '...' : "" }}</p>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
