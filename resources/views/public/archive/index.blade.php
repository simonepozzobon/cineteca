@extends('layouts.public', ['active' => 'archivio'])
@section('title', 'Archivio e MIC Lab')
@section('seo')
  <meta name="description" content="L’archivio storico dei Film di Fondazione Cineteca Italiana è il primo Archivio Film fondato in Italia (1947) e conta 35.000 pellicole cinematografiche." />
@endsection
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/single.css') }}">
@endsection
@section('content')
  <section id="main-image">
    @include('components.main.film-header', ['image' => asset('img/archivio_slide.jpg'), 'title' => 'Archivio e MIC Lab'])
  </section>
  <section id="description">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <p class="pt-5 px-5">
          L’archivio storico dei Film di Fondazione Cineteca Italiana è il primo Archivio Film fondato in Italia (1947) e conta 35.000 pellicole cinematografiche.<br>
          Da sempre la Cineteca collabora con la FIAF (International Federation of Film Archives, di cui è membro dal 1948) e con gli altri archivi film italiani e stranieri per tutelare il restauro dei materiali.
        </p>
      </div>
    </div>
  </section>
  <section>
    @include('components.main.square-box', ['title' => 'Attività dell\'Archivio', 'items' => $pages, 'type' => 'pagina'])
  </section>
  <section>
    @include('components.main.title', ['title' => 'Servizi dell\'archivio film' ])
    <div class="row">
      <div class="col-md-8 offset-md-2">
        @include('components.main.title-h2', ['title' => 'Restauro Digitale'])
          <div class="row">
            <div class="col">
              <p class="px-5 pt-5">
                Accanto all'attività di conservazione delle pellicole cinematografiche, il MIC LAB accoglie le apparecchiature necessarie alla revisione dei film in pellicola (tavoli da ripasso, moviole, visori manuali per i film più fragili) e alla digitalizzazione e al restauro e dei fondi filmici e fotografici (telecinema Sondor ALTRA implementato con scanner 2K, una postazione Phoenix Refine per il restauro, una con DaVinci Resolve per la color correction e diverse altre per la post-produzione).
              </p>
              <p class="px-5 pb-5 text-center">
                Per informazioni sul prestito di materiali filmici scrivere a <a href="mailto:diffusioneculturale@cinetecamilano.it">diffusioneculturale@cinetecamilano.it</a>
              </p>
              <div class="row">
                <div class="col-md-6 offset-md-3">
                  <div class="row">
                    <div class="col-md-8 offset-md-2">
                      <div class="pb-5">
                        <a href="mailto:diffusioneculturale@cinetecamilano.it" class="btn btn-primary btn-block"><i class="fa fa-envelope-o" aria-hidden="true"></i> Contattaci</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        @include('components.main.title-h2', ['title' => 'Prestito e consultazione'])
        <div class="row">
          <div class="col">
            <p class="px-5 pt-5 text-center">
              Prestito di materiali per rassegne cinematografiche e vendita di materiali audiovisivi per mostre, servizi televisi.<br>
            </p>
            <p class="px-5 pb-5 text-center">
              Per richieste di restauro film scrivere a <a href="mailto:diffusioneculturale@cinetecamilano.it">diffusioneculturale@cinetecamilano.it</a>
            </p>
            <div class="row">
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-8 offset-md-2">
                    <div class="pb-5">
                      <a href="cinestore" class="btn btn-primary btn-block disabled"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Cinestore</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-8 offset-md-2">
                    <div class="pb-5">
                      <a href="mailto:diffusioneculturale@cinetecamilano.it" class="btn btn-primary btn-block"><i class="fa fa-envelope-o" aria-hidden="true"></i> Contattaci</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="back">
    @include('components.main.back')
  </section>
@endsection
