@extends('layouts.public', ['active' => 'rassegne'])
@section('title', 'Rassegne')
@section('seo')
  <meta name="description" content="Tutte le rassegne in programmazione alla Fondazione Cineteca Italiana di Milano" />
@endsection
@section('content')
  @if ($exhibitions->count() > 0)
    <section id="film-carousel">
      @include('components.main.slider', ['type' => 'Rassegne', 'button' => 'Rassegna', 'items' => $exhibitions, 'root' => '/rassegna'])
    </section>
  @else
    <div class="py-5"></div>
    @include('components.main.title', ['title' => 'Esposizioni'])
  @endif
  <section id="calendar">
    <div class="row no-gutters pt-5">
      @if ($exhibitions->count() > 0)
        <div class="col-md-10 offset-md-1">
      @else
        <div class="col">
      @endif

      @if ($exhibitions->count() > 0)
        <h5>Rassegne</h5>
        <hr>
        @foreach ($exhibitions as $key => $exhibition)
          <div class="row">
            <div class="col-md-2">
              <h6>Dal</h6>
              <p>{{ $exhibition->start_date }}</p>
              <h6>Al</h6>
              <p>{{ $exhibition->end_date }}</p>
            </div>
            <div class="col-md-4">
              <img src="{{ Storage::disk('local')->url($exhibition->media->first()->landscape) }}" class="img-fluid">
            </div>
            <div class="col-md-6">
              <h3>
                <a href="{{ route('single.exhibition', $exhibition->slug) }}">
                  {{ $exhibition->title }}
                </a>
              </h3>
              @if ($exhibition->kids === 1 )
                <span class="badge" style="background-color:#755482">Adatto ai bambini</span>
              @endif
              <h6>{{ $exhibition->location->name }}</h6>
              <p>{{ substr(strip_tags($exhibition->description), 0, 150) }}{{ strlen(strip_tags($exhibition->description)) > 150 ? '...' : "" }}</p>
              <a href="{{ route('single.exhibition', $exhibition->slug) }}" class="btn btn-primary">Vai alla Rassegna</a>
            </div>
          </div>
          <div class="row pb-3">
            <div class="col-md-8 offset-md-2">
              <hr>
            </div>
          </div>
        @endforeach
      @else
        <h1 class="text-center p-5"><i class="fa fa-meh-o" aria-hidden="true"></i></h1>
        <h3 class="text-center pb-5">Ooops, nessuna rassegna in programmazione</h3>
        <div class="row p-5">
          <div class="col-md-6 offset-md-3">
            <div class="row">
              <div class="col-md-6">
                <a class="btn btn-info btn-block" href="{{ url()->previous() }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
              </div>
              <div class="col-md-6">
                <a class="btn btn-primary btn-block" href="{{ route('exhibitions.archive') }}"><i class="fa fa-archive" aria-hidden="true"></i> Vedi quelle passate</a>
              </div>
            </div>
          </div>
        </div>
      @endif
      </div>
    </div>
    <div class="row p-5">
      <div class="col-sm-2 offset-sm-5">
          {{ $exhibitions->links('vendor.pagination.bootstrap-4') }}
      </div>
    </div>
  </section>
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}">
  <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap-datepicker/locales/bootstrap-datepicker.it.min.js') }}"></script>
  <script type="text/javascript">
  $('#datepicker').datepicker({
    autoclose: true,
    todayHighlight: true,
    language: 'it',
  }).on('changeDate', function(e) {
    var date = $('#datepicker').datepicker('getDate');
    console.log(date);
  });

  </script>
@endsection
