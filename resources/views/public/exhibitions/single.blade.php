@extends('layouts.public', ['active' => 'rassegne'])
@section('title', "$exhibition->title")
@section('seo')
  <meta name="description" content="{{ substr(strip_tags($exhibition->description), 0, 170) }}{{ strlen(strip_tags($exhibition->description)) > 170 ? '...' : "" }}" />
@endsection
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/single.css') }}">
@endsection
@section('content')
  <article class="pb-5">
    @include('components.main.film-header', ['title' => $exhibition->title, 'image' => Storage::disk('local')->url($exhibition->media->first()->slide_img)])
    <div class="row p-5">
      <div class="col-md-7 offset-md-1">
        <div class="row">
          <div class="col">
            <h5>Scheda</h5>
            <hr>
            <p>{!! $exhibition->description !!}</p>
          </div>
        </div>
        @if ($elements->count() > 0)
          <div class="row pt-5">
            <div class="col">
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#calendarSchedule" role="tab">
                    <h5>Calendario Programmazione</h5>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#calendar" role="tab">
                    <h5>Calendario Completo</h5>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#program" role="tab">
                    <h5>Contenuti in Programma</h5>
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane {{ $limitedShows->count() > 0 ? 'active' : '' }} pt-5" id="calendarSchedule" role="tabpanel">
                  @if ($limitedShows->count() > 0)
                    @foreach ($limitedShows as $key => $show)
                      <div class="row">
                        <div class="col-md-2">
                          <h3>{{ $show['time'] }}</h3>
                          <p>
                            {{ $show['day_string'] }}<br>
                            {{ $show['day'] }}, {{ $show['month'] }}
                          </p>
                          @if (isset($show['ticket']) && $show['ticket'] == true)
                            <a href="{{ url($show['ticket_url']) }}" target="_blank" class="btn btn-primary">Acquista Biglietti</a>
                          @endif
                        </div>
                        <div class="col-md-4">
                          <img src="{{ Storage::disk('local')->url($show['img']) }}" class="img-fluid">
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            <div class="col">
                              <h3>
                                <a href="{{ url($show['url']) }}">
                                  {{ $show['title'] }}
                                </a>
                              </h3>
                              @if ($show['type'] == 'Evento')
                                <span class="badge mb-1" style="background-color:{{ $show['color'] }}">{{ $show['type'] }}</span>
                              @endif
                            </div>
                          </div>
                          <h6>{{ $show['location'] }}</h6>
                          <p>
                            @if (isset($show['production_date']) && isset($show['region']))
                              {{ $show['production_date'] }}, {{ $show['region'] }}
                            @endif
                          </p>
                          <p>{{ substr(strip_tags($show['description']), 0, 150) }}{{ strlen(strip_tags($show['description'])) > 150 ? '...' : "" }}</p>
                        </div>
                      </div>
                      <div class="row pb-3">
                        <div class="col-md-8 offset-md-2">
                          <hr>
                        </div>
                      </div>
                    @endforeach
                  @endif
                </div>
                <div class="tab-pane {{ $shows->count() > 0 && $limitedShows->count() == 0 ? 'active' : '' }} pt-5" id="calendar" role="tabpanel">
                  @if ($shows->count() > 0)
                    @foreach ($shows as $key => $show)
                      <div class="row">
                        <div class="col-md-2">
                          <h3>{{ $show['time'] }}</h3>
                          <p>{{ $show['day'] }}, {{ $show['month'] }}</p>
                        </div>
                        <div class="col-md-4">
                          <img src="{{ Storage::disk('local')->url($show['img']) }}" class="img-fluid">
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                            @if (isset($show['ticket']) && $show['ticket'] == true)
                              <div class="col-md-8">
                                <h3>
                                  <a href="{{ url($show['url']) }}">
                                    {{ $show['title'] }}
                                  </a>
                                </h3>
                              </div>
                              <div class="col-md-4">
                                <a href="{{ url($show['ticket_url']) }}" target="_blank" class="btn btn-primary">Acquista Biglietti</a>
                              </div>
                            @else
                              <div class="col">
                                <h3>
                                  <a href="{{ url($show['url']) }}">
                                    {{ $show['title'] }}
                                  </a>
                                </h3>
                                @if ($show['type'] == 'Evento')
                                  <span class="badge mb-1" style="background-color:{{ $show['color'] }}">{{ $show['type'] }}</span>
                                @endif
                              </div>
                            @endif
                          </div>
                          <h6>{{ $show['location'] }}</h6>
                          <p>
                            @if (isset($show['production_date']) && isset($show['region']))
                              {{ $show['production_date'] }}, {{ $show['region'] }}
                            @endif
                          </p>
                          <p>{{ substr(strip_tags($show['description']), 0, 150) }}{{ strlen(strip_tags($show['description'])) > 150 ? '...' : "" }}</p>
                        </div>
                      </div>
                      <div class="row pb-3">
                        <div class="col-md-8 offset-md-2">
                          <hr>
                        </div>
                      </div>
                    @endforeach
                  @endif
                </div>
                <div class="tab-pane {{ $shows->count() == 0 && $limitedShows->count() == 0 ? 'active' : '' }} pt-5" id="program" role="tabpanel">
                  @foreach ($elements as $key => $element)
                    <div class="row">
                      <div class="col-md-4">
                        <img class="w-100 img-fluid" src="{{ Storage::disk('local')->url($element['img']) }}" alt="{{ $element['title'] }}">
                      </div>
                      <div class="col-md-8">
                        <h3>
                          <a href="{{ url($element['url']) }}">{{ $element['title'] }}</a>
                        </h3>
                        <span class="badge" style="background-color:{{ $element['color'] }}">{{ $element['type'] }}</span>
                        @if ($element['kids'] === 1)
                          <span class="badge" style="background-color:#755482">Adatto ai bambini</span>
                        @endif
                        @if ($element['type'] == 'Film')
                          <h6 class="pt-2">
                            {{ $element['location'] }}
                          </h6>
                        @endif
                        <p>
                          @if (isset($element['production_date']) && isset($element['region']))
                            {{ $element['production_date'] }}, {{ $element['region'] }}
                          @endif
                        </p>
                        <span><small></small></span>
                        <p>{{ substr(strip_tags($element['description']), 0, 100) }}{{ strlen(strip_tags($element['description'])) > 100 ? '...' : "" }}</p>
                        <a href="{{ url($element['url']) }}" class="btn btn-primary">Scheda {{ $element['type'] }}</a>
                      </div>
                    </div>
                    <div class="row pb-3">
                      <div class="col-md-8 offset-md-4">
                        <hr>
                      </div>
                    </div>
                  @endforeach
                </div>
              </div>

            </div>
          </div>
        @endif
      </div>
      <div class="col-md-3">
        <div class="row pb-5">
          <div class="col">
            <h5>Programmazione</h5>
            <hr>
            <p>
              <strong>Dal {{ $exhibition->start_date }}
              <br>al {{ $exhibition->end_date }}</strong>
            </p>
          </div>
        </div>
        @if ($exhibition->location->slug != 'no-location')
          @if ($exhibition->id === 4)
            @include('components.sidebar.general-info', ['location' => 'estate'])
          @elseif ($exhibition->festival == 1 && $exhibition->fest_cat_id == 2)
            @include('components.sidebar.general-info', ['location' => 'festival-pgc'])
          @else
            @include('components.sidebar.general-info', ['location' => $exhibition->location->slug])
          @endif
        @endif
        @if ($exhibition->festival == 1 && $exhibition->fest_cat_id == 2 && $exhibition->location->slug == 'no-location')
          @include('components.sidebar.general-info', ['location' => 'festival-pgc'])
        @endif
        @if ($exhibition->location->slug != 'no-location')
          <div class="row pb-5">
            <div class="col">
              <h5>Come Raggiungerci</h5>
              <hr>
              <p>
                <strong>{{ $exhibition->location->name }}</strong><br>
                {{ $exhibition->location->address }}<br>
                {{ $exhibition->location->city }}
              </p>
              @if ($exhibition->location->slug === 'spazio-oberdan-exhibitions')
                <div id="map" style="height: 200px;"></div>
                <script>
                  var map;
                  function initMap() {
                    var myLatLng = {lat: 45.475395, lng: 9.204428};
                    map = new google.maps.Map(document.getElementById('map'), {
                      center: myLatLng,
                      zoom: 16
                    });

                    var marker = new google.maps.Marker({
                      position: myLatLng,
                      map: map,
                      title: '{{ $exhibition->location->name }}'
                    });
                  }
                </script>
              @elseif ($exhibition->location->slug === 'area-metropolis-exhibitions')
                <div id="map" style="height: 200px;"></div>
                <script>
                  var map;
                  function initMap() {
                    var myLatLng = {lat: 45.568631, lng: 9.160292};
                    map = new google.maps.Map(document.getElementById('map'), {
                      center: myLatLng,
                      zoom: 16
                    });

                    var marker = new google.maps.Marker({
                      position: myLatLng,
                      map: map,
                      title: '{{ $exhibition->location->name }}'
                    });
                  }
                </script>
              @elseif ($exhibition->location->slug === 'cinema-mic-exhibitions')
                <div id="map" style="height: 200px;"></div>
                <script>
                  var map;
                  function initMap() {
                    var myLatLng = {lat: 45.513398, lng: 9.204513};
                    map = new google.maps.Map(document.getElementById('map'), {
                      center: myLatLng,
                      zoom: 16
                    });

                    var marker = new google.maps.Marker({
                      position: myLatLng,
                      map: map,
                      title: '{{ $exhibition->location->name }}'
                    });
                  }
                </script>
              @endif
            </div>
          </div>
        @endif
        @if ($exhibition->gallery->count() > 0)
          <div class="row pb-5">
            <div class="col">
              <h5>Galleria</h5>
              <hr class="pb-2">
              @foreach ($exhibition->gallery()->get() as $key => $gallery)
                @if ($gallery->description && strlen($gallery->description) > 1)
                    <div class="row">
                        <div class="col-md-8 offset-md-2 px-md-5 px-4">
                            <p>{{ $gallery->description }}</p>
                        </div>
                    </div>
                @endif
                <div class="row">
                  @foreach ($gallery->medias()->get() as $key => $media)
                    <div class="col-4 pb-4">
                        <img class="img-fluid w-100" src="{{ Storage::disk('local')->url($media->thumb) }}" alt="">
                    </div>
                  @endforeach
                </div>
              @endforeach
            </div>
          </div>
        @endif
      </div>
    </div>
    @include('components.main.back')
  </article>
@endsection
@section('scripts')
  <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCWWjzGK-o2GBGAPabC1nm6AETotrd53TQ&callback=initMap"
  async defer></script>
@endsection
