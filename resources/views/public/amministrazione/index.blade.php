@extends('layouts.public')
@section('title', 'Amministrazione Trasparente')
@section('content')
  <section id="main-image">
    <div class="row py-5 no-gutters">
      <div class="col pt-5">
        <div class="row pt-5">
          <div class="col-md-4 offset-md-4">
          <img class="img-fluid mx-auto w-100" src="{{ asset('img/logo.svg') }}" alt="First slide">
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 offset-md-1 p-5">
        <table class="table table-hover">
          <tbody>
            <tr>
              <td>Statuto</td>
              <td><a href="{{ asset('pdf/CinetecaDiMilano_Statuto_Fondazione_Cineteca_Italiana.pdf') }}" target="_blank"><i class="fa fa-link" aria-hidden="true"></i>link</a></td>
            </tr>
            <tr>
              <td>Atto Costitutivo</td>
              <td><a target="_blank" class="disabled"><i class="fa fa-link" aria-hidden="true"></i>link</a></td>
            </tr>
            <tr>
              <td>Atto Nomina CDA in carica</td>
              <td><a href="{{ asset('pdf/CinetecaDiMilano_Atto_Nomina_CDA_In_Carica.pdf') }}" target="_blank"><i class="fa fa-link" aria-hidden="true"></i>link</a></td>
            </tr>
            <tr>
              <td>Atto Nomina Direttore e Legale Rappresentante</td>
              <td><a href="{{ asset('pdf/CinetecaDiMilano_Atto_Nomina_Direttore_e_legale_rappresentante.pdf') }}" target="_blank"><i class="fa fa-link" aria-hidden="true"></i>link</a></td>
            </tr>
            <tr>
              <td>Compensi Membri CDA</td>
              <td><a href="{{ asset('pdf/CinetecaDiMilano_Compensi_Membri_CDA.pdf') }}"><i class="fa fa-link" aria-hidden="true"></i>link</a></td>
            </tr>
            <tr>
              <td>Curriculum Vitae e Compenso Direttore Generale</td>
              <td><a href="{{ asset('pdf/CinetecaDiMilano_Curriculum_Vitae_e_Compenso_Direttore_Generale.pdf') }}" target="_blank"><i class="fa fa-link" aria-hidden="true"></i>link</a></td>
            </tr>
            <tr>
              <td>Nomina Direttore Cavaliere Al Merito Repubblica Italiana</td>
              <td><a href="{{ asset('pdf/CinetecaDiMilano_Nomina_Direttore_Cavaliere.pdf') }}" target="_blank"><i class="fa fa-link" aria-hidden="true"></i>link</a></td>
            </tr>
            <tr>
              <td>Bilancio 2016</td>
              <td><a href="{{ asset('pdf/CinetecaDiMilano_Bilancio_2016.pdf') }}" target="_blank"><i class="fa fa-link" aria-hidden="true"></i>link</a></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </section>
  <section id="back">
    @include('components.main.back')
  </section>
@endsection
