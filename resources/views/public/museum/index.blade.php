@extends('layouts.public', ['active' => 'museo'])
@section('title', 'Museo')
@section('seo')
  <meta name="description" content="Il MIC è il primo museo interattivo del cinema in Italia dove lo spettatore può, interagendo direttamente con dispositivi e applicazioni create ad hoc, realizzare un doppiaggio cinematografico, sonorizzare un film, modificare un manifesto cinematografico, approfondire le sue conoscenze cinematografiche da una vasta library (100 ore di filmati)." />
@endsection
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/single.css') }}">
@endsection
@section('content')
  <section id="main-image">
    @include('components.main.film-header', ['image' => asset('img/museo.jpg'), 'title' => 'Mic - Museo Interattivo Del Cinema' ])
  </section>
  <section id="description">
    <div class="row pt-5">
      <div class="col-md-8 offset-md-2 px-md-5 px-4">
        <p>
          Il MIC è il primo museo interattivo del cinema in Italia dove lo spettatore può, interagendo direttamente con dispositivi e applicazioni create ad hoc, realizzare un doppiaggio cinematografico, sonorizzare un film, modificare un manifesto cinematografico, approfondire le sue conoscenze cinematografiche da una vasta library (100 ore di filmati), utilizzare mappe dinamiche per scoprire quali sono i set dei film girati a Milano e le sale cinematografiche della città, anche quelle dismesse.<br>
        </p>
        <p>
          Il MIC è un’esperienza innovativa rivolta a tutti coloro che hanno voglia di scoprire un mondo sconosciuto e ai curiosi che vogliono ritrovare frammenti di un cinema milanese dimenticato. Terminata la visita è possibile visionare le proiezioni programmate presso la <a href="/films/cinema-mic">sala cinematografica del MIC</a>.
        </p>
        <p>
          Accanto alle sale espositive il percorso museale prosegue nell'<a href="/pagina/archiviofilm">Archivio Film</a>, dove, grazie agli occhiali Epson Moverio, è possibile visitare l’archivio cinematografico della Cineteca attraverso la realtà aumentata, un percorso unico in Europa!
        </p>
        <p>
          Nel periodo estivo potrete godere della splendida Terrazza MIC, un luogo per rilassarsi tra una proiezione e l'altra, per condividere la passione per il cinema e per fruire di fantastiche proiezioni all'aperto!
        </p>
      </div>
    </div>
  </section>
  <section id="activities">
    @include('components.main.square-box', ['title' => 'Attività del museo', 'items' => $pages, 'type' => 'pagina'])
  </section>
  <section id="back">
   @include('components.main.back')
  </section>
@endsection
