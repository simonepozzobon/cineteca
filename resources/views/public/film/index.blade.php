@extends('layouts.public', ['active' => 'proiezioni'])
@section('title', 'Proiezioni')
@section('seo')
  <meta name="description" content="Tutte le proiezioni in programmazione alla Fondazione Cineteca Italiana di Milano" />
@endsection
@section('content')
<section id="locations-select">
  <div class="row no-gutters">
    <div class="col pt-5">
      <div class="row pt-5">
        <div class="col pt-5 px-md-5">
          <h1 align="center">La Programmazione Per Sala</h1>
        </div>
      </div>
      <div class="row pb-5">
        <div class="col-6 offset-3">
          <hr>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <div class="row no-gutters px-md-5">
            <div class="col-md-4 pb-5 pb-md-0">
              <div class="container">
                <img class="img-fluid pb-3 w-100" src="/img/Metropolis.jpg" alt="">
                <h2 class="text-center">
                  <a href="{{ route('film.location.index', 'area-metropolis') }}" class="text-default">Area Metropolis 2.0</a>
                </h2>
              </div>
            </div>
            <div class="col-md-4 pb-md-0">
              <div class="container">
                <img class="img-fluid pb-3 w-100" src="/img/sala-spazio-oberdan.jpg" alt="">
                <h2 class="text-center">
                  <a href="{{ route('film.location.index', 'spazio-oberdan') }}" class="text-default">Spazio Oberdan</a>
                </h2>
              </div>
            </div>
            <div class="col-md-4 pb-md-0">
              <div class="container">
                <img class="img-fluid pb-3 w-100" src="/img/MIC_sala.jpg" alt="">
                <h2 class="text-center">
                  <a href="{{ route('film.location.index', 'cinema-mic') }}" class="text-default">Sala Cinema MIC</a>
                </h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="film-carousel">
  @include('components.main.slider', ['type' => 'Proiezioni ', 'button' => 'Film', 'items' => $films, 'root' => '/film'])
</section>
<section id="calendar">
  <calendar-main
    featured_director="{{ $featDirector }}"
    featured_page="{{ $featured_page }}"
    featured_exhibit="{{ $featured_exhibit }}"
    featured_exhibition="{{ $featured_exhibition }}"
    featured_product="{{ $featured_product }}"
    starting_shows="{{ $shows }}"
    url_previous="{{ url()->previous() }}" />
  </calendar-main>
</section>
<section id="news-swiper" class="bg-faded">
  <news-slider news="{{ $news }}"></news-slider>
</section>
<section class="pt-5">
  @include('components.main.back')
</section>
<div id="output">

</div>
@endsection
@section('scripts')
  <script src="/js/news.js"></script>
  <script src="/js/calendar.js"></script>
@endsection
