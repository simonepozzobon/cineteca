@extends('layouts.public', ['active' => 'proiezioni'])
@section('title', "Proiezioni $location->name")
@section('seo')
  <meta name="description" content="Tutte le proiezioni in programmazione alla Fondazione Cineteca Italiana di Milano, nella sala {{ $location->name }}" />
@endsection
@section('content')
<section id="film-carousel">
  @include('components.main.slider', ['type' => '', 'button' => 'Film', 'items' => $films, 'root' => '/film'])
</section>
<section id="calendar">
  <div class="row no-gutters pt-5">
    @if (isset($shows))
      <div class="col-md-7 offset-md-1">
    @else
      <div class="col">
    @endif
      @if (isset($shows))
        <h5>Film in proiezione</h5>
        <hr>
        <div id="shows">
          @foreach ($shows as $key => $show)
            <div class="row show-single">
              <div class="col-md-2">
                <h3>{{ $show->time }}</h3>
                <p>
                  {{ $show->day_string }}<br>
                  {{ $show->day }} {{ $show->month }}
                </p>
                @if ($show->ticket == true)
                  <a href="{{ $show->ticket_url }}" target="_blank" class="btn btn-primary"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Biglietti</a>
                @endif
              </div>
              <div class="col-md-4">
                <img src="{{ Storage::disk('local')->url($show->img) }}" class="img-fluid w-100">
              </div>
              <div class="col-md-6">
                <div class="row">
                    <div class="col">
                      <h3>
                        <a href="{{ $show->slug }}">
                          {{ $show->title }}
                        </a>
                      </h3>
                    </div>
                </div>
                <p>
                  @if (isset($show->film->id))
                    @foreach ($show->exhibitions($show->film->id) as $key => $exhibition)
                      <span class="badge badge-info">
                        <a href="{{ route('single.exhibition', $exhibition->slug) }}" class="text-white">
                        {{ $exhibition->title }}
                        </a>
                      </span>
                    @endforeach
                  @endif
                  @if ($show->kids === 1 )
                    <span class="badge" style="background-color:#755482">Adatto ai bambini</span>
                  @endif
                  @if ($show->type == 'film')
                    @if ($show->film->categories->count() > 0)
                      @foreach ($show->film->categories as $key => $film_category)
                        <span class="badge badge-default">{{ $film_category->name }}</span>
                      @endforeach
                    @endif
                  @elseif ($show->type == 'event')
                    <span class="badge" style="background-color:#2AD68C">Evento</span>
                  @endif
                </p>
                @if ($show->type == 'film')
                  <h6>
                    <a href="{{ route('film.location.index', $show->film->location->slug) }}">{{ $show->film->location->name }}</a>
                    @isset($show->film->place)
                      @if ($show->film->place->count() > 0)
                      - {{ $show->film->place->name }}
                      @endif
                    @endisset
                  </h6>
                  <p>{{ $show->film->region }}, {{ $show->film->production_date }}, {{ $show->film->duration }}'</p>
                  <p>{{ substr(strip_tags($show->film->description), 0, 150) }}{{ strlen(strip_tags($show->film->description)) > 150 ? '...' : "" }}</p>
                @elseif ($show->type == 'event')
                  <h6>{{ $show->location->name }}</h6>
                  <p>{{ substr(strip_tags($show->description), 0, 150) }}{{ strlen(strip_tags($show->description)) > 150 ? '...' : "" }}</p>
                @endif
              </div>
            </div>
            <div class="row pb-3 show-single-sep">
              <div class="col-md-8 offset-md-2">
                <hr>
              </div>
            </div>
          @endforeach
        </div>
      @else
        <h1 class="text-center p-5"><i class="fa fa-meh-o" aria-hidden="true"></i></h1>
        <h3 class="text-center pb-5">Ooops, nessun film in programmazione</h3>
        <div class="row p-5">
          <div class="col-md-4 offset-md-4">
            <a class="btn btn-primary btn-block" href="{{ url()->previous() }}">
              <i class="fa fa-arrow-left" aria-hidden="true"></i> Indietro
            </a>
          </div>
        </div>
      @endif
    </div>
    @if (isset($shows))
      <div class="col-md-2 offset-md-1">
        <div class="row pb-5">
          <div class="col">
            <h5>Scegli Una Data</h5>
            <hr>
            <div id="datepicker"></div>
          </div>
        </div>
        <div class="row pb-5">
          <div class="col">
            <h5>Settimana</h5>
            <hr>
            <a href="{{ route('film.location.week', $location->slug) }}" class="btn btn-info">Vedi Programmazione</a>
          </div>
        </div>
        @if ($location->slug !== 'area-metropolis')
          <div class="row pb-5">
            <div class="col">
              <h5>Mese</h5>
              <hr>
              <a href="{{ route('film.location.month', $location->slug) }}" class="btn btn-info">Vedi Programmazione</a>
            </div>
          </div>
        @endif
        @include('components.sidebar.general-info', ['location' => $location->slug])
        @include('components.sidebar.location-map', ['name' => $location->name, 'address' => $location->address, 'city' => $location->city, 'slug' => $location->slug])
      </div>
    @endif
  </div>
</section>
<section id="news-swiper" class="bg-faded">
  <news-slider news="{{ $news }}"></news-slider>
</section>
<section class="pt-5">
  @include('components.main.back')
</section>
@endsection
@section('scripts')
  <script src="/js/news.js"></script>
  <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCWWjzGK-o2GBGAPabC1nm6AETotrd53TQ&callback=initMap" async defer></script>
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}">
  <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap-datepicker/locales/bootstrap-datepicker.it.min.js') }}"></script>
  <script type="text/javascript">

  $('#datepicker').datepicker({
    autoclose: true,
    todayHighlight: true,
    language: 'it',
    startDate: new Date(),
  });

  @if (isset($memory) && $memory = true)
    var cookie = getCookie('cnt-date-{{ $location->slug }}');

    if (cookie !== '') {
      var date = new Date(cookie);
      $('#datepicker').datepicker('setDate', date);
      getShows(date);
    }
  @endif


  $('#datepicker').datepicker().on('changeDate', function(e) {

    /*
     *
     * Catturo il cambio data
     *
    */
    var date = $('#datepicker').datepicker('getDate');

    // Salvo la data nei cookies
    setCookie('cnt-date-{{ $location->slug }}', date, 8);
    getShows(date);
  });

    function getShows(date)
    {
      // Formatto la data secondo necessità
      var day = date.getDate();
      var monthIndex = date.getMonth() + 1;
      var year = date.getFullYear();

      var new_date = year+'-'+monthIndex+'-'+day;

      // Effettuo la richiesta AJAX per ottenere gli show in quel giorno
      $.ajax({
        type: 'GET',
        url:  '/films/{{ $location->slug }}/shows/'+new_date,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        },
        success: function (response) {
            var dataLenght = Object.keys(response).length;
            // Rimuovo i vecchi show
            $('.show-single').remove();
            $('.show-single-sep').remove();
            $('.pagination').remove();

            // Se ci sono Film
            if (dataLenght > 0) {
              for (var i = 0; i < response.length; i++) {
                var data = '';
                data += '<div class="row show-single">';
                data +=   '<div class="col-md-2">';
                data +=     '<h3>'+response[i].time+'</h3>';
                data +=     '<p>'+response[i].day_string+'<br>'+response[i].day+', '+response[i].month+'</p>';
                if (response[i].ticket == true) {
                  if (response[i].ticket_url == undefined) {
                    data +=         '';
                  } else {
                    data +=         '<a href="'+response[i].ticket_url+'" class="btn btn-primary btn-small" target="_blank"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Biglietti</a>';
                  }
                }
                data +=   '</div>';
                data +=   '<div class="col-md-4">';
                data +=     '<img src="'+response[i].img+'" class="img-fluid w-100">';
                data +=   '</div>';
                data +=   '<div class="col-md-6">';
                data +=     '<div class="row">';

                data +=       '<div class="col">';
                data +=          '<h3>';
                data +=           '<a href="'+response[i].url+'">'+response[i].title+'</a>';
                data +=          '</h3>';
                data +=        '</div>';

                data +=      '</div>';
                data +=      '<p>';

                // Film o Evento
                if (response[i].type == 'film') {
                  // Exhibitions o Rassegne
                  for (var k = 0; k < response[i].exhibitions.length; k++) {
                    data += '<span class="badge badge-info">';
                    data +=   '<a href="/rassegna/'+response[i].exhibitions[k].slug+'" class="text-white">'+response[i].exhibitions[k].title+'</a>';
                    data += '</span>';
                  }

                  // Kids
                  if (response[i].film.kids === 1) {
                    data += '<span class="badge" style="background-color:#755482; margin-left: 3px">Adatto ai bambini</span>';
                  }

                  // Categorie collegate allo show
                  for (var k = 0; k < response[i].film_categories.length; k++) {
                    data += '<span class="badge badge-default" style="margin-left: 3px">'+response[i].film_categories[k].name+'</span>';
                  }

                  data +=      '</p>';
                  data +=      '<h6><a href="/films/'+response[i].location.slug+'">'+response[i].location.name+'</a></h6>';
                  data +=      '<p>'+response[i].film.production_date+', '+response[i].film.region+'</p>';
                  data +=      '<p>'+response[i].description+'</p>';
                } else {
                  // Evento
                  data +=      '<span class="badge" style="background-color:#2AD68C">Evento</span>';
                  data +=      '<h6>'+response[i].location.name+'</h6>';
                  data +=      '<p>'+response[i].description+'</p>';
                }

                data +=   '</div>';
                data += '</div>';
                data += '<div class="row pb-3 show-single-sep">';
                data +=   '<div class="col-md-8 offset-md-3">';
                data +=     '<hr>';
                data +=   '</div>';
                data += '</div>';


                $('#shows').append(data);
              }

            } else {
              // Se non ci sono film nella risposta
              var data = '';
              data += '<div class="row show-single">';
              data +=   '<div class="col">';
              data +=     '<h1 class="text-center p-5"><i class="fa fa-meh-o" aria-hidden="true"></i></h1>';
              data +=     '<h3 class="text-center pb-5">Ooops, nessun film in programmazione</h3>';
              data +=     '<h5 class="text-center pb-5">Prova a cambiare giorno</h5>';
              data +=   '</div>';
              data += '</div>';

              $('#shows').append(data);
            }


        },
        error: function (xhr, status) {
            console.log(xhr);
            console.log(status);
        }
      });
    }

    function slugify(text)
    {
      return text.toString().toLowerCase().trim()
        .replace(/\s+/g, '')           // Replace spaces with -
        .replace(/&/g, '')         // Replace & with 'and'
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '')         // Replace multiple - with single -
    }

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
  </script>
@endsection
