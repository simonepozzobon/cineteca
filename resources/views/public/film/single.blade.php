@extends('layouts.public', ['active' => 'proiezioni'])
@section('title', "$film->title")
@section('seo')
  <meta name="description" content="{{ substr(strip_tags($film->description), 0, 170) }}{{ strlen(strip_tags($film->description)) > 170 ? '...' : "" }}" />
@endsection
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/single.css') }}">
@endsection
@section('content')
  <article class="pb-5">
    @if ($film->media->first())
      @include('components.main.film-header', ['image' => Storage::disk('local')->url($film->media->first()->slide_img), 'title' => $film->title])
    @else
      @include('components.main.film-header', ['image' => Storage::disk('local')->url('none'), 'title' => $film->title])
    @endif
    <div class="row p-5">
      <div class="col-md-10 offset-md-1">
        <div class="row">
          <div class="col-md-2">
            <div class="row pb-5">
              <div class="col">
                <h5>Info</h5>
                <hr>
                @if ($film->media->first())
                  <img src="{{ Storage::disk('local')->url($film->media->first()->portrait) }}" alt="{{ $film->title }}" class="img-fluid w-100 pb-5">
                @else
                  <img src="none" alt="{{ $film->title }}" class="img-fluid w-100 pb-5">
                @endif
                @if ($film->directors->count() > 0)
                  <p>Regia:
                    @if ($film->directors->count() > 1)
                      @foreach ($film->directors()->get() as $key => $director)
                        @if ($key == 0)
                          {{ $director->name }},
                        @else
                          {{ $director->name }}
                        @endif
                      @endforeach
                    @else
                      @foreach ($film->directors()->get() as $key => $director)
                          {{ $director->name }}
                      @endforeach
                    @endif
                  </p>
                @endif
                @if ($film->actors->count() > 0)
                  <p>Interpreti:
                    @if ($film->actors->count() > 1)
                      @foreach ($film->actors()->get() as $key => $actor)
                        @if ($key == 0)
                          {{ $actor->name }},
                        @else
                          {{ $actor->name }}
                        @endif
                      @endforeach
                    @else
                      @foreach ($film->actors()->get() as $key => $actor)
                        {{ $actor->name }}
                      @endforeach
                    @endif
                  </p>
                @endif
                @if ($film->screenwriters->count() > 0)
                  <p>
                    Sc.:
                    @if ($film->screenwriters->count() > 1)
                      @foreach ($film->screenwriters()->get() as $key => $screenwriter)
                        {{ $screenwriter->name }}
                      @endforeach
                    @else
                      {{ $film->screenwriters()->first()->name }}
                    @endif
                  </p>
                @endif
                @if ($film->photographers->count() > 0)
                  <p>
                    Fot.:
                    @if ($film->photographers->count() > 1)
                      @foreach ($film->photographers()->get() as $key => $photographer)
                        {{ $photographer->name }}
                      @endforeach
                    @else
                      {{ $film->photographers()->first()->name }}
                    @endif
                  </p>
                @endif
                @if ($film->editors->count() > 0)
                  <p>
                    Mont.:
                    @if ($film->editors->count() > 1)
                      @foreach ($film->editors()->get() as $key => $editor)
                        {{ $editor->name }}
                      @endforeach
                    @else
                      {{ $film->editors()->first()->name }}
                    @endif
                  </p>
                @endif
                @if ($film->producers->count() > 0)
                  <p>
                    Prod.:
                    @if ($film->producers->count() > 1)
                      @foreach ($film->producers()->get() as $key => $producer)
                        {{ $producer->name }}
                      @endforeach
                    @else
                      {{ $film->producers()->first()->name }}
                    @endif
                  </p>
                @endif
                @if ($film->distributors->count() > 0)
                  <p>
                    Distr.:
                    @if ($film->distributors()->count() > 1)
                      @foreach ($film->distributors()->get() as $key => $distributor)
                        {{ $distributor->name }}
                      @endforeach
                    @else
                      {{ $film->distributors()->first()->name }}
                    @endif
                  </p>
                @endif
                <p>{{ $film->region }}, {{ $film->production_date }}, {{ $film->duration }}'</p>
                <p>
                  @foreach ($film->categories as $key => $film_category)
                    <span class="badge badge-default">{{ $film_category->name }}</span>
                  @endforeach
                </p>
              </div>
            </div>
            @if ($film->exhibitions->count() > 0)
              <div class="row pb-5">
                <div class="col">
                  <h5>{{ $film->exhibitions->count() > 1 ? 'Nelle Rassegne' : 'Nella Rassegna' }}</h5>
                  <hr>
                  @foreach ($film->exhibitions as $key => $exhibition)
                    <img class="w-100 img-fluid" src="{{ Storage::disk('local')->url($exhibition->media->first()->thumb) }}" alt="{{ $exhibition->title }}">
                    <h6 class="pt-3">
                      <a href="{{ route('single.exhibition', $exhibition->slug) }}">
                        {{ $exhibition->title }}
                      </a>
                    </h6>
                    <p>{{ substr(strip_tags($exhibition->description), 0, 100) }}{{ strlen(strip_tags($exhibition->description)) > 100 ? '...' : "" }}</p>
                    <a href="{{ route('single.exhibition', $exhibition->slug) }}" class="btn btn-primary">Vai alla Rassegna</a>
                  @endforeach
                </div>
              </div>
            @endif
            @if ($film->exhibits->count() > 0)
              <div class="row">
                <div class="col">
                  <h5>{{ $film->exhibits->count() > 1 ? 'Nelle Esposizioni' : 'Nell\'Esposizione' }}</h5>
                  <hr>
                  @foreach ($film->exhibits as $key => $exhibit)
                    <img class="w-100 img-fluid" src="{{ Storage::disk('local')->url($exhibit->media->first()->thumb) }}" alt="{{ $exhibit->title }}">
                    <h6 class="pt-3">
                      <a href="{{ route('single.exhibit', $exhibit->slug) }}">
                        {{ $exhibit->title }}
                      </a>
                    </h6>
                    <p>{{ substr(strip_tags($exhibit->description), 0, 100) }}{{ strlen(strip_tags($exhibit->description)) > 100 ? '...' : "" }}</p>
                    <a href="{{ route('single.exhibit', $exhibit->slug) }}" class="btn btn-primary">Vai all'Esposizione</a>
                  @endforeach
                </div>
              </div>
            @endif
          </div>
          <div class="col-md-7">
            <h5>Scheda</h5>
            <hr>
            <p>{!! $film->description !!}</p>
          </div>
          <div class="col-md-3">
            <div class="row pb-5">
              <div class="col">
                <h5>Programmazione</h5>
                <hr>
                <div class="row">
                  <div class="col">
                    @foreach ($shows as $key => $show)
                      <p>
                        <strong>{{ $show->day }}, {{ $show->month }}</strong> - {{ $show->time }}
                        @if ($film->festival != 1 && $film->fest_cat_id != 1)
                          @if ($show->ticket == true)
                            (<a href="{{ $show->ticket_url }}" target="_blank">Acquista</a>)
                          @endif
                        @endif
                      </p>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
            @if ($film->festival == 1 && $film->fest_cat_id == 1)
              @include('components.sidebar.general-info', ['location' => $film->location->slug, 'free' => true])
            @elseif ($film->festival == 1 && $film->fest_cat_id == 2)
              @include('components.sidebar.general-info', ['location' => 'festival-pgc'])
            @else
              @include('components.sidebar.general-info', ['location' => $film->location->slug])
            @endif
            @isset($show->film->place)
              @if ($show->film->place->count() > 0)
                <div class="row pb-5">
                  <div class="col">
                    <h5>Sala</h5>
                    <hr>
                    {{ $show->film->place->name }}
                  </div>
                </div>
              @endif
            @endisset

            @include('components.sidebar.location-map', ['name' => $film->location->name, 'address' => $film->location->address, 'city' => $film->location->city, 'slug' => $film->location->slug])
          </div>
        </div>
      </div>
    </div>
    @include('components.main.back')
  </article>
@endsection
@section('scripts')
  <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCWWjzGK-o2GBGAPabC1nm6AETotrd53TQ&callback=initMap" async defer></script>
@endsection
