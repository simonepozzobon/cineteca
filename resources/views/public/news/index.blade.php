@extends('layouts.public', ['active' => 'news'])
@section('title', 'News')
@section('seo')
  <meta name="description" content="Tutte le notizie della Fondazione Cineteca Italiana di Milano" />
@endsection
@section('content')
<section id="news-carousel">
  @include('components.main.slider', ['type' => 'News ', 'button' => 'News', 'items' => $featured, 'root' => '/news'])
</section>
<section id="news-archive">
  <div class="row no-gutters pt-5">
    <div class="col-md-10 offset-md-1">
      @if (isset($news))
        <h5>Archivio News</h5>
        <hr>
        <div id="news">
          @foreach ($news as $key => $post)
            <div class="row show-single">
              <div class="col-md-4">
                <img src="{{ Storage::disk('local')->url($post->media->first()->landscape) }}" class="img-fluid w-100">
              </div>
              <div class="col-md-6">
                <div class="row">
                    <div class="col">
                      <h3>
                        <a href="{{ route('single.news', $post->slug) }}">
                          {{ $post->title }}
                        </a>
                      </h3>
                    </div>
                </div>
                {{-- <h6>{{ $post->location->name }}</h6> --}}
                <p class="pt-3">{{ substr(strip_tags($post->content), 0, 150) }}{{ strlen(strip_tags($post->content)) > 150 ? '...' : "" }}</p>
              </div>
            </div>
            <div class="row pb-3 show-single-sep">
              <div class="col-md-8 offset-md-2">
                <hr>
              </div>
            </div>
          @endforeach
        </div>
      @else
        <h1 class="text-center p-5"><i class="fa fa-meh-o" aria-hidden="true"></i></h1>
        <h3 class="text-center pb-5">Ooops, nessuna news</h3>
        <div class="row p-5">
          <div class="col-md-4 offset-md-4">
            <a class="btn btn-primary btn-block" href="{{ url()->previous() }}">
              <i class="fa fa-arrow-left" aria-hidden="true"></i> Indietro
            </a>
          </div>
        </div>
      @endif
    </div>
  </div>
</section>
<section id="back">
  <div class="row">
    <div class="col-md-4 offset-md-4 pb-5 text-center">
      <div class="container-fluid d-flex justify-content-around">
        {{ $news->links('vendor.pagination.bootstrap-4') }}
      </div>
    </div>
  </div>
</section>
@endsection
