@extends('layouts.public')
@section('title', "News - $news->title")
@section('seo')
  <meta name="description" content="{{ substr(strip_tags($news->content), 0, 170) }}{{ strlen(strip_tags($news->content)) > 170 ? '...' : "" }}" />
@endsection
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/single.css') }}">
@endsection
@section('content')
<article class="pt-5 pb-5">
  @include('components.main.film-header', ['title' => $news->title, 'image' => Storage::disk('local')->url($news->media->first()->slide_img)])
  <div class="row p-5">
    <div class="col-md-10 offset-md-1">
      <p>{!! $news->content !!}</p>
    </div>
  </div>
  @include('components.main.back')
</article>
@endsection
