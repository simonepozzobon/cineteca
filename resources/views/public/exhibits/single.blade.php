@extends('layouts.public', ['active' => 'espozioni'])
@section('title', "$exhibit->title")
@section('seo')
  <meta name="description" content="{{ substr(strip_tags($exhibit->description), 0, 170) }}{{ strlen(strip_tags($exhibit->description)) > 170 ? '...' : "" }}" />
@endsection
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/single.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/baguettebox/baguetteBox.css') }}">
@endsection
@section('content')
  <article class="pb-5">
    @include('components.main.film-header', ['title' => $exhibit->title, 'image' => Storage::disk('local')->url($exhibit->media->first()->slide_img)])
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div class="row p-5">
          <div class="col-md-2">
            <div class="row pb-5">
              <div class="col">
                <h5>Ingresso</h5>
                <hr>
                <p>
                  Libero
                </p>
              </div>
            </div>
          </div>
          <div class="col-md-7">
            <div class="row">
              <div class="col">
                <h5>Scheda</h5>
                <hr>
                <p>{!! $exhibit->description !!}</p>
              </div>
            </div>
            @if ($exhibit->films()->count() > 0)
              <div class="row pt-5">
                <div class="col">
                  <h5>Film In Esposizione</h5>
                  <hr>
                  @foreach ($films as $key => $film)
                    <div class="row pb-3">
                      <div class="col-md-4">
                        <img class="w-100 img-fluid" src="{{ Storage::disk('local')->url($film->media->first()->landscape) }}" alt="{{ $film->title }}">
                      </div>
                      <div class="col-md-6">
                        <h6>
                          <a href="{{ route('single.film', $film->slug) }}">{{ $film->title }}</a>
                        </h6>
                        <span><small>{{ $film->location->name }}</small></span>
                        <p>{{ substr(strip_tags($film->description), 0, 100) }}{{ strlen(strip_tags($film->description)) > 100 ? '...' : "" }}</p>
                      </div>
                      <div class="col-md-2">
                        <a href="{{ route('single.film', $film->slug) }}" class="btn btn-sm btn-primary">Scheda Film</a>
                      </div>
                    </div>
                  @endforeach
                </div>
              </div>
            @endif
            @if ($exhibit->exhibitions()->count() > 0)
              <div class="row pt-5">
                <div class="col">
                  <h5>Rassegne In Esposizione</h5>
                  <hr>
                  @foreach ($exhibit->exhibitions()->get() as $key => $exhibition)
                    <div class="row pb-3">
                      <div class="col-md-4">
                        <img class="w-100 img-fluid" src="{{ Storage::disk('local')->url($exhibition->media->first()->landscape) }}" alt="{{ $exhibition->title }}">
                      </div>
                      <div class="col-md-6">
                        <h6>
                          <a href="{{ route('single.exhibition', $exhibition->slug) }}">{{ $exhibition->title }}</a>
                        </h6>
                        <span><small>{{ $exhibition->location->name }}</small></span>
                        <p>{{ substr(strip_tags($exhibition->description), 0, 100) }}{{ strlen(strip_tags($exhibition->description)) > 100 ? '...' : "" }}</p>
                      </div>
                      <div class="col-md-2">
                        <a href="{{ route('single.exhibition', $exhibition->slug) }}" class="btn btn-sm btn-primary">Scheda Rassegna</a>
                      </div>
                    </div>
                  @endforeach
                </div>
              </div>
            @endif
          </div>
          <div class="col-md-3">
            <div class="row pb-5">
              <div class="col">
                <h5>Programmazione</h5>
                <hr>
                <p>
                  <strong>Dal {{ $exhibit->start_date }}
                  <br>al {{ $exhibit->end_date }}</strong>
                </p>
              </div>
            </div>
            <div class="row pb-5">
              <div class="col">
                <h5>Info Generali</h5>
                <hr>
                <p>
                  <a class="" data-toggle="collapse" href="#orari" aria-expanded="false" aria-controls="biglietti">
                    Orari apertura
                  </a>
                  <div class="collapse" id="orari">
                    @if ($exhibit->location->slug === 'spazio-oberdan-exhibits')
                      <p>
                        <strong>le sale aprono mezz'ora prima dell'inizio della proiezione</strong>
                        <br>
                        <ul>
                          <li>
                            dal lunedì al venerdì<br>
                            dalle 16:30 alle 21:00
                          </li>
                          <li>
                            sabato e domenica<br>
                            dalle 14:30 alle 21:00
                          </li>
                        </ul>
                        <strong>Se non ci sono proiezioni in programma le sale rimangono chiuse</strong>
                      </p>
                    @elseif ($exhibit->location->slug === 'area-metropolis-exhibits')
                      <p>
                        <strong>le sale aprono mezz'ora prima dell'inizio della proiezione</strong>
                        <br>
                        <ul>
                          <li>
                            dal lunedì al giovedi<br>
                            dalle 20:30
                          </li>
                          <li>
                            venerdì, sabato e domenica<br>
                            dalle 14:30
                          </li>
                        </ul>
                        <strong>Se non ci sono proiezioni in programma le sale rimangono chiuse</strong>
                      </p>
                    @elseif ($exhibit->location->slug === 'cinema-mic-exhibits')
                      <p>
                        lunedì chiuso<br>
                        dal martedi al venerdi<br>
                        dalle 15:00 alle 18:00<br>
                        sabato e domenica<br>
                        dalle 15:00 alle 19:00<br>
                      </p>
                    @endif
                  </div>
                </p>
              </div>
            </div>
            <div class="row pb-5">
              <div class="col">
                <h5>Come Raggiungerci</h5>
                <hr>
                <p>
                  <strong>{{ $exhibit->location->name }}</strong><br>
                  {{ $exhibit->location->address }}<br>
                  {{ $exhibit->location->city }}
                </p>
                @if ($exhibit->location->slug === 'spazio-oberdan-exhibits')
                  <div id="map" style="height: 200px;"></div>
                  <script>
                    var map;
                    function initMap() {
                      var myLatLng = {lat: 45.475395, lng: 9.204428};
                      map = new google.maps.Map(document.getElementById('map'), {
                        center: myLatLng,
                        zoom: 16
                      });

                      var marker = new google.maps.Marker({
                        position: myLatLng,
                        map: map,
                        title: '{{ $exhibit->location->name }}'
                      });
                    }
                  </script>
                @elseif ($exhibit->location->slug === 'area-metropolis-exhibits')
                  <div id="map" style="height: 200px;"></div>
                  <script>
                    var map;
                    function initMap() {
                      var myLatLng = {lat: 45.568631, lng: 9.160292};
                      map = new google.maps.Map(document.getElementById('map'), {
                        center: myLatLng,
                        zoom: 16
                      });

                      var marker = new google.maps.Marker({
                        position: myLatLng,
                        map: map,
                        title: '{{ $exhibit->location->name }}'
                      });
                    }
                  </script>
                @elseif ($exhibit->location->slug === 'cinema-mic-exhibits')
                  <div id="map" style="height: 200px;"></div>
                  <script>
                    var map;
                    function initMap() {
                      var myLatLng = {lat: 45.513398, lng: 9.204513};
                      map = new google.maps.Map(document.getElementById('map'), {
                        center: myLatLng,
                        zoom: 16
                      });

                      var marker = new google.maps.Marker({
                        position: myLatLng,
                        map: map,
                        title: '{{ $exhibit->location->name }}'
                      });
                    }
                  </script>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        @include('components.main.title', ['title' => 'Galleria'])
        @if ($exhibit->gallery->count() > 0)
          @foreach ($exhibit->gallery()->get() as $key => $gallery)
            @if ($gallery->description && strlen($gallery->description) > 1)
                <div class="row">
                    <div class="col-md-8 offset-md-2 px-md-5 px-4">
                        <p>{{ $gallery->description }}</p>
                    </div>
                </div>
            @endif
            <div class="row gallery">
              @foreach ($gallery->medias()->get() as $key => $media)
                <div class="col-md-4 pb-4">
                    <a href="{{ Storage::disk('local')->url($media->img) }}">
                      <img class="img-fluid w-100" src="{{ Storage::disk('local')->url($media->thumb) }}" alt="">
                    </a>
                </div>
              @endforeach
            </div>
          @endforeach
        @endif
      </div>
    </div>
    @include('components.main.back')
  </article>
@endsection
@section('scripts')
  <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCWWjzGK-o2GBGAPabC1nm6AETotrd53TQ&callback=initMap"
  async defer></script>
  <script src="{{ asset('plugins/baguettebox/baguetteBox.min.js') }}"></script>
  <script type="text/javascript">
    baguetteBox.run('.gallery');
  </script>
@endsection
