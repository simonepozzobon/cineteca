@extends('layouts.public', ['active' => 'cinestore', 'categories' => $product_categories])
@section('title', 'Cinestore - Carrello')
@section('seo')
  <meta name="description" content="Il cinestore è uno spazio virtuale dove acquistare i prodotti ed i servizi della Fondazione Cineteca di Milano." />
@endsection
@section('content')
  <section class="pt-5">
    @include('components.main.title', ['title' => 'Carrello'])
  </section>
  <section id="cart">

  </section>
  @include('components.main.back')
@endsection
@section('scripts')
  <script src="/js/cinestore.js"></script>
@endsection
