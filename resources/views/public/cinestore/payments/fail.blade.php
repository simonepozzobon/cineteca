@extends('layouts.public', ['active' => 'cinestore', 'categories' => $product_categories])
@section('title', "Cinestore - Pagamento Rifiutato")
@section('seo')
  <meta name="description" content="Il cinestore è uno spazio virtuale dove acquistare i prodotti ed i servizi della Fondazione Cineteca di Milano." />
@endsection
@section('content')
<section>
  <div class="row no-gutters">
    <div class="col pt-5">
      <div class="row pt-md-5">
        <div class="col pt-5 px-5">
          <h1 align="center">Ci dispiace ma qualcosa è andato storto</h1>
        </div>
      </div>
      <div class="row pb-5">
        <div class="col-6 offset-3">
          <hr>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <h3 class="text-center pb-5">L'ordine non andato a buon fine</h3>
          <p class="text-center">Prova ad effettuare nuovamente l'ordine</p>
          <p class="text-center pb-5">
            Nel caso dovessi continuare a riscontrare problemi scrivi un messaggio a
            <a href="mailto:cinestore@cinetecamilano.it">cinestore@cinetecamilano.it</a> indicando il codice dell'ordine (<strong>{{ $order->transaction_id }}</strong>).
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
@include('components.main.back')
@endsection
@section('scripts')
  <script src="/js/cinestore.js"></script>
@endsection
