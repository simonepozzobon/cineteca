@extends('layouts.public', ['active' => 'cinestore', 'categories' => $product_categories])
@section('title', "Cinestore - Pagamento effettuato")
@section('seo')
  <meta name="description" content="Il cinestore è uno spazio virtuale dove acquistare i prodotti ed i servizi della Fondazione Cineteca di Milano." />
@endsection
@section('content')
<section id="payment-success">
  <div class="row no-gutters">
    <div class="col pt-5">
      <div class="row pt-md-5">
        <div class="col pt-5 px-5">
          <h1 align="center">Grazie per aver effettuato l'acquisto</h1>
        </div>
      </div>
      <div class="row pb-5">
        <div class="col-6 offset-3">
          <hr>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <h3 class="text-center pb-5">L'ordine è andato a buon fine</h3>
          <p class="text-center">A breve riceverai una mail di conferma con tutti i dettagli dell'ordine all'indirizzo {{ $order->email }}.</p>
          <p class="text-center pb-5">
            Nel caso dovessi riscontrare dei problemi contattaci rispondendo direttamente alla mail o scrivendo un messaggio a
            <a href="mailto:cinestore@cinetecamilano.it">cinestore@cinetecamilano.it</a> indicando il codice dell'ordine (<strong>{{ $order->transaction_id }}</strong>).
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
@include('components.main.back')
@endsection
@section('scripts')
  <script src="/js/cinestore.js"></script>
@endsection
