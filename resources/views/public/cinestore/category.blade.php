@extends('layouts.public', ['active' => 'cinestore', 'categories' => $product_categories])
@section('title', "Cinestore - $category->name")
@section('seo')
  <meta name="description" content="Il cinestore è uno spazio virtuale dove acquistare i prodotti ed i servizi della Fondazione Cineteca di Milano." />
@endsection
@section('content')
<section>
  <div class="row no-gutters">
    <div class="col pt-5">
      <div class="row pt-md-5">
        <div class="col pt-5 px-5">
          <h1 align="center">{{ $category->name }}</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-6 offset-3">
          <hr>
        </div>
      </div>
    </div>
  </div>
  <div class="row p-4">
    <div class="col-md-6 offset-md-3">
      <form method="post" action="{{ route('search.cinestore') }}">
        <div class="row">
          <div class="col-md-8">
            <div class="form-group">
              {{ csrf_field() }}
              {{ method_field('POST') }}
              <input type="hidden" name="category" value="{{ $category->id }}">
              <input type="text" name="query" placeholder="Cerca..." class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Cerca</button>
          </div>
        </div>
    </div>
  </div>
</section>
<section id="main-grid">
  <div class="row">
    <div class="col-md-10 offset-md-1">
      <div class="container-fluid pb-5 pr-3 pl-3">
        <div class="row no-gutters">
            @foreach ($products as $key => $product)
                  <div class="col-md-4 p-3">
                    <img src="{{ Storage::disk('local')->url($product->media()->first()->thumb) }}" alt="{{ $product->name }}" class="img-fluid w-100">
                    <div class="container-fluid p-4 bg-dark-gray">
                      @if ($category->id != 6)
                        <a href="{{ route('cinestore.single', [$product->category->slug, $product->slug]) }}">
                          <h3>{{ $product->name }}</h3>
                        </a>
                      @else
                        <a href="{{ route('cinestore.single', ['percorsi', $product->slug]) }}">
                          <h3>{{ $product->name }}</h3>
                        </a>
                      @endif
                      <p><span class="badge badge-default">{{ $product->category_name }}</span></p>
                      <p>{{ substr(strip_tags($product->description), 0, 100) }}{{ strlen(strip_tags($product->description)) > 100 ? '...' : "" }}</p>
                      @if ($category->id != 6)
                        @if ($product->purchasable == 1 && $product->available > 0 && $product->product_prices()->count() > 0)
                          <div id="price-{{ $product->id }}" class="price-select form-group">
                            <select class="form-control">
                              @foreach ($product->product_prices()->get() as $key => $product_price)
                                <option value="{{ json_encode($product_price) }}">{{ $product_price->description }} - {{ $product_price->price }}€</option>
                              @endforeach
                            </select>
                          </div>
                          <a class="btn btn-primary text-white mb-3 btn-block add-to-cart" data-options="true" data-id="{{ $product->id }}" data-name="{{ $product->name }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Acquista</a>
                          <a class="btn btn-info btn-block" href="{{ route('cinestore.single', [$product->category->slug, $product->slug]) }}"><i class="fa fa-info" aria-hidden="true"></i> Dettagli</a>
                        @else
                          <a class="btn btn-info btn-block" href="{{ route('cinestore.single', [$product->category->slug, $product->slug]) }}"><i class="fa fa-info" aria-hidden="true"></i> Dettagli</a>
                        @endif
                      @else
                        <a class="btn btn-info btn-block" href="{{ route('cinestore.single', ['percorsi', $product->slug]) }}"><i class="fa fa-info" aria-hidden="true"></i> Dettagli</a>
                      @endif
                    </div>
                  </div>
            @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
@include('components.main.back')
@endsection
@section('scripts')
  <script src="/js/cinestore.js"></script>
  {{-- <script src="/js/cinestoreV2.js') }}"></script> --}}
@endsection
