@extends('layouts.public', ['active' => 'cinestore', 'categories' => $product_categories])
@section('title', 'Cinestore')
@section('seo')
  <meta name="description" content="Il cinestore è uno spazio virtuale dove acquistare i prodotti ed i servizi della Fondazione Cineteca di Milano." />
@endsection
@section('content')
<section>
  <div id="main-slider" class="w-100">
    <div class="row px-2 px-md-5 bg-faded mx-4">
      <div class="col">
        <div class="container-fluid">
          <div id="myCarousel" class="carousel-custom carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              @foreach ($slides as $key => $slide)
                <li data-target="#myCarousel" data-slide-to="{{ $key }}" class="{{ $key == 0 ? 'active' : '' }}"></li>
              @endforeach
            </ol>
            <div class="carousel-inner" role="listbox">
              @foreach ($slides as $key => $slide)
                <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                  <img class="img-fluid h-100 w-100" src="{{ $slide['img'] }}" alt="First slide">
                  <div class="carousel-caption">
                    <h1 class="pt-3">{{ $slide['name'] }}</h1>
                    <p class="pt-4"><a class="btn btn-primary" href="{{ url($slide['url']) }}" role="button"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Scopri di più</a></p>
                  </div>
                </div>
              @endforeach
            </div>
            <a class="carousel-control-prev text-default" href="#myCarousel" role="button" data-slide="prev">
              <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next text-default" href="#myCarousel" role="button" data-slide="next">
              <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="cerca" class="m5-t pt-5">
  <div class="row pt-5">
    <div class="col-md-6 offset-md-3">
      <form method="post" action="{{ route('search.cinestore.mixed') }}">
        <div class="row">
          <div class="col-md-8">
            <div class="form-group">
              {{ csrf_field() }}
              {{ method_field('POST') }}
              <input type="text" name="query" placeholder="Cerca..." class="form-control">
            </div>
          </div>
          <div class="col-md-4">
            <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Cerca</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<section id="main-grid">
  @include('components.cinestore.featured', ['title' => 'Prodotti in evidenza', 'products' => $general_products])
  @include('components.cinestore.featured', ['title' => 'Video in evidenza', 'products' => $video_products])
  @include('components.cinestore.featured', ['title' => 'Foto in evidenza', 'products' => $photo_products])
  @include('components.cinestore.featured-group', ['title' => 'Percorsi in evidenza', 'products' => $products_group])
</section>
<section id="menu-boxes" class="mt-5 pt-2">
  @include('components.main.title', ['title' => 'Categorie Prodotti'])
  <div class="row no-gutters pt-5">
    <div class="col">
      <div class="row">
        <div class="col-md-10 offset-md-1">
          @foreach ($product_categories as $key => $category)
            @if ($key % 3 == 0)
              <div class="row no-gutters">
            @endif
            <div class="col-md-4 pb-5">
              <div class="container box-container">
                <img class="img-fluid pb-3 w-100" src="{{ Storage::disk('local')->url($category->img) }}" alt="{{ $category->name }}">
                <h2 class="text-center">
                  <a href="{{ route('cinestore.category', $category->slug) }}" class="text-default">{{ $category->name }}</a>
                </h2>
              </div>
            </div>
            @if ($key % 3 == 2)
              </div>
            @endif
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@section('scripts')
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
  <script src="/js/cinestore.js"></script>
@endsection
