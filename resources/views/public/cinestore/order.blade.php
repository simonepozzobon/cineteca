@extends('layouts.public', ['active' => 'cinestore', 'categories' => $product_categories])
@section('title', "Verifica l'ordine")
@section('stylesheets')
  <style media="screen">
    #shipping-check {
      cursor: pointer;
    }
  </style>
@endsection
@section('content')
  <div id="cart">
    <cart></cart>
  </div>
  <section id="braintree" class="d-none">
    <div class="row no-gutters">
      <div class="col">
        <div class="row pt-5">
          <div class="col pt-5 pl-5 pr-5">
            <h1 align="center">Pagamento</h1>
          </div>
        </div>
        <div class="row pb-5">
          <div class="col-md-6 offset-md-3">
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-md-8 offset-md-2 pb-5">
            <form id="checkout" action="{{ route('cinestore.payment') }}" method="post">
              {{ csrf_field() }}
              {{ method_field('POST') }}
              <div id="dropin-container" class="pb-5"></div>
              <input type="hidden" id="nonce" name="payment_method_nonce"></input>
              <input type="hidden" id="order_id" name="order_id" value="">
              <div class="container-fluid d-flex justify-content-around">
                <button id="payment-nonce" type="button" name="button" class="btn btn-primary">Verifica</button>
                <button id="payment-button" class="btn btn-primary d-none"><i class="fa fa-check" aria-hidden="true"></i> Procedi</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('scripts')
  <script src="/js/cart.js"></script>
  <script src="//js.braintreegateway.com/web/dropin/1.7.0/js/dropin.min.js"></script>
  <script type="text/javascript">

    /**
     *
     *  Braintree Pagamento
     *
     */

    var form = document.querySelector('#checkout');
    var nonceInput = document.querySelector('#nonce');

    braintree.dropin.create({
        authorization: '@braintreeClientToken',
        container: '#dropin-container',
        paypal: {
          flow: 'vault'
        },
        card: {
          cardholderName: {
            required: true
          },
          cvv: {
            required: true
          }
        },
        locale: 'it_IT'
      }, function (err, dropinInstance) {
        if (err) {
          // Handle any errors that might've occurred when creating Drop-in
          console.log('Error in create');
          console.error(err);
          return;
        }

        $('#payment-nonce').on('click', function(e){
          e.preventDefault();
          var order_id = localStorage.getItem('cinestore-order');
          console.log(order_id);
          $('#order_id input[name="order_id"]').val(order_id);

          dropinInstance.requestPaymentMethod(function (err, payload) {
              if (err) {
                // Handle errors in requesting payment method
                console.error(err);
                return;
              }
              // Send payload.nonce to your server
              nonceInput.value = payload.nonce;
              $('#payment-nonce').addClass('d-none');
              $('#payment-button').removeClass('d-none');
              $('div[data-braintree-id="toggle"]').addClass('d-none');
          });
      });
    });

  </script>
@endsection
