@extends('layouts.public', ['active' => 'cinestore', 'categories' => $product_categories])
@section('title', "$product->name")
@section('seo')
  <meta name="description" content="{{ substr(strip_tags($product->description), 0, 170) }}{{ strlen(strip_tags($product->description)) > 170 ? '...' : "" }}" />
@endsection
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/single.css') }}">
@endsection
@section('content')
  <article class="pb-5">
    @if ($product->video()->count() > 0)
      @include('components.cinestore.video-header', ['title' => $product->name, 'videoSrc' => $videoSrc])
    @else
      @include('components.main.film-header', ['title' => $product->name, 'image' => Storage::disk('local')->url($product->media->first()->slide_img) ])
    @endif
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div class="row p-5">
          <div class="col-md-2">
            <div class="row">
              <div class="col">
                <h5>Foto Prodotto</h5>
                <hr>
                <img src="{{ Storage::disk('local')->url($product->media->first()->portrait) }}" alt="{{ $product->name }}" class="img-fluid w-100">
              </div>
            </div>
          </div>
          <div class="col-md-7">
            <div class="row">
              <div class="col">
                <h5>Descrizione Prodotto</h5>
                <hr>
                <p>{!! $product->description !!}</p>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            @if ($product->purchasable == 1 && $product->available > 0 && $product->product_prices()->count() > 0)
              <div class="row pb-5">
                <div class="col">
                  <h5>Prezzo</h5>
                  <hr>
                  <div id="price-{{ $product->id }}" class="price-select form-group">
                    <select class="form-control">
                      @foreach ($product->product_prices()->get() as $key => $price)
                        {{ dd($price) }}
                        <option value="{{ json_encode($price) }}">{{ $price->description }} - {{ $price->price }}€</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="pt-4">
                    <a href="#" class="btn btn-primary add-to-cart" data-options="true" data-id="{{ $product->id }}" data-name="{{ $product->name }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Acquista</a>
                  </div>
                </div>
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </article>
  @include('components.main.back')
@endsection
@section('scripts')
  <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCWWjzGK-o2GBGAPabC1nm6AETotrd53TQ&callback=initMap"
  async defer></script>
@endsection
