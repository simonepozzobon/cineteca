@extends('layouts.public', ['active' => 'cinestore', 'categories' => $product_categories])
@section('title', "Cinestore $product->name")
@section('seo')
  <meta name="description" content="{{ substr(strip_tags($product->description), 0, 170) }}{{ strlen(strip_tags($product->description)) > 170 ? '...' : "" }}" />
@endsection
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/single.css') }}">
@endsection
@section('content')
  <article class="pb-5">
    @include('components.cinestore.video-header', ['title' => $product->name, 'videoSrc' => $videoSrc ])
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div class="row pt-5">
          <div class="col-md-2">
            <div class="row">
              <div class="col">
                <h5>Foto Prodotto</h5>
                <hr>
                <img src="{{ Storage::disk('local')->url($product->media->first()->portrait) }}" alt="{{ $product->name }}" class="img-fluid w-100">
              </div>
            </div>
          </div>
          <div class="col-md-7">
            <div class="row">
              <div class="col">
                <h5>Descrizione Prodotto</h5>
                <hr>
                <p>{!! $product->description !!}</p>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            @if ($product->purchasable == 1 && $product->available > 0 && $product->product_prices()->count() > 0)
              <div class="row pb-5">
                <div class="col">
                  <h5>Prezzo</h5>
                  <hr>
                  <div id="price-{{ $product->id }}" class="price-select form-group">
                    <select class="form-control">
                      @foreach ($product->product_prices()->get() as $key => $price)
                        <option value="{{ $price->price }}">{{ $price->description }} - {{ $price->price }}€</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="pt-4">
                    <a href="#" class="btn btn-primary add-to-cart" data-options="true" data-id="{{ $product->id }}" data-name="{{ $product->name }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Acquista</a>
                  </div>
                </div>
              </div>
            @endif
            @if ($product->category->id == 5)
              <div class="row pb-5">
                <div class="col">
                  <h5>Informazioni per l'acquisto</h5>
                  <hr>
                  <a href="mailto:diffusione.culturale@cinetecamilano.it">diffusione.culturale@cinetecamilano.it</a>
                </div>
              </div>
            @endif
            <div class="row pb-5">
                <div class="col">
                  <h5>Prodotto in Evidenza</h5>
                  <hr>
                  <img src="{{ Storage::disk('local')->url($last_prod->media()->first()->landscape) }}" alt="{{ $last_prod->name }}" class="img-fluid w-100">
                  <h5 class="py-4">{{ $last_prod->name }}</h5>
                  <a href="{{ route('cinestore.single', [$last_prod->category, $last_prod->slug]) }}" class="btn btn-primary"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Vai al prodotto</a>
                </div>
            </div>
          </div>
        </div>
        @if ($product->gallery()->count() > 0)
          <div class="row pb-5">
            <div class="col">
              @include('components.main.title', ['title' => 'Galleria'])
              @foreach ($product->gallery()->get() as $key => $gallery)
                <div class="row gallery">
                  @foreach ($gallery->medias()->get() as $key => $media)
                    <div class="col-md-4 pb-4">
                        <img class="img-fluid w-100" src="{{ Storage::disk('local')->url($media->thumb) }}" alt="{{ $product->name }}">
                    </div>
                  @endforeach
                </div>
              @endforeach
            </div>
          </div>
        @endif
      </div>
    </div>
    @include('components.cinestore.related-product', ['products' => $feat_prods, 'last_prod' => $last_prod])
  </article>
  @include('components.main.back')
@endsection
@section('scripts')
  <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCWWjzGK-o2GBGAPabC1nm6AETotrd53TQ&callback=initMap"
  async defer></script>
@endsection
