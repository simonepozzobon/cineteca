@extends('layouts.public')
@section('title', 'Risultati Ricerca')
@section('content')
<section class="p-5">
  <div class="row pt-5">
    <div class="col-md-10 offset-md-1">
      @if ($results->count() > 0)
        <h5>Risultati della ricerca</h5>
        <hr>
        @foreach ($results as $key => $result)
          <div class="row">
            <div class="col-md-2">
              <h5><span class="badge badge-default" style="background-color:{{ $result['color'] }}">{{ $result['type'] }}</span></h5>
            </div>
            <div class="col-md-4">
              <img src="{{ $result['img'] }}" class="img-fluid">
            </div>
            <div class="col-md-6">
              <h3>{{ $result['title'] }}</h3>
              <p>{{ substr(strip_tags($result['description']), 0, 200) }}{{ strlen(strip_tags($result['description'])) > 200 ? '...' : "" }}</p>
              <a href="{{ url($result['url']) }}" class="btn btn-primary">Leggi di più</a>
            </div>
          </div>
          <div class="row pb-5">
            <div class="col-md-6 offset-md-3">
              <hr>
            </div>
          </div>
        @endforeach
      @else
        <h1 class="text-center p-5"><i class="fa fa-meh-o" aria-hidden="true"></i></h1>
        <h3 class="text-center pb-5">Ooops, nessun risultato trovato per "{{ $query }}"</h3>
        <div class="row p-5">
          {{-- <div class="col-md-4 offset-md-4">
            <a class="btn btn-primary btn-block" href="{{ url()->previous() }}">
              <i class="fa fa-arrow-left" aria-hidden="true"></i> Indietro
            </a>
          </div> --}}
        </div>
      @endif
    </div>
  </div>
</section>
@endsection
