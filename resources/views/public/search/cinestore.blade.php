@extends('layouts.public', ['active' => 'cinestore', 'categories' => $product_categories])
@section('title', "Cinestore - $category->name")
@section('seo')
  <meta name="description" content="Il cinestore è uno spazio virtuale dove acquistare i prodotti ed i servizi della Fondazione Cineteca di Milano." />
@endsection
@section('content')
  <div class="pt-5"></div>
  @include('components.main.title', ['title' => $category->name])
  <div class="row">
    <div class="col-md-10 offset-md-1">
      <div class="row">
        @foreach ($products as $key => $product)
          <div class="col-md-4 p-3">
            <img src="{{ Storage::disk('local')->url($product->media()->first()->thumb) }}" alt="{{ $product->name }}" class="img-fluid w-100">
            <div class="container-fluid p-4 bg-dark-gray">
              @if ($product->category->id != 6)
                <a href="{{ route('cinestore.single', [$product->category->slug, $product->slug]) }}">
                  <h3>{{ $product->name }}</h3>
                </a>
              @else
                <a href="{{ route('cinestore.single', ['percorsi', $product->slug]) }}">
                  <h3>{{ $product->name }}</h3>
                </a>
              @endif
              <p><span class="badge badge-default">{{ $product->category_name }}</span></p>
              <p>{{ substr(strip_tags($product->description), 0, 100) }}{{ strlen(strip_tags($product->description)) > 100 ? '...' : "" }}</p>
              @if ($product->category->id != 6)
                @if ($product->purchasable == 1 && $product->available > 0 && $product->product_prices()->count() > 0)
                  <div id="price-{{ $product->id }}" class="price-select form-group">
                    <select class="form-control">
                      @foreach ($product->product_prices()->get() as $key => $product_price)
                        <option value="{{ json_encode($product_price) }}">{{ $product_price->description }} - {{ $product_price->price }}€</option>
                      @endforeach
                    </select>
                  </div>
                  <a class="btn btn-primary text-white mb-3 btn-block add-to-cart" data-options="true" data-id="{{ $product->id }}" data-name="{{ $product->name }}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Acquista</a>
                  <a class="btn btn-info btn-block" href="{{ route('cinestore.single', [$product->category->slug, $product->slug]) }}"><i class="fa fa-info" aria-hidden="true"></i> Dettagli</a>
                @else
                  <a class="btn btn-info btn-block" href="{{ route('cinestore.single', [$product->category->slug, $product->slug]) }}"><i class="fa fa-info" aria-hidden="true"></i> Dettagli</a>
                @endif
              @else
                <a class="btn btn-info btn-block" href="{{ route('cinestore.single', ['percorsi', $product->slug]) }}"><i class="fa fa-info" aria-hidden="true"></i> Dettagli</a>
              @endif
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </div>
  @include('components.main.back')
@endsection
@section('scripts')
  <script src="/js/cinestore.js"></script>
@endsection
