@extends('layouts.public')
@section('title', "$page->title")
@section('seo')
  <meta name="description" content="{{ substr(strip_tags($page->content), 0, 170) }}{{ strlen(strip_tags($page->content)) > 170 ? '...' : "" }}" />
@endsection
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/single.css') }}">
@endsection
@section('content')
<article>
  @include('components.main.film-header', ['image' => Storage::disk('local')->url($page->media->first()->slide_img), 'title' => $page->title])
  <div class="row pb-5">
      @if ($page->category->id === 3 && $page->id !== 40)
        <div class="col-md-10 offset-md-1">
          <div class="row">
            <div id="content" class="col-md-9">
              <p class="px-5 pb-5">
                {!! $page->content !!}
              </p>
            </div>
            <div class="col-md-3 pt-5">
              @if ($page->hide_info == 0)
                <div class="row pb-5">
                  <div class="col">
                    <h5>Info Generali</h5>
                    <hr>
                    <p>
                      <a class="" data-toggle="collapse" href="#orari" aria-expanded="false" aria-controls="biglietti">
                        Orari apertura
                      </a>
                      <div class="collapse" id="orari">
                          <p>
                            lunedì chiuso<br>
                            dal martedi al giovedì<br>
                            dalle 15:00 alle 19:00<br>
                            venerdì<br>
                            dalle 15:00 alle 21:00<br>
                            sabato e domenica<br>
                            dalle 15:00 alle 19:00<br><br>
                            <b>Ultimo ingresso un'ora prima della chiusura</b>
                          </p>
                      </div>
                    </p>
                    <p>
                      <a class="" data-toggle="collapse" href="#biglietti" aria-expanded="false" aria-controls="biglietti">
                        Costo Biglietti
                      </a>
                      <div class="collapse" id="biglietti">
                            <ul>
                              <li>Intero € 6,50</li>
                              <li>Ridotto* € 5,00</li>
                              <li>1 adulto + 1 bambino € 7,00</li>
                            </ul>
                            <em>*Under 14, studenti universitari, cinetessera, tessera FAI, Touring Junior, Soci Touring Club Adulti, Milan ID Card</em><br>
                            <br>
                            OMAGGIO<br>
                            Abbonamento Musei Lombardia Milano<br>
                            <br>
                            <strong>L'ingresso ridotto e omaggio verrà applicato solo previa esibizione in cassa della tessera in possesso</strong><br>
                            <br>
                            La CINETESSERA costa € 10,00 e consente l’ingresso ridotto a tutte le proiezioni del MIC<br>
                            <br>
                            Inoltre venerdi, sabato e domenica alle ore 15.30 e 17.30, alle medesime condizioni economiche, è possibile visitare anche il Nuovo Archivio Storico dei Film, previa prenotazione obbligatoria al numero 0287242114.
                      </div>
                    </p>
                  </div>
                </div>
              @endif
              @include('components.sidebar.location-map', ['name' => 'MIC', 'address' => 'Viale Fulvio Testi, 121 - 20162', 'city' => 'Milano', 'slug' => 'cinema-mic'])

            </div>
          </div>
        </div>
      @else
        <div class="col-md-8 offset-md-2">
          <p class="px-5 pb-5">
            {!! $page->content !!}
          </p>
        </div>
      @endif
  </div>
</article>
@if ($page->pageLinks->count() > 0)
  <section>
    <div class="row no-gutters">
      <div class="col">
        <div class="row pt-5">
          <div class="col pt-5 px-5">
            <h1 align="center">Attività</h1>
          </div>
        </div>
        <div class="row pb-5">
          <div class="col-6 offset-3">
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col-md-10 offset-md-1">
            @foreach ($page->pageLinks as $key => $link)
              @if ($key % 3 == 0)
                <div class="row no-gutters">
              @endif
              <div class="col-md-4 pb-5">
                <div class="container box-container">
                  <img class="img-fluid pb-3 w-100" src="{{ Storage::disk('local')->url($link->page->media->first()->thumb) }}" alt="{{ $link->title }}">
                  <h2 class="text-center">
                    <a href="/pagina/{{ $link->page->slug }}" class="text-default">{{ $link->page->title }}</a>
                  </h2>
                </div>
              </div>
              @if ($key % 3 == 2)
                </div>
              @endif
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </section>
@endif
@if ($page->gallery()->count() > 0)
  <section>
    <div class="row no-gutters">
      <div class="col">
        <div class="row pt-5">
          <div class="col pt-5 px-5">
            <h1 align="center">Galleria</h1>
          </div>
        </div>
        <div class="row pb-5">
          <div class="col-6 offset-3">
            <hr>
          </div>
        </div>
        @foreach ($page->gallery()->get() as $key => $gallery)
          @if ($gallery->title)
            <div class="row pb-4">
              <div class="col">
                <h2 class="text-center">{{ $gallery->title }}</h2>
              </div>
            </div>
          @endif
          @if ($gallery->description && strlen($gallery->description) > 1)
            <div class="row">
              <div class="col-md-8 offset-md-2 px-md-5 px-4">
                <p>{{ $gallery->description }}</p>
              </div>
            </div>
          @endif
          <div class="row">
            <div class="col-md-10 offset-md-1">
              <div class="row no-gutters">
                @foreach ($gallery->medias()->get() as $key => $media)
                  <div class="col-md-4 pb-5">
                    <div class="container box-container">
                      <img class="img-fluid pb-3 w-100" src="{{ Storage::disk('local')->url($media->thumb) }}" alt="">
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
@endif
@include('components.main.back')
@endsection
@section('scripts')
  <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCWWjzGK-o2GBGAPabC1nm6AETotrd53TQ&callback=initMap" async defer></script>
  <script type="text/javascript">
    $('#content img').each(function(){
      $(this).addClass('img-fluid');
    });
  </script>
@endsection
