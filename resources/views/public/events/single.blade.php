@extends('layouts.public')
@section('title', "$event->title")
@section('seo')
  <meta name="description" content="{{ substr(strip_tags($event->description), 0, 170) }}{{ strlen(strip_tags($event->description)) > 170 ? '...' : "" }}" />
@endsection
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/single.css') }}">
  <style media="screen">
     .modal-center {
       top: 50%;
       left: 50%;
       transform: translate(-50%, -50%);
     }
  </style>
@endsection
@section('content')
  <article class="pb-5">
    @include('components.main.film-header', ['title' => $event->title, 'image' => Storage::disk('local')->url($event->media->first()->slide_img)])
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div class="row p-5">
          <div class="col-md-2">
            <div class="row pb-5">
              <div class="col">
                <h5>Data Evento</h5>
                <hr>
                <p>
                  <strong>{{ $event->day }}, {{ $event->month }} {{ $event->year }}</strong>
                </p>
                <h3>{{ $event->time }}</h3>
              </div>
            </div>
            @if ($event->limit_guest == 1 && $event->available_hidden == 0)
              <div class="row pb-5">
                <div class="col">
                  <h5>Disponibilità Posti</h5>
                  <hr>
                  @if ($event->guest_left > 0)
                    <h3>{{ $event->guest_left }}</h3>
                    <p>Posti disponibili.</p>
                  @elseif ($event->wait_left > 0)
                    <p class="text-warning"><b>Easuriti</b></p>
                    <h3>{{ $event->wait_left }}</h3>
                    <p>Posti in lista d'attesa.</p>
                  @else
                    <span class="d-inline-block text-danger"><b>Easuriti</b></span>
                  @endif
                </div>
              </div>
            @endif
          </div>
          <div class="col-md-7">
            <div class="row pb-5">
              <div class="col">
                <h5>Scheda</h5>
                <hr>
                <p>{!! $event->description !!}</p>
              </div>
            </div>
            @if ($event->film_id != null)
              <div class="row pb-5">
                <div class="col">
                  <h5>Film</h5>
                  <hr>
                  <div class="row pb-3">
                    <div class="col-md-4">
                      <img class="w-100 img-fluid" src="{{ Storage::disk('local')->url($event->film->media->first()->landscape) }}" alt="{{ $event->film->title }}">
                    </div>
                    <div class="col-md-8">
                      <h3>
                        <a href="{{ route('single.film', $event->film->slug) }}">{{ $event->film->title }}</a>
                      </h3>
                      <h6>{{ $event->film->location->name }}</h6>
                      <p>{{ substr(strip_tags($event->film->description), 0, 100) }}{{ strlen(strip_tags($event->film->description)) > 100 ? '...' : "" }}</p>
                      <a href="{{ route('single.film', $event->film->slug) }}" class="btn btn-primary">Scheda Film</a>
                    </div>
                  </div>
                </div>
              </div>
            @endif
            @if ($event->limit_guest == 1)
              @if ($event->guest_left > 0 || $event->wait_left > 0)
                <div class="row pb-5">
                  <div class="col">
                    <h5 class="d-inline-block">Prenota il tuo posto</h5>
                    <hr>
                    @if ($event->guest_left == 0)
                      <p class="text-center text-warning"><b>Attenzione:</b> I posti sono esauriti ma è ancora possibile registrarsi in lista d'attesa</p>
                    @endif
                    <form id="rsvp">
                      {{ csrf_field() }}
                      {{ method_field('POST') }}
                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="name">Nome:</label>
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="surname">Cognome:</label>
                            <input type="text" name="surname" value="{{ old('surname') }}" class="form-control" placeholder="">
                          </div>
                        </div>
                      </div>
                      @if ($event->single_guest == 1)
                        <div class="row pb-5">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="email">Email:</label>
                              <input type="text" name="email" value="{{ old('email') }}" class="form-control" placeholder="">
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="phone">Numero di telefono:</label>
                              <input type="text" name="phone" value="{{ old('phone') }}" class="form-control" placeholder="">
                            </div>
                          </div>
                          <input type="hidden" name="guests" value="1">
                          <input type="hidden" name="kids_guests" value="0">
                        </div>
                      @else
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label for="email">Email:</label>
                              <input type="text" name="email" value="{{ old('email') }}" class="form-control" placeholder="">
                            </div>
                          </div>
                        </div>
                        @if ($event->kids_list == 1)
                          <div class="row pb-5">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="phone">Numero di telefono:</label>
                                <input type="text" name="phone" value="{{ old('phone') }}" class="form-control" placeholder="">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="guests">N. totale di Adulti:</label>
                                <input type="text" name="guests" value="{{ old('guests') }}" class="form-control" placeholder="">
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="form-group">
                                <label for="surname">N. totale di bambini:</label>
                                <input type="text" name="kids_guest" value="{{ old('kids_guest') }}" class="form-control" placeholder="">
                              </div>
                            </div>
                          </div>
                        @else
                          <div class="row pb-5">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="phone">Numero di telefono:</label>
                                <input type="text" name="phone" value="{{ old('phone') }}" class="form-control" placeholder="">
                              </div>
                            </div>
                            <div class="col-md-6">
                              <div class="form-group">
                                <label for="guests">N. totale di biglietti:</label>
                                <input type="text" name="guests" value="{{ old('guests') }}" class="form-control" placeholder="">
                              </div>
                            </div>
                          </div>
                        @endif
                      @endif


                      <div class="d-flex justify-content-center">
                        <button id="submit" type="button" name="button" class="btn btn-primary"><i class="fa fa-user-plus" aria-hidden="true"></i> Prenota</button>
                      </div>
                    </form>
                    <div id="msg-rsvp"></div>
                    <div class="modal fade modal-center" id="rsvp-alert" tabindex="-1" role="dialog" aria-labelledby="rsvpalert" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-body">
                            <h3 class="text-center">Attenzione!</h3>
                            <h6 id="rsvp-alert-msg" class="text-center py-5"></h6>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Annulla</button>
                            <button id="rsvp-alert-accept" type="button" class="btn btn-primary">Accetta e Prenota</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              @else
                <div class="row pb-5">
                  <div class="col">
                    <h5 class="d-inline-block">Prenota il tuo posto</h5>
                    <hr>
                    <h3 class="p-5 text-center text-danger">Siamo spiacenti!</h3>
                    @if ($event->event_type_id == 9)
                      <p class="text-center text-danger">
                        Purtroppo i posti sono terminati<br>
                        E’ comunque possibile accedere allo spettacolo previo pagamento del biglietto direttamente in cassa o sul sito webtic.
                      </p>
                    @elseif ($event->event_type_id == 10)
                      <p class="text-center text-danger">
                        Purtroppo tutti i posti sono terminati
                      </p>
                    @endif
                  </div>
                </div>
              @endif
            @endif
          </div>
          <div class="col-md-3">
            @if ($event->festival == 1 && $event->fest_cat_id == 1)
              @include('components.sidebar.general-info', ['location' => $event->location->slug, 'free' => true])
            @else
              @include('components.sidebar.general-info', ['location' => $event->location->slug])
            @endif
            @include('components.sidebar.location-map', ['name' => $event->location->name, 'address' => $event->location->address, 'city' => $event->location->city, 'slug' => $event->location->slug])
          </div>
        </div>
      </div>
    </div>
    @include('components.main.back')
  </article>
@endsection
@section('scripts')
  <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCWWjzGK-o2GBGAPabC1nm6AETotrd53TQ&callback=initMap"
  async defer></script>
  <script type="text/javascript">
    $('#submit').on('click', function(e) {
      e.preventDefault();
      $(this).addClass('disabled');
      var form = {
        '_token' :      $('input[name=_token]').val(),
        'event_id' :    {{ $event->id }},
        'name' :        $('input[name="name"]').val(),
        'surname' :     $('input[name="surname"]').val(),
        'email' :       $('input[name="email"]').val(),
        'phone' :       $('input[name="phone"]').val(),
        'guests' :      $('input[name="guests"]').val(),
        'kids_guest' :  $('input[name="kids_guest"]').val(),
      };

      $.ajax({
        type: 'post',
        url:  '{{ route('event.check') }}',
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
        data: form,
        success: function(response) {
          console.log(response);
          if (response.available == true && response.alert == true) {
            if (response.wait == 1) {
              $('#rsvp-alert-msg').html('Purtroppo sono rimasti solo '+response.left+' posti assicurati,<br><br> <u>uno dei suoi ospiti verrà messo in lista d\' attesa</u>');
            } else {
              $('#rsvp-alert-msg').html('Purtroppo sono rimasti solo '+response.left+' posti assicurati,<br><br> <u>'+response.wait+' dei suoi ospiti verranno messi in lista d\' attesa</u>');
            }
            $('#rsvp-alert').modal('show');

            $('#rsvp-alert-accept').on('click', function(e) {
              e.preventDefault();
              $('#rsvp-alert').modal('hide');

              sendRequest(form);
            });
          } else if (response.available == true) {
            sendRequest(form);
          } else {
            var msg = '<h3 class="p-5 text-center text-danger">Siamo spiacenti!</h3><p class="text-center text-danger">Si è verificato un errore.</p>';
            $('#rsvp').remove();
            $('#msg-rsvp').prepend(msg);
          }
        },
        error: function(errors) {
          console.log(errors);
        }
      });

      // sendRequest(data);
    });

    function sendRequest(data)
    {
      $.ajax({
        type: 'post',
        url:  '{{ route('event.rsvp') }}',
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') },
        data: data,
        success: function(data) {
          console.log(data);
          $('.form-control-danger').removeClass('form-control-danger');
          $('.has-danger').removeClass('has-danger');
          $('.form-control-feedback').remove();

          $('#rsvp').remove();
          $('#msg-rsvp').prepend(data.message);
        },
        error: function(errors) {
          console.log(errors);
          $('.form-control-danger').removeClass('form-control-danger');
          $('.has-danger').removeClass('has-danger');
          $('.form-control-feedback').remove();
          $.each(errors.responseJSON.errors, function(k, v) {
            var elem = $('input[name='+k+']');
            elem.addClass('form-control-danger');
            elem.parent().addClass('has-danger');
            elem.parent().append('<div class="form-control-feedback">Richiesto</div>');
          });
        }
      });
    }
  </script>
@endsection
