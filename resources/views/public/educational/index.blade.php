@extends('layouts.public', ['active' => 'scuole'])
@section('title', 'Scuole')
@section('seo')
  <meta name="description" content="Vedere un film al cinema insieme ai propri compagni può essere un’esperienza unica, così come sperimentare e toccare con mano gli strumenti e i mestieri del cinema!" />
@endsection
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/single.css') }}">
@endsection
@section('content')
  <section id="main-image">
    @include('components.main.film-header', ['title' => 'Scuole', 'image' => asset('img/scuole_slider.jpg')])
  </section>
  <section id="description">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <p class="p-5">
          Vedere un film al cinema insieme ai propri compagni può essere un’esperienza unica, così come sperimentare e toccare con mano gli strumenti e i mestieri del cinema!
          Attraverso visite guidate, proiezioni e laboratori il MIC offre alle classi di ogni ordine e grado un percorso di educazione all’immagine, intesa sia come strumento di approfondimento didattico che come mezzo espressivo da esplorare nelle sue infinite possibilità.
          Durante il corso dell’anno il MIC organizza ed ospita seminari e corsi per insegnanti.
        </p>
      </div>
    </div>
  </section>
  <section id="activities">
    @include('components.main.square-box', ['title' => 'Attività Per Le Scuole', 'items' => $pages, 'type' => 'pagina'])
  </section>
  <section id="back">
    @include('components.main.back')
  </section>
@endsection
