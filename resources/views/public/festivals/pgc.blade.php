@extends('layouts.public', ['teacher' => true])
@section('title', "$festival->title")
@section('seo')
  <meta name="description" content="{{ substr(strip_tags($festival->description), 0, 170) }}{{ strlen(strip_tags($festival->description)) > 170 ? '...' : "" }}" />
@endsection
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/single.css') }}">
  {{-- Custom Slider Styles --}}
  <style media="screen">

    #carouselExampleIndicators .carousel-caption {
      position: inherit;
      right: 0;
      bottom: 0;
      left: 0;
    }

    #carouselExampleIndicators .carousel-control-prev,
    #carouselExampleIndicators .carousel-control-next,
    .carousel-custom .carousel-control-prev,
    .carousel-custom .carousel-control-next {
      width: 8%;
      min-width: 40px;
      font-size: 2rem;
    }

    #carouselExampleIndicators .carousel-indicators {
      /*right: 1.6rem;*/
      bottom: -1.6rem;
    }

    #carouselExampleIndicators .carousel-indicators .active {
      background-color: #292b2c;
    }

    #carouselExampleIndicators .carousel-indicators li {
      background-color: rgba(41, 43, 44, .5)
    }

    #carouselExampleIndicators .carousel-caption,
    #carouselExampleIndicators .carousel-overlay {
      background: transparent !important;
    }

    @media (min-width: 768px) {

        /* show 3 items */
        #carouselExampleIndicators .carousel-inner .active,
        #carouselExampleIndicators .carousel-inner .active + .carousel-item,
        #carouselExampleIndicators .carousel-inner .active + .carousel-item + .carousel-item {
            display: block;
        }

        #carouselExampleIndicators .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
        #carouselExampleIndicators .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
        #carouselExampleIndicators .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item {
            transition: none;
        }

        #carouselExampleIndicators .carousel-inner .carousel-item-next,
        #carouselExampleIndicators .carousel-inner .carousel-item-prev {
          position: relative;
          transform: translate3d(0, 0, 0);
        }

        #carouselExampleIndicators .carousel-inner .active.carousel-item + .carousel-item + .carousel-item + .carousel-item {
            position: absolute;
            top: 0;
            right: -33.33333%;
            z-index: -1;
            display: block;
            visibility: visible;
        }

        /* left or forward direction */
        #carouselExampleIndicators .active.carousel-item-left + .carousel-item-next.carousel-item-left,
        #carouselExampleIndicators .carousel-item-next.carousel-item-left + .carousel-item,
        #carouselExampleIndicators .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
        #carouselExampleIndicators .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(-100%, 0, 0);
            visibility: visible;
        }

        /* farthest right hidden item must be abso position for animations */
        #carouselExampleIndicators .carousel-inner .carousel-item-prev.carousel-item-right {
            position: absolute;
            top: 0;
            left: 0;
            z-index: -1;
            display: block;
            visibility: visible;
        }

        /* right or prev direction */
        #carouselExampleIndicators .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
        #carouselExampleIndicators .carousel-item-prev.carousel-item-right + .carousel-item,
        #carouselExampleIndicators .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
        #carouselExampleIndicators .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(100%, 0, 0);
            visibility: visible;
            display: block;
            visibility: visible;
        }

    }
  </style>
@endsection
@section('content')
<section id="header">
  @include('components.main.film-header', ['title' => $festival->title, 'image' => Storage::disk('local')->url($festival->media->first()->slide_img) ])
</section>
<section>
  @include('components.main.title', ['title' => 'Teaser'])
  <div class="row pt-5">
    <div class="col-md-8 offset-md-2">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/236910015?title=0&byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
      </div>
    </div>
  </div>
</section>
<section id="menu-boxes" class="py-5">
  @include('components.main.title', ['title' => 'Attività'])
  <div class="row pt-5">
    <div class="col-md-10 offset-md-1">
      <div class="row d-flex justify-content-around">
        @foreach ($festival->festivalCategory()->get() as $key => $category)
          <div class="col-md-4">
            <div class="container box-container">
              <img class="img-fluid pb-3 w-100" src="{{ Storage::disk('local')->url($category->media()->first()->thumb) }}">
              <h2 class="text-center">
                <a href="{{ route('single.festival.category', [$festival->slug, $category->slug]) }}" class="text-default">{{ $category->name }}</a>
              </h2>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
<section id="news" class="py-5">
  @include('components.main.title', ['title' => 'News'])
  <div class="row pt-5">
    <div class="col-md-10 offset-md-1">
      @isset($news)
        @if (!$news->isEmpty())
          <div class="row pb-5">
            <div class="col">
              <div class="container-fluid">
                @if (!$news->isEmpty())
                  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner row w-100 mx-auto" role="listbox">
                      @foreach ($news as $key => $newsItem)
                        <div class="carousel-item col-md-4 {{ $key==0 ? 'active' : '' }}">
                          <img class="img-fluid d-block w-100" src="{{ Storage::disk('local')->url($newsItem->media->first()->landscape) }}" alt="{{ $newsItem->title }}">
                          <div class="carousel-caption d-md-block home-carousel">
                            <h2 class="text-default carousel-heading">{{ $newsItem->title }}</h2>
                            <a href="{{ route('single.news', $newsItem->slug) }}" class="btn btn-primary"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Vai alla News</a>
                          </div>
                        </div>
                      @endforeach
                    </div>
                    <a class="carousel-control-prev text-default" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next text-default" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                @endif
              </div>
            </div>
          </div>
        @endif
      @endisset
    </div>
  </div>
</section>
<section id="guests" class="pb-5 d-none">
  @include('components.main.title', ['title' => 'Ospiti'])
  <div class="row pt-5">
    <div class="col-md-10 offset-md-1 text-center">
      <h3 class="d-inline-block">Sandra Rossi - </h3>
      <h3 class="d-inline-block">Gianni Punzo - </h3>
      <h3 class="d-inline-block">Roberto Albanese - </h3>
      <h3 class="d-inline-block">Simone de Martini - </h3>
      <h3 class="d-inline-block">Martina di Iorio - </h3>
      <h3 class="d-inline-block">Giuseppina Ganassin</h3>
    </div>
  </div>
</section>
<section id="gallery" class="py-5">
  @if ($festival->gallery->count() > 0)
    @include('components.main.title', ['title' => 'Galleria'])
    <div class="row pt-5">
      <div class="col-md-8 offset-md-2">
          @foreach ($festival->gallery()->get() as $key => $gallery)
            @if ($gallery->description && strlen($gallery->description) > 1)
              <p>{{ $gallery->description }}</p>
            @endif
            <div class="row gallery">
              @foreach ($gallery->medias()->get() as $key => $media)
                <div class="col-md-4 pb-4">
                    <a href="{{ Storage::disk('local')->url($media->img) }}">
                      <img class="img-fluid w-100" src="{{ Storage::disk('local')->url($media->thumb) }}" alt="">
                    </a>
                </div>
              @endforeach
            </div>
          @endforeach
      </div>
    </div>
  @endif
</section>
<section id="location" class="py-5">
  @include('components.main.title', ['title' => 'Location'])
  <div class="row pt-5">
    <div class="col-md-10 offset-md-1">
      <div class="row">
        <div class="col-md-4">
          <h3>Cinema Mic</h3>
          <hr>
          <p>
            Viale Fulvio Testi, 121 - 20162<br>
            Milano
          </p>
          <div id="map_mic" style="height: 200px;"></div>

        </div>
        <div class="col-md-4">
          <h3>Spazio Oberdan</h3>
          <hr>
          <p>
            Viale Vittorio Veneto, 2 (angolo piazza Oberdan) - 20124 <br>
            Milano
          </p>
          <div id="map_oberdan" style="height: 200px;"></div>
        </div>
        <div class="col-md-4">
          <h3>Area Metropolis 2.0</h3>
          <hr>
          <p>
            Via Oslavia 8 – 20037<br>
            Milano
          </p>
          <div id="map_metropolis" style="height: 200px;"></div>
        </div>
      </div>
      <script type="text/javascript">
          var map_mic;
          var map_oberdan;
          var map_metropolis;
          function initMap() {
            var myLatLng_mic = {lat: 45.513398, lng: 9.204513};
            var myLatLng_oberdan = {lat: 45.475395, lng: 9.204428};
            var myLatLng_metropolis = {lat: 45.568631, lng: 9.160292};

            map_mic = new google.maps.Map(document.getElementById('map_mic'), {
              center: myLatLng_mic,
              zoom: 16
            });

            var marker_mic = new google.maps.Marker({
              position: myLatLng_mic,
              map: map_mic,
              title: 'MIC'
            });

            map_oberdan = new google.maps.Map(document.getElementById('map_oberdan'), {
              center: myLatLng_oberdan,
              zoom: 16
            });

            var marker_oberdan = new google.maps.Marker({
              position: myLatLng_oberdan,
              map: map_oberdan,
              title: 'Spazio Oberdan'
            });

            map_metropolis = new google.maps.Map(document.getElementById('map_metropolis'), {
              center: myLatLng_metropolis,
              zoom: 16
            });

            var marker_metropolis = new google.maps.Marker({
              position: myLatLng_metropolis,
              map: map_metropolis,
              title: 'Area Metropolis'
            });
          }
      </script>
    </div>
  </div>
</section>
<section id="press-releases" class="py-5">
</section>
<section id="archive-editions" class="py-5">
  @include('components.main.title', ['title' => 'Edizioni Passate'])
  <div class="row pt-5">
    <div class="col-md-10 offset-md-1">
      <div class="row">
        <div class="col-md-6">
          <div class="container box-container">
            <img class="img-fluid pb-3 w-100" src="{{ asset('img/PGC_2017.png') }}">
            <h2 class="text-center">
              <a href="{{ asset('pdf/CinetecaMilano_Catalogo_PGC_2017.pdf') }}" class="text-default">2017</a>
            </h2>
          </div>
        </div>
        <div class="col-md-6">
          <div class="container box-container">
            <img class="img-fluid pb-3 w-100" src="{{ asset('img/PGC_2016.png') }}">
            <h2 class="text-center">
              <a href="{{ asset('pdf/CinetecaMilano_Catalogo_PGC_2016.pdf') }}" class="text-default">2016</a>
            </h2>
          </div>
        </div>
        <div class="col-md-6">
          <div class="container box-container">
            <img class="img-fluid pb-3 w-100" src="{{ asset('img/PGC_2015.png') }}">
            <h2 class="text-center">
              <a href="{{ asset('pdf/CinetecaMilano_Catalogo_PGC_2015.pdf') }}" class="text-default">2015</a>
            </h2>
          </div>
        </div>
        <div class="col-md-6">
          <div class="container box-container">
            <img class="img-fluid pb-3 w-100" src="{{ asset('img/PGC_2014.png') }}">
            <h2 class="text-center">
              <a href="{{ asset('pdf/CinetecaMilano_Catalogo_PGC_2014.pdf') }}" class="text-default">2014</a>
            </h2>
          </div>
        </div>
        <div class="col-md-6">
          <div class="container box-container">
            <img class="img-fluid pb-3 w-100" src="{{ asset('img/PGC_2013.png') }}">
            <h2 class="text-center">
              <a href="{{ asset('pdf/CinetecaMilano_Catalogo_PGC_2013.pdf') }}" class="text-default">2013</a>
            </h2>
          </div>
        </div>
        <div class="col-md-6">
          <div class="container box-container">
            <img class="img-fluid pb-3 w-100" src="{{ asset('img/PGC_2012.png') }}">
            <h2 class="text-center">
              <a href="{{ asset('pdf/CinetecaMilano_Catalogo_PGC_2012.pdf') }}" class="text-default">2012</a>
            </h2>
          </div>
        </div>
        <div class="col-md-6">
          <div class="container box-container">
            <img class="img-fluid pb-3 w-100" src="{{ asset('img/PGC_2011.png') }}">
            <h2 class="text-center">
              <a href="{{ asset('pdf/CinetecaMilano_Catalogo_PGC_2011.pdf') }}" class="text-default">2011</a>
            </h2>
          </div>
        </div>
        <div class="col-md-6">
          <div class="container box-container">
            <img class="img-fluid pb-3 w-100" src="{{ asset('img/PGC_2010.png') }}">
            <h2 class="text-center">
              <a href="{{ asset('pdf/CinetecaMilano_Catalogo_PGC_2010.pdf') }}" class="text-default">2010</a>
            </h2>
          </div>
        </div>
        <div class="col-md-6">
          <div class="container box-container">
            <img class="img-fluid pb-3 w-100" src="{{ asset('img/PGC_2009.png') }}">
            <h2 class="text-center">
              <a href="{{ asset('pdf/CinetecaMilano_Catalogo_PGC_2009.pdf') }}" class="text-default">2009</a>
            </h2>
          </div>
        </div>
        <div class="col-md-6">
          <div class="container box-container">
            <img class="img-fluid pb-3 w-100" src="{{ asset('img/PGC_2008.png') }}">
            <h2 class="text-center">
              <a href="{{ asset('pdf/CinetecaMilano_Catalogo_PGC_2008.pdf') }}" class="text-default">2008</a>
            </h2>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="partners" class="py-5">
  @include('components.main.title', ['title' => 'Partners'])
  <div class="row pt-5">
    <div class="col-md-10 offset-md-1">
      <div class="row">
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/DGC.png') }}" alt="Regione Lombardia" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/REGIONE.png') }}" alt="Città Metropolitana di Milano" class="img-fluid w-50">
        </div>

        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/COMUNE-MILANO.png') }}" alt="Comune di Milano" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/EUROPACREATIVA.png') }}" alt="Europa Creativa" class="img-fluid w-50">
        </div>

        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/EUROPACINEMAS.png') }}" alt="Europa Cinemas" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/UFFICIOSCOLASTICO.png') }}" alt="Ufficio scolastico" class="img-fluid w-50">
        </div>

        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/INAIL.png') }}" alt="Inail" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/PADERNO.png') }}" alt="Paderno dugnano" class="img-fluid w-50">
        </div>

        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/ECFA.png') }}" alt="Ecfa" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/CSC.png') }}" alt="Csc" class="img-fluid w-50">
        </div>

        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/SIAE.png') }}" alt="SIAE" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/AEM.png') }}" alt="AEM" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/MEDICINEMA.png') }}" alt="MEDICINEMA" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/LEDHA.png') }}" alt="Ledha" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/TRENTO-FILM-FESTIVAL.png') }}" alt="Trento Film Festival" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/CASTELLINARIA.png') }}" alt="Castellinaria" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/MFN.png') }}" alt="Milano Film Network" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/TVM.png') }}" alt="Tvm" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/NOVOTOEL.png') }}" alt="Novotel" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/logo-epson.png') }}" alt="Epson" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/INSIEME.png') }}" alt="Insieme" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/STYLE.png') }}" alt="Style" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/LONGATAKE.png') }}" alt="Long Take" class="img-fluid w-50">
        </div>
        <div class="col-6 col-md-2 pb-5 text-center">
          <img src="{{ asset('img/partner/nuovi/Loaker.png') }}" alt="Loaker" class="img-fluid w-50">
        </div>
      </div>
    </div>
  </div>
</section>
<section id="twitter-updates" class="py-5">
  @include('components.main.title', ['title' => 'Tweets'])
  <div class="row">
    <div class="col-md-10 offset-md-1">
      <div class="row">
        @foreach ($tweets as $key => $tweet)
          <div class="col-md-4 p-4">
            <div class="container-fluid bg-dark-gray p-4">
              <div class="row">
                <div class="col-2">
                  <img src="{{ $tweet['user']['profile_image_url'] }}" alt="" class="rounded-circle img-fluid" width="64">
                </div>
                <div class="col-10">
                  <p>{{ $tweet['text'] }}</p>
                  <p>
                    @foreach ($tweet['entities']['hashtags'] as $key => $hashtag)
                      <span class="badge badge-default">#{{ $hashtag['text'] }}</span>
                    @endforeach
                  </p>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
@include('components.main.back')
@endsection
@section('scripts')
  <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCWWjzGK-o2GBGAPabC1nm6AETotrd53TQ&callback=initMap" async defer></script>
  {{-- three slide carousel News--}}
  <script type="text/javascript">
  $('#carouselExampleIndicators').on('slide.bs.carousel', function (e) {
      var $e = $(e.relatedTarget);
      var idx = $e.index();
      var itemsPerSlide = 3;
      var totalItems = $('#carouselExampleIndicators .carousel-item').length;
      if (idx >= totalItems-(itemsPerSlide-1)) {
          var it = itemsPerSlide - (totalItems - idx);
          for (var i=0; i<it; i++) {
              // append slides to end
              if (e.direction=="left") {
                  $('#carouselExampleIndicators .carousel-item').eq(i).appendTo('#carouselExampleIndicators .carousel-inner');
              }
              else {
                  $('#carouselExampleIndicators .carousel-item').eq(0).appendTo('#carouselExampleIndicators .carousel-inner');
              }
          }
      }
  });
  </script>
@endsection
