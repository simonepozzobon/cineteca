@extends('layouts.public')
@section('title', 'Festival')
@section('seo')
  <meta name="description" content="Tutti i festival in programmazione alla Fondazione Cineteca Italiana di Milano" />
@endsection
@section('content')
<section id="slider">
  @include('components.main.slider', ['type' => 'Festival', 'button' => 'Festival', 'items' => $festivals, 'root' => '/festival'])
</section>
<section id="boxes">
  @include('components.main.square-box', ['title' => 'Festival In Programmazione', 'type' => 'festival', 'items' => $festivals, ])
</section>
@endsection
