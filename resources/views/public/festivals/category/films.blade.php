@extends('layouts.public', ['teacher' => true])
@section('title', 'Proiezioni')
@section('seo')
  <meta name="description" content="Tutti gli eventi in programma per le {{ $category->name }} del festival {{ $festival->title }} alla Fondazione Cineteca Italiana di Milano" />
@endsection
@section('content')
<section id="film-carousel">
  {{-- @include('components.main.slider', ['type' => 'Eventi ', 'button' => 'Evento', 'items' => $films, 'root' => '/evento']) --}}
  <div class="row no-gutters">
    <div class="col pt-5 pb-5">
      <div class="row pt-5">
        <div class="col pt-5 pl-5 pr-5">
          <h1 align="center">Proiezioni In Evidenza</h1>
        </div>
      </div>
      <div class="row pb-5">
        <div class="col-md-6 offset-md-3">
          <hr>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <div id="carouselFilm" class="carousel-custom carousel slide" data-ride="carousel">
            <div class="carousel-overlay"></div>
            <div class="carousel-inner" role="listbox">
              @if (isset($featured))
                @foreach ($featured as $key => $film)
                  <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                    <img class="d-block" style="height: 100%; width: 100%;" src="{{ Storage::disk('local')->url($film->media->first()->slide_img) }}" alt="First slide">
                    <div class="carousel-caption">
                      <h1 class="pt-3">{{ $film->title }}</h1>
                      <p class="pt-4"><a class="btn btn-primary" href="{{ route('single.film', $film->slug) }}" role="button"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Scheda Film</a></p>
                    </div>
                  </div>
                @endforeach
              @endif
            </div>
            <a class="carousel-control-prev text-default" href="#carouselFilm" role="button" data-slide="prev">
              <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next text-default" href="#carouselFilm" role="button" data-slide="next">
              <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="calendar">
  <div class="row no-gutters pt-5">
    @if (isset($films))
      <div class="col-md-7 offset-md-1">
    @else
      <div class="col">
    @endif
      @if (isset($films))
        <h5>Proiezioni in programma</h5>
        <hr>
        <div id="events">
          @foreach ($films as $key => $film)
            <div class="row show-single">
              <div class="col-md-2">
                <h3>{{ $film->time }}</h3>
                <p>
                  {{ $film->day_string }}<br>
                  {{ $film->day }} {{ $film->month }}
                </p>
                @if ($film->ticket == true)
                  <a href="{{ $film->ticket_url }}" target="_blank" class="btn btn-primary"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Biglietti</a>
                @endif
              </div>
              <div class="col-md-4">
                <img src="{{ Storage::disk('local')->url($film->img) }}" class="img-fluid w-100">
              </div>
              <div class="col-md-6">
                <div class="row">
                    <div class="col">
                      <h3>
                        <a href="{{ route('single.film', $film->slug) }}">
                          {{ $film->title }}
                        </a>
                      </h3>
                    </div>
                </div>
                <p>
                  @if ($film->kids === 1 )
                    <span class="badge" style="background-color:#755482">Adatto ai bambini</span>
                  @endif
                  @foreach ($film->film()->first()->categories()->get() as $key => $film_category)
                    <span class="badge badge-default">{{ $film_category->name }}</span>
                  @endforeach
                </p>
                  <h6>{{ $film->location }}</h6>
                  <p class="pt-3">{{ substr(strip_tags($film->description), 0, 150) }}{{ strlen(strip_tags($film->description)) > 150 ? '...' : "" }}</p>
              </div>
            </div>
            <div class="row pb-3 show-single-sep">
              <div class="col-md-8 offset-md-2">
                <hr>
              </div>
            </div>
          @endforeach
        </div>
      @else
        <h1 class="text-center p-5"><i class="fa fa-meh-o" aria-hidden="true"></i></h1>
        <h3 class="text-center pb-5">Ooops, nessun film in programmazione</h3>
        <div class="row p-5">
          <div class="col-md-4 offset-md-4">
            <a class="btn btn-primary btn-block" href="{{ url()->previous() }}">
              <i class="fa fa-arrow-left" aria-hidden="true"></i> Indietro
            </a>
          </div>
        </div>
      @endif
    </div>
    <div class="col-md-2 offset-md-1">
      @isset ($film)
        <div class="row pb-5">
          <div class="col">
            <h5>Scegli Una Data</h5>
            <hr>
            <div id="datepicker"></div>
          </div>
        </div>
      @endisset
      @isset($featDirector)
        <div class="row pb-5">
          <div class="col">
            <h5>Incontro Con Il Regista</h5>
            <hr>
            <p>{{ $featDirector->day_string }}, {{ $featDirector->day }} {{ $featDirector->month }}</p>
            <h3>{{ $featDirector->time }}</h3>
            <img src="{{ Storage::disk('local')->url($featDirector->media->first()->thumb) }}" alt="{{ $featDirector->title }}" class="img-fluid">
            <h3 class="mt-3"><a href="{{ route('single.event', $featDirector->slug) }}">{{ $featDirector->title }}</a></h3>
            <h6>{{ $featDirector->location->name }}</h6>
            <p class="pt-3">{{ substr(strip_tags($featDirector->description), 0, 100) }}{{ strlen(strip_tags($featDirector->description)) > 100 ? '...' : "" }}</p>
            <a href="{{ route('single.event', $featDirector->slug) }}" class="btn btn-primary">Vai all'evento</a>
          </div>
        </div>
      @endisset
    </div>
  </div>

</section>
<div id="output">
</div>
@include('components.main.back')
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}">
  <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap-datepicker/locales/bootstrap-datepicker.it.min.js') }}"></script>
  <script type="text/javascript">
  $('#datepicker').datepicker({
    autoclose: true,
    todayHighlight: true,
    language: 'it',
    startDate: new Date("November 3, 2017 00:00:00"),
    endDate: new Date("November 12, 2017 00:00:00")
  });


  $('#datepicker').datepicker().on('changeDate', function(e) {
      /*
       *
       * Catturo il cambio data
       *
      */
      var date = $('#datepicker').datepicker('getDate');
      // Salvo la data nei cookies
      setCookie('cnt-festival-{{ $festival->slug }}-{{ $category->slug }}-date', date, 8);
      getShows(date);
    });

    function getShows(date)
    {
      // Formatto la data secondo necessità
      var day = date.getDate();
      var monthIndex = date.getMonth() + 1;
      var year = date.getFullYear();

      var new_date = year+'-'+monthIndex+'-'+day;

      // Effettuo la richiesta AJAX per ottenere gli show in quel giorno
      $.ajax({
        type: 'GET',
        url:  '/festival/{{ $festival->slug }}/{{ $category->slug }}/{{ $section }}/'+new_date,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        },
        success: function (response) {
            // console.log(response);
            var dataLenght = Object.keys(response).length;
            // Rimuovo i vecchi show
            $('.show-single').remove();
            $('.show-single-sep').remove();
            $('.pagination').remove();

            // Se ci sono Film
            if (dataLenght > 0) {
              for (var i = 0; i < dataLenght; i++) {
                var data = '';
                data += '<div class="row show-single">';
                data +=   '<div class="col-md-2">';
                data +=     '<h3>'+response[i].time+'</h3>';
                data +=     '<p>'+response[i].day_string+'<br>'+response[i].day+' '+response[i].month+'</p>';

                if (response[i].ticket == true) {
                  data +=     '<a href="'+response[i].ticket_url+'" target="_blank" class="btn btn-primary"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Biglietti</a>';
                }

                data +=   '</div>';
                data +=   '<div class="col-md-4">';
                data +=     '<img src="'+response[i].img+'" class="img-fluid w-100">';
                data +=   '</div>';
                data +=   '<div class="col-md-6">';
                data +=     '<div class="row">';
                data +=       '<div class="col">';
                data +=          '<h3>';
                data +=           '<a href="'+response[i].url+'">'+response[i].title+'</a>';
                data +=          '</h3>';
                data +=        '</div>';
                data +=      '</div>';
                data +=      '<p>';

                for (var y = 0; y < response[i].categories.length; y++) {
                  // console.log(response[i].categories[y]);
                  data += '<span class="badge badge-default">'+response[i].categories[y].name+'</span> ';
                }

                data +=      '</p>';
                data +=      '<h6>'+response[i].location+'</h6>';
                data +=      '<p class="pt-3">'+response[i].description+'</p>';

                data +=   '</div>';
                data += '</div>';
                data += '<div class="row pb-3 show-single-sep">';
                data +=   '<div class="col-md-8 offset-md-3">';
                data +=     '<hr>';
                data +=   '</div>';
                data += '</div>';


                $('#events').append(data);
              }

            } else {
              // Se non ci sono film nella risposta
              var data = '';
              data += '<div class="row show-single">';
              data +=   '<div class="col">';
              data +=     '<h1 class="text-center p-5"><i class="fa fa-meh-o" aria-hidden="true"></i></h1>';
              data +=     '<h3 class="text-center pb-5">Ooops, nessun film in programmazione</h3>';
              data +=     '<h5 class="text-center pb-5">Prova a cambiare giorno</h5>';
              data +=   '</div>';
              data += '</div>';

              $('#events').append(data);
            }


        },
        error: function (xhr, status) {
            console.log(xhr);
            console.log(status);
        }

      });
    }

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
  </script>
@endsection
