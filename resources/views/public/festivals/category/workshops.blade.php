@extends('layouts.public', ['teacher' => true])
@section('title', "Festival $festival->title - $category->name - Laboratori")
@section('seo')
  <meta name="description" content="Tutti i laboratori in programma per le {{ $category->name }} del festival {{ $festival->title }} alla Fondazione Cineteca Italiana di Milano" />
@endsection
@section('content')
<section id="film-carousel">
  @include('components.main.slider', ['type' => 'Laboratori ', 'button' => 'Laboratorio', 'items' => $events, 'root' => '/evento'])
</section>
<section id="calendar">
  <div class="row no-gutters pt-5">
    @if (isset($events))
      <div class="col-md-7 offset-md-1">
    @else
      <div class="col">
    @endif
      @if (isset($events))
        <h5>Laboratori in programma</h5>
        <hr>
        <div id="events">
          @foreach ($events as $key => $event)
            <div class="row show-single">
              <div class="col-md-2">
                <h3>{{ $event->time }}</h3>
                <p>
                  {{ $event->day_string }}<br>
                  {{ $event->day }} {{ $event->month }}
                </p>
              </div>
              <div class="col-md-4">
                <img src="{{ Storage::disk('local')->url($event->img) }}" class="img-fluid w-100">
              </div>
              <div class="col-md-6">
                <div class="row">
                    <div class="col">
                      <h3>
                        <a href="{{ route('single.event', $event->slug) }}">
                          {{ $event->title }}
                        </a>
                      </h3>
                    </div>
                </div>
                <p>
                  @if ($event->kids === 1 )
                    <span class="badge" style="background-color:#755482">Adatto ai bambini</span>
                  @endif
                  <span class="badge" style="background-color:#2AD68C">{{ $event->eventType->name }}</span>
                </p>
                  <h6>{{ $event->location->name }}</h6>
                  <p class="pt-3">{{ substr(strip_tags($event->description), 0, 150) }}{{ strlen(strip_tags($event->description)) > 150 ? '...' : "" }}</p>
              </div>
            </div>
            <div class="row pb-3 show-single-sep">
              <div class="col-md-8 offset-md-2">
                <hr>
              </div>
            </div>
          @endforeach
        </div>
      @else
        <h1 class="text-center p-5"><i class="fa fa-meh-o" aria-hidden="true"></i></h1>
        <h3 class="text-center pb-5">Ooops, nessun laboratorio in programmazione</h3>
        <div class="row p-5">
          <div class="col-md-4 offset-md-4">
            <a class="btn btn-primary btn-block" href="{{ url()->previous() }}">
              <i class="fa fa-arrow-left" aria-hidden="true"></i> Indietro
            </a>
          </div>
        </div>
      @endif
    </div>
    <div class="col-md-2 offset-md-1">
      @isset ($events)
        <div class="row pb-5">
          <div class="col">
            <h5>Scegli Una Data</h5>
            <hr>
            <div id="datepicker"></div>
          </div>
        </div>
      @endisset
      @isset($featDirector)
        <div class="row pb-5">
          <div class="col">
            <h5>Incontro Con Il Regista</h5>
            <hr>
            <p>{{ $featDirector->day_string }}, {{ $featDirector->day }} {{ $featDirector->month }}</p>
            <h3>{{ $featDirector->time }}</h3>
            <img src="{{ Storage::disk('local')->url($featDirector->media->first()->thumb) }}" alt="{{ $featDirector->title }}" class="img-fluid">
            <h3 class="mt-3"><a href="{{ route('single.event', $featDirector->slug) }}">{{ $featDirector->title }}</a></h3>
            <h6>{{ $featDirector->location->name }}</h6>
            <p class="pt-3">{{ substr(strip_tags($featDirector->description), 0, 100) }}{{ strlen(strip_tags($featDirector->description)) > 100 ? '...' : "" }}</p>
            <a href="{{ route('single.event', $featDirector->slug) }}" class="btn btn-primary">Vai all'evento</a>
          </div>
        </div>
      @endisset
    </div>
  </div>
</section>
<div id="output">

</div>
@include('components.main.back')
@endsection
@section('scripts')
  <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}">
  <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('plugins/bootstrap-datepicker/locales/bootstrap-datepicker.it.min.js') }}"></script>
  <script type="text/javascript">
  $('#datepicker').datepicker({
    autoclose: true,
    todayHighlight: true,
    language: 'it',
    startDate: new Date("November 3, 2017 00:00:00"),
    endDate: new Date("November 12, 2017 00:00:00")
  });

  // var cookie = getCookie('cnt-festival-{{ $festival->slug }}-{{ $category->slug }}-date');
  // if (cookie !== '') {
  //   var date = new Date(cookie);
  //   $('#datepicker').datepicker('setDate', date);
  //   getShows(date);
  // }


  $('#datepicker').datepicker().on('changeDate', function(e) {
      /*
       *
       * Catturo il cambio data
       *
      */
      var date = $('#datepicker').datepicker('getDate');
      // Salvo la data nei cookies
      setCookie('cnt-festival-{{ $festival->slug }}-{{ $category->slug }}-date', date, 8);
      getShows(date);
    });

    function getShows(date)
    {
      // Formatto la data secondo necessità
      var day = date.getDate();
      var monthIndex = date.getMonth() + 1;
      var year = date.getFullYear();

      var new_date = year+'-'+monthIndex+'-'+day;

      // Effettuo la richiesta AJAX per ottenere gli show in quel giorno
      $.ajax({
        type: 'GET',
        url:  '/festival/{{ $festival->slug }}/{{ $category->slug }}/laboratori/'+new_date,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        },
        success: function (response) {
            console.log(response);
            var dataLenght = Object.keys(response).length;
            // Rimuovo i vecchi show
            $('.show-single').remove();
            $('.show-single-sep').remove();
            $('.pagination').remove();

            // Se ci sono Film
            if (dataLenght > 0) {
              for (var i = 0; i < dataLenght; i++) {
                var data = '';
                data += '<div class="row show-single">';
                data +=   '<div class="col-md-2">';
                data +=     '<h3>'+response[i].time+'</h3>';
                data +=     '<p>'+response[i].day_string+'<br>'+response[i].day+' '+response[i].month+'</p>';
                data +=   '</div>';
                data +=   '<div class="col-md-4">';
                data +=     '<img src="'+response[i].img+'" class="img-fluid w-100">';
                data +=   '</div>';
                data +=   '<div class="col-md-6">';
                data +=     '<div class="row">';
                data +=       '<div class="col">';
                data +=          '<h3>';
                data +=           '<a href="'+response[i].url+'">'+response[i].title+'</a>';
                data +=          '</h3>';
                data +=        '</div>';
                data +=      '</div>';
                data +=      '<p>';
                data +=         '<span class="badge" style="background-color:#2AD68C">'+response[i].event_type.name+'</span>';
                data +=      '</p>';
                // Evento
                data +=      '<h6>'+response[i].location.name+'</h6>';
                data +=      '<p class="pt-3">'+response[i].description+'</p>';

                data +=   '</div>';
                data += '</div>';
                data += '<div class="row pb-3 show-single-sep">';
                data +=   '<div class="col-md-8 offset-md-3">';
                data +=     '<hr>';
                data +=   '</div>';
                data += '</div>';


                $('#events').append(data);
              }

            } else {
              // Se non ci sono laboratori nella risposta
              var data = '';
              data += '<div class="row show-single">';
              data +=   '<div class="col">';
              data +=     '<h1 class="text-center p-5"><i class="fa fa-meh-o" aria-hidden="true"></i></h1>';
              data +=     '<h3 class="text-center pb-5">Ooops, nessun laboratorio in programmazione</h3>';
              data +=     '<h5 class="text-center pb-5">Prova a cambiare giorno</h5>';
              data +=   '</div>';
              data += '</div>';

              $('#events').append(data);
            }


        },
        error: function (xhr, status) {
            console.log(xhr);
            console.log(status);
        }

      });
    }

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
  </script>
@endsection
