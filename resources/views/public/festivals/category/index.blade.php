@extends('layouts.public', ['teacher' => true])
@section('title', "Festival $festival->title - $category->name")
@section('seo')
  <meta name="description" content="Calendario delle attività dedicate alle {{ $category->name }} del festival {{ $festival->title }}" />
@endsection
@section('content')
<section id="header">
  @include('components.main.film-header', ['image' => Storage::disk('local')->url($category->media()->first()->slide_img), 'title' => "$festival->title - $category->name"])
</section>
<section id="pages">
  <div class="row no-gutters">
    <div class="col">
      <div class="row pt-5">
        <div class="col pt-5 px-5">
          @if ($category->name == 'Pubblico')
            <h1 align="center">Attività per il {{ $category->name }}</h1>
          @else
            <h1 align="center">Attività per le {{ $category->name }}</h1>
          @endif
        </div>
      </div>
      <div class="row pb-5">
        <div class="col-6 offset-3">
          <hr>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <div class="row no-gutters">
            @foreach ($items as $key => $item)
              <div class="col-md-4 pb-5">
                <div class="container box-container">
                  <img class="img-fluid pb-3 w-100" src="{{ $item['img'] }}" alt="{{ $item['title'] }}">
                  <h2 class="text-center">
                    <a href="{{ $item['url'] }}" class="text-default">{{ $item['title'] }}</a>
                  </h2>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
      @if ($category->name == 'Pubblico')
        @include('components.main.title', ['title' => 'Modalità di ingresso'])
        <div class="row">
          <div class="col-md-10 offset-md-1">
            <p class="px-5 pt-5 text-center">
              Tutti i biglietti delle proiezioni sono disponibili in prevendita alle casse delle sale di riferimento.<br>
              È possibile acquistare anche online i biglietti di ingresso alle proiezioni di Spazio Oberdan sulla piattaforma Webtic o direttamente sul sito della Cineteca<br><br>
              <b>Ingresso unico: € 5,00</b><br><br>
              <u>Salvo diversa indicazione segnalata nel singolo evento.</u>
            </p>
            <div class="row">
              <div class="col-md-6">
                @include('components.main.title-h4', ['title' => 'Programma Family'])
                <p class="text-center">
                  Adulti: ingresso gratuito con *Cinetessera 2018 / ingresso intero € 5<br>
                  Bambini: ingresso unico € 5<br>
                  Ove è indicato è richiesta la prenotazione obbligatoria.<br>
                </p>
              </div>
              <div class="col-md-6">
                @include('components.main.title-h4', ['title' => 'Concorso Rivelazioni'])
                <p class="text-center">
                  Ingresso gratuito con Cinetessera 2018 / ingresso intero € 5
                </p>
              </div>
              <div class="col-md-6">
                @include('components.main.title-h4', ['title' => 'Speciale - Una notte al museo'])
                <p class="text-center">
                  Max 20 partecipanti. Costo euro 35,00.<br>
                  Per prenotazioni e info: 0287242114
                </p>
              </div>
              <div class="col-md-6">
                @include('components.main.title-h4', ['title' => 'Open day scuole, corsi e professioni del cinema'])
                <p class="text-center">
                  MIC – Museo Interattivo del Cinema<br>
                  Ingresso libero fino ad esaurimento posti. Prenotazione obbligatoria.
                </p>
              </div>
              <div class="col-md-6">
                @include('components.main.title-h4', ['title' => 'Seminario - IL MONDO DEL LAVORO NEI FILM DI KEN LOACH'])
                <p class="text-center">
                  Ingresso libero, prenotazione obbligatoria.
                </p>
              </div>
              <div class="col-md-6">
                @include('components.main.title-h4', ['title' => 'Laboratorio - LA MATERIA DEI SOGNI'])
                <p class="text-center">
                  Biglietto unico euro 5,00
                </p>
              </div>
            </div>
            <div class="row pb-5">
              <div class="col-md-6 offset-md-3">
                @include('components.main.title-h4', ['title' => 'Proiezioni - Tempo Limite'])
                <p class="text-center">
                  Ingresso libero.
                </p>
              </div>
            </div>
            <div class="row pb-5">
              <div class="col">
                <p class="text-center"><u>*La Cinetessera 2018, la tessera annuale delle sale della Cineteca, ha un costo di € 10.</u></p>
              </div>
            </div>
          </div>
        </div>
      @endif
    </div>
  </div>
</section>
@include('components.main.back')
@endsection
