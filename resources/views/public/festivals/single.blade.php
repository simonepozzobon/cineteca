@extends('layouts.public')
@section('title', "$festival->title")
@section('seo')
  <meta name="description" content="{{ substr(strip_tags($festival->description), 0, 170) }}{{ strlen(strip_tags($festival->description)) > 170 ? '...' : "" }}" />
@endsection
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/single.css') }}">
  {{-- Custom Slider Styles --}}
  <style media="screen">

    #carouselExampleIndicators .carousel-caption {
      position: inherit;
      right: 0;
      bottom: 0;
      left: 0;
    }

    #carouselExampleIndicators .carousel-control-prev,
    #carouselExampleIndicators .carousel-control-next,
    .carousel-custom .carousel-control-prev,
    .carousel-custom .carousel-control-next {
      width: 8%;
      min-width: 40px;
      font-size: 2rem;
    }

    #carouselExampleIndicators .carousel-indicators {
      /*right: 1.6rem;*/
      bottom: -1.6rem;
    }

    #carouselExampleIndicators .carousel-indicators .active {
      background-color: #292b2c;
    }

    #carouselExampleIndicators .carousel-indicators li {
      background-color: rgba(41, 43, 44, .5)
    }

    #carouselExampleIndicators .carousel-caption,
    #carouselExampleIndicators .carousel-overlay {
      background: transparent !important;
    }

    @media (min-width: 768px) {

        /* show 3 items */
        #carouselExampleIndicators .carousel-inner .active,
        #carouselExampleIndicators .carousel-inner .active + .carousel-item,
        #carouselExampleIndicators .carousel-inner .active + .carousel-item + .carousel-item {
            display: block;
        }

        #carouselExampleIndicators .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
        #carouselExampleIndicators .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
        #carouselExampleIndicators .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item {
            transition: none;
        }

        #carouselExampleIndicators .carousel-inner .carousel-item-next,
        #carouselExampleIndicators .carousel-inner .carousel-item-prev {
          position: relative;
          transform: translate3d(0, 0, 0);
        }

        #carouselExampleIndicators .carousel-inner .active.carousel-item + .carousel-item + .carousel-item + .carousel-item {
            position: absolute;
            top: 0;
            right: -33.33333%;
            z-index: -1;
            display: block;
            visibility: visible;
        }

        /* left or forward direction */
        #carouselExampleIndicators .active.carousel-item-left + .carousel-item-next.carousel-item-left,
        #carouselExampleIndicators .carousel-item-next.carousel-item-left + .carousel-item,
        #carouselExampleIndicators .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
        #carouselExampleIndicators .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(-100%, 0, 0);
            visibility: visible;
        }

        /* farthest right hidden item must be abso position for animations */
        #carouselExampleIndicators .carousel-inner .carousel-item-prev.carousel-item-right {
            position: absolute;
            top: 0;
            left: 0;
            z-index: -1;
            display: block;
            visibility: visible;
        }

        /* right or prev direction */
        #carouselExampleIndicators .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
        #carouselExampleIndicators .carousel-item-prev.carousel-item-right + .carousel-item,
        #carouselExampleIndicators .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
        #carouselExampleIndicators .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item {
            position: relative;
            transform: translate3d(100%, 0, 0);
            visibility: visible;
            display: block;
            visibility: visible;
        }

    }
  </style>
@endsection
@section('content')
  <article class="p-5">
    @include('components.main.film-header', ['title' => $festival->title, 'image' => Storage::disk('local')->url($festival->media->first()->slide_img) ])
    <div class="row p-5">
      <div class="col-md-7 offset-md-1">
        @isset($news)
          @if (!$news->isEmpty())
            <div class="row pb-5">
              <div class="col">
                <h5>News</h5>
                <hr>
                <div class="container-fluid">
                  @if (!$news->isEmpty())
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                      <div class="carousel-inner row w-100 mx-auto" role="listbox">
                        @foreach ($news as $key => $newsItem)
                          <div class="carousel-item col-md-4 {{ $key==0 ? 'active' : '' }}">
                            <img class="img-fluid d-block w-100" src="{{ Storage::disk('local')->url($newsItem->media->first()->landscape) }}" alt="{{ $newsItem->title }}">
                            <div class="carousel-caption d-md-block home-carousel">
                              <h2 class="text-default carousel-heading">{{ $newsItem->title }}</h2>
                              <a href="{{ route('single.news', $newsItem->slug) }}" class="btn btn-primary"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Vai alla News</a>
                            </div>
                          </div>
                        @endforeach
                      </div>
                      <a class="carousel-control-prev text-default" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next text-default" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                  @endif
                </div>
              </div>
            </div>
          @endif
        @endisset
        <div class="row pb-5">
          <div class="col">
            <h5>Scheda</h5>
            <hr>
            <p>{!! $festival->description !!}</p>
          </div>
        </div>
        @if ($pages->count() > 0)
          <div class="row pb-5">
            <div class="col-6 offset-3">
              <hr>
            </div>
          </div>
          <div class="row no-gutters">
            @foreach ($pages as $key => $page)
              <div class="col-md-4 pb-5">
                <div class="container box-container">
                  <img class="img-fluid pb-3 w-100" src="{{ $page['img'] }}" alt="{{ $page['title'] }}">
                  <h2 class="text-center">
                    <a href="{{ $page['url'] }}" class="text-default">{{ $page['title'] }}</a>
                  </h2>
                </div>
              </div>
            @endforeach
          </div>
        @endif
        @if ($elements->count() > 0)
          <div class="row pb-5">
            <div class="col">
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a class="nav-link {{ $shows->count() > 0 ? 'active' : '' }}" data-toggle="tab" href="#calendar" role="tab">
                    <h5>Calendario Proiezioni</h5>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link {{ $shows->count() == 0 ? 'active' : '' }}" data-toggle="tab" href="#program" role="tab">
                    <h5>Programma Completo</h5>
                  </a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane {{ $shows->count() > 0 ? 'active' : '' }} pt-5" id="calendar" role="tabpanel">
                  @if ($shows->count() > 0)
                    @foreach ($shows as $key => $show)
                      <div class="row">
                        <div class="col-md-2">
                          <h3>{{ $show['time'] }}</h3>
                          <p>{{ $show['day'] }}, {{ $show['month'] }}</p>
                        </div>
                        <div class="col-md-4">
                          <img src="{{ Storage::disk('local')->url($show['img']) }}" class="img-fluid">
                        </div>
                        <div class="col-md-6">
                          <div class="row">
                              <div class="col">
                                <h3>
                                  <a href="{{ url($show['url']) }}">
                                    {{ $show['title'] }}
                                  </a>
                                </h3>
                              </div>
                          </div>
                          <h6>{{ $show['location'] }}</h6>
                          <p>{{ $show['production_date'] }}, {{ $show['region'] }}</p>
                          <p>{{ substr(strip_tags($show['description']), 0, 150) }}{{ strlen(strip_tags($show['description'])) > 150 ? '...' : "" }}</p>
                        </div>
                      </div>
                      <div class="row pb-3">
                        <div class="col-md-8 offset-md-2">
                          <hr>
                        </div>
                      </div>
                    @endforeach
                  @endif
                </div>
                <div class="tab-pane {{ $shows->count() == 0 ? 'active' : '' }} pt-5" id="program" role="tabpanel">
                  @foreach ($elements as $key => $element)
                    <div class="row">
                      <div class="col-md-4">
                        <img class="w-100 img-fluid" src="{{ Storage::disk('local')->url($element['img']) }}" alt="{{ $element['title'] }}">
                      </div>
                      <div class="col-md-8">
                        <h3>
                          <a href="{{ url($element['url']) }}">{{ $element['title'] }}</a>
                        </h3>
                        <span class="badge" style="background-color:{{ $element['color'] }}">{{ $element['type'] }}</span>
                        @if ($element['kids'] === 1)
                          <span class="badge" style="background-color:#755482">Adatto ai bambini</span>
                        @endif
                        <span><small></small></span>
                        <p>{{ substr(strip_tags($element['description']), 0, 100) }}{{ strlen(strip_tags($element['description'])) > 100 ? '...' : "" }}</p>
                        <a href="{{ url($element['url']) }}" class="btn btn-primary">Scheda {{ $element['type'] }}</a>
                      </div>
                    </div>
                    <div class="row pb-3">
                      <div class="col-md-8 offset-md-4">
                        <hr>
                      </div>
                    </div>
                  @endforeach
                </div>
              </div>

            </div>
          </div>
        @endif
      </div>
      <div class="col-md-3">
        <div class="row">
          <div class="col">
            <h5>Durata</h5>
            <hr>
            <p>
              <strong>Dal {{ $festival->start_date }}
              <br>al {{ $festival->end_date }}</strong>
            </p>
          </div>
        </div>
        <div class="row pt-5">
          <div class="col">
            <h5>Info Generali</h5>
            <hr>
            <p>
              <a class="" data-toggle="collapse" href="#biglietti" aria-expanded="false" aria-controls="biglietti">
                Costo Biglietti
              </a>
              <div class="collapse" id="biglietti">
                <ul>
                  <li>Ingresso intero: € 8,00</li>
                  <li>Ingresso ridotto (con Cinetessera): € 5,50</li>
                  <li>Ingresso under 14: € 5,50</li>
                  <li>Ingresso studenti universitari muniti di tesserino o carta IO STUDIO: € 6,50</li>
                  <li>Ingresso venerdì feriali spettacolo ore 15: € 4,50</li>
                  <li>
                    Convenzioni: € 6,50
                    <br>
                    <small>(carta oro lun-dom, cral lun-ven.)</small>
                  </li>
                  <li>TECA +: € 7,00</li>
                  <li>Ingresso proiezioni scolastiche: € 3,50</li>
                  <li>Cinetessera annuale valida da gennaio a dicembre: € 10,00</li>
                  <li>Abilitato per APP 18  e CARTA DOCENTI</li>
                </ul>
              </div>
            </p>
          </div>
        </div>
      </div>
    </div>
    @include('components.main.back')
  </article>
@endsection
@section('scripts')
  <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCWWjzGK-o2GBGAPabC1nm6AETotrd53TQ&callback=initMap"
  async defer></script>
  {{-- three slide carousel News--}}
  <script type="text/javascript">
  $('#carouselExampleIndicators').on('slide.bs.carousel', function (e) {
      var $e = $(e.relatedTarget);
      var idx = $e.index();
      var itemsPerSlide = 3;
      var totalItems = $('#carouselExampleIndicators .carousel-item').length;
      if (idx >= totalItems-(itemsPerSlide-1)) {
          var it = itemsPerSlide - (totalItems - idx);
          for (var i=0; i<it; i++) {
              // append slides to end
              if (e.direction=="left") {
                  $('#carouselExampleIndicators .carousel-item').eq(i).appendTo('#carouselExampleIndicators .carousel-inner');
              }
              else {
                  $('#carouselExampleIndicators .carousel-item').eq(0).appendTo('#carouselExampleIndicators .carousel-inner');
              }
          }
      }
  });
  </script>
@endsection
