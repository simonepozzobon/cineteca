@extends('layouts.public', ['active' => 'biblioteca'])
@section('title', 'Biblioteca di Morando')
@section('seo')
  <meta name="description" content="Fondazione Cineteca Italiana ha inaugurato LA BIBLIOTECA DI MORANDO, uno spazio speciale dove immergersi nella lettura e nello studio del cinema dedicato al grande critico cinematografico Morando Morandini." />
@endsection
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('css/single.css') }}">
@endsection
@section('content')
  <section id="main-image">
    @include('components.main.film-header', ['image' => asset('img/biblioteca.jpg'), 'title' => 'Biblioteca di Morando'])
  </section>
  <section id="description">
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div class="row">
          <div class="col-md-9">
            <div class="row">
              <div class="col">
                <div class="d-flex justify-content-center pt-5 px-5">
                  <a href="/storage/files/programma%20giugno%20Biblioteca%20di%20Morando.pdf" class="btn btn-primary" trget="_blank"><i class="fa fa-download"></i> Programma Degli Eventi Della Biblioteca</a>
                </div>
                <p class="pt-5 px-5">
                  <b>Fondazione Cineteca Italiana ha inaugurato LA BIBLIOTECA DI MORANDO</b>, uno spazio speciale dove immergersi nella lettura e nello studio del cinema dedicato al grande critico cinematografico Morando Morandini, la cui vasta biblioteca (composta da oltre tredicimila volumi, lettere, fotografie, faldoni di appunti a partire dal 1952) è stata donata dalla famiglia alla Cineteca di Milano.
                  La biblioteca sorge nel luogo che ha ospitato per tanti anni lo storico archivio cinematografico di via Sammartini lungo il percorso pedonale e ciclabile del Naviglio Martesana, un luogo particolare totalmente ristrutturato ed alimentato ad energia solare, ma dove si respira ancora cinema.<br>
                  <br>All'interno della Biblioteca si trova il murale dedicato a Morando, realizzato dall'artista cubano <a href="http://www.ascaniocuba.com" target="_blank">Ascanio</a> in occasione dell'inaugurazione nell'Aprile 2017.
                </p>
                <div class="d-flex justify-content-center p-5">
                  <a href="/pdf/CinetecaMilano_Catalogo_Fondo_Morando_Morandini.pdf" class="btn btn-primary" target="_blank"><i class="fa fa-list"></i> Catalogo Completo Fondo Morando Morandini</a>
                </div>
              </div>
            </div>
            @include('components.main.title', ['title' => 'Servizi Offerti'])
            <div class="row">
              <div class="col">
                <p class="px-5">
                  Nella Biblioteca di Morando è possibile consultare in loco migliaia di libri, di riviste e di fotografie di cinema.
                  Una parte del fondo è gia consultabile nell’<a href="http://www.biblioteche.regione.lombardia.it/vufind/Searchopacrl/Home" target="_blank">OPAC del Polo regionale lombardo</a> e nell’<a href="http://www.sbn.it/opacsbn/opac/iccu/base.jsp" target="_blank">OPAC SBN – Catalogo del Servizio Bibliotecario Nazionale</a>.
                </p>
                <div class="row px-5">
                  <div class="col-md-6 p-5">
                    <a href="http://www.biblioteche.regione.lombardia.it/vufind/Searchopacrl/Home" class="btn btn-primary btn-block" target="_blank"><i class="fa fa-book" aria-hidden="true"></i> OPAC Lombardia</a>
                  </div>
                  <div class="col-md-6 p-5">
                    <a href="http://www.sbn.it/opacsbn/opac/iccu/base.jsp" class="btn btn-primary btn-block" target="_blank"><i class="fa fa-book" aria-hidden="true"></i> OPAC SBN</a>
                  </div>
                </div>
              </div>
            </div>
            @include('components.main.title', ['title' => 'Approfondimenti'])
            <div class="row">
              <div class="col">
                <p class="px-5 pb-5">
                  Il progetto di catalogazione è tuttora in corso, ma la Biblioteca è già pronta ad accogliere studiosi e ricercatori.
                  Negli ex cellari occupati un tempo dalle pellicole sono stati collocati i fondi librari di proprietà di Fondazione Cineteca Italiana, accanto a una donazione importante come quella di libri, riviste, fotografie, recensioni, cartelle stampa e documenti vari appartenuti a Morando Morandini, il “decano” dei critici italiani, che ci ha lasciato nell’ottobre 2015.
                  Tra gli obiettivi di questo importantissimo progetto: salvaguardare i documenti del fondo Morandini provvedendo alla loro digitalizzazione; fornire uno strumento di consultazione o studio su un protagonista della storia del cinema e dello spettacolo milanesi; divulgare, più in generale, la cultura cinematografica della seconda parte del ‘900 mediante strumenti di facile accesso, in grado di coinvolgere il grande pubblico degli appassionati.
                  Critico e giornalista cinematografico dalla poliedrica personalità, Morando Morandini è stato essenziale per l’evoluzione linguistica dell’arte cinematografica. Era nato nel 1924 a Milano, e la sua città nel 2014 lo aveva premiato con l’Ambrogino d’oro. Nel mondo del cinema e della cultura in generale Morandini ha rappresentato un esempio unico di competenza, passione e rigore professionale e morale. Critico cinematografico al quotidiano “La Notte” (1952-1961), poi a “Il Giorno” (1965-1998), ha firmato inoltre numerose monografie su celebri registi, è stato coautore con Goffredo Fofi e Gianni Volpi di una importante “Storia del cinema” (1988) e nel 1995 ha pubblicato “Non sono che un critico”, aurea sintesi della sua esperienza e del suo pensiero. Dal 1999 ha curato, insieme alla moglie Laura e alla figlia Luisa, il celebre “Il Morandini. Dizionario dei film”, la cui edizione 2016 (la diciottesima) è appena uscita in libreria.
                </p>
              </div>
            </div>
            @if ($medias->count() > 0)
              @include('components.main.title', ['title' => 'Galleria'])
              <div class="row">
                <div class="col">
                  <div class="row">
                    @foreach ($medias as $key => $media)
                      <div class="col-md-4 pb-5">
                        <div class="container box-container">
                          <img class="img-fluid pb-3 w-100" src="{{ Storage::disk('local')->url($media->thumb) }}" alt="">
                        </div>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            @endif
            @if ($funders->count() > 0)
              @include('components.main.title', ['title' => 'Ritratti Cinetecari'])
              <div class="row">
                <div id="funders" class="col">
                  <funders funders="{{ $funders }}" />
                </div>
              </div>
            @endif
            <div class="row">
                  @foreach ($pages as $key => $page)
                    <div class="col-md-4 pb-5">
                      <div class="container box-container">
                        <img class="img-fluid pb-3 w-100" src="{{ Storage::disk('local')->url($page->media->first()->thumb) }}" alt="{{ $page->title }}">
                        <h2 class="text-center">
                          <a href="/pagina/{{ $page->slug }}" class="text-default">{{ $page->title }}</a>
                        </h2>
                      </div>
                    </div>
                  @endforeach
            </div>
          </div>
          <div class="col-md-3">
            <div class="row py-5">
              <div class="col">
                <h5>Info Generali</h5>
                <hr>
                <p>
                  <a class="" data-toggle="collapse" href="#orari" aria-expanded="false" aria-controls="biglietti">
                    Orari apertura
                  </a>
                  <div class="collapse" id="orari">
                    <p>
                      <b>Orario estivo consultazione biblioteca</b>
                      Da Lunedì a Venerdì<br>
                      dalle 14.30 alle 18.00<br><br>
                      <b>Orario eventi biblioteca</b>
                      Martedì, Mercoledì, Giovedì<br>
                      dalle 18:30 alle 20:30<br><br>
                    </p>
                  </div>
                </p>
                <p>
                  <a class="" data-toggle="collapse" href="#biglietti" aria-expanded="false" aria-controls="biglietti">
                    Ingresso gratuito
                  </a>
                </p>
              </div>
            </div>
            @include('components.sidebar.location-map', ['name' => 'Biblioteca di Morando', 'address' => 'Via Tofane, 49 - 20125', 'city' => 'Milano', 'slug' => 'biblioteca-morando'])
          </div>
        </div>

      </div>
    </div>

    @include('components.main.back')
  </section>
@endsection
@section('scripts')
  <script src="/js/funders.js"></script>
  <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCWWjzGK-o2GBGAPabC1nm6AETotrd53TQ&callback=initMap" async defer></script>
@endsection
