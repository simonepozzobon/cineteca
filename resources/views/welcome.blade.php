@extends('layouts.public', ['active' => 'home', 'loading' => 'no'])
@section('title', 'Home Page')
@section('stylesheets')
@endsection
@section('content')
  <section id="second-menu" class="bg-faded pt-5">
    <div class="row no-gutters">
      <div class="col">
        <div class="row">
          <div class="col px-5">
            <h1 align="center">Visita La Fondazione Cineteca Italiana di Milano</h1>
          </div>
        </div>
        <div class="row pb-5">
          <div class="col-6 offset-3">
            <hr>
          </div>
        </div>
        <div class="row">
          <div class="col pb-5">
            @foreach ($categories as $key => $category)
              @if ($key % 3 == 0)
                {{-- primo elemento --}}
                <div class="row no-gutters px-5">
              @endif
              <div class="col-md-4 pb-5">
                <div class="container">
                  <img class="img-fluid pb-3" src="{{ Storage::disk('local')->url($category->square_medium) }}" alt="">
                  <h2 class="text-center">
                    <a href="{{ $category->link }}" class="text-default">{{ $category->name }}</a>
                  </h2>
                </div>
              </div>
              @if ($key % 3 == 2)
                </div>
              @endif
            @endforeach

          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="news-swiper" class="bg-faded">
    <news-slider news="{{ $news }}"></news-slider>
  </section>
@endsection
@section('scripts')
  <script src="/js/news.js"></script>
  {{-- three slide carousel News--}}
  <script type="text/javascript">
  $('#carouselExampleIndicators').on('slide.bs.carousel', function (e) {
      var $e = $(e.relatedTarget);
      var idx = $e.index();
      var itemsPerSlide = 3;
      var totalItems = $('#carouselExampleIndicators .carousel-item').length;
      if (idx >= totalItems-(itemsPerSlide-1)) {
          var it = itemsPerSlide - (totalItems - idx);
          for (var i=0; i<it; i++) {
              // append slides to end
              if (e.direction=="left") {
                  $('#carouselExampleIndicators .carousel-item').eq(i).appendTo('#carouselExampleIndicators .carousel-inner');
              }
              else {
                  $('#carouselExampleIndicators .carousel-item').eq(0).appendTo('#carouselExampleIndicators .carousel-inner');
              }
          }
      }
  });
  </script>
@endsection
