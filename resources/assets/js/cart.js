import Vue from 'vue'

import Cart from './components/Cart.vue'

const app = new Vue({
  el: '#cart',
  components: {
    Cart
  }
});
