import Vue from 'vue'

import NewsSlider from './news/NewsSlider.vue'

const News = new Vue({
    el: '#news-swiper',
    components: {
        NewsSlider,
    }
})
