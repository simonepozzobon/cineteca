window.$ = window.jQuery = require('jquery');
window.Tether = require('tether');

import _ from 'lodash'

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/**
 *
 * Take the heihght of the main nav and position the cinestore menu above
 *
 */
$(document).ready(function() {

  /**
   *
   * Menu positioning
   *
   */

  if (document.getElementById('main-nav')) {
    var mainNav = document.getElementById('main-nav').offsetHeight;
    var cinestoreNav = document.getElementById('cinestore-nav');
    if (cinestoreNav) {
      cinestoreNav.style.top = mainNav + 'px';
    }
  }

  /**
   *
   * Search Form
   *
   */

  var wHeight = window.innerHeight;
  //search bar middle alignment
  $('#fullscreen-searchform').css('top', wHeight / 2);
  //reform search bar
  $(window).resize(function() {
    wHeight = window.innerHeight;
    $('#fullscreen-searchform').css('top', wHeight / 2);
  });
  // Search
  $('#search-button').click(function() {
    $('div.fullscreen-search-overlay').addClass('fullscreen-search-overlay-show');
  });
  $('a.fullscreen-close').click(function() {
    $('div.fullscreen-search-overlay').removeClass('fullscreen-search-overlay-show');
    $('div.fullscreen-cart-overlay').removeClass('fullscreen-cart-overlay-show');
  });

});
