import Vue from 'vue'
import {EventBus} from '_js/EventBus'
import ContentFillerBottom from './components/ContentFillerBottom.vue'
import ContentFillerMain from './components/ContentFillerMain.vue'
import ContentFillerSidebar from './components/ContentFillerSidebar.vue'

new Vue({
    el: '#calendar',
    components: {
        ContentFillerBottom,
        ContentFillerMain,
        ContentFillerSidebar
    },
    data: function() {
        return {
            shows: null,
            sidebar: null,
        }
    },
    computed: {
    },
    methods: {
        init: function() {

        },
        getHeight: function() {

            // var windowEl = document.documentElement.clientWidth
            // var shows = this.shows.clientHeight
            // var sidebar = this.sidebar.clientHeight
            // var difference = shows - sidebar
            //
            // console.log('la differenza è '+difference)
            //
            // // this.position = null
            // if (difference > 0) {
            //     EventBus.$emit('content-goes-sidebar')
            //     console.log('--> il contenuto va nella sidebar')
            // } else {
            //     EventBus.$emit('content-goes-bottom')
            //     console.log('--> il contenuto va giù')
            // }
        }
    },
    created() {},
    mounted() {
        // console.log('principale caricato')
        // emetto l'evento che fa partire tutto
        EventBus.$emit('content-filler-ready')

        // // prendo gli elementi della pagina per usarli dopo e calcolare l'altezza
        // this.shows = document.getElementById('shows')
        // this.sidebar = document.getElementById('show-sidebar')
        //
        // // ascolto il ridimensionamento della pagina e ogni volta che cambia ricalcolo l'altezza e la dimensione della pagina
        // var vue = this
        // this.$nextTick(function() {
        //     window.addEventListener('resize', function() {
        //         vue.getHeight()
        //         // vue.data.window = document.documentElement.clientWidth
        //     })
        // })
        //
        // // ascolto anche per ogni volta che sidebar e contenuto cambiano dimensione
        // this.shows.addEventListener('onresize', function() {
        //     console.log('bottom ridimensionato')
        //     // this.getHeight()
        // }.bind(this))
        //
        // this.sidebar.addEventListener('onresize', function() {
        //     console.log('sidebar ridimensionata')
        //     // this.getHeight()
        // }.bind(this))
        //
        // this.getHeight()

        // Ascolto quando vengono aggiunti gli elementi

    }
})
