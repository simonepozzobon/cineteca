import _ from 'lodash'

$(document).ready(function() {
  init()

  $('.add-to-cart').on('click', (e) => {
    e.preventDefault();
    if (e.target.dataset.options == 'true') {
      addProduct(e.target);
    }
  });
});

function init()
{
    var products = JSON.parse(localStorage.getItem('cinestore-cart'));
    if (products == null)
    {
        updateCart(0);
    }

    else
    {
        var count = countProducts(products);
        updateCart(count);
    }
}


function addProduct(el)
{
    var priceRaw = $('#price-'+el.dataset.id+' select').val(),
        price = JSON.parse(priceRaw),
        product = {
            'id': el.dataset.id,
            'name': el.dataset.name,
            'price': price,
            'quantity': 1
        };


    var products = JSON.parse(localStorage.getItem('cinestore-cart'));
    console.log('prodotti ',products);

    if (products == null)
    {
        console.log('il carrello è vuoto -> aggiungo il prodotto');

        products = [];
        products.push(product);
        localStorage.setItem('cinestore-cart', JSON.stringify(products));

        console.log('prodotti aggiornati ',products);
    }

    else
    {
        // var check = false;

        var check = _.find(products, (cart_product) => {
            return cart_product.id == product.id && cart_product.price.id == product.price.id;
        });

        console.log('check ',check);

        if (check)
        {
            console.log('il carrello non è vuoto e il prodotto c\'è già -> aumento la quantità');

            // Rimuovo il prodotto da modificare
            var filtered = _.reject(products, (cart_product) => {
                return cart_product.id == product.id && cart_product.price.id == product.price.id;
            });

            console.log('il prodotto da modificare ', check);
            console.log('ho rimosso quello selezionato ', filtered);

            // ricalcolo la quantità
            var quantity = check.quantity + 1;
            console.log('nuova quantità ', quantity);

            // Riformatto il prodotto con la quantità corretta
            var new_product = {
                'id': check.id,
                'name': check.name,
                'price': check.price,
                'quantity': quantity
            };

            // lo rimetto nel carrello
            filtered.push(new_product);
            localStorage.setItem('cinestore-cart', JSON.stringify(filtered));
            products = filtered;

            console.log('prodotti aggiornati ', products);
        }

        else
        {
            console.log('il carrello non è vuoto e il prodotto è nuovo -> aggiungo il prodotto');

            products.push(product);
            localStorage.setItem('cinestore-cart', JSON.stringify(products));

            console.log('prodotti aggiornati ', products);
        }

    }
    var count = countProducts(products)
    updateCart(count);
}


function countProducts(products)
{
    var count = 0;
    _.each(products, product => {
        count = count + product.quantity;
    });
    console.log('conteggio quantità ', count);
    return count;
}


function updateCart(count)
{
    // se il carrello non è vuoto
    if (count > 0)
    {
        if (document.getElementById('cart-link'))
        {
            // se il pulsante esiste, lo aggiorno
            var cart = '<a class="nav-link text-primary" href="/cinestore/ordine">'+count+' <i class="fa fa-shopping-cart"></i> Carrello</a>';
            $('#cart-link').html(cart);
        }

        else
        {
            // Altrimenti creo il pulsante nel menu
            var nav = $('#cinestore-nav').find('ul.navbar-nav');
            var cart = '';
            cart += '<li id="cart-link" class="nav-item">';
            cart +=   '<a class="nav-link text-primary" href="/cinestore/ordine">'+count+' <i class="fa fa-shopping-cart"></i> Carrello</a>';
            cart += '</li>';
            nav.append(cart);
        }
    }

    else
    {
        // il carrello è vuoto
        $('#cart-link').remove();
    }

}
