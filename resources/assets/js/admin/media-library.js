import Vue from 'vue'
import MediaLibrary from './components/media-library/MediaLibrary.vue'

new Vue({
  el: '#MediaLibrary',
  components: {
    MediaLibrary
  }
})
