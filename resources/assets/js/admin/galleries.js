import Vue from 'vue'

import UploadImage from './components/gallery/UploadImage.vue'
import List from './components/gallery/List.vue'
import GalleryDetails from './components/gallery/GalleryDetails.vue'

const app = new Vue({
  el: '#app',
  created() {

  },
  mounted() {
    this.$on('add-image', image => {
    });
  },
  components: {
    List,
    UploadImage,
    GalleryDetails
  }
});
