import Vue from 'vue';

import OrderProducts from './components/OrderProducts.vue'

const app = new Vue({
  el: '#app',
  props: ['categories'],
  components: {
    OrderProducts
  }
});
