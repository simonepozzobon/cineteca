import Vue from 'vue'

import Galleries from './components/gallery/Galleries.vue'

const app = new Vue({
  el: '#app',
  components: {
    Galleries
  }
});
