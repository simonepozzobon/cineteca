import Vue from 'vue'
import Shipping from './components/Shipping.vue'

new Vue({
  el: '#shipping',
  components: {
    Shipping
  }
})
