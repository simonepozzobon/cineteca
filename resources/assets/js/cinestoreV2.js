import Vue from 'vue'

import ProductList from './components/ProductList.vue'

const app = new Vue({
  el: '#main-grid',
  components: {
    ProductList
  }
})
