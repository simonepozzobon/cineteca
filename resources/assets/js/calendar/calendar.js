import Vue from 'vue'
import CalendarMain from './components/CalendarMain.vue'

const calendar = new Vue({
    el: '#calendar',
    components: {
        CalendarMain
    }
})
