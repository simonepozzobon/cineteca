import Vue from 'vue'

import Newsletter from './components/Newsletter.vue'

new Vue({
  el: '#newsletter',
  components: {
    Newsletter
  }
})
