import Vue from 'vue'
import Funders from './components/Funders.vue'

new Vue({
  el: '#funders',
  components: {
    Funders
  }
})
