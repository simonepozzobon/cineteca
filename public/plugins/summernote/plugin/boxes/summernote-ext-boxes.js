(function (factory) {
  /* global define */
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // Node/CommonJS
    module.exports = factory(require('jquery'));
  } else {
    // Browser globals
    factory(window.jQuery);
  }
}(function ($) {

  // Extends plugins for adding boxes.
  //  - plugin is external module for customizing.
  $.extend($.summernote.plugins, {
    /**
     * @param {Object} context - context object has status of editor.
     */
    'boxes': function (context) {
      var self = this;
      // ui has renders to build ui elements.
      //  - you can create a button with `ui.button`
      var ui = $.summernote.ui;
      var $editor = context.layoutInfo.editor;
      var options = context.options;
      var lang = options.langInfo;

      var getModalframe = function ($container) {
        // Crea il modal
        var $modal = $(
          '<div id="boxes" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
            '<div class="modal-dialog" role="document">'+
              '<div class="modal-content">'+
                '<div class="modal-header">'+
                  '<h5 class="modal-title" id="exampleModalLabel">Quanti quadri inserire</h5>'+
                  '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
                    '<span aria-hidden="true">&times;</span>'+
                  '</button>'+
                '</div>'+
                '<div id="box-modal" class="modal-body">'+
                  '<div id="selector" class="form-group">'+
                      '<h6>Numero di quadrati</h6>'+
                      '<input id="box-number" type="text" class="form-control">'+
                  '</div>'+
                '</div>'+
                '<div class="modal-footer">'+
                  '<div class="btn-group">'+
                    '<button type="button" class="btn btn-warning" data-dismiss="modal">Annulla</button>'+
                    '<button id="salva" type="button" class="btn btn-primary" disabled >Salva</button>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>'
        );
        $modal.appendTo($editor.parent());
        return $modal
      }

      var getContent = function ($modal) {
        var render = '';
        // Quando scrivo nel campo del modal genera gli altri campi per le impostazioni
        $('#box-number').on('keyup', function(){
          var val = $('#box-number').val();
          if ($.isNumeric(val)) {
            for (var i = 0; i < val; i++) {
              var key = i+1;
              var $input =  $(
                '<h4 class="pt-3 boxes-count">Quadrato '+key+'</h4>'+
                '<hr>'+
                '<div id="titolo-'+i+'" class="form-group">'+
                    '<h6>Titolo:</h6>'+
                    '<input id="box-title-'+i+'" type="text" name="title" class="form-control">'+
                '</div>'+
                '<div id="url-'+i+'" class="form-group">'+
                    '<h6>Url:</h6>'+
                    '<input id="box-url-'+i+'" type="text" name="url" class="form-control">'+
                '</div>'+
                '<div id="img-'+i+'" class="form-group">'+
                    '<h6>Img:</h6>'+
                    '<input id="box-img-'+i+'" type="file" name="media" class="form-control">'+
                '</div>'
              );
              $('#box-modal').append($input);

              // Rimuove il campo della quantità e riabilita il salvataggio
              $('#selector').hide();
              $('#salva').removeAttr('disabled');
            }
          }

          // quando viene cliccato il pulsante salva
          $('#salva').on('click', function() {
            var numItems = 0;
            var titles = [];
            var urls = [];
            var imgs = [];
            $('.boxes-count').each(function(i){
              numItems = numItems + 1;
              var title = $('#box-title-'+i).val();
              var url = $("#box-url-"+i).val();
              var img = $("#box-img-"+i).prop('files');

              // Salva il file con il controller php e ritorna il suo indirizzo nel server
              var imgPath = imgUpload(img[0]);
              imgPath = imgPath.replace('public', '/storage');

              titles.push(title);
              urls.push(url);
              imgs.push(imgPath);

            });

            // Esegui render
            getBoxesRender(numItems, titles, urls, imgs);
          });
        });
      }

      function getBoxesRender (n, titles, urls, imgs) {
        var boxesRender = '<div class="row"><div class="col pb-5">';
        for (var i = 0; i < n; i++) {
          if ( (i % 3) == 0 ) {
            boxesRender += '<div class="row no-gutters">';
          }
          boxesRender += '<div class="col-md-4 menu-box" style="background: url(\''+imgs[i]+'\'); background-size: cover; background-position: center center;">'
          boxesRender += '  <h2 class="text-white p-5">';
          boxesRender += '    <a href="'+urls[i]+'" class="text-white">'+titles[i]+'</a>';
          boxesRender += '  </h2>';
          boxesRender += '</div>';
          if ( (i % 3) == 2 ) {
            boxesRender += '</div>';
          }
        }
        boxesRender += '</div></div></div>';

        $(document).trigger('sendIt', [boxesRender]);
      }

      // global
      var imgPath;

      var imgUpload = function (img) {
        var csrf = $('meta[name=csrf-token]').attr("content");
        data = new FormData();
        data.append('media', img);
        $.ajax({
          type: 'post',
          url:  '/admin/media-upload-api',
          headers: { 'X-CSRF-TOKEN': csrf },
          processData: false,
          contentType: false,
          cache: false,
          async: false,
          data: data,
          success: function(response) {
            imgPath = response.file;
          }
        });
        return imgPath;
      }

      // add boxes button
      context.memo('button.boxes', function () {
        // create button
        var button = ui.button({
          contents: '<i class="fa fa-cubes" aria-hidden="true"></i>',
          tooltip: 'boxes',
          click: function () {
            // self.$panel.show();
            $modal = getModalframe();
            $modal.modal('show');
            var render = getContent($modal);
            $(document).on('sendIt', function(e, data) {
              var node = document.createElement('section');
              node.innerHTML = data;
              console.log(data);
              context.invoke('editor.insertNode', node);
              $modal.modal('hide');
              $('#boxes').on('hidden.bs.modal', function (e) {
                $('#boxes').remove();
              });
            });
          },
        });

        // create jQuery object from button instance.
        var $boxes = button.render();
        return $boxes;
      });

      // This events will be attached when editor is initialized.
      this.events = {
        // This will be called after modules are initialized.
        'summernote.init': function (we, e) {
          console.log('summernote initialized', we, e);
        },
        // This will be called when user releases a key on editable.
        'summernote.keyup': function (we, e) {
          console.log('summernote keyup', we, e);
        }
      };

      // This method will be called when editor is initialized by $('..').summernote();
      // You can create elements for plugin
      this.initialize = function () {
        this.$panel = $('<div class="boxes-panel"/>').css({
          position: 'absolute',
          width: 100,
          height: 100,
          left: '50%',
          top: '50%',
          background: 'red'
        }).hide();

        this.$panel.appendTo('body');
      };

      // This methods will be called when editor is destroyed by $('..').summernote('destroy');
      // You should remove elements on `initialize`.
      this.destroy = function () {
        this.$panel.remove();
        this.$panel = null;
      };
    }
  });
}));
