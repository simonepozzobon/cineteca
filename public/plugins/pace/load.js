(function() {

  /* Page loading counter value holder */
  var paceProgress = document.getElementsByClassName("pace-progress")[0];

  /* Create page loading counter */
  var pageLoadingCounter = document.createElement("div");
  pageLoadingCounter.className = "page-loading-counter";

  /* Add page loading counter to the page */
  document.body.appendChild(pageLoadingCounter);

  /* Get page loading value */
  function pageLoadingValue() {
    return + paceProgress.getAttribute("data-progress");
  }

  /* Set page loading value */
  function setPageLoadingValue() {
    pageLoadingCounter.innerHTML = pageLoadingValue();
  }

  /* Update page loading counter */
  setTimeout(function updatePageLoadingCounter(){
    setPageLoadingValue();
    if (pageLoadingValue() !== 100) {
      setTimeout(updatePageLoadingCounter, 100);
    }
  }, 100);


})();
