<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funder extends Model
{
    public function media()
    {
        return $this->morphToMany('App\Media', 'mediable');
    }
}
