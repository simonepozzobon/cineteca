<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoogleAnalytics extends Model
{
    public function initialize()
    {

      $KEY_FILE_LOCATION = storage_path('cinetecamilano-2fb13712c88b.json');

      // Create and configure a new client object.
      $client = new \Google_Client();
      $client->setApplicationName("Hello Analytics Reporting");
      $client->setAuthConfig($KEY_FILE_LOCATION);
      $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
      $analytics = new \Google_Service_AnalyticsReporting($client);

      return $analytics;
    }

    public function getReport($analytics) {

      // Replace with your view ID, for example XXXX.
      $VIEW_ID = "155054275";

      // Create the DateRange object.
      $dateRange = new \Google_Service_AnalyticsReporting_DateRange();
      $dateRange->setStartDate("7daysAgo");
      $dateRange->setEndDate("today");

      // Create the Metrics object.
      $sessions = new \Google_Service_AnalyticsReporting_Metric();
      $sessions->setExpression("ga:sessions");
      $sessions->setAlias("sessions");

      // Create the ReportRequest object.
      $request = new \Google_Service_AnalyticsReporting_ReportRequest();
      $request->setViewId($VIEW_ID);
      $request->setDateRanges($dateRange);
      $request->setMetrics(array($sessions));

      $body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
      $body->setReportRequests( array( $request) );
      return $analytics->reports->batchGet( $body );
    }

    function printResults($reports) {

      $response = collect();

      for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
        $report = $reports[ $reportIndex ];
        $header = $report->getColumnHeader();
        $dimensionHeaders = $header->getDimensions();
        $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
        $rows = $report->getData()->getRows();

        $headers = collect();
        $gMetrics = collect();

        for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
          $row = $rows[ $rowIndex ];
          $dimensions = $row->getDimensions();
          $metrics = $row->getMetrics();
          for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
            $data = [
              'header' => $dimensionHeaders[$i],
              'dimensione' => $dimensions[$i]
            ];
            $headers->push($data);
          }

          for ($j = 0; $j < count($metrics); $j++) {
            $values = $metrics[$j]->getValues();
            for ($k = 0; $k < count($values); $k++) {
              $entry = $metricHeaders[$k];
              $data = [
                $entry->getName() => $values[$k]
              ];
              $gMetrics->push($data);
            }
          }
        }

        $data = [
          'headers' => $headers,
          'values' => $gMetrics
        ];

        $response->push($data);

      }
      return $response;
    }
}
