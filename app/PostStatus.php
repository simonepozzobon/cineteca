<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostStatus extends Model
{
    protected $table = 'post_statuses';

    public function posts()
    {
      return $this->hasMany('App\Post');
    }
}
