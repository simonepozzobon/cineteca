<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FestivalSubCategory extends Model
{
    protected $table = 'festival_sub_categories';

    public function category()
    {
      return $this->belongsTo('App\FestivalCategory');
    }
}
