<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    public function orderProducts()
    {
      return $this->hasMany('App\OrderProduct');
    }

    public function orderStatus()
    {
      return $this->belongsTo('App\OrderStatus');
    }
}
