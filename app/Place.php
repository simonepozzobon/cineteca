<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $table = 'places';

    public function filmLocation()
    {
      return $this->belongsTo('App\FilmLocation');
    }

    public function films()
    {
      return $this->hasMany('App\Film');
    }
}
