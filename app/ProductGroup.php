<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model
{
    use Searchable;

    protected $table = 'product_groups';

    // public function toSearchableArray()
    // {
    //     return [
    //          'id' => $this->id,
    //          'name' => $this->name,
    //          'slug' => $this->slug,
    //          'description' => $this->description
    //     ];
    // }

    public function media()
    {
        return $this->morphToMany('App\Media', 'mediable');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function gallery()
    {
        return $this->morphToMany('App\Gallery', 'galleryable');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'tagable');
    }

    public function cinestoreSliders()
    {
        return $this->morphMany('App\CinestoreSlider', 'sliderable');
    }

    public function featuredSliders()
    {
        return $this->morphMany('App\FeaturedSlider', 'featurable');
    }
}
