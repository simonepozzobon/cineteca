<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class MediaUpload extends Controller
{
    public function upload(Request $request)
    {
      $filename = $request->file('media')->getClientOriginalName();
      $file = $request->file('media')->storeAs('public/pages', $filename);
      $path = Storage::disk('local')->url($file);
      $data = [
        'filename' => $filename,
        'file' => $file,
        'path' => $path
      ];
      return response()->json($data);
    }
}
