<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use Validator;
use App\Media;
use App\Utility;
use App\PageLink;
use App\Category;
use App\Festival;
use App\PostStatus;
use App\FestivalCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => 'logout']);
    }

    public function index()
    {
        $pages = Page::orderBy('id', 'desc')->get();
        return view('admin.pages.index', compact('pages'));
    }

    public function create()
    {
        $statuses = PostStatus::all();
        $categories = Category::all();
        $festivals = Festival::all();
        return view('admin.pages.create', compact('statuses', 'categories','festivals'));
    }

    public function store(Request $request)
    {
        $utility = new Utility;

        if ($request->input('media-library') != null) {
          $validator = Validator::make($request->all(), [
            'title' => 'required',
            'slug' => 'required',
          ]);
        } elseif ($request->input('redirect') == 'on') {
          $validator = Validator::make($request->all(), [
            'title' => 'required',
            'slug' => 'required',
            'media' => 'required',
            'redirect_url' => 'required',
          ]);
        } else {
          $validator = Validator::make($request->all(), [
            'title' => 'required',
            'slug' => 'required',
            'media' => 'required',
          ]);
        }

        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput();
        }

        $page = new Page;
        $page->title = $request->input('title');
        $page->slug = $request->input('slug');
        $page->kids = $request->input('kids');

        if ($request->input('hide_info') == 'on') {
            $page->hide_info = 1;
        }

        if ($request->input('festival') == 'on') {
          $page->festival = 1;
          $page->festival_id = $request->input('festival_id');
          $page->fest_cat_id = $request->input('festival_cat_id');
        }

        if ($request->input('redirect') == 'on') {
          $page->redirect = 1;
          $page->redirect_url = $request->input('redirect_url');
          $page->content = 'Redirect ...';
        } else {
          $page->content = $request->input('content');
        }

        $page->status_id = $request->input('status_id');
        $page->category_id = $request->input('category_id');
        $page->save();

        if ($request->input('media-library') != null) {
          $media = Media::find($request->input('media-library'));
          $page->media()->save($media);
        } else {
          $utility->storeMedia($request->file('media'), $page);
        }

        session()->flash('success', 'Pagina creata!');
        return redirect()->route('pages.index');

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $page = Page::find($id);
        $categories = Category::all();
        $statuses = PostStatus::all();
        $pages = Page::all();
        $links = PageLink::where('page_id', '=', $id)->with('page')->get();
        $festivals = Festival::all();
        if ($page->festival == 1) {
          $fest_cats = FestivalCategory::where('festival_id', '=', $page->festival_id)->get();
        }
        return view('admin.pages.edit', compact('page', 'categories', 'statuses', 'pages', 'links', 'festivals', 'fest_cats'));
    }

    public function update(Request $request, $id)
    {
        $utility = new Utility;

        if ($request->input('redirect') == 'on') {
          $validator = Validator::make($request->all(), [
            'title' => 'required',
            'slug' => 'required',
            'redirect_url' => 'required',
          ]);
        } else {
          $validator = Validator::make($request->all(), [
            'title' => 'required',
            'slug' => 'required',
          ]);
        }

        if ($validator->fails()) {
          return redirect()->back()->withErrors($validator)->withInput();
        }

        $page = Page::find($id);

        if ($request->input('media-library') != null) {
          $media = Media::find($request->input('media-library'));
          $page->media()->detach();
          $page->media()->save($media);
        } elseif ($request->file('media')) {
          $utility->storeMedia($request->file('media'), $page);
        }

        $page->title = $request->input('title');
        $page->slug = $request->input('slug');
        $page->kids = $request->input('kids');

        if ($request->input('hide_info') == 'on') {
            $page->hide_info = 1;
        }


        if ($request->input('festival') == 'on') {
          $page->festival = 1;
          $page->festival_id = $request->input('festival_id');
          $page->fest_cat_id = $request->input('festival_cat_id');
        } else {
          $page->festival = 0;
          $page->festival_id = null;
          $page->fest_cat_id = null;
        }

        if ($request->input('redirect') == 'on') {
          $page->redirect = 1;
          $page->redirect_url = $request->input('redirect_url');
          $page->content = 'Redirect ...';
        } else {
          $page->content = $request->input('content');
        }

        $page->status_id = $request->input('status_id');
        $page->category_id = $request->input('category_id');
        $page->save();

        session()->flash('success', 'Pagina modificata!');

        return redirect()->route('pages.index');
    }

    public function destroy($id)
    {
        $page = Page::find($id);
        $page->media()->detach();

        // verifica che la pagina non sia collegata a qualche altra pagina
        $links = PageLink::where('linked_id', '=', $id)->get();
        if ($links) {
            // elimina tutti i collegamenti a quella pagina nelle altre pagine
            foreach ($links as $key => $link) {
                $linked = PageLink::find($link->id);
                $linked->delete();
            }
        }

        $page->delete();
        session()->flash('success', 'Pagina eliminata!');
        return redirect()->back();
    }

    public function storeLink(Request $request)
    {
      $link = new PageLink;
      $link->page_id = $request->page_id;
      $link->linked_id = $request->linked_id;
      $link->save();

      $link->img = Storage::disk('local')->url($link->page->media->first()->thumb);
      $link->title = $link->page->title;

      return response()->json($link);
    }

    public function destroyLink($id)
    {
      $data = [
        'id' => $id
      ];
      $link = PageLink::find($id);
      $link->delete();

      return response()->json($data);
    }
}
