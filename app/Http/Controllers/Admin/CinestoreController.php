<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\Product;
use App\Shipping;
use App\OrderStatus;
use App\ProductPrice;
use App\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CinestoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => 'logout']);
    }

    public function index()
    {
        $orders = Order::orderBy('created_at', 'desc')->paginate(25);
        $checkOrders = Order::where('order_status_id', '!=', [6, 3])->count();

        return view('admin.cinestore.index', compact('orders', 'checkOrders'));
    }

    public function single($id)
    {
        $order = Order::with('orderProducts', 'orderProducts.product', 'orderProducts.product.media', 'orderProducts.product.category')->find($id);
        $orderStatuses = OrderStatus::all();

        $tot = 0;
        foreach ($order->orderProducts()->get() as $key => $ord_prod) {
            $price = number_format($ord_prod->quantity * $ord_prod->product->price);
            $tot = $tot + $price;
        }

        return view('admin.cinestore.single', compact('order', 'tot', 'orderStatuses'));
    }

    public function edit_order($id, Request $request)
    {
        $order = Order::find($id);
        $order->order_status_id = $request->status;
        $order->save();

        session()->flash('success', 'Ordine modificato!');

        return redirect()->route('admin.cinestore.single', $id);
    }

    public function addPrice(Request $request)
    {
        $price = new ProductPrice();
        $price->description = $request->description;
        $price->price = $request->price;
        $price->save();
        return response()->json($price, 200);
    }

    public function order_products()
    {
        $product_categories = ProductCategory::all();
        return view('admin.cinestore.order-products.index', compact('product_categories'));
    }

    public function get_category_products(Request $request)
    {
        $products = Product::where([
            ['product_category_id', '=', $request->category_id],
            ['status_id', '=', 2]
        ])->orderBy('order')->get();

        foreach ($products as $key => $product) {
            $product->image = Storage::disk('local')->url($product->media()->first()->thumb);
        }

        return response()->json($products, 200);
    }

    public function save_products_order(Request $request)
    {
        $sorted_products = json_decode($request->products);

        if (count($sorted_products) > 1) {
            foreach ($sorted_products as $key => $product) {
                $product = Product::find($product->id);
                $product->order = $key;
                $product->save();
            }
            return response()->json([
                'success' => true
            ], 200);
        }

        return response()->json([
            'success' => false
        ], 200);
    }

    public function shipping()
    {
        return view('admin.shipping.index');
    }

    public function get_shipping()
    {
        $shippings = Shipping::all();
        return response($shippings);
    }

    public function new_shipping(Request $request)
    {
        $shipping = new Shipping();
        $shipping->description = $request->description;
        $shipping->price = $request->price;
        $shipping->increment = $request->increment;
        $shipping->timing = $request->timing;
        $shipping->save();

        return response($shipping);
    }

    public function update_shipping(Request $request)
    {
        $shipping = Shipping::find($request->id);
        $shipping->description = $request->description;
        $shipping->price = $request->price;
        $shipping->increment = $request->increment;
        $shipping->timing = $request->timing;
        $shipping->save();

        return response($shipping);
    }

    public function delete_shipping(Request $request)
    {
        $shipping = Shipping::find($request->id);
        $shipping->delete();

        return response(['status' => true]);
    }
}
