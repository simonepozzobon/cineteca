<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => 'logout']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('admin.category.index')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
          'name' => 'required',
          'color' => 'required',
        ));



        $category = new Category;
        $category->name = $request->input('name');
        $category->color = $request->input('color');

        $file = $request->file('media');

        if ($file) {
          $images = $category->convertImage($file);
          $category->img = $images['original'];
          $category->thumb = $images['thumb'];
          $category->square_medium = $images['squareMedium'];
          $category->square_big = $images['squareBig'];
        }

        $category->in_home = $request->input('in_home') == 'on' ? 1 : 0;
        $category->save();

        session()->flash('success', 'Categoria creata!');

        return redirect()->route('categories.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
          'name' => 'required',
        ));

        $category = Category::find($id);

        $category->name = $request->input('name');
        $category->color = $request->input('color');

        $file = $request->file('media');

        if ($file) {
          $category->deleteImages($category->img, $category->thumb, $category->square_medium, $category->square_big);
          $images = $category->convertImage($file);
          $category->img = $images['original'];
          $category->thumb = $images['thumb'];
          $category->square_medium = $images['squareMedium'];
          $category->square_big = $images['squareBig'];
        }

        $category->in_home = $request->input('in_home') == 'on' ? 1 : 0;
        $category->save();

        session()->flash('success', 'Category saved!');
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $category = Category::find($id);

        if($category->img) {
          Storage::delete($category->img);
          Storage::delete($category->thumb);
          Storage::delete($category->square_medium);
          Storage::delete($category->square_big);
        }

        $category->delete();

        session()->flash('success', 'Category deleted!');
        return redirect()->route('categories.index');
    }
}
