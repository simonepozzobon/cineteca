<?php

namespace App\Http\Controllers\Admin;

use App\Film;
use App\Event;
use App\Exhibit;
use App\Exhibition;
use App\FilmSchedule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RelationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => 'logout']);
    }

    /**
     *
     * RASSEGNE
     *
    */

    public function storeExhibitionFilm(Request $request)
    {
      $exhibition = Exhibition::find($request['exhibition_id']);
      $contents = [];
      $errors = [];
      if (isset($request['film_ids'])) {
        foreach ($request['film_ids'] as $key => $filmID) {
          if ($exhibition->hasFilm($exhibition->id, $filmID) === 0) {
            $film = Film::find($filmID);
            $exhibition->films()->attach($filmID);
            $data = [
              'id' => $film->id,
              'title' => $film->title,
              'type_id' => 1,
              'type' => 'Film',
            ];
            array_push($contents, $data);
          } else {
            $data = [
              'error' => 'Il film è già stato assegnato alla rassegna'
            ];
            $errors->push($data);
          }
        }
      }

      if (isset($request['event_ids'])) {
        foreach ($request['event_ids'] as $key => $eventID) {
          $event = Event::find($eventID);
          $exhibition->event()->save($event);
          $data = [
            'id' => $event->id,
            'title' => $event->title,
            'type_id' => 2,
            'type' => 'Evento'
          ];
          array_push($contents, $data);
        }
      }

      $data = [
        'objects' => $contents,
        'errors' => $errors
      ];

      return response()->json($data);
    }

    public function destroyExhibitionFilm(Request $request)
    {
      $exhibition = Exhibition::find($request['exhibition_id']);
      if ($request['type'] == 'Film') {
        $exhibition->films()->detach($request['id']);
      } elseif ($request['type'] == 'Evento') {
        $exhibition->event()->detach($request['id']);
      }

      $data = [
        'id' => $request['id']
      ];

      return response()->json($data);
    }

    /**
     *
     * MOSTRE
     *
    */

    public function storeExhibitRelation(Request $request)
    {
      $exhibit = Exhibit::find($request['exhibit_id']);
      $items = collect();
      foreach ($request['item_ids'] as $key => $item_id) {
        // Per ogni elemento analizzo che model appartiene e cerco l'oggetto
        switch ($request['types'][$key]) {
          case 'film':
            // Se non è già stato collegato lo collego al film
            if ($exhibit->hasFilm($exhibit->id, $item_id) === 0) {
              $film = Film::find($item_id);
              $exhibit->films()->attach($item_id);
              $film->type = 'film';
              $items->push($film);
            }
            break;

          case 'exhibition':
            if ($exhibit->hasExhibition($exhibit->id, $item_id) === 0) {
              $exhibition = Exhibition::find($item_id);
              $exhibit->exhibitions()->attach($item_id);
              $exhibition->type = 'rassegna';
              $items->push($exhibition);
            }
            break;
        }
      }

      // Pacchetto la risposta
      $data = [
        'objects' => $items
      ];

      return response()->json($data);
    }

    public function destroyExhibitRelation(Request $request)
    {

      $exhibit = Exhibit::find($request['exhibit_id']);

      switch ($request['type']) {
        case 'film':
          $exhibit->films()->detach($request['id']);
          break;

        case 'rassegna':
          $exhibit->exhibitions()->detach($request['id']);
          break;
      }

      $data = [
        'type' => $request['type'],
        'id' => $request['id'],
      ];

      return response($data);
    }

    /**
     *
     * FILM / SHOW
     *
    */

    public function storeFilmShow(Request $request)
    {
      $show = new FilmSchedule;
      $show->film_id = $request->film_id;
      $dateTime = $show->mergeDateTime($request->day, $request->time);
      $show->date_time = $dateTime;
      $show->save();

      $string = strtotime($dateTime);

      $time = date('H:i', $string);
      $show->time = $time;

      $day = date('d, F', $string);
      $show->day = $day;

      return response()->json($show);
    }

    public function destroyFilmShow($id)
    {
      $data = [
        'id' => $id
      ];
      $show = FilmSchedule::find($id);
      $show->delete();

      return response()->json($data);
    }
}
