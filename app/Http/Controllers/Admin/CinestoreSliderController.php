<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\ProductGroup;
use App\CinestoreSlider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CinestoreSliderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => 'logout']);
    }

    public function index()
    {
        $sliders = CinestoreSlider::all();
        $products = Product::all();
        $product_groups = ProductGroup::all();

        $slides = collect();

        foreach ($sliders as $key => $slider) {
            $slide = $slider->sliderable()->first();
            $slide->img = Storage::disk('local')->url($slide->media()->first()->thumb);
            $slide->slider_id = $slider->id;
            $slides->push($slide);
        }

        return view('admin.cinestore-slider.index', compact('slides', 'products', 'product_groups'));
    }

    public function store(Request $request)
    {
        if ($request->product != null) {
            $slider = new CinestoreSlider();
            $product = Product::find($request->product);
            $product->cinestoreSliders()->save($slider);
        }

        if ($request->product_groups != null) {
            $slider = new CinestoreSlider();
            $product_groups = ProductGroup::find($request->product_groups);
            $product_groups->cinestoreSliders()->save($slider);
        }

        session()->flash('success', 'Slider creato!');
        return redirect()->route('cinestore.slider.index');
    }

    public function destroy($id)
    {
        $slider = CinestoreSlider::find($id);
        $slider->delete();
        session()->flash('success', 'Slider cancellato!');
        return redirect()->route('cinestore.slider.index');
    }
}
