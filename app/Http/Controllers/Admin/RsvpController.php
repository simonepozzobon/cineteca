<?php

namespace App\Http\Controllers\Admin;

use App\Guest;
use App\Event;
use App\RsvpMessage;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class RsvpController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => 'logout']);
    }

    public function index($id)
    {
        $event = Event::find($id);

        $guests = Guest::where([
            ['event_id', '=', $id],
            ['status', '=', 0]
        ])->orderBy('name')->get();

        $pendings = Guest::where([
            ['event_id', '=', $id],
            ['status', '=', 1]
        ])->orderBy('name')->get();

        $waiting = 0;

        foreach ($pendings as $key => $pending) {
            $waiting = $waiting + $pending->guests + $pending->kids;
        }

        return view('admin.rsvp.single', compact('event', 'guests', 'pendings', 'waiting'));
    }

    public function download($id)
    {
        $event = Event::find($id);

        $guests = Guest::where([
            ['event_id', '=', $id],
            ['status', '=', 0]
        ])->orderBy('name')->get();

        $pendings = Guest::where([
            ['event_id', '=', $id],
            ['status', '=', 1]
        ])->orderBy('name')->get();

        Excel::create('Cineteca di Milano - '.$event->title, function($excel) use($guests, $pendings) {
          $excel->setTitle('Ospiti');
          $excel->sheet('Registrazioni', function($sheet) use($guests){
              $sheet->row(1, ['Nome', 'Cognome', 'N. di ospiti', 'Bambini', 'Email', 'Numero di telefono']);
              $sheet->cells('A1:F1', function($cells) {
                $cells->setBackground('#f3f3f3');
                $cells->setFontSize('16');
                $cells->setFontWeight('bold');
              });
              foreach ($guests as $key => $guest) {
                $sheet->row(($key+2), [$guest->name, $guest->surname, $guest->guests, $guest->kids, $guest->email, $guest->phone]);
              }
          });
          $excel->sheet('Lista D\'attesa', function($sheet) use($pendings){
              $sheet->row(1, ['Nome', 'Cognome', 'N. di ospiti', 'Bambini', 'Email', 'Numero di telefono']);
              $sheet->cells('A1:F1', function($cells) {
                $cells->setBackground('#f3f3f3');
                $cells->setFontSize('16');
                $cells->setFontWeight('bold');
              });
              foreach ($pendings as $key => $pending) {
                $sheet->row(($key+2), [$pending->name, $pending->surname, $pending->guests, $pending->kids, $pending->email, $pending->phone]);
              }
          });
      })->export('xls');

      return redirect()->route('rsvp.index', $event->id);
    }

    public function messageSave(Request $request)
    {
        $message = RsvpMessage::where([
            ['type', '=', $request->type],
            ['event_id', '=', $request->event_id]
        ])->first();

        if (isset($message)) {
            if ($message->count() > 0) {
                $message->message = $request->message;
                $message->save();
            } else {
                $message = new RsvpMessage;
                $message->type = $request->type;
                $message->message = $request->message;
                $message->event_id = $request->event_id;
                $message->save();
            }
        } else {
            $message = new RsvpMessage;
            $message->type = $request->type;
            $message->message = $request->message;
            $message->event_id = $request->event_id;
            $message->save();
        }

        $data = [
            'results' => $message
        ];

        return response()->json($data);
    }
}
