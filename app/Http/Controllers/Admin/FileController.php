<?php

namespace App\Http\Controllers\Admin;

use App\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => 'logout']);
    }

    public function index()
    {
      $files = File::orderBy('created_at', 'desc')->get();
      return view('admin.files.index', compact('files'));
    }

    public function fileUpload(Request $request)
    {
      $filename = $request->file('media')->getClientOriginalName();
      $file_path = $request->file('media')->storeAs('public/files', $filename);

      $file = new File;
      $file->title = $request->input('title');
      $file->url = $file_path;
      $file->save();

      session()->flash('success', 'Pdf caricato');
      return redirect()->route('files.index');
    }

    public function destroy($id)
    {
      $file = File::find($id);
      Storage::delete($file->url);
      $file->delete();
      session()->flash('success', 'Pdf eliminato');
      return redirect()->route('files.index');
    }
}
