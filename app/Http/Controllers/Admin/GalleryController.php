<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use App\Media;
use App\Event;
use App\Gallery;
use App\Exhibit;
use App\Festival;
use App\Exhibition;
use App\StaticPage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => 'logout']);
    }

    public function index()
    {
      $galleries = Gallery::orderBy('created_at', 'desc')->get();
      foreach ($galleries as $key => $gallery) {
        if ($gallery->pages !== null && $gallery->pages->count() > 0) {
          $gallery->type_title = $gallery->pages->first()['title'];
          $gallery->url = url('/').'/pagina/'.$gallery->pages->first()['slug'];

        } elseif ($gallery->events !== null && $gallery->events->count() > 0) {
          $gallery->type_title = $gallery->events->first()['title'];
          $gallery->url = url('/').'/evento/'.$gallery->events->first()['slug'];

        } elseif ($gallery->products !== null && $gallery->products->count() > 0) {
          $gallery->type_title = $gallery->products->first()['name'];
          $gallery->url = '#';

        } elseif ($gallery->staticPages !== null && $gallery->staticPages->count() > 0) {
          $gallery->type_title = $gallery->staticPages->first()['name'];
          $gallery->url = '#';

        } elseif ($gallery->exhibitions !== null && $gallery->exhibitions->count() > 0) {
          $gallery->type_title = $gallery->exhibitions->first()['title'];
          $gallery->url = url('/').'/rassegna/'.$gallery->exhibitions->first()['slug'];

        } elseif ($gallery->exhibits !== null && $gallery->exhibits->count() > 0) {
          $gallery->type_title = $gallery->exhibits->first()['title'];
          $gallery->url = url('/').'/esposizione/'.$gallery->exhibits->first()['slug'];

        } elseif ($gallery->festivals !== null && $gallery->festivals->count() > 0) {
          $gallery->type_title = $gallery->festivals->first()['title'];
          $gallery->url = url('/').'/festival/'.$gallery->festivals->first()['slug'];

        } else {
          $gallery->type_title = 'non assegnata';
          $gallery->url = '#';
        }

      }

      return view('admin.galleries.index', compact('galleries'));
    }

    public function create()
    {
      $medias = Media::all();
      $pages = Page::all();
      $events = Event::all();
      $static_pages = StaticPage::all();
      $exhibitions = Exhibition::all();
      $exhibits = Exhibit::all();
      $festivals = Festival::all();

      return view('admin.galleries.create', compact('medias', 'pages', 'events', 'static_pages', 'exhibitions', 'exhibits', 'festivals'));
    }

    public function store(Request $request)
    {
      $gallery = new Gallery;
      $gallery->title = $request->input('title');
      $gallery->description = $request->input('description');
      $gallery->save();
      $gallery->medias()->sync($request->input('imgs'));

      if ($request->input('event') !== null) {
        // è un evento
        $event = Event::find($request->input('event'));
        $event->gallery()->save($gallery);

      } elseif ($request->input('page') !== null) {
        // è una pagina
        $page = Page::find($request->input('page'));
        $page->gallery()->save($gallery);

      } elseif ($request->input('static_page') !== null) {
        // è una pagina statica
        $static = StaticPage::find($request->input('static_page'));
        $static->gallery()->save($gallery);

      } elseif ($request->input('exhibition') !== null) {
        // è una rassegna
        $exhibition = Exhibition::find($request->input('exhibition'));
        $exhibition->gallery()->save($gallery);

      } elseif ($request->input('exhibit') !== null) {
        // è una rassegna
        $exhibit = Exhibit::find($request->input('exhibit'));
        $exhibit->gallery()->save($gallery);

      } elseif ($request->input('festival') !== null) {
        // è una festival
        $festival = Festival::find($request->input('festival'));
        $festival->gallery()->save($gallery);
      }

      session()->flash('success', 'Galleria creata!');
      return redirect()->route('gallery.index');
    }

    public function edit($id)
    {
        $gallery = Gallery::where('id', '=', $id)->with('medias')->first();
        $medias = Media::orderBy('created_at', 'desc')->get();
        $pages = Page::all();
        $events = Event::all();
        $static_pages = StaticPage::all();
        $exhibitions = Exhibition::all();
        $exhibits = Exhibit::all();
        $festivals = Festival::all();

        $link = Gallery::relationType($gallery);
        // $link = 'gianni';

        $listImg = [];
        foreach ($gallery->medias as $key => $media) {
            array_push($listImg, $media->id);
        }

        foreach ($gallery->medias as $key => $media) {
            $media->thumb = Storage::disk('local')->url($media->thumb);
        }

        foreach ($medias as $key => $media) {
            $media->thumb = Storage::disk('local')->url($media->thumb);
        }

        return view('admin.galleries.edit', compact('gallery', 'medias', 'pages', 'events', 'static_pages', 'exhibitions', 'exhibits', 'link', 'listImg', 'festivals'));
    }

    public function update($id, Request $request)
    {
      $gallery = Gallery::find($id);
      $gallery->title = $request->input('title');
      $gallery->description = $request->input('description');
      // $gallery->medias()->detach();
      // $gallery->medias()->sync($request->input('imgs'));

      if ($request->input('event') !== null) {
        // è un evento
        $event = Event::find($request->input('event'));
        $event->gallery()->detach($gallery);
        $event->gallery()->save($gallery);

      } elseif ($request->input('page') !== null) {
        // è una pagina
        $page = Page::find($request->input('page'));
        $page->gallery()->detach($gallery);
        $page->gallery()->save($gallery);

      } elseif ($request->input('static_page') !== null) {
        // è una pagina statica
        $static = StaticPage::find($request->input('static_page'));
        $static->gallery()->detach($gallery);
        $static->gallery()->save($gallery);

      } elseif ($request->input('exhibition') !== null) {
        // è una rassegna
        $exhibition = Exhibition::find($request->input('exhibition'));
        $exhibition->gallery()->detach($gallery);
        $exhibition->gallery()->save($gallery);

      } elseif ($request->input('exhibit') !== null) {
        // è una rassegna
        $exhibit = Exhibit::find($request->input('exhibit'));
        $exhibit->gallery()->detach($gallery);
        $exhibit->gallery()->save($gallery);

      } elseif ($request->input('festival') !== null) {
        // è un festival
        $festival = Festival::find($request->input('festival'));
        $festival->gallery()->detach($gallery);
        $festival->gallery()->save($gallery);
      }

      $gallery->save();

      session()->flash('success', 'Galleria modificata!');
      return redirect()->route('gallery.index');
    }

    public function destroy($id)
    {
      $gallery = Gallery::find((int)$id);
      $gallery->medias()->detach();

      if ($gallery->pages !== null && $gallery->pages->count() > 0) {
          $pages = $gallery->pages;
          foreach ($pages as $key => $page) {
            $page->gallery()->detach();
          }

      } elseif ($gallery->event !== null && $gallery->events->count() > 0) {
          $events = $gallery->events;
          foreach ($events as $key => $event) {
            $event->gallery()->detach();
          }

      } elseif ($gallery->products !== null && $gallery->products->count() > 0) {
          $products = $gallery->products;
          foreach ($products as $key => $product) {
            $product->gallery()->detach();
          }

      } elseif ($gallery->staticPages !== null && $gallery->staticPages->count() > 0) {
          $staticPages = $gallery->staticPages;
          foreach ($staticPages as $key => $static) {
            $static->gallery()->detach();
          }

      } elseif ($gallery->exhibitions !== null && $gallery->exhibitions->count() > 0) {
          $exhibitions = $gallery->exhibitions;
          foreach ($exhibitions as $key => $exhibition) {
            $exhibition->gallery()->detach();
          }

      } elseif ($gallery->exhibits !== null && $gallery->exhibits->count() > 0) {
          $exhibits = $gallery->exhibits;
          foreach ($exhibits as $key => $exhibit) {
            $exhibit->gallery()->detach();
          }

      } elseif ($gallery->festivals !== null && $gallery->festivals->count() > 0) {
          $festivals = $gallery->festivals;
          foreach ($festivals as $key => $festival) {
            $festival->gallery()->detach();
          }
      }

      $gallery->delete();

      session()->flash('success', 'Galleria cancellata!');
      return redirect()->route('gallery.index');
    }

    public function upload(Request $request)
    {
      $img = $request->file('media');

      $filename = $img->getClientOriginalName();
      $file = $img->storeAs('public/media', $filename);

      // preparo gli altri formati
      $thumb = $img->storeAs('public/media/thumb', $filename);
      $slide_img = $img->storeAs('public/media/slide_img', $filename);
      $portrait = $img->storeAs('public/media/portrait', $filename);
      $landscape = $img->storeAs('public/media/landscape', $filename);

      // genero gli altri formati
      $path = storage_path('app/public/media');

      // Thumb
      Image::make($path.'/thumb/'.$filename)->fit(500, 500, function ($constraint) {
          $constraint->upsize();
      })->save();

      // slide_img
      Image::make($path.'/slide_img/'.$filename)->fit(1920, 720, function ($constraint) {
          $constraint->upsize();
      })->save();

      // portrait
      Image::make($path.'/portrait/'.$filename)->fit(720, 960, function ($constraint) {
          $constraint->upsize();
      })->save();

      // landscape 960 540
      Image::make($path.'/landscape/'.$filename)->fit(960, 540, function ($constraint) {
          $constraint->upsize();
      })->save();

      // Salvo L'immagine nella Libreria
      $media = new Media;
      $media->img = $file;
      $media->thumb = $thumb;
      $media->slide_img = $slide_img;
      $media->portrait = $portrait;
      $media->landscape = $landscape;
      $media->save();

      $media->thumb = Storage::disk('local')->url($media->thumb);

      $data = [
        'message' => 'success',
        'id' => $media->id,
        'url' => Storage::disk('local')->url($media->thumb),
        'media' => $media,
      ];

      return response()->json($data);
    }

    public function gallery_update(Request $r)
    {
        $gallery = Gallery::where('id', '=', $r->gallery_id)->first();
        $gallery->medias()->detach();

        $images = json_decode($r->images);
        foreach ($images as $key => $image) {
          $img = Media::find($image->id);
          $gallery->medias()->attach($img);
        }

        return response()->json($gallery->medias()->get(), 200);
    }
}
