<?php

namespace App\Http\Controllers\Admin;

use App\Media;
use App\Utility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;


class MediaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => 'logout']);
    }

    public function index()
    {
      $medias = Media::limit(30)->orderBy('created_at', 'desc')->get();
      return view('admin.media.index', compact('medias'));
    }

    public function edit($id)
    {
      $media = Media::find($id);

      return view('admin.media.edit', compact('media'));
    }

    public function upload(Request $request)
    {
      $img = $request->file('media');

      $filename = $img->getClientOriginalName();
      $file = $img->storeAs('public/media', $filename);

      // preparo gli altri formati
      $thumb = $img->storeAs('public/media/thumb', $filename);
      $slide_img = $img->storeAs('public/media/slide_img', $filename);
      $portrait = $img->storeAs('public/media/portrait', $filename);
      $landscape = $img->storeAs('public/media/landscape', $filename);

      // genero gli altri formati
      $path = storage_path('app/public/media');

      // Thumb
      Image::make($path.'/thumb/'.$filename)->fit(500, 500, function ($constraint) {
          $constraint->upsize();
      })->save();

      // slide_img
      Image::make($path.'/slide_img/'.$filename)->fit(1920, 720, function ($constraint) {
          $constraint->upsize();
      })->save();

      // portrait
      Image::make($path.'/portrait/'.$filename)->fit(720, 960, function ($constraint) {
          $constraint->upsize();
      })->save();

      // landscape 960 540
      Image::make($path.'/landscape/'.$filename)->fit(960, 540, function ($constraint) {
          $constraint->upsize();
      })->save();

      // Salvo L'immagine nella Libreria
      $media = new Media;
      $media->img = $file;
      $media->thumb = $thumb;
      $media->slide_img = $slide_img;
      $media->portrait = $portrait;
      $media->landscape = $landscape;
      $media->save();

      session()->flash('success', 'Immagine caricata!');
      return redirect()->route('media.index');
    }

    public function mediaCrop(Request $request)
    {
      $url = str_replace('/storage', 'app/public', $request['path']);
      $path = storage_path($url);

      Image::make($request['base64'])->save($path, 90);

      $data = [
        'message' => 'success',
        'path' => $request['path']
      ];

      return response()->json($data);
    }

    public function mediaReplace(Request $request)
    {
      $img = $request->file('media');

      $filename = basename($request->input('path'));
      $file = $img->storeAs('public/media', $filename);

      // preparo gli altri formati
      $thumb = $img->storeAs('public/media/thumb', $filename);
      $slide_img = $img->storeAs('public/media/slide_img', $filename);
      $portrait = $img->storeAs('public/media/portrait', $filename);
      $landscape = $img->storeAs('public/media/landscape', $filename);

      // genero gli altri formati
      $path = storage_path('app/public/media');

      // Thumb
      Image::make($path.'/thumb/'.$filename)->fit(500, 500, function ($constraint) {
          $constraint->upsize();
      })->save();

      // slide_img
      Image::make($path.'/slide_img/'.$filename)->fit(1920, 720, function ($constraint) {
          $constraint->upsize();
      })->save();

      // portrait
      Image::make($path.'/portrait/'.$filename)->fit(720, 960, function ($constraint) {
          $constraint->upsize();
      })->save();

      // landscape 960 540
      Image::make($path.'/landscape/'.$filename)->fit(960, 540, function ($constraint) {
          $constraint->upsize();
      })->save();

      $data = [
        'message' => 'success',
        'thumb' => $thumb,
        'slide_img' => $slide_img,
        'filename' => $filename
      ];

      return response()->json($data);
    }

    public function loadMore(Request $request)
    {
        $medias = Media::orderBy('created_at', 'desc')->offset($request->objs)->limit(30)->get();

        $medias_formatted = $medias->transform(function($media, $key) {
            $media->url = Storage::disk('local')->url($media->thumb);
            return $media;
        });

        $count = $medias->count();
        $data = [
            'message' => 'success',
            'imgs' => $medias_formatted,
            'newObjs' => $count,
        ];
        return response()->json($data);
    }

    public function searchImages(Request $request)
    {
        $medias = Media::search($request->search_string)->get();
        return response($medias);
    }
}
