<?php

namespace App\Http\Controllers\Admin;

use App\Film;
use App\News;
use App\Page;
use App\Post;
use App\Guest;
use App\Event;
use App\Media;
use App\Utility;
use App\Exhibit;
use App\Product;
use App\Festival;
use App\Category;
use App\Exhibition;
use App\HomeSlider;
use App\MediaRelation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class ToolsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => 'logout']);
    }

    /**
     *
     * Square Images on Categories.
     *
     */

    public function squareImgCategories()
    {
      $categories = Category::all();

      foreach ($categories as $key => $category) {
        $file = $category->img;

        if ($file != null) {
          $images = $category->restoreImage($file);
          $category->thumb = $images['thumb'];
          $category->square_medium = $images['squareMedium'];
          $category->square_big = $images['squareBig'];
          $category->save();
        }
      }

      session()->flash('success', 'immagini modificate');
      return view('admin');
    }


    /**
     *
     * Populate Media Table
     *
     */

    public function mediable()
    {
      $utility = new Utility;

      $events = Event::all();
      foreach ($events as $key => $event) {
        $utility->mediaMove($event, $event->img);
      }

      $exhibits = Exhibit::all();
      foreach ($exhibits as $key => $exhibit) {
        $utility->mediaMove($exhibit, $exhibit->img);
      }

      $exhibitions = Exhibition::all();
      foreach ($exhibitions as $key => $exhibition) {
        $utility->mediaMove($exhibition, $exhibition->img);
      }

      $festivals = Festival::all();
      foreach ($festivals as $key => $festival) {
        $utility->mediaMove($festival, $festival->img);
      }

      $films = Film::all();
      foreach ($films as $key => $film) {
        $utility->mediaMove($film, $film->featured_img);
      }

      $pages = Page::all();
      foreach ($pages as $key => $page) {
        $utility->mediaMove($page, $page->img);
      }

      $posts = Post::all();
      foreach ($posts as $key => $post) {
        $utility->mediaMove($post, $post->img);
      }

      $products = Product::all();
      foreach ($products as $key => $product) {
        $utility->mediaMove($product, $product->img);
      }

      $home_slider = HomeSlider::all();
      foreach ($home_slider as $key => $slide) {
        $utility->mediaMove($slide, $slide->img);
      }

      session()->flash('success', 'Nuovi Media Aggiunti');
      return view('admin');
    }


    /**
     *
     * Generate Thumbnails
     *
     */

    public function regenerateThumbnails()
    {
      $medias = Media::where('thumb', '=', '')->orWhereNull('thumb')->get();
      foreach ($medias as $key => $media) {
        $filename = basename($media->img);

        // verifico che il file non esista altrimenti lo elimino
        $types = ['thumb', 'slide_img', 'portrait', 'landscape'];
        foreach ($types as $key => $type) {
          $exists = Storage::disk('local')->exists('public/media/'.$type.'/'.$filename);
          if ($exists) {
            Storage::delete('public/media/'.$type.'/'.$filename);
          }
          Storage::copy($media->img, 'public/media/'.$type.'/'.$filename);
        }

        // genero gli altri formati
        $path = storage_path('app/public/media');
        // Thumb
        Image::make($path.'/thumb/'.$filename)->fit(500, 500, function ($constraint) {
            $constraint->upsize();
        })->save();

        // slide_img
        Image::make($path.'/slide_img/'.$filename)->fit(1920, 720, function ($constraint) {
            $constraint->upsize();
        }, 'top')->save();

        // portrait
        Image::make($path.'/portrait/'.$filename)->fit(720, 960, function ($constraint) {
            $constraint->upsize();
        })->save();

        // landscape
        Image::make($path.'/landscape/'.$filename)->fit(960, 540, function ($constraint) {
            $constraint->upsize();
        }, 'top')->save(10);

        // Salvo i nuovi formati
        $media->thumb = 'public/media/thumb/'.$filename;
        $media->slide_img = 'public/media/slide_img/'.$filename;
        $media->portrait = 'public/media/portrait/'.$filename;
        $media->landscape = 'public/media/landscape/'.$filename;
        $media->save();
      }

      session()->flash('success', 'thumbnails rigenerate');
      return view('admin');
    }


    public function correctUrl()
    {
      $utility = new Utility;

      $events = Event::all();
      foreach ($events as $key => $event) {
        $slug = $utility->slugifyUrl($event->slug);
        $event->save();
      }

      $exhibits = Exhibit::all();
      foreach ($exhibits as $key => $exhibit) {
        $slug = $utility->slugifyUrl($exhibit->slug);
        $exhibit->save();
      }

      $exhibitions = Exhibition::all();
      foreach ($exhibitions as $key => $exhibition) {
        $slug = $utility->slugifyUrl($exhibition->slug);
        $exhibition->save();
      }

      $festivals = Festival::all();
      foreach ($festivals as $key => $festival) {
        $slug = $utility->slugifyUrl($festival->slug);
        $festival->save();
      }

      $films = Film::all();
      foreach ($films as $key => $film) {
        $slug = $utility->slugifyUrl($film->featured_slug);
        $film->save();
      }

      $pages = Page::all();
      foreach ($pages as $key => $page) {
        $slug = $utility->slugifyUrl($page->slug);
        $page->save();
      }

      $posts = Post::all();
      foreach ($posts as $key => $post) {
        $slug = $utility->slugifyUrl($post->slug);
        $post->save();
      }

      $products = Product::all();
      foreach ($products as $key => $product) {
        $slug = $utility->slugifyUrl($product->slug);
        $product->save();
      }

      session()->flash('success', 'slug fixed');
      return view('admin');
    }

    public function getInfo()
    {
      return view('admin.tools.info');
    }

    public function recover_events_guests()
    {
        // Reverse research
        $events_ids = array();
        $guests = Guest::all();

        // prendo solo gli id degli eventi e li metto in un'array $event_ids per confrontarli dopo con gli eventi
        foreach ($guests as $key => $guest) {
            if (!in_array($guest->event_id, $events_ids)) {
                array_push($events_ids, $guest->event_id);
            }
        }

        // prendo tutti gli eventi
        $matching = collect();
        $not_matching = collect();

        foreach ($events_ids as $key => $id) {
            $match = Event::find($id);
            if ($match) {
              $matching->push($match);
            } else {
              $guests_count = Guest::where('event_id', '=', $id)->get();
              $first_guest = $guests_count->first();

              $medias = Utility::recover_events_media($id);

              if ($medias->count() > 0) {
                dd($medias);
              }

              $item = collect();
              $item->id = $id;
              $item->created_at = $first_guest->created_at;
              $item->medias = $medias;
              $item->guests_count = $guests_count->count();
              $not_matching->push($item);
            }
        }

        $not_matching = $not_matching->sortByDesc('created_at')->values();

        $results = [
          'matching' => $matching,
          'not_matched' => $not_matching
        ];

        return view('admin.tools.recover-events-guests', compact('results'));
    }

    public function recover_events_guests_single($id)
    {
        $guests = Guest::where('event_id', '=', $id)->get();
        return view('admin.tools.recover-events-guests.single', compact('guests', 'id'));
    }

    public function restore_event()
    {

    }

    public function restore_event_save()
    {

    }

}
