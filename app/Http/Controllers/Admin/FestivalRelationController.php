<?php

namespace App\Http\Controllers\Admin;

use App\Film;
use App\Event;
use App\Exhibit;
use App\Exhibition;
use App\FestivalRelation;
use App\FestivalCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FestivalRelationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => 'logout']);
    }

    public function store(Request $request)
    {
      $element = new FestivalRelation;
      $element->festival_id = $request->festival_id;
      switch ($request->type) {
        case 'film':
            $element->film_id = $request->id;
            $response = Film::find($request->id);
            $response->type = 'Film';
          break;

        case 'exhibit':
            $element->exhibit_id = $request->id;
            $response = Exhibit::find($request->id);
            $response->type = 'Esposizione';
          break;

        case 'exhibition':
            $element->exhibition_id = $request->id;
            $response = Exhibition::find($request->id);
            $response->type = 'Rassegna';
          break;

        case 'event':
            $element->event_id = $request->id;
            $response = Event::find($request->id);
            $response->type = 'Evento';
          break;
      }

      $element->save();

      $data = [
        'msg' => 'fatto',
        'id' => $element->id,
        'type' => $response->type,
        'title' => $response->title,
      ];

      return response()->json($data);
    }

    public function destroy(Request $request)
    {

      $relation = FestivalRelation::find($request->relation_id);
      $relation->delete();

      $data = [
        'relation_id' => $request->relation_id
      ];

      return response()->json($data);
    }

    public function getFestivalCategories($id) {
      $festival_Cats = FestivalCategory::where('festival_id', '=', $id)->get();

      return response()->json($festival_Cats);
    }
}
