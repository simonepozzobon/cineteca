<?php

namespace App\Http\Controllers\Admin;

use App\Place;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlaceController extends Controller
{
    public function placesLoad(Request $request)
    {
      $places = Place::where('film_location_id', '=', $request->location_id)->get();
      return response()->json($places);
    }
}
