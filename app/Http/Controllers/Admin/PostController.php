<?php

namespace App\Http\Controllers\Admin;

use App\Tag;
use App\Post;
use App\Film;
use App\Media;
use App\Video;
use App\Event;
use App\Admin;
use App\Actor;
use App\Press;
use Validator;
use App\Editor;
use App\Utility;
use App\Release;
use App\Gallery;
use App\Product;
use App\Exhibit;
use App\Producer;
use App\Director;
use App\PostType;
use App\Category;
use App\Location;
use App\Festival;
use App\EventType;
use App\Exhibition;
use App\PostStatus;
use App\Distributor;
use App\RsvpMessage;
use App\ProductPrice;
use App\Photographer;
use App\FilmLocation;
use App\ProductGroup;
use App\FilmSchedule;
use App\FilmCategory;
use App\Screenwriter;
use App\PicCollection;
use App\ProductCategory;
use App\FestivalCategory;
use App\FestivalRelation;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\LengthAwarePaginator;


class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => 'logout']);
    }

    public function filter(string $post_type)
    {
      $post_types = PostType::all();
      $filter = $post_type;
      switch ($post_type) {
        case 'all':
          return redirect()->route('posts.index');
          break;
        case 'news':
          $items = Post::orderBy('created_at', 'desc')->paginate(10);
          foreach ($items as $key => $item) {
            $item->type = 'News';
            $item->url = 'admin/posts/news';
            $item->public_url = '/news/'.$item->slug;
          }
          return view('admin.posts.filter', compact('items', 'post_types', 'filter'));
          break;

        case 'exhibit':
          $items = Exhibit::orderBy('created_at', 'desc')->paginate(10);
          foreach ($items as $key => $item) {
            $item->type = 'Esposizione';
            $item->url = 'admin/posts/exhibit';
            $item->public_url = '/esposizione/'.$item->slug;
          }
          return view('admin.posts.filter', compact('items', 'post_types', 'filter'));
          break;

        case 'exhibition':
          $items = Exhibition::orderBy('created_at', 'desc')->paginate(10);
          foreach ($items as $key => $item) {
            $item->type = 'Rassegna';
            $item->url = 'admin/posts/exhibition';
            $item->public_url = '/rassegna/'.$item->slug;
          }
          return view('admin.posts.filter', compact('items', 'post_types', 'filter'));
          break;

        case 'film':
          $items = Film::orderBy('created_at', 'desc')->paginate(10);
          foreach ($items as $key => $item) {
            $item->type = 'Film';
            $item->img = $item->featured_img;
            $item->url = 'admin/posts/film';
            $item->public_url = '/film/'.$item->slug;
          }
          return view('admin.posts.filter', compact('items', 'post_types', 'filter'));
          break;

        case 'festival':
          $items = Festival::orderBy('created_at', 'desc')->paginate(10);
          foreach ($items as $key => $item) {
            $item->type = 'Festival';
            $item->url = 'admin/posts/festival';
            $item->public_url = '/festival/'.$item->slug;
          }
          return view('admin.posts.filter', compact('items', 'post_types', 'filter'));
          break;

        case 'cinestore':
          $items = collect();
          $products = Product::orderBy('created_at', 'desc')->get();
          $product_groups = ProductGroup::orderBy('created_at', 'desc')->get();

          foreach ($products as $key => $item) {
            $item->title = $item->name;
            $item->type = 'Prodotto';
            $item->url = 'admin/posts/cinestore';
            $item->public_url = '/cinestore/prodotto/'.$item->slug;
            $items->push($item);
          }

          foreach ($product_groups as $key => $item) {
            $item->title = $item->name;
            $item->type = 'Percorso';
            $item->url = 'admin/posts/percorsi';
            $item->public_url = '/cinestore/percorsi/'.$item->slug;
            $items->push($item);
          }

          $items = $items->sortByDesc('created_at');

          // Pagination
          $collection = $items;
          $currentPage = LengthAwarePaginator::resolveCurrentPage();
          $perPage = 15;
          $currentPageSearchResults = $collection->slice(($currentPage - 1) * $perPage, $perPage)->all();
          $items = new LengthAwarePaginator($currentPageSearchResults, count($collection), $perPage);
          $items->setPath(route('posts.filter', 'cinestore'));

          return view('admin.posts.filter', compact('items', 'post_types', 'filter'));
          break;

        case 'event':
          $items = Event::orderBy('created_at', 'desc')->paginate(10);
          foreach ($items as $key => $item) {
            $item->type = 'Evento';
            $item->url = 'admin/posts/event';
            $item->public_url = '/evento/'.$item->slug;
          }
          return view('admin.posts.filter', compact('items', 'post_types', 'filter'));
          break;

        case 'release':
          $items = Release::orderBy('created_at', 'desc')->paginate(10);
          foreach ($items as $key => $item) {
            $item->type = 'Stampa';
            $item->url = 'admin/posts/release';
            $item->public_url = '/rassegna-stampa/'.$item->slug;
          }
          return view('admin.posts.filter', compact('items', 'post_types', 'filter'));
          break;
      }
    }

    public function index(Request $request)
    {
        $posts = Post::with('media')->get();
        $post_types = PostType::all();
        $films = Film::with('media')->get();
        $exhibitions = Exhibition::with('media')->get();
        $exhibits = Exhibit::with('media')->get();
        $festivals = Festival::with('media')->get();
        $events = Event::with('media')->get();
        $products = Product::with('media')->get();
        $products_groups = ProductGroup::with('media')->get();
        $releases = Release::with('media')->get();

        $items = collect();

        foreach ($posts as $key => $post) {
            $item = [
                'id' => $post->id,
                'type' => 'News',
                'title' => $post->title,
                'img' => ($post->media->first()) ? $post->media->first()->thumb : 'no-img',
                'created_at' => $post->created_at,
                'url' => 'admin/posts/news',
                'public_url' => '/news/'.$post->slug,
                'status_id' => $post->status_id,
            ];
            $items->push($item);
        }

        foreach ($films as $key => $film) {
            $item = [
                'id' => $film->id,
                'type' => 'Film',
                'title' => $film->title,
                'img' => ($film->media->first()) ? $film->media->first()->thumb : 'no-img',
                'created_at' => $film->created_at,
                'url' => 'admin/posts/film',
                'public_url' => '/film/'.$film->slug,
                'status_id' => $film->status_id,
            ];

            $items->push($item);
        }

        foreach ($exhibitions as $key => $exhibition) {
            $item = [
                'id' => $exhibition->id,
                'type' => 'Rassegna',
                'title' => $exhibition->title,
                'img' => ($exhibition->media->first()) ? $exhibition->media->first()->thumb : 'no-img',
                'created_at' => $exhibition->created_at,
                'url' => 'admin/posts/exhibition',
                'public_url' => '/rassegna/'.$exhibition->slug,
                'status_id' => $exhibition->status_id,
            ];
            $items->push($item);
        }

        foreach ($exhibits as $key => $exhibit) {
            $item = [
                'id' => $exhibit->id,
                'type' => 'Esposizione',
                'title' => $exhibit->title,
                'img' => ($exhibit->media->first()) ? $exhibit->media->first()->thumb : 'no-img',
                'created_at' => $exhibit->created_at,
                'url' => 'admin/posts/exhibit',
                'public_url' => '/esposizione/'.$exhibit->slug,
                'status_id' => $exhibit->status_id,
            ];
            $items->push($item);
        }

        foreach ($festivals as $key => $festival) {
            $item = [
                'id' => $festival->id,
                'type' => 'Festival',
                'title' => $festival->title,
                'img' => ($festival->media->first()) ? $festival->media->first()->thumb : 'no-img',
                'created_at' => $festival->created_at,
                'url' => 'admin/posts/festival',
                'public_url' => '/festival/'.$festival->slug,
                'status_id' => $festival->status_id,
            ];
            $items->push($item);
        }

        foreach ($events as $key => $event) {
          if ($event->limit_guest == 1) {
            $item = [
                'id' => $event->id,
                'type' => 'Evento',
                'title' => $event->title,
                'img' => ($event->media->first()) ? $event->media->first()->thumb : 'no-img',
                'created_at' => $event->created_at,
                'url' => 'admin/posts/event',
                'public_url' => '/evento/'.$event->slug,
                'status_id' => $event->status_id,
                'limit_guest' => true,
              ];
            $items->push($item);
          } else {
            $item = [
                'id' => $event->id,
                'type' => 'Evento',
                'title' => $event->title,
                'img' => ($event->media->first()) ? $event->media->first()->thumb : 'no-img',
                'created_at' => $event->created_at,
                'url' => 'admin/posts/event',
                'public_url' => '/evento/'.$event->slug,
                'status_id' => $event->status_id,
              ];
            $items->push($item);
          }

        }

        foreach ($products as $key => $product) {
            $item = [
                'id' => $product->id,
                'type' => 'Prodotto',
                'title' => $product->name,
                'img' => ($product->media->first()) ? $product->media->first()->thumb : 'no-img',
                'created_at' => $product->created_at,
                'url' => 'admin/posts/cinestore',
                'public_url' => '/cinestore/'.$product->slug,
                'status_id' => $product->status_id,
            ];
            $items->push($item);
        }

        foreach ($products_groups as $key => $product_group) {
          $item = [
              'id' => $product_group->id,
              'type' => 'Percorso',
              'title' => $product_group->name,
              'img' => ($product_group->media->first()) ? $product_group->media->first()->thumb : 'no-img',
              'created_at' => $product_group->created_at,
              'url' => 'admin/posts/percorsi',
              'public_url' => '/cinestore/percorsi/'.$product_group->slug,
              'status_id' => $product_group->status_id,
            ];
          $items->push($item);
        }

        foreach ($releases as $key => $release) {
          $item = [
              'id' => $release->id,
              'type' => 'Stampa',
              'title' => $release->title,
              'img' => ($release->media->first()) ? $release->media->first()->thumb : 'no-img',
              'created_at' => $release->created_at,
              'url' => 'admin/posts/release',
              'public_url' => '/stampa/'.$release->slug,
              'status_id' => $release->status_id,
            ];
          $items->push($item);
        }

        $items = $items->sortByDesc('created_at');

        // Pagination
        $collection = $items;
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 15;
        $currentPageSearchResults = $collection->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $items = new LengthAwarePaginator($currentPageSearchResults, count($collection), $perPage);
        $items->setPath(route('posts.index'));

        return view('admin.posts.index', compact('items', 'post_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(string $post_type)
    {
      $admins = Admin::all();
      $statuses = PostStatus::all();
      $categories = Category::all();

      switch ($post_type) {
        case 'news':
              $locations = Location::where('type_id', '=', 5)->get();
              return view('admin.posts.news.create', compact('statuses', 'categories', 'locations'));
          break;

        case 'exhibit':
              $locations = Location::where('type_id', '=', '3')->get();
              $festivals = Festival::all();
              return view('admin.posts.exhibit.create', compact('statuses', 'locations', 'festivals'));
          break;

        case 'exhibition':
              $locations = Location::where('type_id', '=', '2')->get();
              $festivals = Festival::all();
              return view('admin.posts.exhibition.create', compact('statuses', 'locations', 'festivals'));
          break;

        case 'film':
              $locations = FilmLocation::with('places')->get();
              $actors = Actor::all();
              $directors = Director::all();
              $distributors = Distributor::all();
              $editors = Editor::all();
              $photographers = Photographer::all();
              $producers = Producer::all();
              $screenwriters = Screenwriter::all();
              $film_categories = FilmCategory::all();
              $festivals = Festival::all();
              return view('admin.posts.film.create', compact('statuses', 'locations', 'actors', 'directors', 'distributors', 'editors', 'photographers', 'producers', 'screenwriters', 'film_categories', 'festivals'));
          break;

        case 'festival':
              return view('admin.posts.festival.create', compact('statuses'));
          break;

        case 'cinestore':
              $product_categories = productCategory::all();
              $products = Product::all();
              $tags = Tag::all();
              $pic_collects = PicCollection::all();
              $product_prices = ProductPrice::all();
              return view('admin.posts.product.create', compact('statuses', 'product_categories', 'products', 'product_prices', 'tags', 'pic_collects'));
          break;

        case 'event':
              $event_types = EventType::all();
              $locations = Location::where('type_id', '=', '4')->get();
              $films = Film::all();
              $festivals = Festival::all();
              $tags = Tag::all();
              return view('admin.posts.event.create', compact('statuses', 'event_types', 'locations', 'films', 'festivals', 'tags'));
          break;

        case 'release':
              $presses = Press::all();
              return view('admin.posts.release.create', compact('statuses', 'presses'));
          break;

        default:
              return view('admin.posts.general.create', compact('admins', 'statuses', 'categories'));
          break;
      }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(string $post_type, Request $request)
    {
      $utility = new Utility;

      switch ($post_type) {
        case 'news':
            if ($request->input('media-library') != null) {
              $validator = Validator::make($request->all(), [
                'title' => 'required',
                'slug' => 'required',
              ]);
            } else {
              $validator = Validator::make($request->all(), [
                'title' => 'required',
                'slug' => 'required',
                'media' => 'required',
              ]);
            }

            if ($validator->fails()) {
              return redirect()->back()->withErrors($validator)->withInput();
            }

            $news = new Post;
            $news->title = $request->input('title');
            $news->content = $request->input('content');
            $news->slug = $utility->slugifyUrl($request->input('slug'));
            $news->in_home = $request->input('in_home');
            $news->status_id = $request->input('status_id');
            $news->category_id = $request->input('category_id');
            if ($request->location_id) {
                $news->location_id = $request->location_id;
            }
            $news->save();

            if ($request->input('media-library') != null) {
              $media = Media::find($request->input('media-library'));
              $news->media()->save($media);
            } else {
              $utility->storeMedia($request->file('media'), $news);
            }

            session()->flash('success', 'News creata!');
            return redirect()->route('posts.index');
          break;

        case 'exhibit':
            if ($request->input('media-library') != null) {
              $validator = Validator::make($request->all(), [
                'title' => 'required',
                'slug' => 'required',
                'location_id' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
              ]);
            } else {
              $validator = Validator::make($request->all(), [
                'title' => 'required',
                'slug' => 'required',
                'media' => 'required',
                'location_id' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
              ]);
            }

            if ($validator->fails()) {
              return redirect('admin/posts/exhibit/create')->withErrors($validator)->withInput();
            }

            $exhibit = new Exhibit;
            $exhibit->status_id = $request->input('status_id');
            $exhibit->title = $request->input('title');
            $exhibit->slug = $utility->slugifyUrl($request->input('slug'));
            $exhibit->location_id = $request->input('location_id');
            $exhibit->kids = $request->input('kids');

            if ($request->input('festival') == 'on') {
              $exhibit->festival = 1;
              $exhibit->festival_id = $request->input('festival_id');
              $exhibit->fest_cat_id = $request->input('festival_cat_id');
            }

            $exhibit->start_date = $exhibit->convertDate($request->input('start_date'));
            $exhibit->end_date = $exhibit->convertDate($request->input('end_date'));
            $exhibit->description = $request->input('description');
            $exhibit->save();

            if ($request->input('media-library') != null) {
              $media = Media::find($request->input('media-library'));
              $exhibit->media()->save($media);
            } else {
              $utility->storeMedia($request->file('media'), $exhibit);
            }


            session()->flash('success', 'Esposizione creata!');
            return redirect()->route('posts.index');
          break;

        case 'exhibition':
            if ($request->input('media-library') != null) {
              $validator = Validator::make($request->all(), [
                'title' => 'required',
                'slug' => 'required',
                'location_id' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
              ]);
            } else {
              $validator = Validator::make($request->all(), [
                'title' => 'required',
                'slug' => 'required',
                'media' => 'required',
                'location_id' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
              ]);
            }

            if ($validator->fails()) {
              return redirect('admin/posts/exhibition/create')->withErrors($validator)->withInput();
            }

            $exhibition = new Exhibition;
            $exhibition->status_id = $request->input('status_id');
            $exhibition->title = $request->input('title');
            $exhibition->slug = $utility->slugifyUrl($request->input('slug'));
            $exhibition->location_id = $request->input('location_id');

            if ($request->input('festival') == 'on') {
              $exhibition->festival = 1;
              $exhibition->festival_id = $request->input('festival_id');
              $exhibition->fest_cat_id = $request->input('festival_cat_id');
            }

            $exhibition->kids = $request->input('kids');
            $exhibition->start_date = $exhibition->convertDate($request->input('start_date'));
            $exhibition->end_date = $exhibition->convertDate($request->input('end_date'));
            $exhibition->description = $request->input('description');
            $exhibition->save();

            if ($request->input('media-library') != null) {
              $media = Media::find($request->input('media-library'));
              $exhibition->media()->save($media);
            } else {
              $utility->storeMedia($request->file('media'), $exhibition);
            }

            session()->flash('success', 'Rassegna creata!');
            return redirect()->route('posts.index');
          break;

        case 'film';
            if ($request->input('media-library') != null) {
              // Se il media selezione dalla libreria non, è necessario
              $validator = Validator::make($request->all(), [
                'title' => 'required',
                'slug' => 'required',
                'location_id' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
              ]);
            } else {
              // Altrimenti è necessario averlo caricato ex-novo
              $validator = Validator::make($request->all(), [
                'title' => 'required',
                'slug' => 'required',
                'media' => 'required',
                'location_id' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
              ]);
            }

            if ($validator->fails()) {
              return redirect('admin/posts/film/create')->withErrors($validator)->withInput();
            }

            $film = new Film;

            if ($request->input('place_id') != null) {
              $film->place_id = $request->input('place_id');
            }

            $film->status_id = $request->input('status_id');
            $film->title = $request->input('title');
            $film->location_id = $request->input('location_id');
            $film->slug = $utility->slugifyUrl($request->input('slug'));
            $film->kids = $request->input('kids');

            if ($request->input('festival') == 'on') {
              $film->festival = 1;
              $film->festival_id = $request->input('festival_id');
              $film->fest_cat_id = $request->input('festival_cat_id');
            }

            $film->start_date = $film->convertDate($request->input('start_date'));
            $film->end_date = $film->convertDate($request->input('end_date'));
            $film->region = $request->input('region');
            $film->production_date = $request->input('production_date');
            $film->duration = $request->input('duration');
            $film->description = $request->input('description');
            $film->save();

            if ($request->input('media-library') != null) {
              $media = Media::find($request->input('media-library'));
              $film->media()->save($media);
            } else {
              $utility->storeMedia($request->file('media'), $film);
            }

            if ($request->input('actors')) {
              foreach ($request->input('actors') as $key => $new_actor) {
                // Se non è presente lo salvo nella tabella actors
                $check = Actor::where('name', '=', $new_actor)->count();
                if ($check === 0) {
                  $actor = new Actor;
                  $actor->name = $new_actor;
                  $actor->save();
                }
                $actor = Actor::where('name', '=', $new_actor)->first();
                $film->actors()->attach($actor->id);
              }
            }

            if ($request->input('directors')) {
              foreach ($request->input('directors') as $key => $new_director) {
                // Se non è presente lo salvo nella tabella actors
                $check = Director::where('name', '=', $new_director)->count();
                if ($check === 0) {
                  $director = new Director;
                  $director->name = $new_director;
                  $director->save();
                }
                $director = Director::where('name', '=', $new_director)->first();
                $film->directors()->attach($director->id);
              }
            }

            if ($request->input('distributors')) {
              foreach ($request->input('distributors') as $key => $new_distributor) {
                // Se non è presente lo salvo nella tabella actors
                $check = Distributor::where('name', '=', $new_distributor)->count();
                if ($check === 0) {
                  $distributor = new Distributor;
                  $distributor->name = $new_distributor;
                  $distributor->save();
                }
                $distributor = Distributor::where('name', '=', $new_distributor)->first();
                $film->distributors()->attach($distributor->id);
              }
            }

            if ($request->input('editors')) {
              foreach ($request->input('editors') as $key => $new_editor) {
                // Se non è presente lo salvo nella tabella actors
                $check = Editor::where('name', '=', $new_editor)->count();
                if ($check === 0) {
                  $editor = new Editor;
                  $editor->name = $new_editor;
                  $editor->save();
                }
                $editor = Editor::where('name', '=', $new_editor)->first();
                $film->editors()->attach($editor->id);
              }
            }

            if ($request->input('photographers')) {
              foreach ($request->input('photographers') as $key => $new_photographer) {
                // Se non è presente lo salvo nella tabella actors
                $check = Photographer::where('name', '=', $new_photographer)->count();
                if ($check === 0) {
                  $photographer = new Photographer;
                  $photographer->name = $new_photographer;
                  $photographer->save();
                }
                $photographer = Photographer::where('name', '=', $new_photographer)->first();
                $film->photographers()->attach($photographer->id);
              }
            }

            if ($request->input('producers')) {
              foreach ($request->input('producers') as $key => $new_producer) {
                // Se non è presente lo salvo nella tabella actors
                $check = Producer::where('name', '=', $new_producer)->count();
                if ($check === 0) {
                  $producer = new Producer;
                  $producer->name = $new_producer;
                  $producer->save();
                }
                $producer = Producer::where('name', '=', $new_producer)->first();
                $film->producers()->attach($producer->id);
              }
            }

            if ($request->input('screenwriters')) {
              foreach ($request->input('screenwriters') as $key => $new_screenwriter) {
                // Se non è presente lo salvo nella tabella actors
                $check = Screenwriter::where('name', '=', $new_screenwriter)->count();
                if ($check === 0) {
                  $screenwriter = new Screenwriter;
                  $screenwriter->name = $new_screenwriter;
                  $screenwriter->save();
                }
                $screenwriter = Screenwriter::where('name', '=', $new_screenwriter)->first();
                $film->screenwriters()->attach($screenwriter->id);
              }
            }

            if ($request->input('film_categories')) {
              foreach ($request->input('film_categories') as $key => $new_category) {

                $film_category = FilmCategory::where('name', '=', $new_category)->first();
                $film->categories()->attach($film_category->id);
              }
            }

            session()->flash('success', 'Post created!');

            $url = 'admin/posts/film/'.$film->id.'/edit';

            return redirect($url);
          break;

        case 'festival';
            if ($request->input('media-library') != null) {
              $validator = Validator::make($request->all(), [
                'title' => 'required',
                'slug' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
              ]);
            } else {
              $validator = Validator::make($request->all(), [
                'title' => 'required',
                'slug' => 'required',
                'media' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
              ]);
            }

            if ($validator->fails()) {
              return redirect('admin/posts/festival/create')->withErrors($validator)->withInput();
            }

            $festival = new Festival;
            $festival->status_id = $request->input('status_id');
            $festival->title = $request->input('title');
            $festival->slug = $utility->slugifyUrl($request->input('slug'));
            $festival->kids = $request->input('kids');
            $festival->description = $request->input('description');
            $festival->start_date = $festival->convertDate($request->input('start_date'));
            $festival->end_date = $festival->convertDate($request->input('end_date'));
            $festival->save();

            if ($request->input('media-library') != null) {
              $media = Media::find($request->input('media-library'));
              $festival->media()->save($media);
            } else {
              $utility->storeMedia($request->file('media'), $festival);
            }

            session()->flash('success', 'Post created!');
            return redirect()->route('posts.index');
          break;

        case 'event':
            // verifico che il se ci sono posti limitati verifico in un modo altrimenti in un altro
            if ($request->input('media-library') != null) {
              if ($request->input('limitGuest') === 'on') {
                $required = [
                  'title' => 'required',
                  'slug' => 'required',
                  'date' => 'required',
                  'time' => 'required',
                  'guest' => 'required',
                  'description' => 'required',
                ];
              } else {
                $required = [
                  'title' => 'required',
                  'slug' => 'required',
                  'date' => 'required',
                  'time' => 'required',
                  'description' => 'required',
                ];
              }
            } else {
              if ($request->input('limitGuest') === 'on') {
                $required = [
                  'title' => 'required',
                  'slug' => 'required',
                  'date' => 'required',
                  'media' => 'required',
                  'time' => 'required',
                  'guest' => 'required',
                  'description' => 'required',
                ];
              } else {
                $required = [
                  'title' => 'required',
                  'slug' => 'required',
                  'media' => 'required',
                  'date' => 'required',
                  'time' => 'required',
                  'description' => 'required',
                ];
              }
            }

            $validator = Validator::make($request->all(), $required);

            if ($validator->fails()) {
              return redirect()->back()->withErrors($validator)->withInput();
            }

            $event = new Event;
            $event->status_id = $request->input('status_id');
            $event->title = $request->input('title');
            $event->slug = $utility->slugifyUrl($request->input('slug'));
            $event->location_id = $request->input('location_id');
            $event->event_type_id = $request->input('event_type');

            if ($request->input('festival') == 'on') {
              $event->festival = 1;
              $event->festival_id = $request->input('festival_id');
              $event->fest_cat_id = $request->input('festival_cat_id');
            }

            $event->kids = $request->input('kids');

            if ($request->input('film') != null) {
              $event->film_id = $request->input('film');
            }

            // Se deve esserci il limite di partecipanti allora predispongo l'evento
            if ($request->input('limitGuest') === 'on') {
              $event->limit_guest = 1;
              $event->guest_list = $request->input('guest');
              $event->guest_left = $request->input('guest');
              // se il campo è nullo lo salvo a 0
              if ($request->input('waiting') === null) {
                $event->wait_list = 0;
                $event->wait_left = 0;
              } else {
                $event->wait_list = $request->input('waiting');
                $event->wait_left = $request->input('waiting');
              }
              // Se è presente il campo bambini lo salvo
              if ($request->input('kidsList') === 'on') {
                $event->kids_list = 1;
              }

              // available_hidden
              if ($request->input('available_hidden') === 'on') {
                $event->available_hidden = 1;
              }

              // Single guest booking
              if ($request->input('single_guest') === 'on') {
                $event->single_guest = 1;
              }
            }

            $event->description = $request->input('description');

            $dateTime = $event->mergeDateTime($request->input('date'), $request->input('time'));
            $event->date_time = $dateTime;

            $event->save();

            if ($request->input('media-library') != null) {
              $media = Media::find($request->input('media-library'));
              $event->media()->save($media);
            } else {
              $utility->storeMedia($request->file('media'), $event);
            }

            session()->flash('success', 'Evento creato!');
            return redirect()->route('posts.index');
          break;

        case 'cinestore':
          if ($request->input('category_id') == 6) {
            $rules = [
              'product_name' => 'required',
              'slug' => 'required',
              'media' => 'required',
              'category_id' => 'required',
            ];
          } else if ($request->input('media-library') != null && $request->input('category_id') !== 6) {
            $rules = [
              'product_name' => 'required',
              'slug' => 'required',
              'category_id' => 'required',
            ];
          } else {
            $rules = [
              'product_name' => 'required',
              'slug' => 'required',
              'media' => 'required',
              'category_id' => 'required',
            ];
          }

          $validator = Validator::make($request->all(), $rules);

          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
          }

          // Se non è un percorso
          if ($request->input('category_id') != 6) {
              $product = new Product;
              $product->status_id = $request->input('status_id');
              $product->available = $request->input('available');

              if ($request->purchasable == 'on') {
                $product->purchasable = 1;
              }

              $product->name = $request->input('product_name');
              $product->slug = $utility->slugifyUrl($request->input('slug'));
              $product->description = $request->input('description');
              // $product->price = $request->input('price');
              $product->product_category_id = $request->input('category_id');
              $product->save();

              if ($request->product_prices) {
                foreach ($request->product_prices as $key => $product_price) {
                  $product->product_prices()->attach($product_price);
                }
              }

              if ($request->input('tags')) {
                foreach ($request->input('tags') as $key => $tags) {
                  // Se non è presente lo salvo nella tabella actors
                  $check = Tag::where('name', '=', $tags)->count();
                  if ($check === 0) {
                    $tag = new Tag;
                    $tag->name = $tags;
                    $tag->save();
                  }
                  $tag = Tag::where('name', '=', $tags)->first();
                  $product->tags()->attach($tag->id);
                }
              }

              if ($request->input('category_id') == 4 && $request->pics_collection != null) {
                foreach ($request->pics_collection as $key => $pic_collect) {
                  // Se non è presente lo salvo nella tabella collections
                  $check = PicCollection::where('name', '=', $pic_collect)->count();
                  if ($check === 0) {
                    $lower = strtolower($pic_collect);
                    $str = $utility->slugifyUrl($lower);
                    $pic_collection = new PicCollection();
                    $pic_collection->name = $pic_collect;
                    $pic_collection->description = '';
                    $pic_collection->slug = $str;
                    $pic_collection->save();
                  }
                  $pic_collection = PicCollection::where('name', '=', $pic_collect)->first();
                  $product->picCollections()->attach($pic_collection->id);
                }
              }

              // Salvo l'immagine
              if ($request->input('media-library') != null) {
                $media = Media::find($request->input('media-library'));
                $product->media()->save($media);
              } else {
                $utility->storeMedia($request->file('media'), $product);
                // Se è una foto aggiungo il watermark
                if ($request->input('category_id') == 4) {
                  $utility->addWatermark($product);
                }
              }

              // Se c'è una galleria prodotto
              if ($request->input('media_list')) {
                // estraggo gli id
                $mediaIds = explode(',', $request->input('media_list'));
                // Rimuovo i nulli
                $mediaIds = array_filter($mediaIds);

                // Se non ci sono gallerie collegate a questo prodotto ne creo una
                if ($product->gallery()->count() == 0) {
                  // Creo la galleria
                  $gallery = new Gallery;
                  $gallery->title = $product->name;
                  $gallery->save();
                  // Sincronizzo i media collegati alla nuova galleria
                  $gallery->medias()->sync($mediaIds);

                  // collego la galleria al prodotto
                  $product->gallery()->save($gallery);

                } else {
                  // Recupero la galleria e la salvo con le modifiche
                  $gallery = $product->gallery()->first();
                  $gallery->title = $product->name;
                  $gallery->save();

                  // Sincronizzo i media collegati alla galleria
                  $gallery->medias()->sync($mediaIds);

                  // prima scollego le gallerie collegate a quel prodotto e poi le aggiorno con la nuova
                  $product->gallery()->detach();
                  $product->gallery()->save($gallery);

                }
              }

              // Salvo il video
              if ($request['video']) {
                $video = new Video;
                $video->url = $request['video'];
                $video->save();

                $product->video()->save($video);
              }
          } else {
            // Percorsi
            $products = explode(',', $request->input('product_list'));

            $product_group = new ProductGroup;
            $product_group->status_id = $request->input('status_id');
            $product_group->name = $request->input('product_name');
            $product_group->slug = $utility->slugifyUrl($request->input('slug'));
            $product_group->description = $request->input('description');
            $product_group->save();

            if ($request->input('tags')) {
              foreach ($request->input('tags') as $key => $tags) {
                // Se non è presente lo salvo nella tabella actors
                $check = Tag::where('name', '=', $tags)->count();
                if ($check === 0) {
                  $tag = new Tag;
                  $tag->name = $tags;
                  $tag->save();
                }
                $tag = Tag::where('name', '=', $tags)->first();
                $product_group->tags()->attach($tag->id);
              }
            }

            if ($request->input('media-library') != null) {
              $media = Media::find($request->input('media-library'));
              $product_group->media()->save($media);
            } else {
              $utility->storeMedia($request->file('media'), $product_group);
            }

            // Aggiungo il link con i prodotti
            foreach ($products as $key => $product_id) {
              $product = Product::find($product_id);
              $product_group->products()->attach($product);
            }

            // Se c'è una galleria prodotto
            if ($request->input('media_list')) {
              // estraggo gli id
              $mediaIds = explode(',', $request->input('media_list'));
              // Rimuovo i nulli
              $mediaIds = array_filter($mediaIds);

              // Se non ci sono gallerie collegate a questo prodotto ne creo una
              if ($product_group->gallery()->count() == 0) {
                // Creo la galleria
                $gallery = new Gallery;
                $gallery->title = $product_group->name;
                $gallery->save();
                // Sincronizzo i media collegati alla nuova galleria
                $gallery->medias()->sync($mediaIds);

                // collego la galleria al prodotto
                $product_group->gallery()->save($gallery);

              } else {
                // Recupero la galleria e la salvo con le modifiche
                $gallery = $product_group->gallery()->first();
                $gallery->title = $product_group->name;
                $gallery->save();

                // Sincronizzo i media collegati alla galleria
                $gallery->medias()->sync($mediaIds);

                // prima scollego le gallerie collegate a quel prodotto e poi le aggiorno con la nuova
                $product_group->gallery()->detach();
                $product_group->gallery()->save($gallery);
              }
            }
          }



          session()->flash('success', 'Prodotto creato!');
          return redirect()->route('posts.index');
          break;

        case 'release':
          $required = [
            'title' => 'required',
            'slug' => 'required',
          ];

          $validator = Validator::make($request->all(), $required);

          if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
          }

          $release = new Release;
          $release->status_id = $request->input('status_id');
          $release->title = $request->input('title');
          $release->slug = $utility->slugifyUrl($request->input('slug'));
          $release->date_time = $release->convertDate($request->input('date'));;
          $release->description = $request->input('description');
          $release->save();

          // Verifico le testate giornalistiche
          if ($request->input('presses')) {
            foreach ($request->input('presses') as $key => $new_press) {
              // Se non è presente lo salvo nella tabella actors
              $check = Press::where('name', '=', $new_press)->count();
              if ($check === 0) {
                $press = new Press;
                $press->name = $new_press;
                $press->save();
              }

              $press = Press::where('name', '=', $new_press)->first();
              $release->presses()->attach($press->id);

            }
          }

          // Salvo l'immagine
          if ($request->input('media-library') != null) {
            // ho scelto un'immagine dalla libreria
            $media = Media::find($request->input('media-library'));
            $release->media()->save($media);
          } elseif ($request->file('media')) {
            // upload di una nuova immagine
            $utility->storeMedia($request->file('media'), $release);
          }

          // Salvo il video
          if ($request->input('video')) {
            $video = new Video;
            $video->url = $request->input('video');
            $video->save();
            $release->video()->save($video);
          }

          session()->flash('success', 'Rassegna stampa creata!');
          return redirect()->route('posts.index');
          break;

        default:
            session()->flash('success', 'Post created!');
            return redirect()->route('posts.index');
          break;
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $post->media_url = $post->printImageConvertedUrl($id, 'images', 'rectangle');
        return view('admin.posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(string $post_type, $id)
    {
      $admins = Admin::all();
      $statuses = PostStatus::all();

      switch ($post_type) {
        case 'news':
            $news = Post::find($id);
            $categories = Category::all();
            $locations = Location::where('type_id', '=', 5)->get();
            return view('admin.posts.news.edit', compact('statuses', 'categories', 'news', 'locations'));
          break;

        case 'exhibit':
            $exhibit = Exhibit::where('id', '=', $id)->with('films', 'exhibitions')->first();
            $locations = Location::where('type_id', '=', '3')->get();
            $exhibitions = Exhibition::all();
            $films = Film::all();
            $festivals = Festival::all();

            $items = collect();

            foreach ($exhibit->films()->get() as $key => $film) {
              $item = collect();
              $item->id = $film->id;
              $item->title = $film->title;
              $item->type = 'film';
              $items->push($item);
            }

            foreach ($exhibit->exhibitions()->get() as $key => $exhibition) {
              $item = collect();
              $item->id = $exhibition->id;
              $item->title = $exhibition->title;
              $item->type = 'rassegna';
              $items->push($item);
            }

            if ($exhibit->festival == 1) {
              $fest_cats = FestivalCategory::where('festival_id', '=', $exhibit->festival_id)->get();
            }

            return view('admin.posts.exhibit.edit', compact('exhibit', 'locations', 'films', 'statuses', 'exhibitions', 'items', 'festivals', 'fest_cats'));
          break;

        case 'exhibition':
            $exhibition = Exhibition::find($id);
            $locations = Location::where('type_id', '=', '2')->get();
            $films = Film::all();
            $events = Event::all();
            $festivals = Festival::all();

            $contents = collect();

            foreach ($exhibition->event as $key => $event) {
              $data = [
                'id' => $event->id,
                'title' => $event->title,
                'type' => 'Evento'
              ];
              $contents->push($data);
            }

            foreach ($exhibition->films as $key => $film) {
              $data = [
                'id' => $film->id,
                'title' => $film->title,
                'type' => 'Film'
              ];
              $contents->push($data);
            }

            if ($exhibition->festival == 1) {
              $fest_cats = FestivalCategory::where('festival_id', '=', $exhibition->festival_id)->get();
            }

            return view('admin.posts.exhibition.edit', compact('exhibition', 'locations', 'films', 'statuses', 'events', 'contents', 'festivals', 'fest_cats'));
          break;

        case 'film';
            $film = Film::find($id);
            $locations = FilmLocation::with('places')->get();
            $directors = Director::all();
            $actors = Actor::all();
            $distributors = Distributor::all();
            $editors = Editor::all();
            $photographers = Photographer::all();
            $producers = Producer::all();
            $screenwriters = Screenwriter::all();
            $film_categories = FilmCategory::all();
            $shows = FilmSchedule::where('film_id', '=', $id)->orderBy('date_time')->get();
            $festivals = Festival::all();

            foreach ($shows as $key => $show) {
              $string = strtotime($show->date_time);

              $time = date('H:i', $string);
              $show->time = $time;

              $day = date('d, F', $string);
              $show->day = $day;
            }

            if ($film->festival == 1) {
              $fest_cats = FestivalCategory::where('festival_id', '=', $film->festival_id)->get();
            }

            return view('admin.posts.film.edit', compact('film', 'shows', 'statuses', 'locations', 'actors', 'directors', 'distributors', 'editors', 'photographers', 'producers', 'screenwriters', 'film_categories', 'festivals', 'fest_cats'));
          break;

        case 'festival';
              $festival = Festival::find($id);
              $films = Film::all();
              $exhibits = Exhibit::all();
              $exhibitions = Exhibition::all();
              $festivalRelation = FestivalRelation::where('festival_id', '=', $id)->get();
              $events = Event::all();
              $relations = collect();
              foreach ($festivalRelation as $key => $relation) {
                if ($relation->film_id != null) {
                  $film = Film::find($relation->film_id);
                  $item = [
                    'id' => $relation->id,
                    'title' => $film->title,
                    'type' => 'Film'
                  ];
                  $relations->push($item);
                }

                if ($relation->exhibit_id != null) {
                  $exhibit = Exhibit::find($relation->exhibit_id);
                  $item = [
                    'id' => $relation->id,
                    'title' => $exhibit->title,
                    'type' => 'Esposizione'
                  ];
                  $relations->push($item);
                }

                if ($relation->exhibition_id != null) {
                  $exhibition = Exhibition::find($relation->exhibition_id);
                  $item = [
                    'id' => $relation->id,
                    'title' => $exhibition->title,
                    'type' => 'Rassegna'
                  ];
                  $relations->push($item);
                }

              }
            return view('admin.posts.festival.edit', compact('festival', 'statuses', 'films', 'exhibits', 'exhibitions', 'relations', 'events'));
          break;

        case 'event';
            $event_types = EventType::all();
            $event = Event::where('id', '=', $id)->with('film')->first();
            $locations = Location::where('type_id', '=', '4')->get();
            $string = strtotime($event->date_time);
            $event->time = date('H:i', $string);
            $event->date = date('m/d/Y', $string);
            $films = Film::all();
            $festivals = Festival::all();
            $messages = RsvpMessage::where('event_id', '=', $id)->get();
            $message_guest = $messages->first(function ($value, $key) {
                return $value->type == 1;
            });
            $message_wait = $messages->first(function ($value, $key){
                return $value->type == 2;
            });

            if ($event->festival == 1) {
              $fest_cats = FestivalCategory::where('festival_id', '=', $event->festival_id)->get();
            }

            return view('admin.posts.event.edit', compact('event', 'statuses', 'event_types', 'locations', 'films', 'message_guest', 'message_wait', 'festivals', 'fest_cats'));
          break;

        case 'cinestore';
            $product = Product::find($id);
            $products = Product::all();
            $product_categories = ProductCategory::all();
            $tags = Tag::all();
            $pic_collects = PicCollection::all();
            $prodIds = collect();
            $mediaIds = null;
            $gallery = $product->gallery()->first();
            $product_prices = ProductPrice::all();

            if ($gallery != null) {
              foreach ($gallery->medias()->get() as $key => $media) {
                $mediaIds = $mediaIds.','.$media->id;
              }
            } else {
              $mediaIds = '';
            }

            // $product_prices_ids = $product->product_prices()->get()->pluck('id')->all();

            return view('admin.posts.product.edit', compact('product', 'statuses', 'product_categories', 'products', 'prodIds', 'mediaIds', 'tags', 'product_prices', 'pic_collects'));
          break;

        case 'percorsi':
            $product = ProductGroup::where('id', '=', $id)->with('products', 'products.category')->first();
            $product->product_category_id = 6;
            $product_categories = ProductCategory::all();
            $products = Product::all();
            $tags = Tag::all();

            $prodIds = collect();
            foreach ($product->products()->get() as $key => $prod) {
              $prodIds->push($prod->id);
            }
            $prodIds = implode(',', $prodIds->toArray());

            $mediaIds = null;
            $gallery = $product->gallery()->first();

            if ($gallery != null) {
              foreach ($gallery->medias()->get() as $key => $media) {
                $mediaIds = $mediaIds.','.$media->id;
              }
            } else {
              $mediaIds = '';
            }

            return view('admin.posts.product.edit', compact('product', 'statuses', 'product_categories', 'products', 'prodIds', 'mediaIds', 'tags'));
          break;

        case 'release':

            $release = Release::find($id);
            $presses = Press::all();

            return view('admin.posts.release.edit', compact('release', 'statuses', 'presses'));
          break;

        default:
            return view();
          break;
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(string $post_type, Request $request, $id)
    {
      $utility = new Utility;

      switch ($post_type) {
        case 'news':
              $validator = Validator::make($request->all(), [
                'title' => 'required',
                'slug' => 'required',
              ]);

              if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
              }

              $news = Post::find($id);

              if ($request->input('media-library') != null) {
                $media = Media::find($request->input('media-library'));
                $news->media()->detach();
                $news->media()->save($media);
              }

              if ($request->file('media')) {
                $utility->storeMedia($request->file('media'), $news);
              }

              $news->title = $request->input('title');
              $news->slug = $utility->slugifyUrl($request->input('slug'));
              $news->content = $request->input('content');
              $news->in_home = $request->input('in_home');
              $news->status_id = $request->input('status_id');
              $news->category_id = $request->input('category_id');

              if ($request->location_id) {
                $news->location_id = $request->location_id;
              } else {
                $news->location_id = 0;
              }

              $news->save();

            session()->flash('success', 'News modificata!');
            return redirect()->route('posts.index');
          break;

        case 'exhibit':
            $validator = Validator::make($request->all(), [
              'title' => 'required',
              'slug' => 'required',
              'location_id' => 'required',
              'start_date' => 'required',
              'end_date' => 'required',
            ]);

            if ($validator->fails()) {
              return redirect()->back()->withErrors($validator)->withInput();
            }

            $exhibit = Exhibit::find($id);

            if ($request->input('media-library') != null) {
              $media = Media::find($request->input('media-library'));
              $exhibit->media()->detach();
              $exhibit->media()->save($media);
            }

            if ($request->file('media')) {
              $utility->storeMedia($request->file('media'), $exhibit);
            }

            $exhibit->status_id = $request->input('status_id');
            $exhibit->title = $request->input('title');
            $exhibit->slug = $utility->slugifyUrl($request->input('slug'));
            $exhibit->location_id = $request->input('location_id');
            $exhibit->kids = $request->input('kids');

            if ($request->input('festival') == 'on') {
              $exhibit->festival = 1;
              $exhibit->festival_id = $request->input('festival_id');
              $exhibit->fest_cat_id = $request->input('festival_cat_id');
            } else {
              $exhibit->festival = 0;
              $exhibit->festival_id = null;
              $exhibit->fest_cat_id = null;
            }

            $exhibit->start_date = $exhibit->convertDate($request->input('start_date'));
            $exhibit->end_date = $exhibit->convertDate($request->input('end_date'));
            $exhibit->description = $request->input('description');
            $exhibit->save();


            session()->flash('success', 'Esposizione modificata!');
            return redirect()->route('posts.index');
          break;

        case 'exhibition':
            $validator = Validator::make($request->all(), [
              'title' => 'required',
              'slug' => 'required',
              'location_id' => 'required',
              'start_date' => 'required',
              'end_date' => 'required',
            ]);

            if ($validator->fails()) {
              return redirect()->back()->withErrors($validator)->withInput();
            }

            $exhibition = Exhibition::find($id);

            if ($request->input('media-library') != null) {
              $media = Media::find($request->input('media-library'));
              $exhibition->media()->detach();
              $exhibition->media()->save($media);
            }

            if ($request->file('media')) {
              $utility->storeMedia($request->file('media'), $exhibition);
            }

            $exhibition->status_id = $request->input('status_id');
            $exhibition->title = $request->input('title');
            $exhibition->slug = $utility->slugifyUrl($request->input('slug'));
            $exhibition->location_id = $request->input('location_id');
            $exhibition->kids = $request->input('kids');

            if ($request->input('festival') == 'on') {
              $exhibition->festival = 1;
              $exhibition->festival_id = $request->input('festival_id');
              $exhibition->fest_cat_id = $request->input('festival_cat_id');
            } else {
              $exhibition->festival = 0;
              $exhibition->festival_id = null;
              $exhibition->fest_cat_id = null;
            }

            $exhibition->start_date = $exhibition->convertDate($request->input('start_date'));
            $exhibition->end_date = $exhibition->convertDate($request->input('end_date'));
            $exhibition->description = $request->input('description');
            $exhibition->save();


            session()->flash('success', 'Rassegna modificata!');
            return redirect()->route('posts.index');
          break;

        case 'film':
            $this->validate($request, array(
              'title' => 'required',
              'slug' => 'required',
              'location_id' => 'required',
              'start_date' => 'required',
              'end_date' => 'required',
            ));

            $film = Film::find($id);

            if ($request->input('media-library') != null) {
              $media = Media::find($request->input('media-library'));
              $film->media()->detach();
              $film->media()->save($media);
            }

            if ($request->file('media')) {
              $utility->storeMedia($request->file('media'), $film);
            }

            if ($request->input('place_id') != null) {
              $film->place_id = $request->input('place_id');
            }

            $film->status_id = $request->input('status_id');
            $film->title = $request->input('title');
            $film->location_id = $request->input('location_id');
            $film->slug = $utility->slugifyUrl($request->input('slug'));
            $film->kids = $request->input('kids');

            if ($request->input('festival') == 'on') {
              $film->festival = 1;
              $film->festival_id = $request->input('festival_id');
              $film->fest_cat_id = $request->input('festival_cat_id');
            } else {
              $film->festival = 0;
              $film->festival_id = null;
              $film->fest_cat_id = null;
            }

            $film->start_date = $film->convertDate($request->input('start_date'));
            $film->end_date = $film->convertDate($request->input('end_date'));
            $film->region = $request->input('region');
            $film->production_date = $request->input('production_date');
            $film->duration = $request->input('duration');
            $film->description = $request->input('description');
            $film->save();

            if ($request->input('actors')) {
              foreach ($request->input('actors') as $key => $new_actor) {

                // Se non è presente lo salvo nella tabella actors
                $check = Actor::where('name', '=', $new_actor)->count();
                if ($check === 0) {
                  $actor = new Actor;
                  $actor->name = $new_actor;
                  $actor->save();
                }

                // se la relazione non esiste la crea
                if ($film->hasActor($new_actor) != 1) {
                  $actor = Actor::where('name', '=', $new_actor)->first();
                  $film->actors()->attach($actor->id);
                }

                // se una relazione è stata eliminata
                $actors = $film->actors()->get();
                $new_actors = $request->input('actors');

                foreach ($actors as $key => $actor) {
                  $check = in_array($actor->name, $new_actors);
                  if ($check === false) {
                    $film->actors()->detach($actor->id);
                  }
                }

              }
            }

            if ($request->input('directors')) {

              foreach ($request->input('directors') as $key => $new_director) {
                // Se non è presente lo salvo nella tabella directors
                $check = Director::where('name', '=', $new_director)->count();
                if ($check === 0) {
                  $director = new Director;
                  $director->name = $new_director;
                  $director->save();
                }

                // se la relazione non esiste la crea
                if ($film->hasDirector($new_director) != 1) {
                  $director = Director::where('name', '=', $new_director)->first();
                  $film->directors()->attach($director->id);
                }

                // se una relazione è stata eliminata
                $directors = $film->directors()->get();
                $new_directors = $request->input('directors');

                foreach ($directors as $key => $director) {
                  $check = in_array($director->name, $new_directors);
                  if ($check === false) {
                    $film->directors()->detach($director->id);
                  }
                }

              }
            }

            if ($request->input('distributors')) {
              $film->distributors()->detach();
              foreach ($request->input('distributors') as $key => $new_distributor) {
                // Se non è presente lo salvo nella tabella actors
                $check = Distributor::where('name', '=', $new_distributor)->count();
                if ($check === 0) {
                  $distributor = new Distributor;
                  $distributor->name = $new_distributor;
                  $distributor->save();
                } else {
                  $distributor = Distributor::where('name', '=', $new_distributor)->first();
                }
                $film->distributors()->attach($distributor->id);
              }
            }

            if ($request->input('editors')) {
              $film->editors()->detach();
              foreach ($request->input('editors') as $key => $new_editor) {
                // Se non è presente lo salvo nella tabella actors
                $check = Editor::where('name', '=', $new_editor)->count();
                if ($check === 0) {
                  $editor = new Editor;
                  $editor->name = $new_editor;
                  $editor->save();
                } else {
                  $editor = Editor::where('name', '=', $new_editor)->first();
                }
                $film->editors()->attach($editor->id);
              }
            }

            if ($request->input('photographers')) {
              $film->photographers()->detach();
              foreach ($request->input('photographers') as $key => $new_photographer) {
                // Se non è presente lo salvo nella tabella actors
                $check = Photographer::where('name', '=', $new_photographer)->count();
                if ($check === 0) {
                  $photographer = new Photographer;
                  $photographer->name = $new_photographer;
                  $photographer->save();
                } else {
                  $photographer = Photographer::where('name', '=', $new_photographer)->first();
                }
                $film->photographers()->attach($photographer->id);
              }
            }

            if ($request->input('producers')) {
              $film->producers()->detach();
              foreach ($request->input('producers') as $key => $new_producer) {
                // Se non è presente lo salvo nella tabella actors
                $check = Producer::where('name', '=', $new_producer)->count();
                if ($check === 0) {
                  $producer = new Producer;
                  $producer->name = $new_producer;
                  $producer->save();
                } else {
                  $producer = Producer::where('name', '=', $new_producer)->first();
                }
                $film->producers()->attach($producer->id);
              }
            }

            if ($request->input('screenwriters')) {
              $film->screenwriters()->detach();
              foreach ($request->input('screenwriters') as $key => $new_screenwriter) {
                // Se non è presente lo salvo nella tabella actors
                $check = Screenwriter::where('name', '=', $new_screenwriter)->count();
                if ($check === 0) {
                  $screenwriter = new Screenwriter;
                  $screenwriter->name = $new_screenwriter;
                  $screenwriter->save();
                } else {
                  $screenwriter = Screenwriter::where('name', '=', $new_screenwriter)->first();
                }
                $film->screenwriters()->attach($screenwriter->id);
              }
            }


            if ($request->input('film_categories')) {

              foreach ($request->input('film_categories') as $key => $new_category) {
                if ($film->hasCategory($new_category) != 1) {
                  $film_category = FilmCategory::where('name', '=', $new_category)->first();
                  $film->categories()->attach($film_category->id);
                }

                $film_categories = $film->categories()->get();
                $new_categories = $request->input('film_categories');

                foreach ($film_categories as $key => $film_category) {
                  $check = in_array($film_category->name, $new_categories);
                  if ($check === false) {
                    $film->categories()->detach($film_category->id);
                  }

                }

              }

            }

            session()->flash('success', 'Film modificato!');
            return redirect()->route('posts.index');
          break;

        case 'festival':
            $validator = Validator::make($request->all(), [
              'title' => 'required',
              'slug' => 'required',
              'status_id' => 'required',
              'start_date' => 'required',
              'end_date' => 'required',
            ]);

            if ($validator->fails()) {
              return redirect()->back()->withErrors($validator)->withInput();
            }

            $festival = Festival::find($id);

            if ($request->input('media-library') != null) {
              $media = Media::find($request->input('media-library'));
              $festival->media()->detach();
              $festival->media()->save($media);
            }

            if ($request->file('media')) {
              $utility->storeMedia($request->file('media'), $festival);
            }

            $festival->title = $request->input('title');
            $festival->status_id = $request->input('status_id');
            $festival->slug = $utility->slugifyUrl($request->input('slug'));
            $festival->kids = $request->input('kids');
            $festival->start_date = $festival->convertDate($request->input('start_date'));
            $festival->end_date = $festival->convertDate($request->input('end_date'));
            $festival->title = $request->input('title');
            $festival->save();

            session()->flash('success', 'Festival modificato!');
            return redirect()->route('posts.index');
          break;

        case 'event':

            if ($request->input('limitGuest') === 'on') {
              $validator = Validator::make($request->all(), [
                'title' => 'required',
                'slug' => 'required',
                'date' => 'required',
                'time' => 'required',
                'guest' => 'required',
              ]);

              if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
              }
            } else {
              $validator = Validator::make($request->all(), [
                'title' => 'required',
                'slug' => 'required',
                'date' => 'required',
                'time' => 'required',
              ]);

              if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
              }
            }

            $event = Event::find($id);

            if ($request->input('media-library') != null) {
              $media = Media::find($request->input('media-library'));
              $event->media()->detach();
              $event->media()->save($media);
            }

            if ($request->file('media')) {
              $utility->storeMedia($request->file('media'), $event);
            }

            $event->status_id = $request->input('status_id');
            $event->title = $request->input('title');
            $event->slug = $utility->slugifyUrl($request->input('slug'));
            $event->location_id = $request->input('location_id');
            $event->event_type_id = $request->input('event_type');
            $event->kids = $request->input('kids');

            if ($request->input('festival') == 'on') {
              $event->festival = 1;
              $event->festival_id = $request->input('festival_id');
              $event->fest_cat_id = $request->input('festival_cat_id');
            } else {
              $event->festival = 0;
              $event->festival_id = null;
              $event->fest_cat_id = null;
            }

            if ($request->input('film') != null) {
              $event->film_id = $request->input('film');
            }

            // Se prima non era attivo, devo resettare il numero dei posti disponibili
            if ($event->limit_guest == 0) {
              $reset = true;
            } else {
              $reset = false;
            }

            // Se deve esserci il limite di partecipanti allora predispongo l'evento
            if ($request->input('limitGuest') === 'on') {

              // Imposto true il campo per gli eventi con limite di partecipanti
              $event->limit_guest = 1;

              // se il campo degli ospiti totali cambia
              if ($request->input('guest') != $event->guest_list) {
                // prendo la differenza
                $difference = $request->input('guest') - $event->guest_list;
                $event->guest_left = ($event->guest_left + $difference);
              }

              $event->guest_list = $request->input('guest');

              // se reset è true, resetto il numero di posti disponibili
              if ($reset == true) {
                $event->guest_left = $request->input('guest');
              }

              // se il campo è nullo lo salvo a 0
              if ($request->input('waiting') === null) {
                $event->wait_list = 0;
                // se reset è true, resetto il numero di posti disponibili
                if ($reset == true) {
                  $event->wait_left = 0;
                }
              } else {
                $event->wait_list = $request->input('waiting');
                // se reset è true, resetto il numero di posti disponibili
                if ($reset == true) {
                  $event->wait_left = $request->input('waiting');
                }

                // se il campo degli ospiti totali cambia
                if ($request->input('waiting') != $event->wait_list) {
                  // prendo la differenza
                  $difference = $request->input('waiting') - $event->wait_list;
                  $event->wait_left = ($event->wait_left + $difference);
                }
              }

              // Se è presente il campo bambini lo salvo
              if ($request->input('kidsList') === 'on') {
                $event->kids_list = 1;
              } else {
                $event->kids_list = 0;
              }

              // available_hidden
              if ($request->input('available_hidden') === 'on') {
                $event->available_hidden = 1;
              }

              // Single guest booking
              if ($request->input('single_guest') === 'on') {
                $event->single_guest = 1;
              }

            } else {
              // Se modificando tolgo la spunta su limita partecipanti salvo la modifica
              $event->limit_guest = 0;
            }

            $event->description = $request->input('description');

            $dateTime = $event->mergeDateTime($request->input('date'), $request->input('time'));
            $event->date_time = $dateTime;

            $event->save();

            session()->flash('success', 'Evento modificato!');
            return redirect()->route('posts.index');
          break;

        case 'cinestore':
            $validator = Validator::make($request->all(), [
              'product_name' => 'required',
              'slug' => 'required',
              'category_id' => 'required',
            ]);

            if ($validator->fails()) {
              return redirect()->back()->withErrors($validator)->withInput();
            }

            $product = Product::find($id);
            $product->status_id = $request->input('status_id');
            $product->available = $request->input('available');

            if ($request->purchasable == 'on') {
              $product->purchasable = 1;
            } else {
              $product->purchasable = 0;
            }

            $product->name = $request->input('product_name');
            $product->slug = $utility->slugifyUrl($request->input('slug'));
            $product->description = $request->input('description');
            // $product->price = $request->input('price');
            $product->product_category_id = $request->input('category_id');
            $product->save();

            if ($request->product_prices) {
              $product->product_prices()->detach();
              foreach ($request->product_prices as $key => $product_price) {
                $product->product_prices()->attach($product_price);
              }
            }

            if ($request->file('media')) {
              $utility->storeMedia($request->file('media'), $product);
              // Se è una foto aggiungo il watermark
              if ($request->input('category_id') == 4) {
                $utility->addWatermark($product);
              }
            }

            if ($request->input('tags')) {
              $product->tags()->detach();
              foreach ($request->input('tags') as $key => $new_tag) {
                // Se non è presente lo salvo nella tabella actors
                $check = Tag::where('name', '=', $new_tag)->count();
                if ($check === 0) {
                  $tag = new Tag;
                  $tag->name = $new_tag;
                  $tag->save();
                } else {
                  $tag = Tag::where('name', '=', $new_tag)->first();
                }
                $product->tags()->attach($tag->id);
              }
            }

            if ($request->input('category_id') == 4) {
              $product->picCollections()->detach();
              if ($request->pics_collection != null) {
                foreach ($request->pics_collection as $key => $pic_collect) {
                  // Se non è presente lo salvo nella tabella collections
                  $check = PicCollection::where('name', '=', $pic_collect)->count();
                  if ($check === 0) {
                    $lower = strtolower($pic_collect);
                    $str = $utility->slugifyUrl($lower);
                    $pic_collection = new PicCollection();
                    $pic_collection->name = $pic_collect;
                    $pic_collection->description = '';
                    $pic_collection->slug = $str;
                    $pic_collection->save();
                  }
                  $pic_collection = PicCollection::where('name', '=', $pic_collect)->first();
                  $product->picCollections()->attach($pic_collection->id);
                }
              }
            }

            // Se c'è una galleria prodotto
            if ($request->input('media_list')) {
              // estraggo gli id
              $mediaIds = explode(',', $request->input('media_list'));
              // Rimuovo i nulli
              $mediaIds = array_filter($mediaIds);

              // Se non ci sono gallerie collegate a questo prodotto ne creo una
              if ($product->gallery()->count() == 0) {
                // Creo la galleria
                $gallery = new Gallery;
                $gallery->title = $product->name;
                $gallery->save();
                // Sincronizzo i media collegati alla nuova galleria
                $gallery->medias()->sync($mediaIds);

                // collego la galleria al prodotto
                $product->gallery()->save($gallery);

              } else {
                // Recupero la galleria e la salvo con le modifiche
                $gallery = $product->gallery()->first();
                $gallery->title = $product->name;
                $gallery->save();

                // Sincronizzo i media collegati alla galleria
                $gallery->medias()->sync($mediaIds);

                // prima scollego le gallerie collegate a quel prodotto e poi le aggiorno con la nuova
                $product->gallery()->detach();
                $product->gallery()->save($gallery);

              }
            }

            session()->flash('success', 'Prodotto modificato!');
            return redirect()->route('posts.index');

          break;

        case 'percorsi':
            $validator = Validator::make($request->all(), [
              'product_name' => 'required',
              'slug' => 'required',
              // 'category_id' => 'required',
            ]);

            if ($validator->fails()) {
              return redirect()->back()->withErrors($validator)->withInput();
            }

            $products_group = ProductGroup::find($id);

            if ($request->input('media-library') != null) {
              $media = Media::find($request->input('media-library'));
              $products_group->media()->detach();
              $products_group->media()->save($media);
            }

            if ($request->file('media')) {
              $utility->storeMedia($request->file('media'), $products_group);
            }

            $products_group->status_id = $request->input('status_id');
            $products_group->name = $request->input('product_name');
            $products_group->slug = $utility->slugifyUrl($request->input('slug'));
            $products_group->description = $request->input('description');
            $products_group->save();

            $products_group->products()->detach();

            $products = explode(',', $request->input('product_list'));

            // Aggiungo il link con i prodotti
            foreach ($products as $key => $product_id) {
              $product = Product::find($product_id);
              $products_group->products()->attach($product);
            }

            if ($request->input('tags')) {
              $product_group->tags()->detach();
              foreach ($request->input('tags') as $key => $new_tag) {
                // Se non è presente lo salvo nella tabella actors
                $check = Tag::where('name', '=', $new_tag)->count();
                if ($check === 0) {
                  $tag = new Tag;
                  $tag->name = $new_tag;
                  $tag->save();
                } else {
                  $tag = Tag::where('name', '=', $new_tag)->first();
                }
                $product_group->tags()->attach($distributor->id);
              }
            }

            // Se c'è una galleria prodotto
            if ($request->input('media_list')) {
              // estraggo gli id
              $mediaIds = explode(',', $request->input('media_list'));
              // Rimuovo i nulli
              $mediaIds = array_filter($mediaIds);
              // Se non ci sono gallerie collegate a questo prodotto ne creo una
              if ($products_group->gallery()->count() == 0) {
                // Creo la galleria
                $gallery = new Gallery;
                $gallery->title = $products_group->name;
                $gallery->save();
                // Sincronizzo i media collegati alla nuova galleria
                $gallery->medias()->sync($mediaIds);

                // collego la galleria al prodotto
                $products_group->gallery()->save($gallery);
              } else {
                // Recupero la galleria e la salvo con le modifiche
                $gallery = $products_group->gallery()->first();
                $gallery->title = $products_group->name;
                $gallery->save();

                // Sincronizzo i media collegati alla galleria
                $gallery->medias()->sync($mediaIds);

                // prima scollego le gallerie collegate a quel prodotto e poi le aggiorno con la nuova
                $products_group->gallery()->detach();
                $products_group->gallery()->save($gallery);
              }
            }


            session()->flash('success', 'Prodotto modificato!');
            return redirect()->route('posts.index');

          break;

        case 'release':
            $required = [
              'title' => 'required',
              'slug' => 'required',
            ];

            $validator = Validator::make($request->all(), $required);

            if ($validator->fails()) {
              return redirect()->back()->withErrors($validator)->withInput();
            }

            $release = Release::find($id);
            $release->status_id = $request->input('status_id');
            $release->title = $request->input('title');
            $release->slug = $utility->slugifyUrl($request->input('slug'));
            $release->date_time = $release->convertDate($request->input('date'));;
            $release->description = $request->input('description');
            $release->save();

            // Verifico le testate giornalistiche
            if ($request->input('presses')) {
              foreach ($request->input('presses') as $key => $new_press) {

                // Se non è presente lo salvo nella tabella presses
                $check = Press::where('name', '=', $new_press)->count();
                if ($check === 0) {
                  $press = new Press;
                  $press->name = $new_press;
                  $press->save();
                }

                // se la relazione non esiste la crea
                if ($release->hasPress($new_press) != 1) {
                  $press = Press::where('name', '=', $new_press)->first();
                  $release->presses()->attach($press->id);
                }

                // se una relazione è stata eliminata
                $presses = $release->presses()->get();
                $new_presses = $request->input('presses');

                foreach ($presses as $key => $press) {
                  $check = in_array($press->name, $new_presses);
                  if ($check === false) {
                    $release->presses()->detach($press->id);
                  }
                }

              }
            }

            // Salvo l'immagine
            if ($request->input('media-library') != null) {
              // ho scelto un'immagine dalla libreria
              $media = Media::find($request->input('media-library'));
              $release->media()->detach();
              $release->media()->save($media);
            } elseif ($request->file('media')) {
              // upload di una nuova immagine
              $utility->storeMedia($request->file('media'), $release);
            }

            // Salvo il video
            if ($request->input('video')) {
              $video = new Video;
              $video->url = $request->input('video');
              $video->save();
              $release->video()->save($video);
            }

            session()->flash('success', 'Rassegna stampa modificata!');
            return redirect()->route('posts.index');
          break;

        default:
            return view();
          break;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $post_type, $id)
    {
        switch ($post_type) {
          case 'news':
              $news = Post::find($id);
              $news->media()->detach();
              $news->delete();
              session()->flash('success', 'News eliminata!');
              return redirect()->route('posts.index');
            break;

          case 'exhibit':
              $exhibit = Exhibit::find($id);
              $exhibit->media()->detach();
              $exhibit->delete();
              session()->flash('success', 'Esposizione eliminata!');
              return redirect()->route('posts.index');
            break;

          case 'exhibition':
              $exhibition = Exhibition::find($id);
              $exhibition->media()->detach();
              $exhibition->delete();
              session()->flash('success', 'Rassegna eliminata!');
              return redirect()->route('posts.index');
            break;

          case 'film':
              $film = Film::find($id);
              $shows = FilmSchedule::where('film_id', '=', $id)->get();
              foreach ($shows as $key => $show) {
                $show->delete();
              }
              $film->media()->detach();
              $film->delete();
              session()->flash('success', 'Film eliminato!');
              return redirect()->route('posts.index');
            break;

          case 'festival':
              $festival = Festival::find($id);
              $festival->media()->detach();
              $festival->delete();
              session()->flash('success', 'Festival eliminato!');
              return redirect()->route('posts.index');
            break;

          case 'event':
            $event = Event::find($id);
            $event->media()->detach();
            $event->delete();
            session()->flash('success', 'Evento eliminato!');
            return redirect()->route('posts.index');
            break;

          case 'cinestore':
            $product = Product::find($id);
            $product->media()->detach();
            $product->delete();
            session()->flash('success', 'Prodotto eliminato!');
            return redirect()->route('posts.index');
            break;

          case 'percorsi':
            $products_group = ProductGroup::find($id);
            $products_group->media()->detach();
            $products_group->products()->detach();
            $products_group->delete();
            session()->flash('success', 'Prodotto eliminato!');
            return redirect()->route('posts.index');
            break;

          case 'release':
            $release = Release::find($id);
            $release->media()->detach();
            $release->video()->detach();
            $release->delete();
            session()->flash('success', 'Rassegna stampa eliminata!');
            return redirect()->route('posts.index');
            break;
        }
    }
}
