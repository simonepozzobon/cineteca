<?php

namespace App\Http\Controllers\Admin;

use App\Funder;
use App\Utility;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class FunderController extends Controller
{
    public function index() {
        return view('admin.funders.index');
    }

    public function get_funders()
    {
        $funders = Funder::all();

        foreach ($funders as $key => $funder) {
            $img = $funder->media()->get();
            if ($img->count() > 0) {
                $image = $funder->media()->first()->thumb;
                $funder->img = Storage::disk('local')->url($image);
            } else {
                $funder->img = '';
            }
        }

        return response($funders);
    }

    public function new_funder(Request $request)
    {
        $utility = new Utility();

        $funder = new Funder();
        $funder->name = $request->name;
        $funder->surname = $request->surname;
        $funder->description = $request->description;
        $funder->save();

        $utility->storeMedia($request->file('media'), $funder);

        $img = $funder->media()->first();
        $funder->img = Storage::disk('local')->url($img->thumb);

        return response($funder);
    }

    public function update_funder(Request $request)
    {
        $utility = new Utility();

        $funder = Funder::find($request->id);
        $funder->name = $request->name;
        $funder->surname = $request->surname;
        $funder->description = $request->description;
        $funder->save();

        if ($request->file('media') != null) {
            $funder->media()->detach();
            $utility->storeMedia($request->file('media'), $funder);
        }

        $img = $funder->media()->first();
        $funder->img = Storage::disk('local')->url($img->thumb);

        return response($funder);
    }
}
