<?php

namespace App\Http\Controllers\Admin;

use App\Film;
use App\Page;
use App\Exhibit;
use App\PostType;
use App\Exhibition;
use App\HomeSlider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class HomeSliderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => 'logout']);
    }

    public function index()
    {
      $homeSliders = HomeSlider::all();
      $slides = collect();

      foreach ($homeSliders as $key => $slide) {
        if (isset($slide->post_type_id)) {
          $postType = $slide->postType->name;
        } else {
          $postType = 'page';
        }

        switch ($postType) {
          case 'news':
            break;

          case 'exhibit':
              $exhibit = Exhibit::find($slide->post_id);
              $item = [
                'id' => $slide->id,
                'title' => $exhibit->title,
                'content' => $exhibit->description,
                'img' => $exhibit->media->first()->slide_img,
                'type' => 'Esposizione',
                'color' => '#48DEE4'
              ];
              $slides->push($item);
            break;

          case 'exhibition':
              $exhibition = Exhibition::find($slide->post_id);
              $item = [
                'id' => $slide->id,
                'title' => $exhibition->title,
                'content' => $exhibition->description,
                'img' => $exhibition->media->first()->slide_img,
                'type' => 'Rassegna',
                'color' => '#F1B340'
              ];
              $slides->push($item);
            break;

          case 'film';
              $film = Film::find($slide->post_id);
              $item = [
                'id' => $slide->id,
                'title' => $film->title,
                'content' => $film->description,
                'img' => $film->media->first()->slide_img,
                'type' => 'Film',
                'color' => '#CA4A4A'
              ];
              $slides->push($item);
            break;

          case 'festival';
              $festival = Festival::find($slide->post_id);
              $item = [
                'id' => $slide->id,
                'title' => $festival->title,
                'content' => $festival->description,
                'img' => $festival->media->first()->slide_img,
                'type' => 'Festival',
                'color' => '#F1B340'
              ];
              $slides->push($item);
            break;

          case 'cinestore';
              $product = Product::find($slide->post_id);
              $item = [
                'id' => $slide->id,
                'title' => $product->title,
                'content' => $product->description,
                'img' => $product->media->first()->slide_img,
                'type' => 'Cinestore',
                'color' => '#32B8C9'
              ];
              $slides->push($item);
            break;

          default: 'page';
              $page = Page::find($slide->post_id);
              $item = [
                'id' => $slide->id,
                'title' => $page->title,
                'content' => $page->content,
                'img'     => $page->media->first()->slide_img,
                'color'   => '#9C5A40'
              ];
              $slides->push($item);
            break;
        }
      }

      return view('admin.home-slider.index', compact('slides'));
    }

    public function create()
    {
      $post_types = PostType::all();
      $films = Film::all();
      $exhibitions = Exhibition::all();
      $exhibits = Exhibit::all();
      $pages = Page::all();

      $items = collect();

      foreach ($films as $key => $film) {
        $item = [
          'id' => $film->id,
          'title' => $film->title,
          'img' => $film->media->first()->slide_img,
          'content' => $film->description,
          'created_at' => $film->created_at,
          'type_id' => '1',
          'type' => 'film'
        ];
        $items->push($item);
      }

      foreach ($exhibitions as $key => $exhibition) {
        $item = [
          'id' => $exhibition->id,
          'title' => $exhibition->title,
          'img' => $exhibition->media->first()->slide_img,
          'content' => $exhibition->description,
          'created_at' => $exhibition->created_at,
          'type_id' => '2',
          'type' => 'rassegna'
        ];
        $items->push($item);
      }

      foreach ($exhibits as $key => $exhibit) {
        $item = [
          'id' => $exhibit->id,
          'title' => $exhibit->title,
          'img' => $exhibit->media->first()->slide_img,
          'content' => $exhibit->description,
          'created_at' => $exhibit->created_at,
          'type_id' => '3',
          'type' => 'esposizione'
        ];
        $items->push($item);
      }

      foreach ($pages as $key => $page) {
        $item = [
          'id' => $page->id,
          'title' => $page->title,
          'img' => $page->media->first()->slide_img,
          'content' => $page->content,
          'created_at' => $page->created_at,
          'type_id' => null,
          'type' => 'pagina'
        ];
        $items->push($item);
      }

      $items = $items->sortByDesc('created_at');

      return view('admin.home-slider.create', compact('items'));
    }

    public function store(Request $request)
    {
      $slide = new Homeslider;
      $slide->post_id = $request->input('post_id');
      $slide->post_type_id = $request->input('post_type_id');

      // Genero la nuova immagine adattata per lo slider
      $path = storage_path('app/public/home-slider');
      $filename = basename($request->input('img_path'));

      $exists = Storage::disk('local')->exists('public/home-slider/'.$filename);
      if ($exists) {
        Storage::delete('public/home-slider/'.$filename);
      }

      Storage::copy($request->input('img_path'), 'public/home-slider/'.$filename);
      Image::make($path.'/'.$filename)->fit(1920, 540, function ($constraint) {
          $constraint->upsize();
      })->save();
      $img = 'public/home-slider/'.$filename;

      // salvo l'immagine e la slide
      $slide->img = $img;
      $slide->save();

      session()->flash('success', 'Slide aggiunta!');
      return redirect()->route('homeslider.index');
    }

    public function destroy($id)
    {
      $slide = Homeslider::find($id);
      $slide->delete();

      session()->flash('success', 'Slide eliminata!');
      return redirect()->route('homeslider.index');
    }
}
