<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class WatermarkController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin', ['except' => 'logout']);
    }

    public function bulkAddWatermarkToPhoto()
    {
        $prods = Product::where('product_category_id', '=', 4)->with('media')->get();

        foreach ($prods as $key => $prod) {
          $pics = $prod->media()->get();
          foreach ($pics as $key => $pic) {
            $img = storage_path('app/'.$pic->img);
            $thumb = storage_path('app/'.$pic->thumb);
            $slide_img = storage_path('app/'.$pic->slide_img);
            $portrait = storage_path('app/'.$pic->portrait);
            $landscape = storage_path('app/'.$pic->landscape);

            $watermark_src = public_path('img/logo_fondazione_cineteca_milano.png');
            $watermark = Image::make($watermark_src)->resize(150, null, function ($constraint) {
                $constraint->aspectRatio();
            })->invert()->opacity(60);

            $mark_img = Image::make($img)->insert($watermark, 'bottom-left', 10, 10)->save();
            $mark_thumb = Image::make($thumb)->insert($watermark, 'bottom-left', 10, 10)->save();
            $mark_slide_img = Image::make($slide_img)->insert($watermark, 'bottom-left', 10, 10)->save();
            $mark_portrait = Image::make($portrait)->insert($watermark, 'bottom-left', 10, 10)->save();
            $mark_landscape = Image::make($landscape)->insert($watermark, 'bottom-left', 10, 10)->save();

            echo $img;
          }
        }
    }
}
