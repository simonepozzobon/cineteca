<?php

namespace App\Http\Controllers\Admin;

use File;
use App\Tweet;
use App\Event;
use App\Utility;
use App\Exhibit;
use App\Exhibition;
use \Carbon\Carbon;
use App\TweetStatus;
use \UrlShortener;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Testing\MimeType;

class TwitterController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin', ['except' => 'logout']);
    }

    public function index()
    {
        $tweets = Tweet::orderBy('date_time', 'desc')->with('media', 'status')->get();
        foreach ($tweets as $key => $tweet) {
            \Carbon\Carbon::setLocale('it');
            $dt = new \Carbon\Carbon($tweet->date_time);
            $tweet->diffForHumans = $dt->subHours(2)->diffForHumans();
        }

        return view('admin.tweets.index', compact('tweets'));
    }

    public function edit($id)
    {
        $statuses = TweetStatus::all();
        $tweet = Tweet::find($id);
        $model = class_basename($tweet->tweetable);

        $string = strtotime($tweet->date_time);
        $tweet->date = date('d-m-Y', $string);
        $tweet->time = date('H:i', $string);

        switch ($model) {
            case 'Event':
                $tweet->obj_link = route('single.event', $tweet->tweetable->slug);
            break;

            case 'Exhibit':
                $tweet->obj_link = route('single.exhibit', $tweet->tweetable->slug);
            break;

            case 'Exhibition':
                $tweet->obj_link = route('single.exhibition', $tweet->tweetable->slug);
            break;
        }

        return view('admin.tweets.edit', compact('tweet', 'statuses'));
    }

    public function update($id, Request $request)
    {
        $utility = new Utility;
        $dateTime = $utility->mergeDateTime($request->date, $request->time);

        $tweet = Tweet::find($id);
        $tweet->status_id = $request->status_id;
        $tweet->date_time = $dateTime;
        $tweet->text = $request->text;
        // $tweet->link = $request->link;
        $tweet->save();

        session()->flash('success', 'Tweet Modificato!');
        return redirect()->route('admin.twitter.index');
    }

    public function tweetSchedule()
    {
      setlocale(LC_TIME, "it_IT.UTF-8");
      date_default_timezone_set('Europe/Rome');
      $today = date('Y-m-d H:i:s');

      $events = Event::where([
          ['status_id', '=', 2],
          ['date_time', '>=', $today]
        ])->get();

      $exhibits = Exhibit::where([
          ['status_id', '=', 2],
          ['start_date', '>=', $today]
        ])->get();

      $exhibitions = Exhibition::where([
          ['status_id', '=', 2],
          ['start_date', '>=', $today]
        ])->get();

      $events = $events->reject(function($value, $key) {
        $value->img = Storage::disk('local')->url($value->media()->first()->landscape);
        $value->link = route('single.event', $value->slug);
        $value->location = $value->location->name;
        $string = strtotime($value->date_time);
        $time = date('H:i', $string);
        $value->time = $time;
        $day = date('d', $string);
        $value->day = $day;
        $month = strftime('%m', $string);
        $value->month = $month;
        $value->diff_time = date('Y-m-d H:i:s', strtotime('-3 days', strtotime($value->date_time)));
        return $value->tweets()->count() > 0;
      });

      $events->all();

      $exhibits = $exhibits->reject(function($value, $key) {
        $value->img = Storage::disk('local')->url($value->media()->first()->landscape);
        $value->link = route('single.exhibit', $value->slug);
        $value->location = $value->location->name;
        $string = strtotime($value->start_date);
        $time = date('H:i', $string);
        $value->time = $time;
        $day = date('d', $string);
        $value->day = $day;
        $month = strftime('%m', $string);
        $value->month = $month;
        $value->date_time = $value->start_date;
        $value->diff_time = date('Y-m-d H:i:s', strtotime('-3 days', strtotime($value->date_time)));
        return $value->tweets()->count() > 0;
      });

      $exhibits->all();

      $exhibitions = $exhibitions->reject(function($value, $key) {
        $value->img = Storage::disk('local')->url($value->media()->first()->landscape);
        $value->link = route('single.exhibition', $value->slug);
        $value->location = $value->location->name;
        $string = strtotime($value->start_date);
        $time = date('H:i', $string);
        $value->time = $time;
        $day = date('d', $string);
        $value->day = $day;
        $month = strftime('%m', $string);
        $value->month = $month;
        $value->date_time = $value->start_date;
        $value->diff_time = date('Y-m-d H:i:s', strtotime('-3 days', strtotime($value->date_time)));
        return $value->tweets()->count() > 0;
      });

      $exhibitions->all();


      foreach ($events as $key => $event) {
        $tweet = new Tweet;

        // se la data è futura allora uso lo scheduling dei tre giorni prima altrimenti all'ora dell'evento
        if ($event->diff_time >= $today) {
          $tweet->status_id = 1;
          $tweet->date_time = $event->diff_time;
        } else {
          $tweet->status_id = 3;
          $tweet->date_time = $event->date_time;
        }

        $tweet->text = Tweet::default_message($event);
        $tweet->link = $event->link;
        $event->tweets()->save($tweet);
        $tweet->save();

        $img = $event->media()->first();
        $tweet->media()->save($img);
      }

      foreach ($exhibits as $key => $exhibit) {
        $tweet = new Tweet;

        // se la data è futura allora uso lo scheduling dei tre giorni prima altrimenti all'ora dell'evento
        if ($exhibit->diff_time >= $today) {
          $tweet->status_id = 1;
          $tweet->date_time = $exhibit->diff_time;
        } else {
          $tweet->status_id = 3;
          $tweet->date_time = $exhibit->date_time;
        }

        $tweet->text = Tweet::default_message($exhibit);
        $tweet->link = $exhibit->link;
        $exhibit->tweets()->save($tweet);
        $tweet->save();

        $img = $exhibit->media()->first();
        $tweet->media()->save($img);
      }

      foreach ($exhibitions as $key => $exhibition) {
        $tweet = new Tweet;

        // se la data è futura allora uso lo scheduling dei tre giorni prima altrimenti all'ora dell'evento
        if ($exhibition->diff_time >= $today) {
          $tweet->status_id = 1;
          $tweet->date_time = $exhibition->diff_time;
        } else {
          $tweet->status_id = 3;
          $tweet->date_time = $exhibition->date_time;
        }

        $tweet->text = Tweet::default_message($exhibition);
        $tweet->link = $exhibition->link;
        $exhibition->tweets()->save($tweet);
        $tweet->save();

        $img = $exhibition->media()->first();
        $tweet->media()->save($img);
      }

      $count = $events->count() + $exhibits->count() + $exhibitions->count();

      session()->flash('success', "$count nuovi tweets creati");
      return redirect()->route('admin.twitter.index');
    }

    public function sendTest()
    {
      date_default_timezone_set('Europe/Rome');
      $today = date('Y-m-d H:i:s');
      $tomorrow = date('Y-m-d H:i:s', strtotime('+1 min'));

      print_r($today);
      print_r($tomorrow);
      // dd($tomorrow);
      //
      // $today = date('Y-m-d H:i:s', strtotime($today));
      // $tomorrow = date('Y-m-d H:i:s', strtotime($tomorrow));

      $tweets = Tweet::where([
          ['status_id', '=', 1],
          ['date_time', '>=', $today],
          ['date_time', '<=', $tomorrow]
        ])->get();

      print_r('<pre>');
      print_r($tweets);

      $settings = [
        'oauth_access_token' => env('TW_OAUTH_ACCESS_TOKEN'),
        'oauth_access_token_secret' => env('TW_OAUTH_ACCESS_TOKEN_SECRET'),
        'consumer_key' => env('TW_CONSUMER_KEY'),
        'consumer_secret' => env('TW_CONSUMER_SECRET')
      ];

      foreach ($tweets as $key => $tweet) {
        $text = $tweet->text;
        if (strlen($text) > 118) {
          $text = substr(strip_tags($text), 0, 118);
        }
        $shorten = UrlShortener::shorten($tweet->link);
        $text = $text.'...'.$shorten;

        print_r(strlen($text));
        print_r('<br>');
        print_r($text);


        // UPLOAD Media
        $url = 'https://upload.twitter.com/1.1/media/upload.json';
        $requestMethod = 'POST';

        $abs_path = storage_path('app/'.$tweet->media()->first()->landscape);
        $base_64 = base64_encode(file_get_contents($abs_path));

        $postfields = [
          'media_data' => $base_64,
        ];

        $twitter = new \TwitterAPIExchange($settings);
        $r_media = $twitter->buildOauth($url, $requestMethod)
          ->setPostfields($postfields)
          ->performRequest();

        $r_media = json_decode($r_media);

        // Effettua il post
        $url = 'https://api.twitter.com/1.1/statuses/update.json';
        $requestMethod = 'POST';

        $postfields = [
          'status' => $text,
          'media_ids' => $r_media->media_id,
        ];

        $twitter = new \TwitterAPIExchange($settings);
        $response = $twitter->buildOauth($url, $requestMethod)
          ->setPostfields($postfields)
          ->performRequest();

        $response = json_decode($response);
        dd($response);
      }
    }

    public function command()
    {
        $tweet = Tweet::find(47);
        $text = $tweet->text;
        if (strlen($text) > 118) {
          $text = substr(strip_tags($text), 0, 118);
        }
        $shorten = UrlShortener::shorten($tweet->link);
        $text = $text.'...'.$shorten;
    }

    public function destroy($id)
    {
        $tweet = Tweet::find($id);
        $tweet->media()->detach();
        $tweet->delete();
        session()->flash('success', 'Tweet eleminato!');
        return response()->json([
            'status' => 'deleted'
        ]);
    }
}
