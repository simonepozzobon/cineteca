<?php

namespace App\Http\Controllers\Admin;

use App\Film;
use App\Page;
use App\Post;
use App\Event;
use App\Product;
use App\Exhibit;
use App\Release;
use App\Festival;
use App\Exhibition;
use App\ProductGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class SearchController extends Controller
{
    public function search(string $query)
    {
      $films = Film::search($query)->get();
      $posts = Post::search($query)->get();
      $events = Event::search($query)->get();
      $exhibits = Exhibit::search($query)->get();
      $exhibitions = Exhibition::search($query)->get();
      // new
      $festivals = Festival::search($query)->get();
      $products = Product::search($query)->get();
      $product_groups = ProductGroup::search($query)->get();
      $releases = Release::search($query)->get();

      $results = collect();

      foreach ($films as $key => $item) {
        $result = [
          'id' => $item->id,
          'title' => $item->title,
          'img' => Storage::disk('local')->url($item->media->first()->landscape),
          'description' => $item->description,
          'type' => 'Film',
          'url' => '/admin/posts/film',
          'public_url' => '/film/'.$item->slug,
          'status_id' => $item->status_id,
        ];
        $results->push($result);
      }

      foreach ($posts as $key => $item) {
        $result = [
          'id' => $item->id,
          'title' => $item->title,
          'img' => Storage::disk('local')->url($item->media->first()->landscape),
          'description' => $item->content,
          'type' => 'News',
          'url' => '/admin/posts/news',
          'public_url' => '/news/'.$item->slug,
          'status_id' => $item->status_id,
        ];
        $results->push($result);
      }

      foreach ($exhibits as $key => $item) {
        $result = [
          'id' => $item->id,
          'title' => $item->title,
          'img' => Storage::disk('local')->url($item->media->first()->landscape),
          'description' => $item->description,
          'type' => 'Esposizione',
          'url' => '/admin/posts/exhibit',
          'public_url' => '/esposizione/'.$item->slug,
          'status_id' => $item->status_id,
        ];
        $results->push($result);
      }

      foreach ($exhibitions as $key => $item) {
        $result = [
          'id' => $item->id,
          'title' => $item->title,
          'img' => Storage::disk('local')->url($item->media->first()->landscape),
          'description' => $item->description,
          'type' => 'Rassegna',
          'url' => '/admin/posts/exhibition',
          'public_url' => '/rassegna/'.$item->slug,
          'status_id' => $item->status_id,
        ];
        $results->push($result);
      }

      foreach ($events as $key => $item) {
        $result = [
          'id' => $item->id,
          'title' => $item->title,
          'img' => Storage::disk('local')->url($item->media->first()->landscape),
          'description' => $item->description,
          'type' => 'Evento',
          'url' => '/admin/posts/event',
          'public_url' => '/evento/'.$item->slug,
          'status_id' => $item->status_id,
          'limit_guest' => $item->limit_guest,
          'rsvp_url' => '/admin/rsvp/'.$item->id,
        ];
        $results->push($result);
      }

      foreach ($festivals as $key => $item) {
        $result = [
          'id' => $item->id,
          'title' => $item->title,
          'img' => Storage::disk('local')->url($item->media->first()->landscape),
          'description' => $item->description,
          'type' => 'Festival',
          'url' => '/admin/posts/festival',
          'public_url' => '/festival/'.$item->slug,
          'status_id' => $item->status_id,
        ];
        $results->push($result);
      }

      foreach ($products as $key => $item) {
        $result = [
          'id' => $item->id,
          'title' => $item->name,
          'img' => Storage::disk('local')->url($item->media->first()->landscape),
          'description' => $item->description,
          'type' => 'Cinestore',
          'url' => '/admin/posts/cinestore',
          'public_url' => '/cinestore/prodotto/'.$item->slug,
          'status_id' => $item->status_id,
        ];
        $results->push($result);
      }

      foreach ($product_groups as $key => $item) {
        $result = [
          'id' => $item->id,
          'title' => $item->name,
          'img' => Storage::disk('local')->url($item->media->first()->landscape),
          'description' => $item->description,
          'type' => 'Cinestore',
          'url' => '/admin/posts/percorsi',
          'public_url' => '/cinestore/percorsi/'.$item->slug,
          'status_id' => $item->status_id,
        ];
        $results->push($result);
      }

      foreach ($releases as $key => $item) {
        $result = [
          'id' => $item->id,
          'title' => $item->title,
          'img' => Storage::disk('local')->url($item->media->first()->landscape),
          'description' => $item->description,
          'type' => 'Stampa',
          'url' => '/admin/posts/release',
          'public_url' => '/rassegna-stampa/'.$item->slug,
          'status_id' => $item->status_id,
        ];
        $results->push($result);
      }


      return response()->json($results);
    }

    public function searchFiltered(string $post_type, string $query)
    {
      $results = collect();

      switch ($post_type) {
        case 'film':
            $films = Film::search($query)->get();
            foreach ($films as $key => $item) {
              $result = [
                'id' => $item->id,
                'title' => $item->title,
                'img' => Storage::disk('local')->url($item->media->first()->landscape),
                'description' => $item->description,
                'type' => 'Film',
                'url' => '/admin/posts/film',
                'public_url' => '/film/'.$item->slug,
                'status_id' => $item->status_id,
              ];
              $results->push($result);
            }
          break;

        case 'exhibition':
            $exhibitions = Exhibition::search($query)->get();
            foreach ($exhibitions as $key => $item) {
              $result = [
                'id' => $item->id,
                'title' => $item->title,
                'img' => Storage::disk('local')->url($item->media->first()->landscape),
                'description' => $item->description,
                'type' => 'Rassegna',
                'url' => '/admin/posts/exhibition',
                'public_url' => '/rassegna/'.$item->slug,
                'status_id' => $item->status_id,
              ];
              $results->push($result);
            }
          break;

        case 'exhibit':
            $exhibits = Exhibit::search($query)->get();
            foreach ($exhibits as $key => $item) {
              $result = [
                'id' => $item->id,
                'title' => $item->title,
                'img' => Storage::disk('local')->url($item->media->first()->landscape),
                'description' => $item->description,
                'type' => 'Esposizione',
                'url' => '/admin/posts/exhibit',
                'public_url' => '/esposizione/'.$item->slug,
                'status_id' => $item->status_id,
              ];
              $results->push($result);
            }
          break;

        case 'festival':
            $festivals = Festival::search($query)->get();
            foreach ($festivals as $key => $item) {
              $result = [
                'id' => $item->id,
                'title' => $item->title,
                'img' => Storage::disk('local')->url($item->media->first()->landscape),
                'description' => $item->description,
                'type' => 'Festival',
                'url' => '/admin/posts/festival',
                'public_url' => '/festival/'.$item->slug,
                'status_id' => $item->status_id,
              ];
              $results->push($result);
            }
          break;

        case 'cinestore':
            $products = Product::search($query)->get();
            $product_groups = ProductGroup::search($query)->get();
            foreach ($products as $key => $item) {
              $result = [
                'id' => $item->id,
                'title' => $item->name,
                'img' => Storage::disk('local')->url($item->media->first()->landscape),
                'description' => $item->description,
                'type' => 'Cinestore',
                'url' => '/admin/posts/cinestore',
                'public_url' => '/cinestore/prodotto/'.$item->slug,
                'status_id' => $item->status_id,
              ];
              $results->push($result);
            }

            foreach ($product_groups as $key => $item) {
              $result = [
                'id' => $item->id,
                'title' => $item->name,
                'img' => Storage::disk('local')->url($item->media->first()->landscape),
                'description' => $item->description,
                'type' => 'Cinestore',
                'url' => '/admin/posts/percorsi',
                'public_url' => '/cinestore/percorsi/'.$item->slug,
                'status_id' => $item->status_id,
              ];
              $results->push($result);
            }
          break;

        case 'news':
            $posts = Post::search($query)->get();
            foreach ($posts as $key => $item) {
              $result = [
                'id' => $item->id,
                'title' => $item->title,
                'img' => Storage::disk('local')->url($item->media->first()->landscape),
                'description' => $item->content,
                'type' => 'News',
                'url' => '/admin/posts/news',
                'public_url' => '/news/'.$item->slug,
                'status_id' => $item->status_id,
              ];
              $results->push($result);
            }
          break;

        case 'event':
            $events = Event::search($query)->get();
            foreach ($events as $key => $item) {
              $result = [
                'id' => $item->id,
                'title' => $item->title,
                'img' => Storage::disk('local')->url($item->media->first()->landscape),
                'description' => $item->description,
                'type' => 'Evento',
                'url' => '/admin/posts/event',
                'public_url' => '/evento/'.$item->slug,
                'status_id' => $item->status_id,
                'limit_guest' => $item->limit_guest,
                'rsvp_url' => '/admin/rsvp/'.$item->id,
              ];
              $results->push($result);
            }
          break;

        case 'release':
            $releases = Release::search($query)->get();
            foreach ($releases as $key => $item) {
              $result = [
                'id' => $item->id,
                'title' => $item->title,
                'img' => Storage::disk('local')->url($item->media->first()->landscape),
                'description' => $item->description,
                'type' => 'Stampa',
                'url' => '/admin/posts/release',
                'public_url' => '/rassegna-stampa/'.$item->slug,
                'status_id' => $item->status_id,
              ];
              $results->push($result);
            }
          break;
      }
      
      return response()->json($results);
    }
}
