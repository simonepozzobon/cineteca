<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\ProductGroup;
use App\FeaturedSlider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CinestoreFeaturedController extends Controller
{
    public function index()
    {
        $products = Product::all();
        $prod_groups = ProductGroup::all();

        $pics = $products->filter(function($product, $key){
          return $product->product_category_id == 4;
        })->all();

        $videos = $products->filter(function($product, $key){
          return $product->product_category_id == 5;
        })->all();

        $feats = FeaturedSlider::all();

        $feat_prods = $feats->filter(function($feat, $key){
            return $feat->category_id == 0;
        })->all();

        $feat_pics = $feats->filter(function($feat, $key){
            return $feat->category_id == 4;
        })->all();

        $feat_videos = $feats->filter(function($feat, $key){
            return $feat->category_id == 5;
        })->all();

        $feat_groups = $feats->filter(function($feat, $key){
            return $feat->category_id == 6;
        })->all();


        return view('admin.cinestore-featured.index', compact('products', 'prod_groups', 'pics', 'videos', 'feat_prods', 'feat_pics', 'feat_videos', 'feat_groups'));
    }

    public function store(Request $request)
    {

      // Se non è un percorso
      if ($request['category'] != 6) {
        $product = Product::where('id', '=', $request['id'])->with('media')->first();
        $featuredSlider = new FeaturedSlider();
        $featuredSlider->category_id = $request['category'];
        $product->featuredSliders()->save($featuredSlider);
        $product->img = Storage::disk('local')->url($product->media()->first()->thumb);
      }
      else {
        $product = ProductGroup::where('id', '=', $request['id'])->with('media')->first();
        $featuredSlider = new FeaturedSlider();
        $featuredSlider->category_id = $request['category'];
        $product->featuredSliders()->save($featuredSlider);
        $product->img = Storage::disk('local')->url($product->media()->first()->thumb);
      }

      return response()->json([
        'request' => $request->all(),
        'feat' => $featuredSlider,
        'product' => $product,
      ], 200);
    }

    public function destroy($id)
    {
      $featuredSlider = FeaturedSlider::find($id);
      $featuredSlider->delete();
      session()->flash('success', 'Prodotto rimosso!');
      return redirect()->route('cinestore.featured.index');
    }
}
