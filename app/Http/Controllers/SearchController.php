<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Film;
use App\Page;
use App\Post;
use App\Product;
use App\Exhibit;
use App\Exhibition;
use App\ProductGroup;
use App\ProductCategory;
use Illuminate\Support\Facades\Storage;

class SearchController extends Controller
{
    public function search(Request $request)
    {
      $films = Film::search($request->input('query'))->get();
      $pages = Page::search($request->input('query'))->get();
      $posts = Post::search($request->input('query'))->get();
      $exhibits = Exhibit::search($request->input('query'))->get();
      $exhibitions = Exhibition::search($request->input('query'))->get();

      $results = collect();

      foreach ($films as $key => $film) {
        $item = [
          'title' => $film->title,
          'img' => Storage::disk('local')->url($film->media->first()->landscape),
          'description' => $film->description,
          'type' => 'Film',
          'url' => 'film/'.$film->slug,
          'color' => '#CA4A4A'
        ];
        $results->push($item);
      }

      foreach ($pages as $key => $page) {
        $item = [
          'title' => $page->title,
          'img' => Storage::disk('local')->url($page->media->first()->landscape),
          'description' => $page->content,
          'type' => 'Pagina',
          'url' => 'pagina/'.$page->slug,
          'color' => $page->category->color
        ];
        $results->push($item);
      }

      foreach ($posts as $key => $post) {
        $item = [
          'title' => $post->title,
          'img' => Storage::disk('local')->url($post->media->first()->landscape),
          'description' => $post->content,
          'type' => 'News',
          'url' => 'news/'.$post->slug,
          'color' => $post->category->color
        ];
        $results->push($item);
      }

      foreach ($exhibits as $key => $exhibit) {
        $item = [
          'title' => $exhibit->title,
          'img' => Storage::disk('local')->url($exhibit->media->first()->landscape),
          'description' => $exhibit->description,
          'type' => 'Esposizione',
          'url' => 'esposizione/'.$exhibit->slug,
          'color' => '#48DEE4'
        ];
        $results->push($item);
      }

      foreach ($exhibitions as $key => $exhibition) {
        $item = [
          'title' => $exhibition->title,
          'img' => Storage::disk('local')->url($exhibition->media->first()->landscape),
          'description' => $exhibition->description,
          'type' => 'Rassegna',
          'url' => 'rassegna/'.$exhibition->slug,
          'color' => '#F1B340'
        ];
        $results->push($item);
      }

      $query = $request->input('query');
      return view('public.search.index', compact('results', 'query'));
    }

    public function searchCinestore(Request $request)
    {
        if ($request->category == 6) {
          $products = ProductGroup::search($request->input('query'))->get();
        } else {
          $products = Product::search($request->input('query'))->get();
          $products = $products->filter(function($product, $key) use ($request) {
            return $product->product_category_id == $request->input('category');
          })->all();
        }

        $product_categories = ProductCategory::all();
        $category = $product_categories->filter(function($category, $key) use ($request) {
          return $category->id == $request->input('category');
        })->first();

        return view('public.search.cinestore', compact('products', 'product_categories', 'category'));
    }

    public function searchMainCinestore(Request $request)
    {
        $products = Product::search($request->input('query'))->get();
        $product_groups = ProductGroup::search($request->input('query'))->get();
        foreach ($product_groups as $key => $product_group) {
          $products->push($product_group);
        }


        $product_categories = ProductCategory::all();
        $category = collect();
        $category->name = 'Risultati Ricerca Cinestore';

        return view('public.search.cinestore', compact('products', 'product_categories', 'category'));
    }
}
