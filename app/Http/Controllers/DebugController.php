<?php

namespace App\Http\Controllers;

use App\PageLink;
use App\Page;
use App\Media;
use Illuminate\Http\Request;

class DebugController extends Controller
{
    public function debug()
    {
        $page = Page::find(10);
        $links = $page->pageLinks()->get();
        foreach ($links as $key => $link) {
            $check = Page::find($link->linked_id);
            if (!$check) {
                echo 'pagina non trovata. id = '.$link->linked_id.'<br>';
            }
        }
    }

    public function add_title_to_media()
    {
        $medias = Media::all();
        $count = 0;
        foreach ($medias as $key => $media) {
            if (!$media->title) {
                $filename = basename($media->img);
                $name = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
                $media->title = $name;
                $media->save();
                $count++;
            }
        }
        echo 'Titoli aggiunti a '.$count.' immagini';
    }
}
