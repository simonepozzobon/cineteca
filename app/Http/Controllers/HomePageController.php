<?php

namespace App\Http\Controllers;

use App\Post;
use App\Film;
use App\Page;
use Validator;
use App\Guest;
use App\Event;
use App\Press;
use App\Funder;
use App\Editor;
use App\Release;
use App\Utility;
use App\Exhibit;
use App\Product;
use App\Category;
use App\Producer;
use App\Festival;
use App\EventType;
use Carbon\Carbon;
use App\StaticPage;
use App\HomeSlider;
use App\Exhibition;
use App\Distributor;
use App\RsvpMessage;
use App\Photographer;
use App\Screenwriter;
use App\FilmSchedule;
use App\FilmLocation;
use App\FestivalRelation;
use Illuminate\Http\Request;
use App\Mail\RsvpConfirmedGuestList;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class HomePageController extends Controller
{
    /*
    *
    * HOME
    *
    */

    public function getHome()
    {
        $news = Post::where([
            ['status_id', '=', 2],
            ['in_home', '=', 1]  // status = published & in evidenza
        ])->with('media')->orderBy('created_at', 'desc')->limit(15)->get();

        $news = $news->transform(function($single, $key) {
            $single->img = Storage::disk('local')->url($single->media->first()->landscape);
            return $single;
        })->all();

        $news = json_encode($news, JSON_UNESCAPED_UNICODE);

        $categories = Category::where([
            ['in_home', '=', 1]
        ])->orderBy('order')->get();

        $homeSliders = Homeslider::all();
        $slides = collect();

        foreach ($homeSliders as $key => $slide) {
            if (isset($slide->post_type_id)) {
                $postType = $slide->postType->name;
            } else {
                $postType = 'page';
            }

            switch ($postType) {

                case 'exhibit':
                    $exhibit = Exhibit::find($slide->post_id);
                    $item = [
                        'title' => $exhibit->title,
                        'content' => $exhibit->description,
                        'img' => $slide->img,
                        'type' => 'Esposizione',
                        'url' => 'esposizione/'.$exhibit->slug,
                        'color' => '#48DEE4'
                    ];
                    $slides->push($item);
                break;

                case 'exhibition':
                    $exhibition = Exhibition::find($slide->post_id);
                    $item = [
                        'title' => $exhibition->title,
                        'content' => $exhibition->description,
                        'img' => $slide->img,
                        'type' => 'Rassegna',
                        'url' => 'rassegna/'.$exhibition->slug,
                        'color' => '#F1B340'
                    ];
                    $slides->push($item);
                break;

                case 'film':
                    $film = Film::find($slide->post_id);
                    $item = [
                    'title' => $film->title,
                    'content' => $film->description,
                    'img' => $slide->img,
                    'type' => 'Film',
                    'url' => 'film/'.$film->slug,
                    'color' => '#CA4A4A'
                    ];
                    $slides->push($item);
                break;

                case 'festival':
                    $festival = Festival::find($slide->post_id);
                    $item = [
                        'title' => $festival->title,
                        'content' => $festival->description,
                        'img' => $slide->img,
                        'type' => 'Festival',
                        'url' => 'festival/'.$festival->slug,
                        'color' => '#CA4A4A'
                    ];
                    $slides->push($item);
                break;

                case 'cinestore':
                break;

                default: 'page';
                    $page = Page::find($slide->post_id);
                    $item = [
                        'title' => $page->title,
                        'content' => $page->content,
                        'img'     => $slide->img,
                        'url'     => 'pagina/'.$page->slug,
                        'color'   => $page->category->color
                    ];
                    $slides->push($item);
                break;
                }
        }

        $festival = Festival::where('slug', '=', 'cineteca70')->first();

        return view('welcome', compact('news', 'categories', 'slides', 'festival'));
    }

    /*
    *
    * PAGINE
    *
    */

    public function getPage($slug)
    {
        $page = Page::where('slug', '=', $slug)->with('media', 'pageLinks', 'pageLinks.page', 'gallery', 'gallery.medias')->first();
        if ($page->redirect == 1) {
            return redirect($page->redirect_url);
        }
        return view('public.page.single', compact('page'));
    }


    /*
    *
    * NEWS
    *
    */

    public function getNews($slug)
    {
        $news = Post::where('slug', '=', $slug)->with('media')->first();
        return view('public.news.single', compact('news'));
    }

    public function news()
    {
        $news = Post::orderBy('created_at', 'desc')->paginate(10);
        $featured = Post::where('in_home', '=', 1)->with('media')->orderBy('created_at', 'desc')->get();
        return view('public.news.index', compact('news', 'featured'));
    }


    /*
    *
    * FILM
    *
    */

    public function getFilmSingle($slug)
    {
        $utility = new Utility;


        $film = Film::where('slug', '=', $slug)->with('media', 'location', 'exhibitions', 'exhibits', 'actors', 'directors', 'editors', 'producers', 'photographers', 'screenwriters', 'distributors')->first();

        $tickets = $utility->getTicketJSON();

        $string_start = strtotime($film->start_date);
        $string_end = strtotime($film->end_date);

        setlocale(LC_TIME, "it_IT.UTF-8");

        $start = strftime('%d %B %G', $string_start);
        $end = strftime('%d %B %G', $string_end);

        $film->start_date = $start;
        $film->end_date = $end;

        $today = date('Y-m-d H:i:s', time());

        $shows = FilmSchedule::where([
            ['film_id', '=', $film->id],
            ['date_time', '>=', $today],
        ])->with('film.location', 'film', 'film.media')->orderBy('date_time')->get();

        foreach ($shows as $key => $show) {
            $string = strtotime($show->date_time);

            setlocale(LC_TIME, "it_IT.UTF-8");

            $time = date('H:i', $string);
            $show->time = $time;

            $day = date('d', $string);
            $show->day = $day;

            $month = strftime('%B', $string);
            $show->month = $month;

            if ($show->film->location->id == 2 || $show->film->location->id == 1) {
                $show->date_ticket = date('Y-m-d', $string).'T00:00:00';
                $data = $utility->searchTicketJSON($tickets, $show->film->title, $show->date_ticket, $show->time);
                if ($data !== false) {
                    $show->ticket_url = $data;
                    $show->ticket = true;
                } else {
                    $show->ticket = false;
                }
            }
        }

        return view('public.film.single', compact('film', 'shows'));
    }

    public function getFilms()
    {
        $utility = new Utility;
        $tickets = $utility->getTicketJSON();
        $today = date('Y-m-d H:i:s', time());
        $end_list = date('Y-m-d H:i:s', strtotime('+1 days'));
        $events = Event::where([
            ['date_time', '>=', $today],
            ['date_time', '<', $end_list],
            ['status_id', '=', 2],
        ])->orderBy('date_time')->with('location', 'media')->get();
        // $films = Film::where('start_date', '>=', $today)->with('media', 'categories')->get();
        $shows = FilmSchedule::where([
            ['date_time', '>=', $today],
            ['date_time', '<', $end_list],
        ])->orderBy('date_time')->with('film', 'film.media', 'film.categories', 'film.place')->get();

        $featured = FilmSchedule::where('date_time', '>=', $today)->orderBy('date_time')->with('film', 'film.media')->get();
        $films = collect();
        foreach ($featured as $key => $show_feat) {
            $films->push($show_feat->film);
        }
        $films = $films->unique('id');

        foreach ($shows as $key => $show) {
            $string = strtotime($show->date_time);

            setlocale(LC_TIME, "it_IT.UTF-8");

            $time = date('H:i', $string);
            $show->time = $time;

            $day = date('d', $string);
            $show->day = $day;

            $day_string = strftime('%A', $string);
            $show->day_string = $day_string;

            $month = strftime('%B', $string);
            $show->month = $month;

            $show->date_ticket = date('Y-m-d', $string).'T00:00:00';

            $show->type = 'film';
            $show->title = $show->film->title;
            $show->kids = $show->film->kids;
            $show->exhibitions = $show->exhibitions($show->film->id);

            if ($show->film->media->first()) {
                $show->img = Storage::disk('local')->url($show->film->media->first()->landscape);
            } else {
                $show->img = 'none';
            }
            $show->slug = route('single.film', $show->film->slug);

            // Biglietti
            if ($show->film->location->id == 2 || $show->film->location->id == 1) {
                $data = $utility->searchTicketJSON($tickets, $show->film->title, $show->date_ticket, $show->time);
                if ($data !== false) {
                    $show->ticket_url = $data;
                    $show->ticket = true;
                } else {
                    $show->ticket = false;
                }
            }
        // Fine Biglietti
        }

        // Eventi nel calendario
        foreach ($events as $key => $event) {
            $string = strtotime($event->date_time);

            setlocale(LC_TIME, "it_IT.UTF-8");

            $time = date('H:i', $string);
            $event->time = $time;

            $day = date('d', $string);
            $event->day = $day;

            $day_string = strftime('%A', $string);
            $event->day_string = $day_string;

            $month = strftime('%B', $string);
            $event->month = $month;

            $event->date_ticket = date('Y-m-d', $string).'T00:00:00';

            $event->type = 'event';
            $event->img = $event->media->first()->landscape;
            $event->slug = route('single.event', $event->slug);
            $shows->push($event);
        }

        $shows = $shows->sortBy('date_time');

        // Featured Event
        $featDirector = Event::where([
            ['event_type_id', '=', 7],
            ['date_time', '>=', $today]
        ])->orderBy('date_time')->with('location', 'media')->first();

        // Formatting Date
        if ($featDirector != null) {
            $string = strtotime($featDirector->date_time);
            setlocale(LC_TIME, "it_IT.UTF-8");
            $time = date('H:i', $string);
            $featDirector->time = $time;
            $day = date('d', $string);
            $featDirector->day = $day;
            $day_string = strftime('%A', $string);
            $featDirector->day_string = $day_string;
            $month = strftime('%B', $string);
            $featDirector->month = $month;
            $featDirector->type = 'director';
        }

        $news = Post::where([
            ['location_id', '!=', 0],
        ])->orderBy('created_at', 'desc')->limit(15)->get();

        $news_count = $news->count();

        if ($news_count <= 15) {
            $news_left = 15 - (int) $news_count;
            $generic_news = Post::where([
                ['location_id', '=', 0],
                ['in_home', '=', 1],
            ])->orderBy('created_at', 'desc')->limit($news_left)->get();
            $news = $news->merge($generic_news);
        }

        $news = $news->transform(function($single, $key) {
            $single->img = Storage::disk('local')->url($single->media->first()->landscape);
            return $single;
        })->all();

        $news = json_encode($news, JSON_UNESCAPED_UNICODE);

        $today = Carbon::today()->format('Y-m-d');

        $featured_exhibition = Exhibition::where([
            ['start_date', '<=', $today],
            ['end_date', '>=', $today],
            ['status_id', '=', 2],
        ])->with('media')->orderBy('created_at', 'desc')->inRandomOrder()->limit(1)->first();
        if ($featured_exhibition) {
            $featured_exhibition->landscape = Storage::disk('local')->url($featured_exhibition->media()->first()->landscape);
            $featured_exhibition->thumb = Storage::disk('local')->url($featured_exhibition->media()->first()->thumb);
            $featured_exhibition->description = Utility::shortenDesc($featured_exhibition->description);
            $featured_exhibition->type = 'exhibition';
        }

        $featured_exhibit = Exhibit::where([
            ['start_date', '<=', $today],
            ['end_date', '>=', $today],
            ['status_id', '=', 2],
        ])->with('media')->orderBy('created_at', 'desc')->inRandomOrder()->limit(1)->first();
        if ($featured_exhibit) {
            $featured_exhibit->landscape = Storage::disk('local')->url($featured_exhibit->media()->first()->landscape);
            $featured_exhibit->thumb = Storage::disk('local')->url($featured_exhibit->media()->first()->thumb);
            $featured_exhibit->description = Utility::shortenDesc($featured_exhibit->description);
            $featured_exhibit->type = 'exhibit';
        }


        $featured_product = Product::inRandomOrder()->limit(1)->first();
        if ($featured_product) {
            $featured_product->landscape = Storage::disk('local')->url($featured_product->media()->first()->landscape);
            $featured_product->thumb = Storage::disk('local')->url($featured_product->media()->first()->thumb);
            $featured_product->description = Utility::shortenDesc($featured_product->description);
            $featured_product->title = $featured_product->name;
            $featured_product->type = 'product';
        }

        $categories_allowed = [2, 3, 4, 6, 8, 9, 10];
        $featured_page = Page::where('status_id', '=', 2)->whereIn('category_id', $categories_allowed)->inRandomOrder()->first();
        if ($featured_page) {
            $featured_page->landscape = Storage::disk('local')->url($featured_page->media()->first()->landscape);
            $featured_page->thumb = Storage::disk('local')->url($featured_page->media()->first()->thumb);
            $featured_page->description = Utility::shortenDesc($featured_page->content);
            $featured_page->type = 'page';
        }

        return view('public.film.index', compact('films', 'shows', 'featDirector', 'news', 'featured_exhibition', 'featured_exhibit', 'featured_product', 'featured_page'));
        // return view('public.film.index', compact('films', 'shows', 'featDirector', 'news'));
    }

    public function getShows($date)
    {
        $utility = new Utility;
        $tickets = $utility->getTicketJSON();

        $string = strtotime($date);
        $string_end = strtotime($date.'+1 day');

        $start_date = date('Y-m-d H:i:s', $string);
        $end_date = date('Y-m-d H:i:s', $string_end);

        $events = Event::where([
            ['date_time', '>=', $start_date],
            ['date_time', '<', $end_date],
            ['status_id', '=', 2],
        ])->orderBy('date_time')->with('location', 'media')->get();

        $shows = FilmSchedule::where([
            ['date_time', '>', $start_date],
            ['date_time', '<', $end_date]
        ])->with('film', 'film.media')->orderBy('date_time')->get();

        $items = collect();

        foreach ($shows as $key => $show) {
            $show->type = 'film';

            // Formatto l'ora e la data
            $string = strtotime($show->date_time);
            setlocale(LC_TIME, "it_IT.UTF-8");
            $time = date('H:i', $string);
            $show->time = $time;
            $day = date('d', $string);
            $show->day = $day;
            $day_string = strftime('%A', $string);
            $show->day_string = $day_string;
            $month = strftime('%B', $string);
            $show->month = $month;
            $show->date_ticket = date('Y-m-d', $string).'T00:00:00';
            $show->title = $show->film->title;
            $show->url = route('single.film', $show->film->slug);
            if ($show->film->media->first()) {
                $show->img = Storage::disk('local')->url($show->film->media->first()->landscape);
            } else {
                $show->img = 'none';
            }

            // Biglietti
            if ($show->film->location->id == 2 || $show->film->location->id == 1) {
                $data = $utility->searchTicketJSON($tickets, $show->film->title, $show->date_ticket, $show->time);
                if ($data !== false) {
                    $show->ticket_url = $data;
                    $show->ticket = true;
                } else {
                    $show->ticket = false;
                }
            }

            // Cambio il link dell'immagine
            // $show->film->img = str_replace('public', 'storage', $show->film->media->first()->landscape);

            // Ottengo l'oggetto location
            $show->location = $show->film->location;

            // Ottengo il nome delle rassegne collegate
            $exhibitions = collect();
            foreach ($show->exhibitions($show->film->id) as $key => $exhibition) {
                $exhibitions->push($exhibition);
            }
            $show->exhibitions = $exhibitions;

            // Ottengo il nome delle categorie collegate allo shows
            $film_categories = collect();
            foreach ($show->film->categories as $key => $film_category) {
                $film_categories->push($film_category);
            }
            $show->film_categories = $film_categories;


            // Accorcio la descrizione
            $description = substr(strip_tags($show->film->description), 0, 150);
            $dots = strlen(strip_tags($show->film->description)) > 150 ? '...' : "";
            $show->description = $description.$dots;

            $items->push($show);
        }

        // Eventi nel calendario
        foreach ($events as $key => $event) {
            $string = strtotime($event->date_time);

            setlocale(LC_TIME, "it_IT.UTF-8");

            $time = date('H:i', $string);
            $event->time = $time;

            $day = date('d', $string);
            $event->day = $day;

            $day_string = strftime('%A', $string);
            $event->day_string = $day_string;

            $month = strftime('%B', $string);
            $event->month = $month;

            $event->date_ticket = date('Y-m-d', $string).'T00:00:00';

            $event->type = 'event';
            $event->img = Storage::disk('local')->url($event->media->first()->landscape);
            $event->url = route('single.event', $event->slug);

            // Accorcio la descrizione
            $description = substr(strip_tags($event->description), 0, 150);
            $dots = strlen(strip_tags($event->description)) > 150 ? '...' : "";
            $event->description = $description.$dots;

        // $shows->push($event);
        $items->push($event);
        }

        $items = $items->sortBy('date_time')->values();
        foreach ($items as $key => $item) {
            $item->title = Utility::cleanUTF($item->title);
            $item->description = Utility::cleanUTF($item->description);
        }

        return response()->json($items, 200, ['Content-type'=> 'application/json;'], JSON_UNESCAPED_UNICODE);
    }

    public function getFilmWithLocation($location)
    {
        $utility = new Utility;
        $location = FilmLocation::where('slug', '=', $location)->first();

        if ($location->id == 2 || $location->id == 1) {
            $tickets = $utility->getTicketJSON();
        }

        $today = date('Y-m-d H:i:s', time());

        $string = strtotime($today);
        $string_end = strtotime($today.'+1 day');

        $start_date = date('Y-m-d H:i:s', $string);
        $end_date = date('Y-m-d H:i:s', $string_end);


        $films = Film::where('location_id', '=', $location->id)->with('media', 'categories')->get();

        $location_group = $location->group;

        $events = Event::where([
            ['date_time', '>=', $today],
            ['date_time', '<', $end_date],
            ['status_id', '=', 2],
        ])->with('location', 'media')->whereHas('location', function($query) use ($location_group) {
            $query->where('group', '=', $location_group);
        })->orderBy('date_time')->get();

        // Se ci sono film
        if (count($films) != 0) {
            foreach ($films as $key => $film) {
                $film_id_list[$key] = $film->id;
            }

            $shows = FilmSchedule::where([
                ['date_time', '>', $start_date],
                ['date_time', '<', $end_date]
            ])->whereIn('film_id', $film_id_list)->with('film')->orderBy('date_time')->get();

            $featured = FilmSchedule::where('date_time', '>=', $start_date)->whereIn('film_id', $film_id_list)->orderBy('date_time')->with('film', 'film.media')->get();
            $films = collect();
            foreach ($featured as $key => $show_feat) {
                $films->push($show_feat->film);
            }

            $films = $films->unique('id');

            foreach ($shows as $key => $show) {
                $string = strtotime($show->date_time);

                setlocale(LC_TIME, "it_IT.UTF-8");

                $time = date('H:i', $string);
                $show->time = $time;

                $day_string = strftime('%A', $string);
                $show->day_string = $day_string;

                $day = date('d', $string);
                $show->day = $day;

                $month = strftime('%B', $string);
                $show->month = $month;

                $show->type = 'film';
                $show->title = $show->film->title;
                $show->kids = $show->film->kids;
                if ($show->film->media->first()) {
                    $show->img = $show->film->media->first()->landscape;
                } else {
                    $show->img = 'none';
                }
                $show->slug = route('single.film', $show->film->slug);

                if ($show->film->location->id == 2 || $show->film->location->id == 1) {
                    $show->date_ticket = date('Y-m-d', $string).'T00:00:00';
                    $data = $utility->searchTicketJSON($tickets, $show->film->title, $show->date_ticket, $show->time);
                    if ($data !== false) {
                        $show->ticket_url = $data;
                        $show->ticket = true;
                    } else {
                        $show->ticket = false;
                    }
                }

                $show->title = Utility::cleanUTF($show->title);
                $show->description = Utility::cleanUTF($show->description);
            }

            // Eventi nel calendario
            foreach ($events as $key => $event) {
                $string = strtotime($event->date_time);

                setlocale(LC_TIME, "it_IT.UTF-8");

                $time = date('H:i', $string);
                $event->time = $time;

                $day = date('d', $string);
                $event->day = $day;

                $day_string = strftime('%A', $string);
                $event->day_string = $day_string;

                $month = strftime('%B', $string);
                $event->month = $month;

                $event->date_ticket = date('Y-m-d', $string).'T00:00:00';

                $event->type = 'event';
                $event->img = $event->media->first()->landscape;
                $event->slug = route('single.event', $event->slug);
                $shows->push($event);
            }

            $shows = $shows->sortBy('date_time');
        }

        $memory = true;

        $news = Post::where([
            ['location_id', '=', $location->id],
        ])->orderBy('created_at', 'desc')->limit(15)->get();

        $news_count = $news->count();

        if ($news_count <= 15) {
            $news_left = 15 - (int) $news_count;
            $generic_news = Post::where([
                ['location_id', '=', 0],
                ['in_home', '=', 1],
            ])->orderBy('created_at', 'desc')->limit($news_left)->get();
            $news = $news->merge($generic_news);
        }

        $news = $news->transform(function($single, $key) {
            $single->img = Storage::disk('local')->url($single->media->first()->landscape);
            return $single;
        })->all();

        $news = json_encode($news, JSON_UNESCAPED_UNICODE);

        return view('public.film.location.index', compact('films', 'shows', 'location', 'memory', 'news'));
    }


    public function getFilmWithLocationWeek($location)
    {
        $utility = new Utility;
        $location = FilmLocation::where('slug', '=', $location)->first();

        if ($location->id == 2 || $location->id == 1) {
            $tickets = $utility->getTicketJSON();
        }

        $today = date('Y-m-d H:i:s', time());

        $string = strtotime($today);
        $string_end = strtotime($today.'+7 day');

        $start_date = date('Y-m-d H:i:s', $string);
        $end_date = date('Y-m-d H:i:s', $string_end);


        $films = Film::where('location_id', '=', $location->id)->with('media', 'categories')->get();

        $location_group = $location->group;

        $events = Event::where([
            ['date_time', '>=', $today],
            ['date_time', '<', $end_date],
            ['status_id', '=', 2],
        ])->with('location', 'media')->whereHas('location', function($query) use ($location_group) {
            $query->where('group', '=', $location_group);
        })->orderBy('date_time')->get();

        // Se ci sono film
        if (count($films) != 0) {
            foreach ($films as $key => $film) {
                $film_id_list[$key] = $film->id;
            }

            $shows = FilmSchedule::where([
                ['date_time', '>', $start_date],
                ['date_time', '<', $end_date]
            ])->whereIn('film_id', $film_id_list)->with('film')->orderBy('date_time')->get();

            $featured = FilmSchedule::where('date_time', '>=', $start_date)->whereIn('film_id', $film_id_list)->orderBy('date_time')->with('film', 'film.media')->get();
            $films = collect();
            foreach ($featured as $key => $show_feat) {
                $films->push($show_feat->film);
            }

            $films = $films->unique('id');

            foreach ($shows as $key => $show) {
                $string = strtotime($show->date_time);

                setlocale(LC_TIME, "it_IT.UTF-8");

                $time = date('H:i', $string);
                $show->time = $time;

                $day_string = strftime('%A', $string);
                $show->day_string = $day_string;

                $day = date('d', $string);
                $show->day = $day;

                $month = strftime('%B', $string);
                $show->month = $month;

                $show->type = 'film';
                $show->title = $show->film->title;
                $show->kids = $show->film->kids;
                $show->img = $show->film->media->first()->landscape;
                $show->slug = route('single.film', $show->film->slug);

                if ($show->film->location->id == 2 || $show->film->location->id == 1) {
                    $show->date_ticket = date('Y-m-d', $string).'T00:00:00';
                    $data = $utility->searchTicketJSON($tickets, $show->film->title, $show->date_ticket, $show->time);
                    if ($data !== false) {
                        $show->ticket_url = $data;
                        $show->ticket = true;
                    } else {
                        $show->ticket = false;
                    }
                }

                $show->title = Utility::cleanUTF($show->title);
                $show->description = Utility::cleanUTF($show->description);
            }

            // Eventi nel calendario
            foreach ($events as $key => $event) {
                $string = strtotime($event->date_time);

                setlocale(LC_TIME, "it_IT.UTF-8");

                $time = date('H:i', $string);
                $event->time = $time;

                $day = date('d', $string);
                $event->day = $day;

                $day_string = strftime('%A', $string);
                $event->day_string = $day_string;

                $month = strftime('%B', $string);
                $event->month = $month;

                $event->date_ticket = date('Y-m-d', $string).'T00:00:00';

                $event->type = 'event';
                $event->img = $event->media->first()->landscape;
                $event->slug = route('single.event', $event->slug);
                $shows->push($event);
            }
        }

        $shows = $shows->sortBy('date_time');

        $news = Post::where([
            ['location_id', '!=', $location->id],
        ])->orderBy('created_at', 'desc')->limit(15)->get();

        $news_count = $news->count();

        if ($news_count <= 15) {
            $news_left = 15 - (int) $news_count;
            $generic_news = Post::where([
                ['location_id', '=', 0],
                ['in_home', '=', 1],
            ])->orderBy('created_at', 'desc')->limit($news_left)->get();
            $news = $news->merge($generic_news);
        }

        return view('public.film.location.index', compact('films', 'shows', 'location', 'news'));
    }


    public function getFilmWithLocationMonth($location)
    {
        $utility = new Utility;
        $location = FilmLocation::where('slug', '=', $location)->first();

        if ($location->id == 2 || $location->id == 1) {
            $tickets = $utility->getTicketJSON();
        }

        $today = date('Y-m-d H:i:s', time());

        $string = strtotime($today);
        $string_end = strtotime($today.'+30 day');

        $start_date = date('Y-m-d H:i:s', $string);
        $end_date = date('Y-m-d H:i:s', $string_end);


        $films = Film::where('location_id', '=', $location->id)->with('media', 'categories')->get();

        $location_group = $location->group;

        $events = Event::where([
            ['date_time', '>=', $today],
            ['date_time', '<', $end_date],
            ['status_id', '=', 2],
        ])->with('location', 'media')->whereHas('location', function($query) use ($location_group) {
            $query->where('group', '=', $location_group);
        })->orderBy('date_time')->get();

        // Se ci sono film
        if (count($films) != 0) {
            foreach ($films as $key => $film) {
                $film_id_list[$key] = $film->id;
            }

            $shows = FilmSchedule::where([
            ['date_time', '>', $start_date],
            ['date_time', '<', $end_date]
            ])->whereIn('film_id', $film_id_list)->with('film', 'film.media')->orderBy('date_time')->get();

            $featured = FilmSchedule::where('date_time', '>=', $start_date)->whereIn('film_id', $film_id_list)->orderBy('date_time')->with('film', 'film.media')->get();
            $films = collect();
            foreach ($featured as $key => $show_feat) {
                $films->push($show_feat->film);
            }

            $films = $films->unique('id');

            foreach ($shows as $key => $show) {
                $string = strtotime($show->date_time);

                setlocale(LC_TIME, "it_IT.UTF-8");

                $time = date('H:i', $string);
                $show->time = $time;

                $day_string = strftime('%A', $string);
                $show->day_string = $day_string;

                $day = date('d', $string);
                $show->day = $day;

                $month = strftime('%B', $string);
                $show->month = $month;

                $show->type = 'film';
                $show->title = $show->film->title;
                $show->kids = $show->film->kids;
                $show->img = $show->film->media->first()->landscape;
                $show->slug = route('single.film', $show->film->slug);

                if ($show->film->location->id == 2 || $show->film->location->id == 1) {
                    $show->date_ticket = date('Y-m-d', $string).'T00:00:00';
                    $data = $utility->searchTicketJSON($tickets, $show->film->title, $show->date_ticket, $show->time);
                    if ($data !== false) {
                        $show->ticket_url = $data;
                        $show->ticket = true;
                    } else {
                        $show->ticket = false;
                    }
                }
                // dd($show);
                $show->title = Utility::cleanUTF($show->title);
                $show->description = Utility::cleanUTF($show->description);
            }

            // Eventi nel calendario
            foreach ($events as $key => $event) {
                $string = strtotime($event->date_time);

                setlocale(LC_TIME, "it_IT.UTF-8");

                $time = date('H:i', $string);
                $event->time = $time;

                $day = date('d', $string);
                $event->day = $day;

                $day_string = strftime('%A', $string);
                $event->day_string = $day_string;

                $month = strftime('%B', $string);
                $event->month = $month;

                $event->date_ticket = date('Y-m-d', $string).'T00:00:00';

                $event->type = 'event';
                $event->img = $event->media->first()->landscape;
                $event->slug = route('single.event', $event->slug);
                $shows->push($event);
            }
        }

        $shows = $shows->sortBy('date_time');

        $news = Post::where([
            ['location_id', '!=', $location->id],
        ])->orderBy('created_at', 'desc')->limit(15)->get();

        $news_count = $news->count();

        if ($news_count <= 15) {
            $news_left = 15 - (int) $news_count;
            $generic_news = Post::where([
                ['location_id', '=', 0],
                ['in_home', '=', 1],
            ])->orderBy('created_at', 'desc')->limit($news_left)->get();
            $news = $news->merge($generic_news);
        }

        return view('public.film.location.index', compact('films', 'shows', 'location', 'news'));
    }

    public function getShowsWithLocation($location, $date)
    {
        $utility = new Utility;
        $location = FilmLocation::where('slug', '=', $location)->first();

        if ($location->id == 2 || $location->id == 1) {
            $tickets = $utility->getTicketJSON();
        }

        $films = Film::where('location_id', '=', $location->id)->with('media')->get();

        if (count($films) != 0) {
            foreach ($films as $key => $film) {
                $film_id_list[$key] = $film->id;
            }

            $string = strtotime($date);
            $string_end = strtotime($date.'+1 day');

            $start_date = date('Y-m-d H:i:s', $string);
            $end_date = date('Y-m-d H:i:s', $string_end);

            $events = Event::where($query = [
            ['date_time', '>=', $start_date],
            ['date_time', '<', $end_date],
            ['status_id', '=', 2],
            ])->orderBy('date_time')->with('location', 'media')->get();

            // Corrispondenza location film con location eventi
            if ($location->slug == 'cinema-mic' || $location->slug == 'terrazza-mic') {
                $events = $events->filter(function ($event, $key) {
                    return ($event->location_id == 10 || $event->location_id == 15);
                });
            } elseif ($location->slug == 'spazio-oberdan') {
                $events = $events->filter(function ($event, $key) {
                    return ($event->location_id == 12 || $event->location_id == 16);
                });
            } elseif ($location->slug == 'area-metropolis') {
                $events  = $events->filter(function ($event, $key) {
                    return ($event->location_id == 14 || $event->location_id == 17);
                });
            }

            $shows = FilmSchedule::where([
            ['date_time', '>', $start_date],
            ['date_time', '<', $end_date]
            ])->whereIn('film_id', $film_id_list)->with('film')->orderBy('date_time')->get();

            $items = collect();

            foreach ($shows as $key => $show) {

                // Formatto l'ora e la data
                $string = strtotime($show->date_time);
                setlocale(LC_TIME, "it_IT.UTF-8");
                $time = date('H:i', $string);
                $show->time = $time;
                $day = date('d', $string);
                $show->day = $day;
                $day_string = strftime('%A', $string);
                $show->day_string = $day_string;
                $month = strftime('%B', $string);
                $show->month = $month;
                $show->date_ticket = date('Y-m-d', $string).'T00:00:00';
                $show->title = $show->film->title;
                $show->url = route('single.film', $show->film->slug);
                if ($show->film->media->first()) {
                    $show->img = Storage::disk('local')->url($show->film->media->first()->landscape);
                } else {
                    $show->img = 'none';
                }
                // Biglietti
                if ($show->film->location->id == 2 || $show->film->location->id == 1) {
                    $data = $utility->searchTicketJSON($tickets, $show->film->title, $show->date_ticket, $show->time);
                    if ($data !== false) {
                        $show->ticket_url = $data;
                        $show->ticket = true;
                    } else {
                        $show->ticket = false;
                    }
                }

                // Cambio il link dell'immagine
                $show->film->img = str_replace('public', 'storage', $show->film->media->first()->landscape);

                // Ottengo l'oggetto location
                $show->location = $show->film->location;

                // Ottengo il nome delle rassegne collegate
                $exhibitions = collect();
                foreach ($show->exhibitions($show->film->id) as $key => $exhibition) {
                    $exhibitions->push($exhibition);
                }
                $show->exhibitions = $exhibitions;

                // Ottengo il nome delle categorie collegate allo shows
                $film_categories = collect();
                foreach ($show->film->categories as $key => $film_category) {
                    $film_categories->push($film_category);
                }
                $show->film_categories = $film_categories;


                // Accorcio la descrizione
                $description = substr(strip_tags($show->film->description), 0, 150);
                $dots = strlen(strip_tags($show->film->description)) > 150 ? '...' : "";
                $show->description = $description.$dots;

                $show->type = 'film';

                $show->title = Utility::cleanUTF($show->title);
                $show->description = Utility::cleanUTF($show->description);

                $items->push($show);
            }

            foreach ($events as $key => $event) {
                $string = strtotime($event->date_time);

                setlocale(LC_TIME, "it_IT.UTF-8");

                $time = date('H:i', $string);
                $event->time = $time;

                $day = date('d', $string);
                $event->day = $day;

                $day_string = strftime('%A', $string);
                $event->day_string = $day_string;

                $month = strftime('%B', $string);
                $event->month = $month;

                $event->date_ticket = date('Y-m-d', $string).'T00:00:00';

                $event->type = 'event';
                $event->img = Storage::disk('local')->url($event->media->first()->landscape);
                $event->url = route('single.event', $event->slug);

                // Accorcio la descrizione
                $description = substr(strip_tags($event->description), 0, 150);
                $dots = strlen(strip_tags($event->description)) > 150 ? '...' : "";
                $event->description = $description.$dots;

                // $shows->push($event);
                $items->push($event);
            }

            $shows = $items->sortBy('date_time')->values();
        }
        return response()->json($shows, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }

    /*
    *
    * RASSEGNE
    *
    */

    public function getExhibitionSingle($slug)
    {
        $exhibition = Exhibition::where('slug', '=', $slug)->with('media', 'films', 'films.location', 'event', 'event.eventType', 'event.location')->first();
        $utility = new Utility;

        $elements = collect();
        $shows = collect();
        $films = collect();
        foreach ($exhibition->event as $key => $event) {
            $string = strtotime($event->date_time);
            setlocale(LC_TIME, "it_IT.UTF-8");
            $time = date('H:i', $string);
            $day_string = strftime('%A', $string);
            $day = date('d', $string);
            $month = strftime('%B', $string);
            $year = strftime('%G', $string);
            $event->date_ticket = date('Y-m-d', $string).'T00:00:00';
            // Preparo i dati per il programma
            $dataElements = [
                'id' => $event->id,
                'title' => $event->title,
                'type' => 'Evento',
                'kids' => $event->kids,
                'img' => $event->media->first()->landscape,
                'description' => $event->description,
                'location' => $event->location->name,
                'url' => 'evento/'.$event->slug,
                'color' => '#2AD68C',
                'date_time' => $event->date_time,
            ];
            $elements->push($dataElements);

            // Preparo dati per il calendario
            $dataShows = [
                'id' => $event->id,
                'type' => 'Evento',
                'time' => $time,
                'day_string' => $day_string,
                'day' => $day,
                'month' => $month,
                'title' => $event->title,
                'kids' => $event->kids,
                'img' => $event->media->first()->landscape,
                'description' => $event->description,
                'location' => $event->location->name,
                'location_slug' => $event->location->slug,
                'url' => 'evento/'.$event->slug,
                'color' => '#2AD68C',
                'date_time' => $event->date_time,
            ];
            $shows->push($dataShows);
        }

        if ($exhibition->films->count() != 0) {

            // Preparo il sistema per cercare i biglietti
            $tickets = $utility->getTicketJSON();

            foreach ($exhibition->films as $key => $film) {
                $film_id_list[$key] = $film->id;

                $film_show = FilmSchedule::where('film_id', '=', $film->id)->orderBy('date_time', 'desc')->first();

                if ($film_show != null) {
                    // Preparo i dati per la programmazione
                    $dataElements = [
                        'id' => $film->id,
                        'title' => $film->title,
                        'type' => 'Film',
                        'kids' => $film->kids,
                        'img' => $film->media->first()->landscape,
                        'description' => $film->description,
                        'location' => $film->location->name,
                        'location_slug' => $film->location->slug,
                        'production_date' => $film->production_date,
                        'region' => $film->region,
                        'url' => 'film/'.$film->slug,
                        'color' => '#CA4A4A',
                        'date_time' => $film_show->date_time,
                    ];
                } else {
                    // Preparo i dati per la programmazione
                    $dataElements = [
                        'id' => $film->id,
                        'title' => $film->title,
                        'type' => 'Film',
                        'kids' => $film->kids,
                        'img' => $film->media->first()->landscape,
                        'description' => $film->description,
                        'location' => $film->location->name,
                        'location_slug' => $film->location->slug,
                        'production_date' => $film->production_date,
                        'region' => $film->region,
                        'url' => 'film/'.$film->slug,
                        'color' => '#CA4A4A',
                        'date_time' => '',
                    ];
                }


                $elements->push($dataElements);
            }

            // preparo i dari per il calendario
            $film_shows = FilmSchedule::whereIn('film_id', $film_id_list)->with('film')->orderBy('date_time')->get();

            foreach ($film_shows as $key => $show) {
                // vecchio
                // $films->push($show->film);

                // Converto il tempo delle proiezioni per la view
                $string = strtotime($show->date_time);
                setlocale(LC_TIME, "it_IT.UTF-8");

                $time = date('H:i', $string);
                $show->time = $time;
                $day_string = strftime('%A', $string);
                $show->day_string = $day_string;
                $day = date('d', $string);
                $show->day = $day;
                $month = strftime('%B', $string);
                $show->month = $month;
                $show->date_ticket = date('Y-m-d', $string).'T00:00:00';

                // Biglietti
                // se è spazio oberdan
                if ($show->film->location->id == 2 || $show->film->location->id == 1) {
                    $show->title = $show->film->title;
                    if ($show->title == 'Io ti salverò') {
                        $show = $utility->searchTicket($tickets, $show);
                    }
                    $show = $utility->searchTicket($tickets, $show);
                }
                if ($show->ticket == null) {
                    $show->ticket = false;
                }

                $show->title = Utility::cleanUTF($show->title);
                $show->description = Utility::cleanUTF($show->description);

                $dataShows = [
                    'id' => $show->id,
                    'type' => 'Proiezione',
                    'time' => $time,
                    'day_string' => $day_string,
                    'day' => $day,
                    'month' => $month,
                    'title' => $show->film->title,
                    'kids' => $show->film->kids,
                    'img' => $show->film->media->first()->landscape,
                    'description' => $show->film->description,
                    'location' => $show->film->location->name,
                    'production_date' => $show->film->production_date,
                    'region' => $show->film->region,
                    'url' => 'film/'.$show->film->slug,
                    'color' => '#CA4A4A',
                    'ticket' => $show->ticket,
                    'ticket_url' => $show->url,
                    'date_time' => $show->date_time
                ];
                $shows->push($dataShows);
            }
        }

        // Ordino le collection
        $shows = $shows->sortBy('date_time');
        $elements = $elements->sortBy('date_time');

        $string_start = strtotime($exhibition->start_date);
        $string_end = strtotime($exhibition->end_date);

        setlocale(LC_TIME, "it_IT.UTF-8");

        $start = strftime('%d %B %G', $string_start);
        $end = strftime('%d %B %G', $string_end);

        $exhibition->start_date = $start;
        $exhibition->end_date = $end;

        $limitedShows = $shows->filter(function ($value, $key) {
            $today = date('Y-m-d H:i:s', time());
            return $value['date_time'] >= $today;
        });

        return view('public.exhibitions.single', compact('exhibition', 'films', 'elements', 'shows', 'limitedShows'));
    }

    public function getExhibitions()
    {
        $today = date('Y-m-d H:i:s', time());
        $exhibitions = Exhibition::where([
            ['end_date', '>', $today],
            ['status_id', '=', 2],
        ])->with('media', 'films')->orderBy('start_date')->paginate(10);

        foreach ($exhibitions as $key => $exhibition) {
            setlocale(LC_TIME, "it_IT.UTF-8");

            $string_start_date = strtotime($exhibition->start_date);
            $start_date = strftime('%d %B %G', $string_start_date);
            $exhibition->start_date = $start_date;

            $string_end_date = strtotime($exhibition->end_date);
            $end_date = strftime('%d %B %G', $string_end_date);
            $exhibition->end_date = $end_date;
        }

        return view('public.exhibitions.index', compact('exhibitions'));
    }

    /*
    *
    * MOSTRE
    *
    */

    public function getExhibitSingle($slug)
    {
        $exhibit = Exhibit::where('slug', '=', $slug)->with('media', 'films', 'films.location')->first();

        $films = collect();

        if ($exhibit->films->count() != 0) {
            foreach ($exhibit->films as $key => $film) {
                $film_id_list[$key] = $film->id;
            }

            $shows = FilmSchedule::whereIn('film_id', $film_id_list)->with('film')->orderBy('date_time')->get();

            foreach ($shows as $key => $show) {
                $films->push($show->film);
            }
        }

        setlocale(LC_TIME, "it_IT.UTF-8");

        $string_start = strtotime($exhibit->start_date);
        $string_end = strtotime($exhibit->end_date);

        $start = strftime('%d %B %G', $string_start);
        $end = strftime('%d %B %G', $string_end);

        $exhibit->start_date = $start;
        $exhibit->end_date = $end;

        return view('public.exhibits.single', compact('exhibit', 'films'));
    }

    public function getExhibits()
    {
        $today = date('Y-m-d H:i:s', time());
        $exhibits = Exhibit::where([
            ['end_date', '>', $today],
            ['status_id', '=', 2],
        ])->with('media', 'films')->orderBy('start_date')->paginate(5);

        foreach ($exhibits as $key => $exhibit) {
            if ($exhibit->end_date >= $today) {
                setlocale(LC_TIME, "it_IT.UTF-8");

                $start = strtotime($exhibit->start_date);
                $end = strtotime($exhibit->end_date);

                $exhibit->start_date = strftime('%d %B %G', $start);
                $exhibit->end_date = strftime('%d %B %G', $end);
            }
        }

        return view('public.exhibits.index', compact('exhibits', 'shows'));
    }

    /*
    *
    * FESTIVAL
    *
    */

    public function getFestivalSingle($slug)
    {
        $festival = Festival::where('slug', '=', $slug)->with('media')->first();
        setlocale(LC_TIME, "it_IT.UTF-8");

        $string_start = strtotime($festival->start_date);
        $string_end = strtotime($festival->end_date);

        $start = strftime('%d %B %G', $string_start);
        $end = strftime('%d %B %G', $string_end);

        $festival->start_date = $start;
        $festival->end_date = $end;

        $relations = FestivalRelation::where('festival_id', '=', $festival->id)->get();
        $elements = collect();
        $shows = collect();
        foreach ($relations as $key => $relation) {
            if ($relation->film_id != null) {
                $film = Film::find($relation->film_id);

            // prendo le informazioni delle proiezioni
            foreach ($film->shows as $key => $show) {
                // Salvo la prima data
            if ($key == 0) {
                $date_time = $show->date_time;
            } else {
                // Se la data salvata è maggiore di quella salvata, riassegno
            if ($date_time > $show->date_time) {
                $date_time = $show->date_time;
            }
        }

        // Converto il tempo delle proiezioni per la view
        $string = strtotime($show->date_time);
        setlocale(LC_TIME, "it_IT.UTF-8");
        $time = date('H:i', $string);
        $show->time = $time;
        $day = date('d', $string);
        $show->day = $day;
        $month = strftime('%B', $string);
        $show->month = $month;

        $show->title = Utility::cleanUTF($show->title);
        $show->description = Utility::cleanUTF($show->description);

        $item = [
            'id' => $show->id,
            'time' => $time,
            'day' => $day,
            'month' => $month,
            'title' => $film->title,
            'kids' => $film->kids,
            'img' => $film->media->first()->landscape,
            'description' => $film->description,
            'location' => $film->location->name,
            'production_date' => $film->production_date,
            'region' => $film->region,
            'labels' => [
                'exhibits' => $film->exhibits,
                'exhibitions' => $film->exhibitions,
            ],
            'url' => 'film/'.$film->slug,
            'color' => '#CA4A4A'
        ];
        $shows->push($item);
    }

                $item = [
                    'id' => $relation->id,
                    'title' => $film->title,
                    'type' => 'Film',
                    'kids' => $film->kids,
                    'img' => $film->media->first()->landscape,
                    'description' => $film->description,
                    'url' => 'film/'.$film->slug,
                    'color' => '#CA4A4A',
                    'date_time' => $date_time,
                ];
                $elements->push($item);
            }

            if ($relation->exhibit_id != null) {
                $exhibit = Exhibit::find($relation->exhibit_id);
                $item = [
                    'id' => $relation->id,
                    'title' => $exhibit->title,
                    'type' => 'Esposizione',
                    'kids' => $exhibit->kids,
                    'img' => $exhibit->media->first()->landscape,
                    'description' => $exhibit->description,
                    'url' => 'esposizione/'.$exhibit->slug,
                    'color' => '#48DEE4',
                    'date_time' => $exhibit->start_date,
                ];
                $elements->push($item);
            }

            if ($relation->exhibition_id != null) {
                $exhibition = Exhibition::find($relation->exhibition_id);
                $item = [
                    'id' => $relation->id,
                    'title' => $exhibition->title,
                    'type' => 'Rassegna',
                    'kids' => $exhibition->kids,
                    'img' => $exhibition->media->first()->landscape,
                    'description' => $exhibition->description,
                    'url' => 'rassegna/'.$exhibition->slug,
                    'color' => '#F1B340',
                    'date_time' => $exhibition->start_date,
                ];
                $elements->push($item);
            }

            if ($relation->event_id != null) {
                $event = Event::find($relation->event_id);
                $item = [
                    'id' => $relation->id,
                    'title' => $event->title,
                    'type' => 'Evento',
                    'kids' => $event->kids,
                    'img' => $event->media->first()->landscape,
                    'description' => $event->description,
                    'url' => 'evento/'.$event->slug,
                    'color' => '#755482',
                    'date_time' => $event->date_time,
                ];
                $elements->push($item);
            }
        }

        $elements = $elements->sortByDesc('date_time');

        // Piccolo Grande Cinema
        if ($festival->id == 2) {
            $news = Post::where('category_id', '=', 12)->orderBy('created_at', 'desc')->get();

            // Twitter
            $url = 'https://api.twitter.com/1.1/search/tweets.json';
                $getfield = '?q=@CinetecaMilano&count=9';
                $requestMethod = 'GET';
                $settings = [
                    'oauth_access_token' => env('TW_OAUTH_ACCESS_TOKEN'),
                    'oauth_access_token_secret' => env('TW_OAUTH_ACCESS_TOKEN_SECRET'),
                    'consumer_key' => env('TW_CONSUMER_KEY'),
                    'consumer_secret' => env('TW_CONSUMER_SECRET')
                ];

                $twitter = new \TwitterAPIExchange($settings);
                $response = $twitter->setGetfield($getfield)
                    ->buildOauth($url, $requestMethod)
                    ->performRequest();

                $response = json_decode($response, true);
                // dd($response);
                $tweets = $response['statuses'];

            // dd($tweets);

            return view('public.festivals.pgc', compact('festival', 'elements', 'shows', 'news', 'tweets'));
        }

        // Cineteca 70
        if ($festival->id == 3) {
            $news = Post::where('category_id', '=', 13)->get();
        }

        $pages = Page::where([
        ['festival', '=', 1],
        ['festival_id', '=', $festival->id],
        ['status_id', '=', 2],
        ])->get();

        foreach ($pages as $key => $page) {
            $page->img = Storage::disk('local')->url($page->media()->first()->thumb);
            $page->url = route('single.page', $page->slug);
        }

        return view('public.festivals.single', compact('festival', 'elements', 'shows', 'news', 'pages'));
    }

    public function getFestivals()
    {
        $today = date('Y-m-d H:i:s', time());
        $festivals = Festival::where('end_date', '>=', $today)->with('media')->orderBy('start_date')->paginate(5);
        $currents = collect();

        foreach ($festivals as $key => $festival) {
            if ($festival->end_date >= $today) {
                setlocale(LC_TIME, "it_IT.UTF-8");

                $start = strtotime($festival->start_date);
                $end = strtotime($festival->end_date);

                $festival->start_date = strftime('%d %B %G', $start);
                $festival->end_date = strftime('%d %B %G', $end);

                $currents->push($festival);
            }
        }

        return view('public.festivals.index', compact('festivals', 'currents'));
    }


    /*
    *
    * Eventi
    *
    */

    public function getEventSingle($slug)
    {
        $event = Event::where('slug', '=', $slug)->with('media', 'location', 'film', 'film.media', 'film.location')->first();
        $string = strtotime($event->date_time);
        setlocale(LC_TIME, "it_IT.UTF-8");
        $time = date('H:i', $string);
        $event->time = $time;
        $day = date('d', $string);
        $event->day = $day;
        $month = strftime('%B', $string);
        $event->month = $month;
        $year = strftime('%G', $string);
        $event->year = $year;
        $event->date_ticket = date('Y-m-d', $string).'T00:00:00';

        return view('public.events.single', compact('event'));
    }

    /*
    *
    * Rsvp per evento
    *
    */

    public function checkAvilability(Request $request)
    {
        $event = Event::find($request->event_id);
        $attempt = $request->guests + $request->kids_guest;
        if ($attempt <= $event->guest_left) {
            // primo caso in cui tutti i posti sono disponibili
        $data = [
            'available' => true,
        ];
        } elseif ($attempt > $event->guest_left && $event->wait_left > 0) {
            $wait_attempt = $attempt - $event->guest_left;
            if ($wait_attempt < $event->wait_left && $event->wait_left > 0) {
                // Secondo caso, la richiesta supera i posti disponibili ma ce ne sono in lista d'attesa
                $data = [
                    'available' => true,
                    'alert' => true,
                    'left' => $event->guest_left,
                    'wait' => $wait_attempt,
                ];
            } else {
                // Terzo caso, la richiesta supera i posti disponibili e sono esauriti anche quelli in lista d'attesa
                $data = [
                    'available' => false
                ];
            }
        } else {
            // quarto caso, nessuna delle condizioni precedenti è vera
            $data = [
                'available' => false
            ];
        }

        return response()->json($data);
    }

    public function rsvp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'email|required',
            'phone' => 'required',
            'guests' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        $event = Event::find($request['event_id']);

        $check = Guest::where([
        ['email', '=', $request['email']],
        ['event_id', '=', $event->id]
        ])->count();

        // se la mail esiste già
        if ($check > 0) {
            $data = [
                'message' => '<h3 class="p-5 text-center text-warning">Attenzione, la tua mail è già presente in lista</h3><p class="text-center text-warning">Se ti servono ulteriori informazioni, scrivici una mail.</p>'
            ];
            return response()->json($data);
        }

        $attempt = $request['guests'] + $request['kids_guest'];

        //Se il numero della guest list è minore dei partecipanti massimi inserisco nella lista degli ospiti
        if ($attempt <= $event->guest_left) {

        // Salvo il nuovo guest
        $guest = new Guest;
        $guest->name = $request['name'];
        $guest->surname = $request['surname'];
        $guest->email = $request['email'];
        $guest->phone = $request['phone'];
        $guest->guests = $request['guests'];
        $guest->kids = $request['kids_guest'];
        $guest->event_id = $request['event_id'];

        // Status 0 = accettato
        $guest->status = 0;
        $guest->save();

        // Tolgo un partecipante dal contatore dell'evento
        $event->guest_left = $event->guest_left - ($request['guests'] + $request['kids_guest']);
        $event->save();

        // Preparo i dati per la mail
        $string = strtotime($event->date_time);
        setlocale(LC_TIME, "it_IT.UTF-8");
        $time = date('H:i', $string);
        $event->time = $time;
        $day = date('d', $string);
        $event->day = $day;
        $month = strftime('%B', $string);
        $event->month = $month;
        $year = strftime('%G', $string);
        $event->year = $year;

        // Recupero il messaggio
        $message = RsvpMessage::where([
        ['type', '=', 1],
        ['event_id', '=', $event->id]
        ])->first();

        $vars = [
            '@nome' => $request['name'],
            '@cognome' => $request['surname'],
            '@ospiti' => $request['guests'],
            '@bambini' => $request['kids_guest'],
            '@giorno' => $event->day,
            '@mese' => $event->month,
            '@anno' => $event->year,
            '@orario' => $event->time,
        ];

        $message->message = strtr($message->message, $vars);


        $mail_data = [
            'message' => $message->message
        ];

        // Invio la mail
        Mail::to($request['email'])->send(new RsvpConfirmedGuestList($mail_data));

        // Ritorno il messaggio di successo
        $data = [
            'msg' => $message->message,
            'message' => '<h3 class="p-5 text-center text-success">Grazie per esserti registrato!</h3><p class="text-center text-success">A breve riceverai una mail di conferma con tutte le indicazioni</p>',
        ];

        return response()->json($data);
        // Se il numero di partecipanti della richiesta supera i posti rimasti e c'è una lista d'attesa
        } elseif ($attempt > $event->guest_left && $event->guest_left > 0) {
            $wait_attempt = $attempt - $event->guest_left;
            if ($wait_attempt < $event->wait_left && $event->wait_left > 0) {
                $guaranteed = $event->guest_left;
                // ospiti garantiti
                $guest = new Guest;
                $guest->name = $request['name'];
                $guest->surname = $request['surname'];
                $guest->email = $request['email'];
                $guest->phone = $request['phone'];
                $guest->guests = $guaranteed;
                $guest->kids = 0;
                $guest->event_id = $request['event_id'];
                $guest->status = 0;
                $guest->save();

                $event->guest_left = 0;
                $event->save();

                // ospiti in lista d'attesa
                $guest = new Guest;
                $guest->name = $request['name'];
                $guest->surname = $request['surname'];
                $guest->email = $request['email'];
                $guest->phone = $request['phone'];
                $guest->guests = $wait_attempt;
                $guest->kids = 0;
                $guest->event_id = $request['event_id'];
                $guest->status = 1;
                $guest->save();

                $event->wait_left = $event->wait_left - $wait_attempt;
                $event->save();

                // Recupero il messaggio
                $message = RsvpMessage::where([
                    ['type', '=', 1],
                    ['event_id', '=', $event->id]
                ])->first();

                $vars = [
                    '@nome' => $request['name'],
                    '@cognome' => $request['surname'],
                    '@ospiti' => $guaranteed,
                    '@bambini' => 0,
                    '@giorno' => $event->day,
                    '@mese' => $event->month,
                    '@anno' => $event->year,
                    '@orario' => $event->time,
                ];

                $message->message = strtr($message->message, $vars);


                $mail_data = [
                    'message' => $message->message
                ];

                // Invio la mail
                Mail::to($request['email'])->send(new RsvpConfirmedGuestList($mail_data));


                // Ritorno il messaggio di successo
                $data = [
                    'message' => '<h3 class="p-5 text-center text-warning">Grazie per esserti registrato!</h3><p class="text-center text-warning"><b>Alcuni dei tuoi ospiti sono stati aggiunti in lista d\'attesa</b><br>A breve riceverai una mail di conferma con tutte le indicazioni</p>',
                ];
                return response()->json($data);
            } else {
                // Se anche la lista d'attesa è terminata ritorno un messaggio di problema
                $data = [
                    'message' => '<h3 class="p-5 text-center text-danger">Siamo spiacenti ma i posti sono terminati!</h3><p class="text-center text-danger">Purtroppo tutti i posti sono terminati</p>',
                ];
                return response()->json($data);
            }
        // Se la lista principale è terminata provo ad inserirlo nella lista d'attesa
        } else {

            //Se il numero della guest list è minore dei partecipanti massimi inserisco nella lista d'attesa
            if ($event->wait_left > 0) {
                // Salvo il nuovo guest
                $guest = new Guest;
                $guest->name = $request['name'];
                $guest->surname = $request['surname'];
                $guest->email = $request['email'];
                $guest->phone = $request['phone'];
                $guest->guests = $request['guests'];
                $guest->kids = $request['kids_guest'];
                $guest->event_id = $request['event_id'];

                // Status 1 = in lista d'attesa
                $guest->status = 1;
                $guest->save();

                // Tolgo un partecipante dal contatore dell'evento
                $event->wait_left = $event->wait_left - ($request['guests'] + $request['kids_guest']);
                $event->save();

                // Recupero il messaggio
                $message = RsvpMessage::where([
                ['type', '=', 2],
                ['event_id', '=', $event->id]
                ])->first();

                $vars = [
                    '@nome' => $request['name'],
                    '@cognome' => $request['surname'],
                    '@ospiti' => $request['guests'],
                    '@bambini' => $request['kids_guest'],
                    '@giorno' => $event->day,
                    '@mese' => $event->month,
                    '@anno' => $event->year,
                    '@orario' => $event->time,
                ];

                $message->message = strtr($message->message, $vars);


                $mail_data = [
                    'message' => $message->message
                ];

                // Invio la mail
                Mail::to($request['email'])->send(new RsvpConfirmedGuestList($mail_data));


                // Ritorno il messaggio di successo
                $data = [
                    'message' => '<h3 class="p-5 text-center text-warning">Grazie per esserti registrato!</h3><p class="text-center text-warning"><b>I posti sono terminati ma sei stato inserito nella lista d\'attesa</b><br>A breve riceverai una mail di conferma con tutte le indicazioni</p>',
                ];
                return response()->json($data);
            } else {
                // Se anche la lista d'attesa è terminata ritorno un messaggio di problema
                $data = [
                    'message' => '<h3 class="p-5 text-center text-danger">Siamo spiacenti ma i posti sono terminati!</h3><p class="text-center text-danger">Purtroppo tutti i posti sono terminati</p>',
                    'event->wait_left' => $event->wait_left
                ];
                return response()->json($data);
            }
        }
    }

    /*
    *
    * KIDS
    *
    */

    public function kids()
    {
        $today = date('Y-m-d H:i:s', time());
        $films = Film::where('kids', '=', 1)->with('shows', 'media')->get();

        $shows = collect();

        foreach ($films as $key => $film) {
            foreach ($film->shows()->get() as $key => $show) {
                if ($show->date_time >= $today) {
                    $show->title = Utility::cleanUTF($show->title);
                    $show->description = Utility::cleanUTF($show->description);
                    $shows->push($film);
                }
            }
        }

        $films = $shows;

        $exhibits = Exhibit::where([
        ['kids', '=', 1],
        ['end_date', '<=', $today],
        ['status_id', '=', 2]
        ])->with('media')->get();

        $exhibitions = Exhibition::where([
        ['kids', '=', 1],
        ['end_date', '>=', $today],
        ['status_id', '=', 2]
        ])->with('media')->get();

        $events = Event::where([
        ['kids', '=', 1],
        ['date_time', '>=', $today],
        ['status_id', '=', 2]
        ])->with('media')->get();

        $festivals = Festival::where([
        ['kids', '=', 1],
        ['end_date', '<=', $today],
        ['status_id', '=', 2]
        ])->with('media')->get();

        $pages = Page::where([
        ['kids', '=', 1],
        ['status_id', '=', 2]
        ])->with('media')->get();

        $items = collect();

        foreach ($films as $key => $film) {
            $item = [
                'id' => $film->id,
                'title' => $film->title,
                'type' => 'Film',
                'kids' => $film->kids,
                'img' => $film->media->first()->landscape,
                'slide_img' => $film->media->first()->slide_img,
                'description' => $film->description,
                'slug' => 'film/'.$film->slug,
                'color' => '#CA4A4A',
            ];
            $items->push($item);
        }

        foreach ($exhibits as $key => $exhibit) {
            $item = [
                'id' => $exhibit->id,
                'title' => $exhibit->title,
                'type' => 'Esposizione',
                'kids' => $exhibit->kids,
                'img' => $exhibit->media->first()->landscape,
                'slide_img' => $exhibit->media->first()->slide_img,
                'description' => $exhibit->description,
                'slug' => 'esposizione/'.$exhibit->slug,
                'color' => '#48DEE4',
            ];
            $items->push($item);
        }

        foreach ($exhibitions as $key => $exhibition) {
            $item = [
                'id' => $exhibition->id,
                'title' => $exhibition->title,
                'type' => 'Rassegna',
                'kids' => $exhibition->kids,
                'img' => $exhibition->media->first()->landscape,
                'slide_img' => $exhibition->media->first()->slide_img,
                'description' => $exhibition->description,
                'slug' => 'rassegna/'.$exhibition->slug,
                'color' => '#48DEE4',
            ];
            $items->push($item);
        }

        foreach ($festivals as $key => $festival) {
            $item = [
                'id' => $festival->id,
                'title' => $festival->title,
                'type' => 'Festival',
                'kids' => $festival->kids,
                'img' => $festival->media->first()->landscape,
                'slide_img' => $festival->media->first()->slide_img,
                'description' => $festival->description,
                'slug' => 'festival/'.$festival->slug,
                'color' => '#48DEE4',
            ];
            $items->push($item);
        }

        foreach ($events as $key => $event) {
            $item = [
                'id' => $event->id,
                'title' => $event->title,
                'type' => 'Evento',
                'kids' => $event->kids,
                'img' => $event->media->first()->landscape,
                'slide_img' => $event->media->first()->slide_img,
                'description' => $event->description,
                'slug' => 'evento/'.$event->slug,
                'color' => '#48DEE4',
            ];
            $items->push($item);
        }
        return view('public.kids.index', compact('items', 'pages'));
    }


    /*
    *
    * Rassegna Stampa / Press Release
    *
    */

    public function indexPress()
    {
        $releases = Release::where('status_id', '=', 2)->orderBy('date_time', 'desc')->with('presses')->get();

        foreach ($releases as $key => $release) {
            $string = strtotime($release->date_time);
            setlocale(LC_TIME, "it_IT.UTF-8");
            $time = date('H:i', $string);
            $release->time = $time;
            $day_string = strftime('%A', $string);
            $release->day_string = $day_string;
            $day = date('d', $string);
            $release->day = $day;
            $month = strftime('%B', $string);
            $release->month = $month;
            $year = strftime('%G', $string);
            $release->year = $year;
        }

        return view('public.press.index', compact('releases'));
    }

    public function singlePress($slug)
    {
        $release = Release::where('slug', '=', $slug)->with('presses')->first();
        $string = strtotime($release->date_time);
        setlocale(LC_TIME, "it_IT.UTF-8");
        $time = date('H:i', $string);
        $release->time = $time;
        $day_string = strftime('%A', $string);
        $release->day_string = $day_string;
        $day = date('d', $string);
        $release->day = $day;
        $month = strftime('%B', $string);
        $release->month = $month;
        $year = strftime('%G', $string);
        $release->year = $year;
        return view('public.press.single', compact('release'));
    }


    /*
    *
    * Schools
    *
    */

    public function schools()
    {
        $school = Category::where('name', '=', 'Scuole')->first();
        $pages = Page::where('category_id', '=', $school->id)->with('media')->orderBy('order')->get();
        return view('public.educational.index', compact('pages'));
    }


    /*
    *
    * Museo
    *
    */

    public function museum()
    {
        $museum = Category::where('name', '=', 'MIC - Museo Interattivo Del Cinema')->first();
        $pages = Page::where([
        ['category_id', '=', $museum->id],
        ['status_id', '=', '2']
        ])->with('media')->orderBy('order')->get();
        return view('public.museum.index', compact('pages'));
    }


    /*
    *
    * Biblioteca
    *
    */

    public function biblio()
    {
        $biblio = Category::where('name', '=', 'Biblioteca Di Morando')->first();
        $pages = Page::where([
        ['category_id', '=', $biblio->id],
        ['status_id', '=', '2']
        ])->with('media')->orderBy('order')->get();

        $static = StaticPage::where('slug', '=', 'biblioteca')->with('gallery')->first();
        $galleries = $static->gallery;
        $medias = collect();
        foreach ($galleries as $key => $gallery) {
            foreach ($gallery->medias as $key => $media) {
                $medias->push($media);
            }
        }

        $funders = Funder::all();
        foreach ($funders as $key => $funder) {
            $funder->img = Storage::disk('local')->url($funder->media()->first()->thumb);
        }


        return view('public.biblio.index', compact('pages', 'medias', 'funders'));
    }


    /*
    *
    * Archivio
    *
    */

    public function archive()
    {
        $archive = Category::where('name', '=', 'Archivio e MIC Lab')->first();
        $pages = Page::where([
        ['category_id', '=', $archive->id],
        ['status_id', '=', '2']
        ])->with('media')->orderBy('order')->get();
        return view('public.archive.index', compact('pages'));
    }

    public function archivioFilm()
    {
        $page = Page::where('slug', '=', 'archiviofilm')->with('media', 'pageLinks', 'pageLinks.page')->first();
        return view('public.page.single', compact('page'));
    }


    /*
    *
    * Archivio
    *
    */

    public function amministrazione()
    {
        return view('public.amministrazione.index');
    }


    /*
    *
    * Galleria
    *
    */

    public function gallery()
    {
        $footer = StaticPage::where('slug', '=', 'footer')->with('gallery')->first();
        $galleries = $footer->gallery;
        $galleries = $galleries->sortByDesc('created_at');

        return view('public.gallery.index', compact('galleries'));
    }
}
