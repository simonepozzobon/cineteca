<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GoogleAnalytics;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $google = new GoogleAnalytics;
      $analytics = $google->initialize();
      $response = $google->getReport($analytics);
      $results = $google->printResults($response);
      $metrics = $results[0]['values'];
      $user_sessions = $metrics[0]['sessions'];

      return view('admin', compact('user_sessions'));
    }
}
