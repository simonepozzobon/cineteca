<?php

namespace App\Http\Controllers;

use Validator;
use App\Order;
use App\Product;
use App\Shipping;
use App\OrderProduct;
use App\ProductGroup;
use App\PicCollection;
use App\FeaturedSlider;
use Braintree_Customer;
use App\CinestoreSlider;
use App\ProductCategory;
use Braintree_CreditCard;
use Braintree_Transaction;
use App\Mail\OrderFail;
use App\Mail\OrderSuccess;
use App\Mail\OrderSuccessAdmin;
use Braintree_Subscription;
use Illuminate\Http\Request;
use Braintree_PaymentMethodNonce;
use Braintree_WebhookNotification;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class CinestoreController extends Controller
{
    public function index()
    {
        $products = Product::with('category', 'media')->orderBy('name')->get();
        $product_categories = ProductCategory::all();
        $products_group = ProductGroup::with('media')->get();

        $feats = FeaturedSlider::all();

        $general_products = $feats->filter(function($feat, $key){
            return $feat->category_id == 0;
        })->all();

        $photo_products = $feats->filter(function($feat, $key){
            return $feat->category_id == 4;
        })->all();

        $video_products = $feats->filter(function($feat, $key){
            return $feat->category_id == 5;
        })->all();

        $products_group = $feats->filter(function($feat, $key){
            return $feat->category_id == 6;
        })->all();

        // Slider
        $cinestoreSliders = CinestoreSlider::all();
        $slides = collect();

        foreach ($cinestoreSliders as $key => $slider) {
            $slide = $slider->sliderable()->first();
            if ($slider->sliderable_type == 'App\Product') {
                $category = $slide->category->slug;
                $slug = $slide->slug;
                $slide->url = route('cinestore.single', [$category, $slug]);
            }
            else {
                // $category = $slide->category->slug;
                $slug = $slide->slug;
                $slide->url = route('cinestore.single', ['percorsi', $slug]);
            }
            $slide->img = Storage::disk('local')->url($slide->media()->first()->slide_img);
            $slides->push($slide);
        }


        return view('public.cinestore.index', compact('products', 'product_categories', 'general_products', 'photo_products', 'video_products', 'products_group', 'slides'));
    }

    public function singleProduct($cat_slug, $slug)
    {
        // numero random
        $feat_prods = Product::inRandomOrder()->limit(3)->get();
        $last_prod = Product::latest()->first();
        $product_categories = ProductCategory::all();

        switch ($cat_slug) {
            case 'video':
                $product = Product::with('category', 'video')->where('slug', '=', $slug)->first();

                // estraggo l'id del video se presente per passarlo alla view
                if ($product->video()->count() > 0) {
                    $str = $product->video->first()['url'];
                    $re = '/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([‌​0-9]{6,11})[?]?.*/';
                    if(preg_match($re, $str, $output)) {
                        $videoSrc = $output[5];
                    }
                }
                return view('public.cinestore.single.video', compact('product', 'videoSrc', 'feat_prods', 'last_prod', 'product_categories'));
                break;

            case 'percorsi':
                $products_group = ProductGroup::with('media', 'products', 'products.media', 'products.category')->where('slug', '=', $slug)->first();
                return view('public.cinestore.single.group', compact('products_group', 'feat_prods', 'last_prod', 'product_categories'));
                break;

            case 'foto':
                $product = Product::with('category', 'video')->where('slug', '=', $slug)->first();

                return view('public.cinestore.single.pic', compact('product', 'feat_prods', 'last_prod', 'product_categories'));
                break;

            default:
                $product = Product::with('category', 'video', 'media')->where('slug', '=', $slug)->first();

                // estraggo l'id del video se presente per passarlo alla view
                if ($product->video()->count() > 0) {
                    $str = $product->video->first()['url'];
                    $re = '/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([‌​0-9]{6,11})[?]?.*/';
                    if(preg_match($re, $str, $output)) {
                        $videoSrc = $output[5];
                    }
                }
                return view('public.cinestore.single', compact('product', 'videoSrc', 'feat_prods', 'last_prod', 'product_categories'));
                break;
        }

    }

    public function cart()
    {
        $product_categories = ProductCategory::all();
        return view('public.cinestore.cart', compact('product_categories'));
    }

    public function order()
    {
        $product_categories = ProductCategory::all();
        return view('public.cinestore.order', compact('product_categories'));
    }

    public function orderStore(Request $request)
    {
        // Se i dati di fatturazione sono gli stessi della spedizione
        if ($request['same_as_shipping'] == 1) {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'surname' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'address' => 'required',
                'CAP' => 'required',
                'city' => 'required',
                'state' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray(),
                ], 400);
            }

            $order = new Order;
            $order->order_status_id = 6;
            $order->name = $request['name'];
            $order->surname = $request['surname'];
            $order->phone = $request['phone'];
            $order->email = $request['email'];
            $order->address = $request['address'];
            $order->CAP = $request['CAP'];
            $order->city = $request['city'];
            $order->state = $request['state'];
            $order->same_as_shipping = $request['same_as_shipping'];
            $order->b_name = $request['name'];
            $order->b_surname = $request['surname'];
            $order->b_phone = $request['phone'];
            $order->b_email = $request['email'];
            $order->b_address = $request['address'];
            $order->b_CAP = $request['CAP'];
            $order->b_city = $request['city'];
            $order->b_state = $request['state'];
            $order->save();

        // Se i dati di fatturazione invece sono diversi dalla spedizione
        } elseif ($request['same_as_shipping'] == 0) {

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'surname' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'address' => 'required',
                'CAP' => 'required',
                'city' => 'required',
                'state' => 'required',
                'b_name' => 'required',
                'b_surname' => 'required',
                'b_phone' => 'required',
                'b_email' => 'required',
                'b_address' => 'required',
                'b_CAP' => 'required',
                'b_city' => 'required',
                'b_state' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray(),
                ], 400);
            }

            $order = new Order;
            $order->order_status_id = 6;
            $order->name = $request['name'];
            $order->surname = $request['surname'];
            $order->phone = $request['phone'];
            $order->email = $request['email'];
            $order->address = $request['address'];
            $order->CAP = $request['CAP'];
            $order->city = $request['city'];
            $order->state = $request['state'];
            $order->same_as_shipping = $request['same_as_shipping'];
            $order->b_name = $request['b_name'];
            $order->b_surname = $request['b_surname'];
            $order->b_phone = $request['b_phone'];
            $order->b_email = $request['b_email'];
            $order->b_address = $request['b_address'];
            $order->b_CAP = $request['b_CAP'];
            $order->b_city = $request['b_city'];
            $order->b_state = $request['b_state'];
            $order->save();

        }

        // $tot = 0;

        // Per ogni prodotto inserisco il suo valore all'interno della tabella order_products
        foreach ($request['products'] as $key => $product) {
            $ord_prod = new OrderProduct;
            $ord_prod->order_id = $order['id'];
            $ord_prod->product_id = $product['id'];
            $ord_prod->quantity = $product['quantity'];
            $ord_prod->product_price_id = $product['price']['id'];
            $ord_prod->save();

            // $price = number_format($ord_prod->quantity * $ord_prod->product->price);
            // $tot = $tot + $price;
        }

        $order->amount = $request['order']['tot'];
        $order->shipping_cost = $request['order']['shipping'];
        $order->save();

        return response()->json($order->id);
    }

    public function payment(Request $request)
    {
        $product_categories = ProductCategory::all();

        // Recupero l'ordine
        $order = Order::where('id', '=', $request->order_id)->with('orderProducts', 'orderProducts.product')->first();

        // $tot = $order->amount + $order->shipping_cost;

        $result = Braintree_Transaction::sale([
            'amount' => $order->amount,
            'paymentMethodNonce' => $request->payment_method_nonce,
            'options' => [
                'submitForSettlement' => true
            ]
        ]);

        // Se la transazione avviene modifico l'ordine
        if ($result->success) {
            $order->order_status_id = 2;
            $order->paid = $result->transaction->amount;
            $order->transaction_id = $result->transaction->id;
            $order->save();

            // Send success email and receipt
            $mail_data = [
                'name' => $order->name,
                'surname' => $order->surname,
                'order' => $order,
                'products' => $order->orderProducts()->get(),
                'transaction_id' => $order->transaction_id
            ];

            // Invio la mail
            Mail::to($order->email)->send(new OrderSuccess($mail_data));
            Mail::to('cinestore@cinetecamilano.it')->send(new OrderSuccessAdmin($mail_data));


            return view('public.cinestore.payments.success', compact('order', 'product_categories'));
        } else {
            $order->order_status_id = 4;
            $order->save();

            $mail_data = [
                'name' => $order->name,
                'surname' => $order->surname,
                'order' => $order,
                'products' => $order->orderProducts()->get(),
                'transaction_id' => $order->transaction_id
            ];

            // Invio la mail
            Mail::to($order->email)->send(new OrderFail($mail_data));

            // send error mail
            return view('public.cinestore.payments.fail', compact('order', 'product_categories'));
        }
    }

    public function category($slug)
    {
        $product_categories = ProductCategory::all();
        $category = ProductCategory::where('slug', '=', $slug)->first();

        if ($category->id != 6) {
            $products = Product::where([
                ['product_category_id', '=', $category->id],
                ['status_id', '=', 2],
            ])->orderBy('name')->with('picCollections')->get();

            $products = $products->sortBy('order')->values();
        } else {
            $products = ProductGroup::where('status_id', '=', 2)->orderBy('name')->get();
            $products = $products->sortBy('order')->values();
        }

        // Foto
        if ($category->id == 4) {
            $pic_collects = PicCollection::whereHas('products')->with('products')->get();
            return view('public.cinestore.pictures', compact('category', 'products', 'product_categories', 'pic_collects'));
        }
        return view('public.cinestore.category', compact('category', 'products', 'product_categories'));
    }

    public function collection($slug)
    {
        $product_categories = ProductCategory::all();
        $pic_collect = PicCollection::where('slug', '=', $slug)->with('products')->first();

        return view('public.cinestore.single.pic-collection', compact('pic_collect', 'product_categories'));
    }

    public function get_shippings()
    {
        $shippings = Shipping::all();
        return response($shippings);
    }

}
