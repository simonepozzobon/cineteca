<?php

namespace App\Http\Controllers;

use App\Exhibition;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ExhibitionController extends Controller
{
    public function archive()
    {
        $today = Carbon::today()->toDateTimeString();
        $exhibitions = Exhibition::where([
            ['end_date', '<', $today],
            ['status_id', '=', 2],
        ])->with('media', 'films')->orderBy('end_date', 'desc')->paginate(5);

        foreach ($exhibitions as $key => $exhibition) {
            setlocale(LC_TIME, "it_IT.UTF-8");

            $string_start_date = strtotime($exhibition->start_date);
            $start_date = strftime('%d %B %G', $string_start_date);
            $exhibition->start_date = $start_date;

            $string_end_date = strtotime($exhibition->end_date);
            $end_date = strftime('%d %B %G', $string_end_date);
            $exhibition->end_date = $end_date;
        }

        return view('public.exhibitions.index', compact('exhibitions'));
    }
}
