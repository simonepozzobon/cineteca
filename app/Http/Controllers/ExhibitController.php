<?php

namespace App\Http\Controllers;

use App\Exhibit;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ExhibitController extends Controller
{
    public function archive()
    {
        $today = Carbon::today()->toDateTimeString();
        $exhibits = Exhibit::where([
            ['end_date', '<', $today],
            ['status_id', '=', 2],
        ])->with('media', 'films')->orderBy('end_date', 'desc')->paginate(5);

        foreach ($exhibits as $key => $exhibit) {
            if ($exhibit->end_date >= $today) {
                setlocale(LC_TIME, "it_IT.UTF-8");

                $start = strtotime($exhibit->start_date);
                $end = strtotime($exhibit->end_date);

                $exhibit->start_date = strftime('%d %B %G', $start);
                $exhibit->end_date = strftime('%d %B %G', $end);
            }
        }

        return view('public.exhibits.index', compact('exhibits', 'shows'));
    }
}
