<?php

namespace App\Http\Controllers;

use Validator;
use App\Newsletter;
use Illuminate\Http\Request;
// use \DrewM\MailChimp\MailChimp;

class NewsletterController extends Controller
{
    public function addSubscriber(Request $request)
    {

      $validator = Validator::make($request->all(), [
        'email' => 'email|required',
      ]);

      if ($validator->fails()) {
        return response()->json([
          'success' => false,
          'errors' => $validator->getMessageBag()->toArray(),
        ], 400);
      }

      $MailChimp = new \DrewM\MailChimp\MailChimp('84879556110a4a8bbfb12b08922870b4-us4');

      $result = [];

      if ($request['oberdan'] == 0 && $request['metropolis'] == 0 && $request['mic'] == 0 && $request['teacher'] == 0) {
        $request['oberdan'] = 1;
      }

      if ($request['oberdan'] == 1) {
        $list_id = 'a584ae48cb';
        $result[0] = $MailChimp->post("lists/$list_id/members", [
          'email_address' => $request['email'],
          'status'        => 'subscribed',
        ]);
      }

      if ($request['metropolis'] == 1) {
        $list_id = 'b61a6903e5';
        $result[1] = $MailChimp->post("lists/$list_id/members", [
          'email_address' => $request['email'],
          'status'        => 'subscribed',
        ]);
      }

      if ($request['mic'] == 1) {
        $list_id = 'b0bd5184b8';
        $result[2] = $MailChimp->post("lists/$list_id/members", [
          'email_address' => $request['email'],
          'status'        => 'subscribed',
        ]);
      }

      if ($request['teacher']) {
        $list_id = '28d3b92a3a';
        $result[3] = $MailChimp->post("lists/$list_id/members", [
          'email_address' => $request['email'],
          'status'        => 'subscribed',
        ]);
      }

      $newsletter = Newsletter::where('email', '=', $request['email'])->first();
      if ($newsletter) {
        return response([
            'status' => 'already exists',
            'message' => 'L\'indirizzo inserito esiste già.'
        ], 400);
      }

      $newsletter = new Newsletter;
      $newsletter->email = $request['email'];
      $newsletter->oberdan = $request['oberdan'];
      $newsletter->metropolis = $request['metropolis'];
      $newsletter->mic = $request['mic'];
      $newsletter->teacher = $request['teacher'];
      $newsletter->save();

      $data = [
        'errors' => $MailChimp->getLastError()
      ];

      return response()->json($data);
    }
}
