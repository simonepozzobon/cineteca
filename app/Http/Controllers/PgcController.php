<?php

namespace App\Http\Controllers;

use App\Page;
use App\Film;
use App\Event;
use App\Utility;
use App\Festival;
use App\FilmSchedule;
use App\FestivalCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PgcController extends Controller
{
    public function festivalCategory($slug, $category)
    {
      $festival = Festival::where('slug', '=', $slug)->first();
      $category = FestivalCategory::where([
        ['slug', '=', $category],
        ['festival_id', '=', $festival->id]
      ])->first();

      $pages = Page::where([
        ['festival', '=', 1],
        ['festival_id', '=', $festival->id],
        ['status_id', '=', 2],
        ['fest_cat_id', '=', $category->id]
      ])->get();

      $items = collect();

      $film = [
        'title' => 'Tutte le Proiezioni',
        'img' => asset('img/pgc/proiezioni.jpg'),
        'url' => route('single.festival.films', [$slug, $category->slug])
      ];
      $items->push($film);

      if ($category->slug == 'pubblico') {
        $event = [
          'title' => 'Eventi',
          'img' => asset('img/pgc/eventi_thumb.jpg'),
          'url' => route('single.festival.events', [$slug, $category->slug])
        ];
        $items->push($event);
      }

      // Workshop e eventi
      $sem = [
        'title' => 'Seminari',
        'img' => asset('img/pgc/seminari.jpg'),
        'url' => route('single.festival.sems', [$slug, $category->slug])
      ];
      $items->push($sem);

      if ($category->slug == 'scuole') {
        $workshop = [
          'title' => 'Laboratori',
          'img' => asset('img/pgc/workshop_thumb.jpg'),
          'url' => route('single.festival.workshops', [$slug, $category->slug])
        ];
        $items->push($workshop);
      }

      if ($category->slug == 'pubblico') {
        $preview = [
          'title' => 'Anteprime',
          'img' => asset('img/pgc/anteprime.png'),
          'url' => route('single.festival.previews', [$slug, $category->slug])
        ];
        $items->push($preview);
      }



      if ($category->slug == 'scuole') {
        $visit = [
          'title' => 'Visite Guidate',
          'img' => asset('img/pgc/visite_guidate.jpg'),
          'url' => route('single.festival.visits', [$slug, $category->slug])
        ];
        $items->push($visit);
      }

      foreach ($pages as $key => $page) {
        $page->img = Storage::disk('local')->url($page->media()->first()->thumb);
        $page->url = route('single.page', $page->slug);
      }
      $pages = $pages->sortBy('order')->values()->all();
      foreach ($pages as $key => $page) {
        $items->push($page);
      }

      return view('public.festivals.category.index', compact('festival', 'pages', 'category', 'items'));
    }


    /*
     *
     * Seminari nel festival
     *
     */

    public function festivalSem($slug, $category)
    {
      $festival = Festival::where('slug', '=', $slug)->first();
      $category = FestivalCategory::where([
        ['slug', '=', $category],
        ['festival_id', '=', $festival->id]
      ])->first();

      $events = Event::where([
        ['festival', '=', 1],
        ['festival_id', '=', $festival->id],
        ['fest_cat_id', '=', $category->id],
        ['status_id', '=', 2],
        ['event_type_id', '=', 8]
      ])->with('location', 'media')->get();

      foreach ($events as $key => $event) {
        $string = strtotime($event->date_time);
        setlocale(LC_TIME, "it_IT.UTF-8");
        $time = date('H:i', $string);
        $event->time = $time;
        $day = date('d', $string);
        $event->day = $day;
        $day_string = strftime('%A', $string);
        $event->day_string = $day_string;
        $month = strftime('%B', $string);
        $event->month = $month;
        $year = strftime('%G', $string);
        $event->year = $year;

        $event->img = $event->media()->first()->landscape;
      }

      return view('public.festivals.category.sems', compact('festival', 'category', 'events'));
    }

    public function festivalSemWithDate($slug, $category, $date)
    {
      $festival = Festival::where('slug', '=', $slug)->first();
      $category = FestivalCategory::where([
        ['slug', '=', $category],
        ['festival_id', '=', $festival->id]
      ])->first();

      $string = strtotime($date);
      $string_end = strtotime($date.'+1 day');

      $start_date = date('Y-m-d H:i:s', $string);
      $end_date = date('Y-m-d H:i:s', $string_end);

      $events = Event::where([
        ['festival', '=', 1],
        ['festival_id', '=', $festival->id],
        ['fest_cat_id', '=', $category->id],
        ['event_type_id', '=', 8],
        ['date_time', '>=', $start_date],
        ['date_time', '<', $end_date],
        ['status_id', '=', 2],
      ])->orderBy('date_time')->with('location', 'media', 'eventType')->get();

      foreach ($events as $key => $event) {
        // Formatto la data
        $string = strtotime($event->date_time);
        setlocale(LC_TIME, "it_IT.UTF-8");
        $time = date('H:i', $string);
        $event->time = $time;
        $day = date('d', $string);
        $event->day = $day;
        $day_string = strftime('%A', $string);
        $event->day_string = $day_string;
        $month = strftime('%B', $string);
        $event->month = $month;
        $year = strftime('%G', $string);
        $event->year = $year;

        // Modifico la descrizione
        $event->img = Storage::disk('local')->url($event->media()->first()->landscape);
        $description = substr(strip_tags($event->description), 0, 150);
        $dots = strlen(strip_tags($event->description)) > 150 ? '...' : "";
        $event->description = $description.$dots;
      }

      return response()->json($events, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }



    /*
     *
     * Eventi nel festival
     *
     */

    public function festivalEvents($slug, $category)
    {
      $festival = Festival::where('slug', '=', $slug)->first();
      $category = FestivalCategory::where([
        ['slug', '=', $category],
        ['festival_id', '=', $festival->id]
      ])->first();

      $events = Event::where([
        ['festival', '=', 1],
        ['festival_id', '=', $festival->id],
        ['fest_cat_id', '=', $category->id],
        ['status_id', '=', 2],
        ['event_type_id', '!=', 8],
        ['event_type_id', '!=', 6],
      ])->with('location', 'media')->get();

      foreach ($events as $key => $event) {
        $string = strtotime($event->date_time);
        setlocale(LC_TIME, "it_IT.UTF-8");
        $time = date('H:i', $string);
        $event->time = $time;
        $day = date('d', $string);
        $event->day = $day;
        $day_string = strftime('%A', $string);
        $event->day_string = $day_string;
        $month = strftime('%B', $string);
        $event->month = $month;
        $year = strftime('%G', $string);
        $event->year = $year;

        $event->img = $event->media()->first()->landscape;
      }

      return view('public.festivals.category.events', compact('festival', 'category', 'events'));
    }

    public function festivalEventsWithDate($slug, $category, $date)
    {
      $festival = Festival::where('slug', '=', $slug)->first();
      $category = FestivalCategory::where([
        ['slug', '=', $category],
        ['festival_id', '=', $festival->id]
      ])->first();

      $string = strtotime($date);
      $string_end = strtotime($date.'+1 day');

      $start_date = date('Y-m-d H:i:s', $string);
      $end_date = date('Y-m-d H:i:s', $string_end);

      $events = Event::where([
        ['festival', '=', 1],
        ['festival_id', '=', $festival->id],
        ['fest_cat_id', '=', $category->id],
        ['event_type_id', '!=', 8],
        ['event_type_id', '!=', 6],
        ['date_time', '>=', $start_date],
        ['date_time', '<', $end_date],
        ['status_id', '=', 2],
      ])->orderBy('date_time')->with('location', 'media', 'eventType')->get();

      foreach ($events as $key => $event) {
        // Formatto la data
        $string = strtotime($event->date_time);
        setlocale(LC_TIME, "it_IT.UTF-8");
        $time = date('H:i', $string);
        $event->time = $time;
        $day = date('d', $string);
        $event->day = $day;
        $day_string = strftime('%A', $string);
        $event->day_string = $day_string;
        $month = strftime('%B', $string);
        $event->month = $month;
        $year = strftime('%G', $string);
        $event->year = $year;

        // Modifico la descrizione
        $event->img = Storage::disk('local')->url($event->media()->first()->landscape);
        $description = substr(strip_tags($event->description), 0, 150);
        $dots = strlen(strip_tags($event->description)) > 150 ? '...' : "";
        $event->description = $description.$dots;
      }

      return response()->json($events, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }


    /*
     *
     * Anteprime nel festival
     *
     */

    public function festivalPreviews($slug, $category)
    {
      $utility = new Utility;
      $tickets = $utility->getTicketJSON();

      $festival = Festival::where('slug', '=', $slug)->first();
      $category = FestivalCategory::where([
        ['slug', '=', $category],
        ['festival_id', '=', $festival->id]
      ])->first();

      $section = 'anteprime';

      $films = Film::where([
        ['festival', '=', 1],
        ['festival_id', '=', $festival->id],
        ['fest_cat_id', '=', $category->id],
        ['status_id', '=', 2],
      ])->whereHas('categories', function($query) {
        $query->where('id', '=', 6);
      })->with('location', 'media', 'shows', 'categories')->get();

      $items = collect();

      foreach ($films as $key => $film) {
        foreach ($film->shows()->get() as $key => $show) {
          $string = strtotime($show->date_time);
          setlocale(LC_TIME, "it_IT.UTF-8");
          $time = date('H:i', $string);
          $show->time = $time;
          $day = date('d', $string);
          $show->day = $day;
          $day_string = strftime('%A', $string);
          $show->day_string = $day_string;
          $month = strftime('%B', $string);
          $show->month = $month;
          $year = strftime('%G', $string);
          $show->year = $year;
          $show->date_ticket = date('Y-m-d', $string).'T00:00:00';

          $show->title = $film->title;
          $show->description = $film->description;
          $show->slug = $film->slug;
          $show->location = $film->location->name;
          $show->kids = $film->kids;
          $show->img = $film->media()->first()->landscape;

          // Biglietti
          if ($show->film->location->id == 2) {
            $data = $utility->searchTicketJSON($tickets, $show->film->title, $show->date_ticket, $show->time);
            if ($data !== false) {
              $show->ticket_url = $data;
              $show->ticket = true;
            } else {
              $show->ticket = false;
            }
          }

          $items->push($show);
        }
      }

      $items = $items->sortBy('date_time');

      $featured = $films;
      $films = $items->values()->all();

      // dd($films);

      return view('public.festivals.category.films', compact('festival', 'category', 'films', 'featured', 'section'));
    }

    public function festivalPreviewsWithDate($slug, $category, $date)
    {
      $utility = new Utility;
      $tickets = $utility->getTicketJSON();

      $festival = Festival::where('slug', '=', $slug)->first();
      $category = FestivalCategory::where([
       ['slug', '=', $category],
       ['festival_id', '=', $festival->id]
      ])->first();

      $string = strtotime($date);
      $string_end = strtotime($date.'+1 day');

      $start_date = date('Y-m-d H:i:s', $string);
      $end_date = date('Y-m-d H:i:s', $string_end);

      $films = Film::where([
       ['festival', '=', 1],
       ['festival_id', '=', $festival->id],
       ['fest_cat_id', '=', $category->id],
       ['status_id', '=', 2],
      ])->whereHas('categories', function($query) {
       $query->where('id', '=', 6);
      })->with('location', 'media', 'shows', 'categories')->get();

      $items = collect();

      foreach ($films as $key => $film) {
        foreach ($film->shows()->get() as $key => $show) {
          if ($show->date_time >= $start_date && $show->date_time <= $end_date) {
            $string = strtotime($show->date_time);
            setlocale(LC_TIME, "it_IT.UTF-8");
            $time = date('H:i', $string);
            $show->time = $time;
            $day = date('d', $string);
            $show->day = $day;
            $day_string = strftime('%A', $string);
            $show->day_string = $day_string;
            $month = strftime('%B', $string);
            $show->month = $month;
            $year = strftime('%G', $string);
            $show->year = $year;
            $show->date_ticket = date('Y-m-d', $string).'T00:00:00';

            $show->title = $film->title;
            $show->description = $film->description;
            $show->slug = $film->slug;
            $show->location = $film->location->name;
            $show->kids = $film->kids;
            $show->img = Storage::disk('local')->url($film->media()->first()->landscape);

            $show->categories = $film->categories()->get();

            // Modifico la descrizione
            $description = substr(strip_tags($film->description), 0, 150);
            $dots = strlen(strip_tags($film->description)) > 150 ? '...' : "";
            $show->description = $description.$dots;

            // Biglietti
            if ($show->film->location->id == 2) {
              $data = $utility->searchTicketJSON($tickets, $show->film->title, $show->date_ticket, $show->time);
              if ($data !== false) {
                $show->ticket_url = $data;
                $show->ticket = true;
              } else {
                $show->ticket = false;
              }
            }

            $show->title = Utility::cleanUTF($show->title);
            $show->description = Utility::cleanUTF($show->description);

            $items->push($show);
          }
        }
      }
      $items = $items->sortBy('date_time');
      $films = $items->values()->all();

      return response()->json($films, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }


    /*
     *
     * Workshops nel festival
     *
     */

    public function festivalWorkshops($slug, $category)
    {
      $festival = Festival::where('slug', '=', $slug)->first();
      $category = FestivalCategory::where([
        ['slug', '=', $category],
        ['festival_id', '=', $festival->id]
      ])->first();

      $events = Event::where([
        ['festival', '=', 1],
        ['festival_id', '=', $festival->id],
        ['fest_cat_id', '=', $category->id],
        ['status_id', '=', 2],
        ['event_type_id', '=', 6]
      ])->with('location', 'media')->get();

      foreach ($events as $key => $event) {
        $string = strtotime($event->date_time);
        setlocale(LC_TIME, "it_IT.UTF-8");
        $time = date('H:i', $string);
        $event->time = $time;
        $day = date('d', $string);
        $event->day = $day;
        $day_string = strftime('%A', $string);
        $event->day_string = $day_string;
        $month = strftime('%B', $string);
        $event->month = $month;
        $year = strftime('%G', $string);
        $event->year = $year;

        $event->img = $event->media()->first()->landscape;
      }

      return view('public.festivals.category.workshops', compact('festival', 'category', 'events'));
    }

    public function festivalWorkshopWithDate($slug, $category, $date)
    {
      $festival = Festival::where('slug', '=', $slug)->first();
      $category = FestivalCategory::where([
        ['slug', '=', $category],
        ['festival_id', '=', $festival->id]
      ])->first();

      $string = strtotime($date);
      $string_end = strtotime($date.'+1 day');

      $start_date = date('Y-m-d H:i:s', $string);
      $end_date = date('Y-m-d H:i:s', $string_end);

      $events = Event::where([
        ['festival', '=', 1],
        ['festival_id', '=', $festival->id],
        ['fest_cat_id', '=', $category->id],
        ['event_type_id', '=', 6],
        ['date_time', '>=', $start_date],
        ['date_time', '<', $end_date],
        ['status_id', '=', 2],
      ])->orderBy('date_time')->with('location', 'media', 'eventType')->get();

      foreach ($events as $key => $event) {
        // Formatto la data
        $string = strtotime($event->date_time);
        setlocale(LC_TIME, "it_IT.UTF-8");
        $time = date('H:i', $string);
        $event->time = $time;
        $day = date('d', $string);
        $event->day = $day;
        $day_string = strftime('%A', $string);
        $event->day_string = $day_string;
        $month = strftime('%B', $string);
        $event->month = $month;
        $year = strftime('%G', $string);
        $event->year = $year;

        // Modifico la descrizione
        $event->img = Storage::disk('local')->url($event->media()->first()->landscape);
        $description = substr(strip_tags($event->description), 0, 150);
        $dots = strlen(strip_tags($event->description)) > 150 ? '...' : "";
        $event->description = $description.$dots;
      }

      return response()->json($events, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }


    /*
     *
     * Visite Guidate nel festival
     *
     */

    public function festivalVisits($slug, $category)
    {
      $festival = Festival::where('slug', '=', $slug)->first();
      $category = FestivalCategory::where([
        ['slug', '=', $category],
        ['festival_id', '=', $festival->id]
      ])->first();

      $events = Event::where([
        ['festival', '=', 1],
        ['festival_id', '=', $festival->id],
        ['fest_cat_id', '=', $category->id],
        ['status_id', '=', 2],
        ['event_type_id', '=', 2]
      ])->with('location', 'media')->get();

      foreach ($events as $key => $event) {
        $string = strtotime($event->date_time);
        setlocale(LC_TIME, "it_IT.UTF-8");
        $time = date('H:i', $string);
        $event->time = $time;
        $day = date('d', $string);
        $event->day = $day;
        $day_string = strftime('%A', $string);
        $event->day_string = $day_string;
        $month = strftime('%B', $string);
        $event->month = $month;
        $year = strftime('%G', $string);
        $event->year = $year;

        $event->img = $event->media()->first()->landscape;
      }

      return view('public.festivals.category.visits', compact('festival', 'category', 'events'));
    }

    public function festivalVisitWithDate($slug, $category, $date)
    {
      $festival = Festival::where('slug', '=', $slug)->first();
      $category = FestivalCategory::where([
        ['slug', '=', $category],
        ['festival_id', '=', $festival->id]
      ])->first();

      $string = strtotime($date);
      $string_end = strtotime($date.'+1 day');

      $start_date = date('Y-m-d H:i:s', $string);
      $end_date = date('Y-m-d H:i:s', $string_end);

      $events = Event::where([
        ['festival', '=', 1],
        ['festival_id', '=', $festival->id],
        ['fest_cat_id', '=', $category->id],
        ['event_type_id', '=', 2],
        ['date_time', '>=', $start_date],
        ['date_time', '<', $end_date],
        ['status_id', '=', 2],
      ])->orderBy('date_time')->with('location', 'media', 'eventType')->get();

      foreach ($events as $key => $event) {
        // Formatto la data
        $string = strtotime($event->date_time);
        setlocale(LC_TIME, "it_IT.UTF-8");
        $time = date('H:i', $string);
        $event->time = $time;
        $day = date('d', $string);
        $event->day = $day;
        $day_string = strftime('%A', $string);
        $event->day_string = $day_string;
        $month = strftime('%B', $string);
        $event->month = $month;
        $year = strftime('%G', $string);
        $event->year = $year;

        // Modifico la descrizione
        $event->img = Storage::disk('local')->url($event->media()->first()->landscape);
        $description = substr(strip_tags($event->description), 0, 150);
        $dots = strlen(strip_tags($event->description)) > 150 ? '...' : "";
        $event->description = $description.$dots;
      }

      return response()->json($events, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }


    /*
     *
     * Films nel festival
     *
     */

    public function festivalFilms($slug, $category)
    {
      $utility = new Utility;
      $tickets = $utility->getTicketJSON();

      $festival = Festival::where('slug', '=', $slug)->first();
      $category = FestivalCategory::where([
        ['slug', '=', $category],
        ['festival_id', '=', $festival->id]
      ])->first();

      $section = 'proiezioni';

      $films = Film::where([
        ['festival', '=', 1],
        ['festival_id', '=', $festival->id],
        ['fest_cat_id', '=', $category->id],
        ['status_id', '=', 2],
      ])->with('location', 'media', 'shows')->get();

      $items = collect();

      foreach ($films as $key => $film) {
        foreach ($film->shows()->get() as $key => $show) {
          $string = strtotime($show->date_time);
          setlocale(LC_TIME, "it_IT.UTF-8");
          $time = date('H:i', $string);
          $show->time = $time;
          $day = date('d', $string);
          $show->day = $day;
          $day_string = strftime('%A', $string);
          $show->day_string = $day_string;
          $month = strftime('%B', $string);
          $show->month = $month;
          $year = strftime('%G', $string);
          $show->year = $year;
          $show->date_ticket = date('Y-m-d', $string).'T00:00:00';

          $show->title = $film->title;
          $show->description = $film->description;
          $show->slug = $film->slug;
          $show->location = $film->location->name;
          $show->kids = $film->kids;
          $show->img = $film->media()->first()->landscape;

          // Biglietti
          if ($show->film->location->id == 2) {
            $data = $utility->searchTicketJSON($tickets, $show->film->title, $show->date_ticket, $show->time);
            if ($data !== false) {
              $show->ticket_url = $data;
              $show->ticket = true;
            } else {
              $show->ticket = false;
            }
          }

          $items->push($show);
        }
      }

      $items = $items->sortBy('date_time');

      $featured = $films;
      $films = $items->values()->all();

      // dd($films);

      return view('public.festivals.category.films', compact('festival', 'category', 'films', 'featured', 'section'));
    }

    public function festivalFilmsWithDate($slug, $category, $date)
    {
      $utility = new Utility;
      $tickets = $utility->getTicketJSON();

      $festival = Festival::where('slug', '=', $slug)->first();
      $category = FestivalCategory::where([
        ['slug', '=', $category],
        ['festival_id', '=', $festival->id]
      ])->first();

      $string = strtotime($date);
      $string_end = strtotime($date.'+1 day');

      $start_date = date('Y-m-d H:i:s', $string);
      $end_date = date('Y-m-d H:i:s', $string_end);

      $films = Film::where([
        ['festival', '=', 1],
        ['festival_id', '=', $festival->id],
        ['fest_cat_id', '=', $category->id],
        ['status_id', '=', 2],
      ])->with('location', 'media', 'shows')->get();

      $items = collect();

      foreach ($films as $key => $film) {
        foreach ($film->shows()->get() as $key => $show) {
          if ($show->date_time >= $start_date && $show->date_time <= $end_date) {
            $string = strtotime($show->date_time);
            setlocale(LC_TIME, "it_IT.UTF-8");
            $time = date('H:i', $string);
            $show->time = $time;
            $day = date('d', $string);
            $show->day = $day;
            $day_string = strftime('%A', $string);
            $show->day_string = $day_string;
            $month = strftime('%B', $string);
            $show->month = $month;
            $year = strftime('%G', $string);
            $show->year = $year;
            $show->date_ticket = date('Y-m-d', $string).'T00:00:00';

            $show->title = $film->title;
            $show->description = $film->description;
            $show->slug = $film->slug;
            $show->location = $film->location->name;
            $show->kids = $film->kids;
            $show->img = Storage::disk('local')->url($film->media()->first()->landscape);

            $show->categories = $film->categories()->get();

            // Modifico la descrizione
            $description = substr(strip_tags($film->description), 0, 150);
            $dots = strlen(strip_tags($film->description)) > 150 ? '...' : "";
            $show->description = $description.$dots;

            // Biglietti
            if ($show->film->location->id == 2) {
              $data = $utility->searchTicketJSON($tickets, $show->film->title, $show->date_ticket, $show->time);
              if ($data !== false) {
                $show->ticket_url = $data;
                $show->ticket = true;
              } else {
                $show->ticket = false;
              }
            }

            $show->title = Utility::cleanUTF($show->title);
            $show->description = Utility::cleanUTF($show->description);

            $items->push($show);
          }
        }
      }
      $items = $items->sortBy('date_time');
      $films = $items->values()->all();

      return response()->json($films, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }


    /*
     *
     * Workshop nel festival
     *
     */
    public function festivalSubCategory($slug, $category)
    {
      $events = Event::where([
        ['festival', '=', 1],
        ['festival_id', '=', $festival->id],
        ['fest_cat_id', '=', $category->id]
      ])->with('media')->get();

      foreach ($events as $key => $event) {
        $string = strtotime($event->date_time);
        setlocale(LC_TIME, "it_IT.UTF-8");
        $time = date('H:i', $string);
        $event->time = $time;
        $day = date('d', $string);
        $event->day = $day;
        $month = strftime('%B', $string);
        $event->month = $month;
        $year = strftime('%G', $string);
        $event->year = $year;
      }

      // dd($events);
      $workshops = $events->filter(function ($event, $key) {
          return $event->event_type_id == 6;
      });

      $events = $events->filter(function ($event, $key) {
          return $event->event_type_id != 6;
      });
    }
}
