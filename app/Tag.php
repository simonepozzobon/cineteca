<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tags';

    public function products()
    {
        return $this->morphedByMany('App\Product', 'tagable');
    }

    public function productGroups()
    {
        return $this->morphedByMany('App\ProductGroup', 'tagable');
    }
}
