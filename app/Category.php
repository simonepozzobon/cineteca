<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class Category extends Model
{

    protected $table = "categories";
    protected $fillable = [
      'name',
      'color',
      'img',
      'link'
    ];

    public function posts () {
      return $this->hasMany('App\Post');
    }

    public function pages()
    {
      return $this->hasMany('App\Page');
    }

    public function convertImage ($file) {
      $filename = $file->getClientOriginalName();
      $original = $file->storeAs('public/categories', $filename);
      $thumb = $file->storeAs('public/categories', 'thumb_'.$filename);
      $squareMedium = $file->storeAs('public/categories', 'square-medium_'.$filename);
      $squareBig = $file->storeAs('public/categories', 'square-big_'.$filename);

      $path = storage_path('app/public/categories');

      $imgThumb = Image::make($path.'/thumb_'.$filename)->fit(57)->save();
      $imgSquareMedium = Image::make($path.'/square-medium_'.$filename)->fit(400)->save();
      $imgSquareBig = Image::make($path.'/square-medium_'.$filename)->fit(800)->save();

      return compact('original', 'thumb', 'squareBig', 'squareMedium');
    }

    public function restoreImage($file_path)
    {
      $filename = str_replace('public/categories/', '', $file_path);
      Storage::copy($file_path, 'public/categories/thumb_'.$filename);
      Storage::copy($file_path, 'public/categories/square-medium_'.$filename);
      Storage::copy($file_path, 'public/categories/square-big_'.$filename);

      $path = storage_path('app/public/categories');

      $imgThumb = Image::make($path.'/thumb_'.$filename)->fit(57)->save();
      $imgSquareMedium = Image::make($path.'/square-medium_'.$filename)->fit(400)->save();
      $imgSquareBig = Image::make($path.'/square-medium_'.$filename)->fit(800)->save();

      $thumb = 'public/categories/thumb_'.$filename;
      $squareBig = 'public/categories/square-medium_'.$filename;
      $squareMedium = 'public/categories/square-medium_'.$filename;

      return compact('thumb', 'squareBig', 'squareMedium');
    }

    public function deleteImages ($img, $thumb, $squareMedium, $squareBig) {
      Storage::delete($img);
      Storage::delete($thumb);
      Storage::delete($squareMedium);
      Storage::delete($squareBig);
    }
}
