<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageLink extends Model
{
    protected $table = 'page_links';

    public function page()
    {
      return $this->belongsTo('App\Page', 'linked_id', 'id');
    }
}
