<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Scout\Searchable;

class Exhibition extends Model
{
    use Searchable;

    public function toSearchableArray()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'slug' => $this->slug,
        ];
    }

    public function films()
    {
        return $this->belongsToMany('App\Film');
    }

    public function exhibits()
    {
        return $this->belongsToMany('App\Exhibit');
    }

    public function shows($filmID)
    {

    }

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function hasFilm($exhibition_id, $film_id)
    {
        $exists = DB::table('exhibition_film')
            ->whereExhibitionId($exhibition_id)
            ->whereFilmId($film_id)
            ->count();
        return $exists;
    }

    public function convertDate($date)
    {
        $date = date('Y-m-d H:i:s', strtotime($date));
        return $date;
    }

    public function media()
    {
        return $this->morphToMany('App\Media', 'mediable');
    }

    public function gallery()
    {
        return $this->morphToMany('App\Gallery', 'galleryable');
    }

    public function event()
    {
        return $this->morphToMany('App\Event', 'eventable');
    }

    public function tweets()
    {
        return $this->morphMany('App\Tweet', 'tweetable');
    }
}
