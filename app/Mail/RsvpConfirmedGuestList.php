<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RsvpConfirmedGuestList extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this
                  ->from('ufficiostampa@cinetecamilano.it', 'Fondazione Cineteca Milano')
                  ->subject('Fondazione Cineteca Milano')
                  ->markdown('mail.rsvp-confirmed-guest')
                  ->with('data', $this->data);
    }
}
