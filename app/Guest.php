<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $table = 'guests';

    public function event()
    {
      return $this->belongsTo('App\Event');
    }
}
