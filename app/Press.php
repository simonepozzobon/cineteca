<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Press extends Model
{
    protected $table = 'presses';

    public function releases()
    {
      return $this->belongsToMany('App\Release');
    }
}
