<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    protected $table = 'actors';

    public function films()
    {
      return $this->belongsToMany('App\Film');
    }
}
