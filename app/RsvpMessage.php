<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RsvpMessage extends Model
{
    protected $table = 'rsvp_messages';

    public function event()
    {
      return $this->belongsTo('App\Event');
    }
}
