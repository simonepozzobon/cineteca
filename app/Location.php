<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public function films()
    {
      return $this->hasMany('App\Film');
    }

    public function exhibitions()
    {
      return $this->hasMany('App\Exhibition');
    }

    public function exhibits()
    {
      return $this->hasMany('App\Exhibit');
    }

    public function event()
    {
      return $this->hasMany('App\Event');
    }

    public function places()
    {
      return $this->hasMany('App\Place');
    }
}
