<?php

namespace App;

use Laravel\Scout\Searchable;
// use Spatie\Image\Manipulations;
use Illuminate\Database\Eloquent\Model;

// class Post extends Model implements HasMediaConversions
class Post extends Model
{
    use Searchable;

    protected $table = "posts";
    protected $fillable = [
        'title',
        'content'
    ];

    public function author()
    {
      return $this->belongsTo('App\Admin', 'author_id');
    }

    public function status()
    {
      return $this->belongsTo('App\PostStatus');
    }

    public function category()
    {
      return $this->belongsTo('App\Category');
    }

    public function postType()
    {
      return $this->belongsTo('App\PostType');
    }

    public function homeSlider()
    {
      return $this->belongsTo('App\HomeSlider');
    }

    // Recupera l'oggetto contentente l'immagine
    public function getFeaturedImage(string $id)
    {
      return $this->getMedia('images');
    }

    // Recupera l'url dell'immagine convertita e quindi modificata
    public function printImageConvertedUrl(string $id, string $folder, string $version)
    {
      $images = $this->getMedia($folder);
      $image = $images->first();
      if ($image) {
        //se c'è un'immagine passa l'url
        $url = $image->getUrl($version);
        return $url;
      }
      // se non c'è l'immagine
      return;
    }

    // recupera lurl dell'immagine originale
    public function printImageUrl(string $id, string $folder)
    {
      $images = $this->getMedia($folder);
      $image = $images->first();
      if ($image) {
        //se c'è un'immagine passa l'url
        $url = $image->getUrl();
        return $url;
      }
      // se non c'è l'immagine
      return;
    }

    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
              ->fit(Manipulations::FIT_CROP, 150, 150);
        $this->addMediaConversion('rectangle')
              ->fit(Manipulations::FIT_CROP, 400, 225);
        $this->addMediaConversion('slider')
              ->fit(Manipulations::FIT_CROP, 600, 600);
    }

    public function media()
    {
        return $this->morphToMany('App\Media', 'mediable');
    }
}
