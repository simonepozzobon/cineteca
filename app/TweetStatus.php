<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TweetStatus extends Model
{
    protected $table = 'tweet_statuses';

    public function tweets()
    {
      return $this->hasMany('App\Tweet');
    }
}
