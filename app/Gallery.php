<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = "galleries";

    public function medias()
    {
      return $this->belongsToMany('App\Media');
    }

    public function pages()
    {
        return $this->morphedByMany('App\Page', 'galleryable');
    }

    public function products()
    {
        return $this->morphedByMany('App\Product', 'galleryable');
    }

    public function events()
    {
        return $this->morphedByMany('App\Event', 'galleryable');
    }

    public function exhibitions()
    {
        return $this->morphedByMany('App\Exhibition', 'galleryable');
    }

    public function exhibits()
    {
        return $this->morphedByMany('App\Exhibit', 'galleryable');
    }

    public function staticPages()
    {
        return $this->morphedByMany('App\StaticPage', 'galleryable');
    }

    public function festivals()
    {
        return $this->morphedByMany('App\Festival', 'galleryable');
    }

    public function productGroups()
    {
        return $this->morphedByMany('App\ProductGroup', 'galleryable');
    }

    public static function relationType($gallery)
    {
      // dd($gallery);
        if ($gallery->pages !== null && count($gallery->pages) > 0) {
            $model = $gallery->pages->first();
            $model->type = 'page';
        } elseif ($gallery->events !== null && count($gallery->events) > 0) {
            $model = $gallery->events->first();
            $model->type = 'event';
        } elseif ($gallery->products !== null && count($gallery->products) > 0) {
            $model = $gallery->products->first();
            $model->type = 'product';
        } elseif ($gallery->staticPages !== null && count($gallery->staticPages) > 0) {
            $model = $gallery->staticPages->first();
            $model->type = 'static';
        } elseif ($gallery->exhibitions !== null && count($gallery->exhibitions) > 0) {
            $model = $gallery->exhibitions->first();
            $model->type = 'exhibition';
        } elseif ($gallery->exhibits !== null && count($gallery->exhibits) > 0) {
            $model = $gallery->exhibits->first();
            $model->type = 'exhibit';
        } elseif ($gallery->festivals !== null && count($gallery->festivals) > 0) {
            $model = $gallery->festivals->first();
            $model->type = 'festival';
        } elseif ($gallery->productGroups !== null && count($gallery->productGroups) > 0) {
            $model = $gallery->productGroups->first();
            $model->type = 'productGroup';
        }

        return $model;
    }
}
