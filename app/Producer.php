<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producer extends Model
{
  protected $table = 'producers';

  public function films()
  {
    return $this->belongsToMany('App\Film');
  }
}
