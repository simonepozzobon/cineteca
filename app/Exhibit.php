<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Scout\Searchable;

class Exhibit extends Model
{
    use Searchable;

    protected $table = 'exhibits';

    public function films()
    {
        return $this->belongsToMany('App\Film');
    }

    public function exhibitions()
    {
        return $this->belongsToMany('App\Exhibition');
    }

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function hasFilm($exhibitID, $filmID)
    {
        $exists = DB::table('exhibit_film')
            ->whereExhibitId($exhibitID)
            ->whereFilmId($filmID)
            ->count();
        return $exists;
    }

    public function hasExhibition($exhibitID, $exhibitionID)
    {
      $exists = DB::table('exhibit_exhibition')
                      ->whereExhibitId($exhibitID)
                      ->whereExhibitionId($exhibitionID)
                      ->count();
      return $exists;
    }

    public function convertDate($date)
    {
      $date = date('Y-m-d H:i:s', strtotime($date));
      return $date;
    }

    public function media()
    {
        return $this->morphToMany('App\Media', 'mediable');
    }

    public function gallery()
    {
        return $this->morphToMany('App\Gallery', 'galleryable');
    }

    public function tweets()
    {
      return $this->morphMany('App\Tweet', 'tweetable');
    }
}
