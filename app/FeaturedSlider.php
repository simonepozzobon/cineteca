<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeaturedSlider extends Model
{
    protected $table = 'featured_sliders';

    public function featurable()
    {
        return $this->morphTo();
    }

    public function category()
    {
        return $this->belongsTo('App\ProductCategory');
    }
}
