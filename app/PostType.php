<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostType extends Model
{
    protected $table = 'post_types';

    public function posts()
    {
      return $this->hasMany('App\Post');
    }

    public function HomeSlide()
    {
      return $this->hasMany('App\HomeSlider');
    }
}
