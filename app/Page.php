<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Page extends Model
{
    use Searchable;

    protected $table = 'pages';

    public function category()
    {
      return $this->belongsTo('App\Category');
    }

    public function media()
    {
      return $this->morphToMany('App\Media', 'mediable');
    }

    public function gallery()
    {
      return $this->morphToMany('App\Gallery', 'galleryable');
    }

    public function pageLinks()
    {
      return $this->hasMany('App\PageLink');
    }
}
