<?php

namespace App\Console\Commands;

use File;
use App\Tweet;
use \UrlShortener;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class PostTweetDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tweets:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatic Post Daily Tweets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        date_default_timezone_set('Europe/Rome');
        $today = date('Y-m-d H:i:s');
        $tomorrow = date('Y-m-d H:i:s', strtotime('+1 min'));

        $tweets = Tweet::where([
            ['status_id', '=', 1],
            ['date_time', '>=', $today],
            ['date_time', '<=', $tomorrow]
        ])->get();
        $this->line('oggi '.$today);
        $this->line('fine '.$tomorrow);
        $this->line($tweets->count().' trovati');

        $settings = [
            'oauth_access_token' => env('TW_OAUTH_ACCESS_TOKEN'),
            'oauth_access_token_secret' => env('TW_OAUTH_ACCESS_TOKEN_SECRET'),
            'consumer_key' => env('TW_CONSUMER_KEY'),
            'consumer_secret' => env('TW_CONSUMER_SECRET')
        ];

        foreach ($tweets as $key => $tweet) {

            // Formatto il testo
            $text = $tweet->text;
            if (strlen($text) > 110) {
                $text = substr(strip_tags($text), 0, 110);
            }
            $shorten = UrlShortener::shorten($tweet->link);
            $text = $text.'... '.$shorten;


            // UPLOAD Media
            $url = 'https://upload.twitter.com/1.1/media/upload.json';
            $requestMethod = 'POST';

            $abs_path = storage_path('app/'.$tweet->media()->first()->landscape);
            $base_64 = base64_encode(file_get_contents($abs_path));

            $postfields = [
                'media_data' => $base_64,
            ];

            $twitter = new \TwitterAPIExchange($settings);
            $r_media = $twitter->buildOauth($url, $requestMethod)
                ->setPostfields($postfields)
                ->performRequest();

            $r_media = json_decode($r_media);

            // Effettua il post
            $url = 'https://api.twitter.com/1.1/statuses/update.json';
            $requestMethod = 'POST';

            $postfields = [
                'status' => $text,
                'media_ids' => $r_media->media_id,
            ];

            $twitter = new \TwitterAPIExchange($settings);
            $response = $twitter->buildOauth($url, $requestMethod)
                ->setPostfields($postfields)
                ->performRequest();

            $response = json_decode($response);

            $tweet->status_id = 2;
            $tweet->save();

        }
        $this->line('Completato');
    }
}
