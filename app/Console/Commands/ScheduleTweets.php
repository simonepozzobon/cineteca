<?php

namespace App\Console\Commands;

use App\Tweet;
use App\Event;
use App\Exhibit;
use App\Exhibition;
use App\TweetStatus;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ScheduleTweets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tweets:schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search for contents to add on schedule list of tweets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        setlocale(LC_TIME, "it_IT.UTF-8");
        date_default_timezone_set('Europe/Rome');
        $today = date('Y-m-d H:i:s');

        $events = Event::where([
            ['status_id', '=', 2],
            ['date_time', '>=', $today]
        ])->get();

        $exhibits = Exhibit::where([
            ['status_id', '=', 2],
            ['start_date', '>=', $today]
        ])->get();

        $exhibitions = Exhibition::where([
            ['status_id', '=', 2],
            ['start_date', '>=', $today]
        ])->get();

        $events = $events->reject(function($value, $key) {
            $value->img = Storage::disk('local')->url($value->media()->first()->landscape);
            $value->link = route('single.event', $value->slug);
            $value->location = $value->location->name;
            $string = strtotime($value->date_time);
            $time = date('H:i', $string);
            $value->time = $time;
            $day = date('d', $string);
            $value->day = $day;
            $month = strftime('%m', $string);
            $value->month = $month;
            $value->diff_time = date('Y-m-d H:i:s', strtotime('-3 days', strtotime($value->date_time)));
            return $value->tweets()->count() > 0;
        });

        $events->all();

        $exhibits = $exhibits->reject(function($value, $key) {
            $value->img = Storage::disk('local')->url($value->media()->first()->landscape);
            $value->link = route('single.exhibit', $value->slug);
            $value->location = $value->location->name;
            $string = strtotime($value->start_date);
            $time = date('H:i', $string);
            $value->time = $time;
            $day = date('d', $string);
            $value->day = $day;
            $month = strftime('%m', $string);
            $value->month = $month;
            $value->date_time = $value->start_date;
            $value->diff_time = date('Y-m-d H:i:s', strtotime('-3 days', strtotime($value->date_time)));
            return $value->tweets()->count() > 0;
        });

        $exhibits->all();

        $exhibitions = $exhibitions->reject(function($value, $key) {
            $value->img = Storage::disk('local')->url($value->media()->first()->landscape);
            $value->link = route('single.exhibition', $value->slug);
            $value->location = $value->location->name;
            $string = strtotime($value->start_date);
            $time = date('H:i', $string);
            $value->time = $time;
            $day = date('d', $string);
            $value->day = $day;
            $month = strftime('%m', $string);
            $value->month = $month;
            $value->date_time = $value->start_date;
            $value->diff_time = date('Y-m-d H:i:s', strtotime('-3 days', strtotime($value->date_time)));
            return $value->tweets()->count() > 0;
        });

        $exhibitions->all();


        foreach ($events as $key => $event) {
            $tweet = new Tweet;

            // se la data è futura allora uso lo scheduling dei tre giorni prima altrimenti all'ora dell'evento
            if ($event->diff_time >= $today) {
                $tweet->status_id = 1;
                $tweet->date_time = $event->diff_time;
            } else {
                $tweet->status_id = 3;
                $tweet->date_time = $event->date_time;
            }

            $tweet->text = Tweet::default_message($event);
            $tweet->link = $event->link;
            $event->tweets()->save($tweet);
            $tweet->save();

            $img = $event->media()->first();
            $tweet->media()->save($img);
        }

        foreach ($exhibits as $key => $exhibit) {
            $tweet = new Tweet;

            // se la data è futura allora uso lo scheduling dei tre giorni prima altrimenti all'ora dell'evento
            if ($exhibit->diff_time >= $today) {
                $tweet->status_id = 1;
                $tweet->date_time = $exhibit->diff_time;
            } else {
                $tweet->status_id = 3;
                $tweet->date_time = $exhibit->date_time;
            }

            $tweet->text = Tweet::default_message($exhibit);
            $tweet->link = $exhibit->link;
            $exhibit->tweets()->save($tweet);
            $tweet->save();

            $img = $exhibit->media()->first();
            $tweet->media()->save($img);
        }

        foreach ($exhibitions as $key => $exhibition) {
            $tweet = new Tweet;

            // se la data è futura allora uso lo scheduling dei tre giorni prima altrimenti all'ora dell'evento
            if ($exhibition->diff_time >= $today) {
                $tweet->status_id = 1;
                $tweet->date_time = $exhibition->diff_time;
            } else {
                $tweet->status_id = 3;
                $tweet->date_time = $exhibition->date_time;
            }

            $tweet->text = Tweet::default_message($exhibition);
            $tweet->link = $exhibition->link;
            $exhibition->tweets()->save($tweet);
            $tweet->save();

            $img = $exhibition->media()->first();
            $tweet->media()->save($img);
        }

        $count = $events->count() + $exhibits->count() + $exhibitions->count();
        $this->line("$count nuovi tweets");
        $this->line('Completato');
    }
}
