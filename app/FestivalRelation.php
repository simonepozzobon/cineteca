<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Scout\Searchable;

class FestivalRelation extends Model
{
    use Searchable;

    public function festival()
    {
      return $this->belongsTo('App\FestivalRelation');
    }
}
