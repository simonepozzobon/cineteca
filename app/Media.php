<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Media extends Model
{
    use Searchable;
    protected $table = 'media';

    // public function toSearchableArray()
    // {
    //     return [
    //         'title' => $this->title
    //     ];
    // }

    public function relation()
    {
        return $this->hasMany('App\MediaRelation');
    }

    public function events()
    {
        return $this->morphedByMany('App\Event', 'mediable');
    }

    public function exhibits()
    {
        return $this->morphedByMany('App\Exhibit', 'mediable');
    }

    public function exhibitions()
    {
        return $this->morphedByMany('App\Exhibition', 'mediable');
    }

    public function festivals()
    {
        return $this->morphedByMany('App\Festival', 'mediable');
    }

    public function pages()
    {
        return $this->morphedByMany('App\Page', 'mediable');
    }

    public function posts()
    {
        return $this->morphedByMany('App\Post', 'mediable');
    }

    public function products()
    {
        return $this->morphedByMany('App\Product', 'mediable');
    }

    public function films()
    {
        return $this->morphedByMany('App\Film', 'mediable');
    }

    public function homesliders()
    {
        return $this->morphedByMany('App\HomeSlider', 'mediable');
    }

    public function releases()
    {
        return $this->morphedByMany('App\Release', 'mediable');
    }

    public function galleries()
    {
        return $this->belongsToMany('App\Gallery');
    }

    public function productGroups()
    {
        return $this->belongsToMany('App\ProductGroup', 'mediable');
    }

    public function festivalCategories()
    {
        return $this->belongsToMany('App\FestivalCategory', 'mediable');
    }

    public function tweets()
    {
        return $this->belongsToMany('App\TweetStatus', 'mediable');
    }

    public function funders()
    {
        return $this->belongsToMany('App\Funder', 'mediable');
    }

}
