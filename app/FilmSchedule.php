<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FilmSchedule extends Model
{
    protected $table = 'film_schedules';

    public function film()
    {
      return $this->belongsTo('App\Film');
    }

    public function exhibitions($filmID)
    {
      $film = Film::find($filmID);
      $exhibitions = $film->exhibitions()->get();
      return $exhibitions;
    }

    public function mergeDateTime($date, $time)
    {
      $dateTime = date('Y-m-d H:i:s', strtotime("$date $time"));
      return $dateTime;
    }
}
