<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Release extends Model
{
    use Searchable;

    protected $table = 'releases';
    // 
    // public function toSearchableArray()
    // {
    //   return [
    //          'id' => $this->id,
    //          'title' => $this->title,
    //          'description' => $this->description,
    //          'date_time' => $this->date_time,
    //     ];
    // }

    public function media()
    {
        return $this->morphToMany('App\Media', 'mediable');
    }

    public function video()
    {
        return $this->morphToMany('App\Video', 'videoable');
    }

    public function presses()
    {
      return $this->belongsToMany('App\Press');
    }

    public function convertDate($date)
    {
      $date = date('Y-m-d H:i:s', strtotime($date));
      return $date;
    }

    public function hasPress($name)
    {
      return ! $this->presses->filter(function($press) use ($name)
      {
          return $press->name === $name;
      })->isEmpty();
    }
}
