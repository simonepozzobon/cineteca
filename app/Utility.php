<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Utility extends Model
{
    /**
     * Encode array to utf8 recursively
     * @param $dat
     * @return array|string
     */
    public function array_utf8_encode($dat)
    {
        if (is_string($dat))
            return utf8_encode($dat);
        if (!is_array($dat))
            return $dat;
        $ret = array();
        foreach ($dat as $i => $d)
            $ret[$i] = self::array_utf8_encode($d);
        return $ret;
    }

    /**
     *
     * New Media table record.
     *
    **/

    public function mediaMove($item, $img_path)
    {
      $filename = basename($img_path);
      $new_file = 'public/media/'.$filename;

      // verifico che il file non esista altrimenti lo elimino
      $exists = Storage::disk('local')->exists($new_file);
      if ($exists) {
        Storage::delete($new_file);
      }

      // sposto il file nella nuova cartella
      Storage::copy($img_path, $new_file);

      // Verifico che l'immagine non sia già presente
      $media = Media::where('img', '=', $new_file)->first();

      // Se il record non esiste ne creo uno nuovo
      if ($media === null) {
        $media = new Media;
        $media->img = $new_file;
        $media->save();
      }

      if ($item->media->count() === 0) {
        $item->media()->save($media);
      }

      return true;

    }

    public function storeMedia($img, $item)
    {
      // Salvo il file
      $filename = $img->getClientOriginalName();
      $file = $img->storeAs('public/media', $filename);

      // preparo gli altri formati
      $thumb = $img->storeAs('public/media/thumb', $filename);
      $slide_img = $img->storeAs('public/media/slide_img', $filename);
      $portrait = $img->storeAs('public/media/portrait', $filename);
      $landscape = $img->storeAs('public/media/landscape', $filename);

      // genero gli altri formati
      $path = storage_path('app/public/media');

      // Thumb
      Image::make($path.'/thumb/'.$filename)->fit(500, 500, function ($constraint) {
          $constraint->upsize();
      })->save();

      // slide_img
      Image::make($path.'/slide_img/'.$filename)->fit(1920, 720, function ($constraint) {
          $constraint->upsize();
      }, 'top')->save();

      // portrait
      Image::make($path.'/portrait/'.$filename)->fit(720, 960, function ($constraint) {
          $constraint->upsize();
      })->save();

      // landscape 960 540
      Image::make($path.'/landscape/'.$filename)->fit(960, 540, function ($constraint) {
          $constraint->upsize();
      })->save();


      // Verifico che l'immagine non sia già presente
      $media = Media::where('img', '=', $file)->first();

      // Se il record non esiste ne creo uno nuovo
      if ($media === null) {
        $media = new Media;
        $media->img = $file;
        $media->thumb = $thumb;
        $media->slide_img = $slide_img;
        $media->portrait = $portrait;
        $media->landscape = $landscape;
        $media->save();
      }


      if ($item->media->count() === 0) {
        $item->media()->save($media);
      } elseif ($item->media->count() > 0) {
        $item->media()->detach($item->media->first()->id);
        $item->media()->save($media);
      }

      return true;

    }

    // $id = the id of the cinema on creaweb

    public function fetch_tickets($id)
    {
        // shortcut for activate/de-activate tickets
        $settings = env('CREA_TICKETS');
        if (!$settings) {
            return null;
        }

        // $url = 'http://www.crea.webtic.it/frame/wtjsonservices.ashx?'."wtid=getFullScheduling&localid=5358";
        try {
            // $json = file_get_contents($url);
            $curl_handle=curl_init();
                curl_setopt($curl_handle, CURLOPT_URL,'http://www.crea.webtic.it/frame/wtjsonservices.ashx?wtid=getFullScheduling&localid='.$id);
                curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
                curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Cineteca Milano');
            $json = curl_exec($curl_handle);
            curl_close($curl_handle);

        } catch (Exception $e) {
            // dd($e);
            return null;
        }
        return $json;
    }

    // Get Json from webtic
    public function getTicketJSON($location = null)
    {
        // shortcut for activate/de-activate tickets
        $settings = env('CREA_TICKETS');
        if (!$settings) {
            return (object) [];
        }

        if ($location) {
            $json = $this->fetch_tickets($location);
            $data = json_decode($json);
            if ($data->userdata->StatusOk === false || $json === false) {
                return (object) [];
            }
            $formatted = $this->format_tickets($data);
            return $formatted;
        } else {
            // Fetch Oberdan Tickets with cinema id = 5358
            $oberdan = $this->fetch_tickets(5358);

            // Fetch Metropolis tickets with cinema id = 5400
            $metropolis = $this->fetch_tickets(5400);

            // $data = json_decode($json);
            $oberdan_decoded = json_decode($oberdan);
            $metropolis_decoded = json_decode($metropolis);
            // Se c'è un problema con Creaweb ritorna una variabile vuota.
            if ($oberdan_decoded->userdata->StatusOk === false || $metropolis_decoded->userdata->StatusOk === false) {
                return $data = [];
            } else if ( $oberdan === false && $metropolis === false) {
                return $data = [];
            } else {
                // Se non ci sono problemi ritorna i biglietti
                $oberdan_formatted = $this->format_tickets($oberdan_decoded->DS->Scheduling);
                $metropolis_formatted = $this->format_tickets($metropolis_decoded->DS->Scheduling);
                return (object) array_merge((array) $oberdan_formatted, (array) $metropolis_formatted);
            }
        }

    }

    public function format_tickets($tickets) {
        $location_id = $tickets->LocalId;
        foreach ($tickets->Events as $key => $event) {
            $event->LocalId = $location_id;
        }
        return $tickets->Events;
    }

    public function searchTicket($tickets, $show)
    {
        // dd($tickets);
        if ($tickets === null) {
            $show->ticket = false;
        } else {
            $title = $this->slugify($show->title);
            $day = $show->date_ticket;
            $time = $show->time;
            foreach ($tickets as $key => $event) {
                if (($this->slugify($event->Title)) == $title) {
                    foreach ($event->Days as $key => $days) {
                        if ($days->Day == $day) {
                            foreach ($days->Performances as $key => $performance) {
                                if ($performance->Time == $time) {
                                    $show->ticket = true;
                                    $show->url = 'http://www.webtic.it/?action=loadPerformance&localId='.$event->LocalId.'&eventId='.$event->EventId.'&performanceId='.$performance->PerformanceId;
                                } else {
                                    $show->ticket = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $show;
    }

    public function searchTicketJSON($tickets, $title, $day, $time)
    {
        $title = $this->slugify($title);
        foreach ($tickets as $key => $event) {
            if (($this->slugify($event->Title)) == $title) {
                foreach ($event->Days as $key => $days) {
                    if ($days->Day == $day) {
                        foreach ($days->Performances as $key => $performance) {
                            if ($performance->Time == $time) {
                                $url = 'http://www.webtic.it/?action=loadPerformance&localId='.$event->LocalId.'&eventId='.$event->EventId.'&performanceId='.$performance->PerformanceId;
                                return $url;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    // Slug sanitize

    static public function slugify($text)
    {
      // replace non letter or digits by -
      $text = preg_replace('~[^\pL\d]+~u', '', $text);

      // transliterate
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

      // remove unwanted characters
      $text = preg_replace('~[^-\w]+~', '', $text);

      // trim
      $text = trim($text, '');

      // remove duplicate -
      $text = preg_replace('~-+~', '', $text);

      // lowercase
      $text = strtolower($text);

      if (empty($text)) {
        return 'n-a';
      }

      return $text;
    }

    static public function slugifyUrl($text)
    {
      // replace non letter or digits by -
      $text = preg_replace('~[^\pL\d]+~u', '-', $text);

      // transliterate
      $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

      // remove unwanted characters
      $text = preg_replace('~[^-\w]+~', '-', $text);

      // trim
      $text = trim($text, '');

      // remove duplicate -
      $text = preg_replace('~-+~', '-', $text);

      // lowercase
      $text = strtolower($text);

      if (empty($text)) {
        return 'n-a';
      }

      return $text;
    }


    public function mergeDateTime($date, $time)
    {
      $date_conv = date('Y-m-d', strtotime($date));

      // Forzo il formato del tempo
      $time = explode(':', $time);
      if (!isset($time[1])) {
        $time[1] = '00';
      }
      if (!isset($time[2])) {
        $time[2] = '00';
      }
      $time = implode(':', $time);

      $dateTime = date('Y-m-d H:i:s', strtotime($date_conv.' '.$time));
      return $dateTime;
    }

    public function addWatermark($model)
    {
        $pics = $model->media()->get();
        foreach ($pics as $key => $pic) {
          $img = storage_path('app/'.$pic->img);
          $thumb = storage_path('app/'.$pic->thumb);
          $slide_img = storage_path('app/'.$pic->slide_img);
          $portrait = storage_path('app/'.$pic->portrait);
          $landscape = storage_path('app/'.$pic->landscape);

          $watermark_src = public_path('img/logo_fondazione_cineteca_milano.png');
          $watermark = Image::make($watermark_src)->resize(150, null, function ($constraint) {
              $constraint->aspectRatio();
          })->invert()->opacity(60);

          $mark_img = Image::make($img)->insert($watermark, 'bottom-left', 10, 10)->save();
          $mark_thumb = Image::make($thumb)->insert($watermark, 'bottom-left', 10, 10)->save();
          $mark_slide_img = Image::make($slide_img)->insert($watermark, 'bottom-left', 10, 10)->save();
          $mark_portrait = Image::make($portrait)->insert($watermark, 'bottom-left', 10, 10)->save();
          $mark_landscape = Image::make($landscape)->insert($watermark, 'bottom-left', 10, 10)->save();
        }
        return true;
    }

    public static function cleanUTF($string)
    {
      $cleaned = htmlspecialchars($string);
      // $cleaned = htmlspecialchars(utf8_encode($string));
      return $cleaned;
    }


    public static function recover_events_media($id)
    {
      $mediable = DB::table('mediables')->where([
        ['mediable_type', '=', 'App\\Event'],
        ['mediable_id', '=', $id]
      ])->get();
      return $mediable;
    }

    public static function shortenDesc($desc)
    {
        // remove tags
        $description = substr(strip_tags($desc), 0, 150);
        if (strlen(strip_tags($desc))) {
            return $description.'...';
        }
        return $description;
    }
}
