<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Screenwriter extends Model
{
    protected $table = 'screenwriters';

    public function films()
    {
      return $this->belongsToMany('App\Film');
    }
}
