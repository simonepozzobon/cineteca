<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Product extends Model
{
    use Searchable;

    protected $table = 'products';

    // public function toSearchableArray()
    // {
    //     return [
    //         'id' => $this->id,
    //         'name' => $this->name,
    //         'description' => $this->description,
    //         'slug' => $this->slug,
    //     ];
    // }

    public function category()
    {
      return $this->belongsTo('App\ProductCategory', 'product_category_id', 'id');
    }

    public function orderProduct()
    {
      return $this->hasMany('App\OrderProduct');
    }

    public function media()
    {
        return $this->morphToMany('App\Media', 'mediable');
    }

    public function gallery()
    {
        return $this->morphToMany('App\Gallery', 'galleryable');
    }

    public function video()
    {
        return $this->morphToMany('App\Video', 'videoable');
    }

    public function product_groups()
    {
        return $this->belongsToMany('App\ProductGroup');
    }

    public function product_prices()
    {
        return $this->belongsToMany('App\ProductPrice');
    }

    public function tags()
    {
        return $this->morphToMany('App\Tag', 'tagable');
    }

    public function cinestoreSliders()
    {
        return $this->morphMany('App\CinestoreSlider', 'sliderable');
    }

    public function featuredSliders()
    {
        return $this->morphMany('App\FeaturedSlider', 'featurable');
    }

    public function picCollections()
    {
        return $this->belongsToMany('App\PicCollection');
    }
}
