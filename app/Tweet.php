<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    protected $table = 'tweets';

    public function status()
    {
        return $this->belongsTo('App\TweetStatus');
    }

    public function media()
    {
        return $this->morphToMany('App\Media', 'mediable');
    }

    public function tweetable()
    {
        return $this->morphTo();
    }

    public static function default_message($object)
    {
        if ($object->location()->first()->slug == 'no-location') {
            return 'dal '.$object->day.'/'.$object->month.', '.$object->title.'.';
        }
        return 'dal '.$object->day.'/'.$object->month.' '.$object->location()->first()->article.$object->location.', '.$object->title.'.';
    }
}
