<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CinestoreSlider extends Model
{
    protected $table = 'cinestore_sliders';

    public function sliderable()
    {
        return $this->morphTo();
    }

}
