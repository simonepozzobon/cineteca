<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    protected $table = 'product_prices';

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
