<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photographer extends Model
{
    protected $table = 'photographers';

    public function films()
    {
      return $this->belongsToMany('App\Film');
    }
}
