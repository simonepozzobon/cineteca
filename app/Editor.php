<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Editor extends Model
{
    protected $table = 'editors';

    public function films()
    {
      return $this->belongsToMany('App\Film');
    }
}
