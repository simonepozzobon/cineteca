<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilmLocation extends Model
{
    protected $table = 'film_locations';

    public function films()
    {
      return $this->hasMany('App\Film');
    }

    public function places()
    {
      return $this->hasMany('App\Place');
    }
}
