<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FestivalCategory extends Model
{
    protected $table = 'festival_categories';

    public function festival()
    {
      return $this->belongsTo('App\Festival');
    }

    public function subCategories()
    {
      return $this->hasMany('App\FestivalSubCategories');
    }

    public function media()
    {
        return $this->morphToMany('App\Media', 'mediable');
    }
}
