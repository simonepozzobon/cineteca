<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PicCollection extends Model
{
    protected $table = 'pic_collections';

    public function products()
    {
        return $this->belongsToMany('App\Product')->orderBy('name');
    }
}
