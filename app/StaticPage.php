<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaticPage extends Model
{
    protected $table = 'static_pages';

    public function gallery()
    {
        return $this->morphToMany('App\Gallery', 'galleryable');
    }

    public static function footerGallery()
    {
      $footer = StaticPage::where('slug', '=', 'footer')->with('gallery')->first();
      $galleries = $footer->gallery;
      $medias = collect();
      foreach ($galleries as $key => $gallery) {
        foreach ($gallery->medias as $key => $media) {
          $medias->push($media);
        }
      }
      return $medias;
    }
}
