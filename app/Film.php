<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Film extends Model
{
    use Searchable;
    protected $table = 'films';

    public function toSearchableArray()
    {
      return [
             'id' => $this->id,
             'title' => $this->title,
             'description' => $this->description,
             'region' => $this->region,
             'production_date' => $this->production_date,
        ];
    }

    public function location()
    {
        return $this->belongsTo('App\FilmLocation');
    }

    public function place()
    {
        return $this->belongsTo('App\Place');
    }

    public function exhibitions()
    {
        return $this->belongsToMany('App\Exhibition');
    }

    public function exhibits()
    {
        return $this->belongsToMany('App\Exhibit');
    }

    public function shows()
    {
        return $this->hasMany('App\FilmSchedule');
    }

    public function actors()
    {
        return $this->belongsToMany('App\Actor');
    }

    public function directors()
    {
        return $this->belongsToMany('App\Director');
    }

    public function distributors()
    {
        return $this->belongsToMany('App\Distributor');
    }

    public function editors()
    {
        return $this->belongsToMany('App\Editor');
    }

    public function photographers()
    {
        return $this->belongsToMany('App\Photographer');
    }

    public function producers()
    {
        return $this->belongsToMany('App\Producer');
    }

    public function screenwriters()
    {
        return $this->belongsToMany('App\Screenwriter');
    }

    public function categories()
    {
        return $this->belongsToMany('App\FilmCategory');
    }

    public function hasActor($name)
    {
        return ! $this->actors->filter(function($actor) use ($name)
        {
            return $actor->name === $name;
        })->isEmpty();
    }

    public function hasDirector($name)
    {
        return ! $this->directors->filter(function($director) use ($name)
        {
            return $director->name === $name;
        })->isEmpty();
    }

    public function hasCategory($name)
    {
        return ! $this->categories->filter(function($film_category) use ($name)
        {
            return $film_category->name === $name;
        })->isEmpty();
    }

    public function hasDistributor($name)
    {
        return ! $this->distributors->filter(function($distributor) use ($name)
        {
            return $distributor->name === $name;
        })->isEmpty();
    }

    public function hasEditor($name)
    {
        return ! $this->editors->filter(function($editor) use ($name)
        {
            return $editor->name === $name;
        })->isEmpty();
    }

    public function hasPhotographer($name)
    {
        return ! $this->photographers->filter(function($photographer) use ($name)
        {
            return $photographer->name === $name;
        })->isEmpty();
    }

    public function hasProducer($name)
    {
        return ! $this->producers->filter(function($producer) use ($name)
        {
            return $producer->name === $name;
        })->isEmpty();
    }

    public function hasScreenwriter($name)
    {
        return ! $this->screenwriters->filter(function($screenwriter) use ($name)
        {
            return $screenwriter->name === $name;
        })->isEmpty();
    }

    public function convertDate($date)
    {
        $date = date('Y-m-d H:i:s', strtotime($date));
        return $date;
    }

    public function media()
    {
        return $this->morphToMany('App\Media', 'mediable');
    }

    public function events()
    {
        return $this->hasMany('App\Event');
    }
}
