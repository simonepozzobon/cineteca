<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeSlider extends Model
{
    public function postType()
    {
      return $this->belongsTo('App\PostType');
    }

    public function post()
    {
      return $this->belongsTo('App\Post');
    }

    public function media()
    {
      return $this->morphToMany('App\Media', 'mediable');
    }
}
