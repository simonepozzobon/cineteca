<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Event extends Model
{
    use Searchable;

    protected $table = 'events';
    //
    // public function toSearchableArray()
    // {
    //   return [
    //          'id' => $this->id,
    //          'title' => $this->title,
    //          'date_time' => $this->date_time,
    //          'description' => $this->description,
    //     ];
    // }

    public function eventType()
    {
      return $this->belongsTo('App\EventType', 'event_type_id', 'id');
    }

    public function mergeDateTime($date, $time)
    {
      $dateTime = date('Y-m-d H:i:s', strtotime("$date $time"));
      return $dateTime;
    }

    public function location()
    {
      return $this->belongsTo('App\Location', 'location_id', 'id');
    }

    public function media()
    {
        return $this->morphToMany('App\Media', 'mediable');
    }

    public function gallery()
    {
        return $this->morphToMany('App\Gallery', 'galleryable');
    }

    public function exhibitions()
    {
        return $this->morphedByMany('App\Exhibition', 'eventable');
    }

    public function guests()
    {
      return $this->hasMany('App\Guest');
    }

    public function film()
    {
      return $this->belongsTo('App\Film');
    }

    public function rsvpMessages()
    {
      return $this->hasMany('App\RsvpMessage');
    }

    public function tweets()
    {
      return $this->morphMany('App\Tweet', 'tweetable');
    }
}
