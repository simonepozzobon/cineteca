<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      \Braintree_Configuration::environment(env('BR_ENV'));
      \Braintree_Configuration::merchantId(env('BR_MERCHANT_ID'));
      \Braintree_Configuration::publicKey(env('BR_PUBLIC_KEY'));
      \Braintree_Configuration::privateKey(env('BR_PRIVATE_KEY'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        config([
          'config/twitter.php',
        ]);
    }
}
