<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/search', 'SearchController@search')->name('search.post');
Route::post('/cinestore-search', 'SearchController@searchCinestore')->name('search.cinestore');
Route::post('/cinestore-mixed-search', 'SearchController@searchMainCinestore')->name('search.cinestore.mixed');

Route::get('/archiviofilm', 'HomePageController@archivioFilm')->name('archivio.film');

Route::get('/pagina/{slug}', 'HomePageController@getPage')->where('slug', '[\w\d\-\_]+')->name('single.page');

// News
Route::get('/news/{slug}', 'HomePageController@getNews')->where('slug', '[\w\d\-\_]+')->name('single.news');
Route::get('/news', 'HomePageController@news')->name('news.index');

// Film
Route::get('/film/{slug}', 'HomePageController@getFilmSingle')->where('slug', '[\w\d\-\_]+')->name('single.film');
Route::get('/films', 'HomePageController@getFilms')->name('film.index');
Route::get('/films/shows/{date}', 'HomePageController@getShows')->name('film.get.shows');
Route::get('/films/{location}/shows/{date}', 'HomePageController@getShowsWithLocation')->name('film.location.get.shows');
Route::get('/films/{location}/week', 'HomePageController@getFilmWithLocationWeek')->name('film.location.week');
Route::get('/films/{location}/month', 'HomePageController@getFilmWithLocationMonth')->name('film.location.month');
Route::get('/films/{location}', 'HomePageController@getFilmWithLocation')->name('film.location.index');

// Rassegne
Route::get('/rassegna/{slug}', 'HomePageController@getExhibitionSingle')->where('slug', '[\w\d\-\_]+')->name('single.exhibition');
Route::get('/rassegne/archivio', 'ExhibitionController@archive')->name('exhibitions.archive');
Route::get('/rassegne', 'HomePageController@getExhibitions')->name('exhibitions.index');

// Mostre
Route::get('/esposizione/{slug}', 'HomePageController@getExhibitSingle')->where('slug', '[\w\d\-\_]+')->name('single.exhibit');
Route::get('/esposizioni/archivio', 'ExhibitController@archive')->name('exhibits.archive');
Route::get('/esposizioni', 'HomePageController@getExhibits')->name('exhibits.index');

// Festival
Route::get('/festival/{slug}', 'HomePageController@getFestivalSingle')->where('slug', '[\w\d\-\_]+')->name('single.festival');
Route::get('/festival/{slug}/{category}/seminari', 'PgcController@festivalSem')->name('single.festival.sems');
Route::get('/festival/{slug}/{category}/seminari/{date}', 'PgcController@festivalSemWithDate')->name('single.festival.sems.date');
Route::get('/festival/{slug}/{category}/eventi', 'PgcController@festivalEvents')->name('single.festival.events');
Route::get('/festival/{slug}/{category}/eventi/{date}', 'PgcController@festivalEventsWithDate')->name('single.festival.events.date');
Route::get('/festival/{slug}/{category}/anteprime', 'PgcController@festivalPreviews')->name('single.festival.previews');
Route::get('/festival/{slug}/{category}/anteprime/{date}', 'PgcController@festivalPreviewsWithDate')->name('single.festival.previews.date');
Route::get('/festival/{slug}/{category}/laboratori', 'PgcController@festivalWorkshops')->name('single.festival.workshops');
Route::get('/festival/{slug}/{category}/laboratori/{date}', 'PgcController@festivalWorkshopWithDate')->name('single.festival.workshops.date');
Route::get('/festival/{slug}/{category}/proiezioni', 'PgcController@festivalFilms')->name('single.festival.films');
Route::get('/festival/{slug}/{category}/proiezioni/{date}', 'PgcController@festivalFilmsWithDate')->name('single.festival.films.date');
Route::get('/festival/{slug}/{category}/visite', 'PgcController@festivalVisits')->name('single.festival.visits');
Route::get('/festival/{slug}/{category}/visite/{date}', 'PgcController@festivalVisitWithDate')->name('single.festival.visits.date');
Route::get('/festival/{slug}/{category}', 'PgcController@festivalCategory')->name('single.festival.category');
Route::get('/festivals', 'HomePageController@getFestivals')->name('festivals.index');

// Eventi
Route::post('/evento/check', 'HomePageController@checkAvilability')->name('event.check');
Route::post('/evento/rsvp', 'HomePageController@rsvp')->name('event.rsvp');
Route::get('/evento/{slug}', 'HomePageController@getEventSingle')->where('slug', '[\w\d\-\_]+')->name('single.event');

// Kids
Route::get('/famiglie', 'HomePageController@kids')->name('kids.index');

// Scuole
Route::get('/scuole', 'HomePageController@schools')->name('schools.index');

// Cinestore
Route::prefix('cinestore')->group(function() {
    Route::get('/', 'CinestoreController@index')->name('cinestore.index');
    Route::get('/carrello', 'CinestoreController@cart')->name('cinestore.cart');
    Route::get('/ordine', 'CinestoreController@order')->name('cinestore.order');
    Route::get('/shippings', 'CinestoreController@get_shippings')->name('cinestore.shippings');
    Route::post('/invia-ordine', 'CinestoreController@orderStore')->name('cinestore.order.place');
    Route::post('/pagamento', 'CinestoreController@payment')->name('cinestore.payment');
    Route::get('/foto/collezione/{slug}', 'CinestoreController@collection')->name('cinestore.pic.collection');
    Route::get('/{category}', 'CinestoreController@category')->name('cinestore.category');
    Route::get('/{category}/{slug}', 'CinestoreController@singleProduct')->where('slug', '[\w\d\-\_]+')->name('cinestore.single');
});

// Press Release
Route::get('/rassegna-stampa', 'HomePageController@indexPress')->name('press.index');
Route::get('/rassegna-stampa/{slug}', 'HomePageController@singlePress')->where('slug', '[\w\d\-\_]+')->name('press.single');

// Museo
Route::get('/museo', 'HomePageController@museum')->name('museum.index');

// Biblioteca
Route::get('/biblioteca', 'HomePageController@biblio')->name('biblio.index');

// Archivio
Route::get('/archivio', 'HomePageController@archive')->name('archive.index');

// Newsletter
Route::post('/newsletter-subscribe', 'NewsletterController@addSubscriber')->name('add.newsletter');

// Amministrazione
Route::get('/amministrazione-trasparente', 'HomePageController@amministrazione')->name('amministrazione');

// Gallerie
Route::get('/galleria', 'HomePageController@gallery')->name('public.gallery.index');

// Home
Route::get('/', 'HomePageController@getHome');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::prefix('admin')->group(function() {

    // Login Section
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');

    // Search
    Route::get('/search/{query}', 'Admin\SearchController@search')->name('admin.search.post');
    Route::get('/search/{post_type}/{query}', 'Admin\SearchController@searchFiltered')->name('admin.search.filtered.post');

    // Post Section
    Route::get('/posts', 'Admin\PostController@index')->name('posts.index');

    // Pages Section
    Route::post('/page-link', 'Admin\PageController@storeLink')->name('store.page.link');
    Route::delete('/page-link/{id}', 'Admin\PageController@destroyLink')->name('destroy.page.link');
    Route::resource('/pages', 'Admin\PageController');

    // Post filter, index, create, store, edit, update, destroy
    Route::get('/posts/filter/{post_type}', 'Admin\PostController@filter')->name('posts.filter');
    Route::get('/posts/{post_type}/create', 'Admin\PostController@create')->name('posts.create');
    Route::post('/posts/{postType}', 'Admin\PostController@store')->name('posts.store');
    Route::get('/posts/{post_type}/{id}/edit', 'Admin\PostController@edit')->name('posts.edit');
    Route::put('/posts/{post_type}/{id}', 'Admin\PostController@update')->name('posts.update');
    Route::patch('/posts/{post_type}/{id}', 'Admin\PostController@update')->name('posts.update');
    Route::delete('/posts/{post_type}/{id}', 'Admin\PostController@destroy')->name('posts.destroy');

    // Product Prices
    Route::post('/product/add-price', 'Admin\CinestoreController@addPrice')->name('admin.add.price');

    Route::post('/scheduling', 'Admin\RelationController@storeFilmShow')->name('store.film.show');
    Route::delete('/scheduling/{id}', 'Admin\RelationController@destroyFilmShow')->name('destroy.film.show');
    Route::post('/festival-relations', 'Admin\FestivalRelationController@store')->name('store.festivalrelation');
    Route::post('/festival-relations-destroy', 'Admin\FestivalRelationController@destroy')->name('destroy.festivalrelation');
    Route::get('/festival-cat/{id}', 'Admin\FestivalRelationController@getFestivalCategories')->name('get.festival.categories');

    Route::post('/places', 'Admin\PlaceController@placesLoad')->name('places.load');

    // RSVP Eventi
    Route::post('/rsvp/message/save', 'Admin\RsvpController@messageSave')->name('rsvp.msg.save');
    Route::get('/rsvp/{id}', 'Admin\RsvpController@index')->name('rsvp.index');
    Route::get('/rsvp/{id}/download', 'Admin\RsvpController@download')->name('rsvp.download');

    // Rassegne
    Route::post('/exhibitions-films', 'Admin\RelationController@storeExhibitionFilm')->name('store.exhibition.film');
    Route::delete('/exhibitions-film-destroy', 'Admin\RelationController@destroyExhibitionFilm')->name('destroy.exhibition.film');

    // Mostre
    Route::post('/exhibits-relations', 'Admin\RelationController@storeExhibitRelation')->name('store.exhibit.relation');
    Route::delete('/exhibits-relation-destroy', 'Admin\RelationController@destroyExhibitRelation')->name('destroy.exhibit.relation');

    // Admin Utilities
    Route::resource('/categories', 'Admin\CategoryController', ['except' => ['create', 'edit']]);

    // Home Slider
    Route::get('/home-slider', 'Admin\HomeSliderController@index')->name('homeslider.index');
    Route::get('/home-slider/create', 'Admin\HomeSliderController@create')->name('homeslider.create');
    Route::post('/home-slider', 'Admin\HomeSliderController@store')->name('homeslider.store');
    Route::delete('/home-slider/{id}', 'Admin\HomeSliderController@destroy')->name('homeslider.destroy');

    // Cinestore Slider
    Route::get('/cinestore-slider', 'Admin\CinestoreSliderController@index')->name('cinestore.slider.index');
    Route::post('/cinestore-slider', 'Admin\CinestoreSliderController@store')->name('cinestore.slider.store');
    Route::delete('/cinestore-slider/{id}', 'Admin\CinestoreSliderController@destroy')->name('cinestore.slider.destroy');

    // Cinestore Evidenza
    Route::get('/cinestore-evidenza', 'Admin\CinestoreFeaturedController@index')->name('cinestore.featured.index');
    Route::post('/cinestore-evidenza/store', 'Admin\CinestoreFeaturedController@store')->name('cinestore.featured.store');
    Route::delete('/cinestore-evidenza/{id}', 'Admin\CinestoreFeaturedController@destroy')->name('cinestore.featured.destroy');

    // Media
    Route::get('/media/edit/{id}', 'Admin\MediaController@edit')->name('media.edit');
    Route::post('/media/upload', 'Admin\MediaController@upload')->name('media.upload');
    Route::post('/media/crop', 'Admin\MediaController@mediaCrop')->name('media.crop');
    Route::post('/media/replace', 'Admin\MediaController@mediaReplace')->name('media.replace');
    Route::post('/media/loadmore', 'Admin\MediaController@loadMore')->name('media.loadmore');
    Route::get('/media', 'Admin\MediaController@index')->name('media.index');
    Route::post('/media/search', 'Admin\MediaController@searchImages');

    // Galleria
    Route::post('/gallery/upload', 'Admin\GalleryController@upload')->name('gallery.upload');
    Route::post('/gallery/store', 'Admin\GalleryController@store')->name('gallery.store');
    Route::get('/gallery/create', 'Admin\GalleryController@create')->name('gallery.create');
    Route::get('/gallery', 'Admin\GalleryController@index')->name('gallery.index');
    Route::post('/gallery/update', 'Admin\GalleryController@gallery_update');
    Route::post('/gallery/delete/{id}', 'Admin\GalleryController@destroy');
    Route::get('/gallery/{id}/edit', 'Admin\GalleryController@edit')->name('gallery.edit');
    Route::patch('/gallery/{id}', 'Admin\GalleryController@update')->name('gallery.update');
    Route::delete('/gallery/{id}', 'Admin\GalleryController@destroy')->name('gallery.destroy');

    // Media Upload Api
    Route::post('/media-upload-api', 'Admin\MediaUpload@upload')->name('media.api.upload');

    // File
    Route::get('/files', 'Admin\FileController@index')->name('files.index');
    Route::post('/files/store', 'Admin\FileController@fileUpload')->name('files.upload');
    Route::delete('/files/delete/{id}', 'Admin\FileController@destroy')->name('files.destroy');

    // Cinestore
    Route::prefix('cinestore')->group(function() {
        Route::get('/order-products', 'Admin\CinestoreController@order_products')->name('admin.cinestore.order_products');
        Route::post('/order-products', 'Admin\CinestoreController@get_category_products')->name('admin.cinestore.get_category_products');
        Route::post('/save_order', 'Admin\CinestoreController@save_products_order')->name('admin.cinestore.save_products_order');
        Route::get('/shipping', 'Admin\CinestoreController@shipping')->name('admin.cinestore.shipping');
        Route::get('/get-shipping', 'Admin\CinestoreController@get_shipping')->name('admin.cinestore.get_shipping');
        Route::post('/new-shipping', 'Admin\CinestoreController@new_shipping')->name('admin.cinestore.new_shipping');
        Route::post('/update-shipping', 'Admin\CinestoreController@update_shipping')->name('admin.cinestore.update_shipping');
        Route::post('/delete-shipping', 'Admin\CinestoreController@delete_shipping')->name('admin.cinestore.delete_shipping');

        // Cinestore -> orders management
        Route::post('/edit/{id}', 'Admin\CinestoreController@edit_order')->name('admin.cinestore.order.edit');

        Route::get('/{id}', 'Admin\CinestoreController@single')->name('admin.cinestore.single');
        Route::get('/', 'Admin\CinestoreController@index')->name('admin.cinestore.index');
    });


    Route::prefix('twitter')->group(function() {
        Route::get('/', 'Admin\TwitterController@index')->name('admin.twitter.index');
        Route::delete('/{id}', 'Admin\TwitterController@destroy')->name('admin.twitter.destroy');
        Route::get('/{id}/edit', 'Admin\TwitterController@edit')->name('admin.twitter.edit');
        Route::patch('/{id}', 'Admin\TwitterController@update')->name('admin.twitter.update');
        Route::get('/tweet', 'Admin\TwitterController@tweetSchedule')->name('admin.twitter.tweet');
        Route::get('/test', 'Admin\TwitterController@command')->name('admin.twitter.test');
    });

    Route::prefix('funders')->group(function(){
        Route::get('/get-funders', 'Admin\FunderController@get_funders')->name('admin.funders.get_funders');
        Route::post('/new-funder', 'Admin\FunderController@new_funder')->name('admin.funders.new_funder');
        Route::post('/update-funder', 'Admin\FunderController@update_funder')->name('admin.funders.update_funder');
        Route::get('/', 'Admin\FunderController@index')->name('admin.funders.index');
    });

    // Tools
    Route::get('/recover-event-guests', 'Admin\ToolsController@recover_events_guests')->name('admin.recover.guests_events');
    Route::get('/recover-event-guests/{id}', 'Admin\ToolsController@recover_events_guests_single')->name('admin.recover.guests_events.single');
    Route::get('/recover-event-guests/{id}/restore', 'Admin\ToolsController@restore_event')->name('admin.restore_events.single');
    Route::post('/recover-event-guests/{id}/restore', 'Admin\ToolsController@restore_event_save')->name('admin.restore_events.single.store');
    Route::get('/square_img_categories', 'Admin\ToolsController@squareImgCategories')->name('tools.square.img');
    // Route::get('/generate_home_slider', 'Admin\ToolsController@homeSliderImg')->name('tools.homeslider.img');
    // Route::get('/generate_panoramic', 'Admin\ToolsController@panoramic')->name('tools.panoramic.img');
    Route::get('/generate_media_table', 'Admin\ToolsController@mediable')->name('tools.populate.media');
    Route::get('/regenerate_thumbnails', 'Admin\ToolsController@regenerateThumbnails')->name('tools.regenerate.thumbnails');
    Route::get('/slug-fix', 'Admin\ToolsController@correctUrl');
    Route::get('/php-info', 'Admin\ToolsController@getInfo');
    Route::get('/media-watermark', 'Admin\WatermarkController@bulkAddWatermarkToPhoto');

    // Route::get('/debug', 'DebugController@debug');
    Route::get('/media-title', 'DebugController@add_title_to_media');
});
